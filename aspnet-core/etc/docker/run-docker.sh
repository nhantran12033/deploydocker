#!/bin/bash

if [[ ! -d certs ]]
then
    mkdir certs
    cd certs/
    if [[ ! -f localhost.pfx ]]
    then
        dotnet dev-certs https -v -ep localhost.pfx -p 4ecb9bc0-a9aa-446b-95dd-378a60c0910a -t
    fi
    cd ../
fi

docker-compose up -d
