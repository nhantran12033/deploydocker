using Shouldly;
using System;
using System.Linq;
using System.Threading.Tasks;
using MEVN.DistributorApp.V2.ProjectDetails;
using MEVN.DistributorApp.V2.EntityFrameworkCore;
using Xunit;

namespace MEVN.DistributorApp.V2.ProjectDetails
{
    public class ProjectDetailRepositoryTests : V2EntityFrameworkCoreTestBase
    {
        private readonly IProjectDetailRepository _projectDetailRepository;

        public ProjectDetailRepositoryTests()
        {
            _projectDetailRepository = GetRequiredService<IProjectDetailRepository>();
        }

        [Fact]
        public async Task GetListAsync()
        {
            // Arrange
            await WithUnitOfWorkAsync(async () =>
            {
                // Act
                var result = await _projectDetailRepository.GetListAsync(
                    projectId: Guid.Parse("398c0945-21d7-4959-96a5-905178eeb4c3"),
                    golfaCode: "5f65ad1d0dbc4d929895fd2e2c11c504f2fd377f",
                    model: "444716ffce0a449591b36fc6f7d276e68b786315f3f0440e9d69cdf7b2684d8aacb1b7e5bda54c4e9f3d775c4f8f",
                    requestStatus: "73d690"
                );

                // Assert
                result.Count.ShouldBe(1);
                result.FirstOrDefault().ShouldNotBe(null);
                result.First().Id.ShouldBe(Guid.Parse("28538b4e-c068-4089-993e-60616e446271"));
            });
        }

        [Fact]
        public async Task GetCountAsync()
        {
            // Arrange
            await WithUnitOfWorkAsync(async () =>
            {
                // Act
                var result = await _projectDetailRepository.GetCountAsync(
                    projectId: Guid.Parse("0803c3c5-8fb9-49f8-b302-7d8313bfdb9c"),
                    golfaCode: "fe97ae0840ab4a07bce2cbd51bc74a107b590f32",
                    model: "5ccf515f5a5f44788c2e40de9e79e6d654df6229e6de40d98f1fc0b1bfbbc79685f774af881e4b29af85003346883aa",
                    requestStatus: "dc0d04650add4b1f87b069"
                );

                // Assert
                result.ShouldBe(1);
            });
        }
    }
}