using Shouldly;
using System;
using System.Linq;
using System.Threading.Tasks;
using MEVN.DistributorApp.V2.MaterialGroups;
using MEVN.DistributorApp.V2.EntityFrameworkCore;
using Xunit;

namespace MEVN.DistributorApp.V2.MaterialGroups
{
    public class MaterialGroupRepositoryTests : V2EntityFrameworkCoreTestBase
    {
        private readonly IMaterialGroupRepository _materialGroupRepository;

        public MaterialGroupRepositoryTests()
        {
            _materialGroupRepository = GetRequiredService<IMaterialGroupRepository>();
        }

        [Fact]
        public async Task GetListAsync()
        {
            // Arrange
            await WithUnitOfWorkAsync(async () =>
            {
                // Act
                var result = await _materialGroupRepository.GetListAsync(
                    materialName: "214b5db15",
                    disCode: "18942b2316794f8c8429d6529c4cf92b8cb5ab1fe09645"
                );

                // Assert
                result.Count.ShouldBe(1);
                result.FirstOrDefault().ShouldNotBe(null);
                result.First().Id.ShouldBe(Guid.Parse("82ad3d83-1826-4ddc-8525-7343c1771a06"));
            });
        }

        [Fact]
        public async Task GetCountAsync()
        {
            // Arrange
            await WithUnitOfWorkAsync(async () =>
            {
                // Act
                var result = await _materialGroupRepository.GetCountAsync(
                    materialName: "5b1f048c6dd848298149ec5e33",
                    disCode: "68c4c98a671a4be9b03752119cea0f3ccc6b35b81c524827917d2eee81ee11c7af20ed39935942bea7af9b389adc"
                );

                // Assert
                result.ShouldBe(1);
            });
        }
    }
}