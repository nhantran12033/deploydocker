﻿using Volo.Abp;

namespace MEVN.DistributorApp.V2.EntityFrameworkCore;

public abstract class V2EntityFrameworkCoreTestBase : V2TestBase<V2EntityFrameworkCoreTestModule>
{

}
