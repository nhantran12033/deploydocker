using Shouldly;
using System;
using System.Linq;
using System.Threading.Tasks;
using MEVN.DistributorApp.V2.FreeOnShipMents;
using MEVN.DistributorApp.V2.EntityFrameworkCore;
using Xunit;

namespace MEVN.DistributorApp.V2.FreeOnShipMents
{
    public class FreeOnShipMentRepositoryTests : V2EntityFrameworkCoreTestBase
    {
        private readonly IFreeOnShipMentRepository _freeOnShipMentRepository;

        public FreeOnShipMentRepositoryTests()
        {
            _freeOnShipMentRepository = GetRequiredService<IFreeOnShipMentRepository>();
        }

        [Fact]
        public async Task GetListAsync()
        {
            // Arrange
            await WithUnitOfWorkAsync(async () =>
            {
                // Act
                var result = await _freeOnShipMentRepository.GetListAsync(
                    materialID: Guid.Parse("b213518c-a12d-4120-8c3c-ad547887a741"),
                    pONo: "a00b95459f0248aea23401afa86422bae1494d3b78244385a59f5d29a868662a61e15dd1e09546c5a3f9945466d60e8cfa16cb95e4d04b308112594dfb997a3449dc91a4c1404aefb3aef3f93ed7cae7bbd9838a6ae44133a2e3fe6aa71af457efdcd144dfe34ef095967cc5b22dfba2d914de493e4d4f5e897dbbe501e99bf",
                    machineNumber: "475349a4a5844c3e8ccbb98d46e61d2ca020dddf55f0446bb74eaf92f661c82aa317c6e2cad046ebb39ce7"
                );

                // Assert
                result.Count.ShouldBe(1);
                result.FirstOrDefault().ShouldNotBe(null);
                result.First().Id.ShouldBe(Guid.Parse("8ea924e2-f909-4254-a1df-7e72c0fe3d19"));
            });
        }

        [Fact]
        public async Task GetCountAsync()
        {
            // Arrange
            await WithUnitOfWorkAsync(async () =>
            {
                // Act
                var result = await _freeOnShipMentRepository.GetCountAsync(
                    materialID: Guid.Parse("dbc55d25-14bd-41a1-b55e-368e0b494011"),
                    pONo: "ea99f7f57c8446879c7ed47e99f21759cb22c45def4d4894a704c6564ca2b86b9307a58857664547b17babfe2b97149eeae04519b4764876836e0088d1b3e132ace0dfad8b254704816443bc87adb9c7b22cd2a2b4e644b89bb98f07ec50d7a056184c7df22a4816bd33a61c30d1e4de985bc2e509c649689a41b5b5768847b",
                    machineNumber: "876d204ac8364931ae236966f446633d1bf39759a800404f9565ea8d170029c8451caa1a3b53434db9"
                );

                // Assert
                result.ShouldBe(1);
            });
        }
    }
}