using System;
using System.Threading.Tasks;
using Volo.Abp.Data;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Uow;
using MEVN.DistributorApp.V2.Venders;

namespace MEVN.DistributorApp.V2.Venders
{
    public class VendersDataSeedContributor : IDataSeedContributor, ISingletonDependency
    {
        private bool IsSeeded = false;
        private readonly IVenderRepository _venderRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public VendersDataSeedContributor(IVenderRepository venderRepository, IUnitOfWorkManager unitOfWorkManager)
        {
            _venderRepository = venderRepository;
            _unitOfWorkManager = unitOfWorkManager;

        }

        public async Task SeedAsync(DataSeedContext context)
        {
            if (IsSeeded)
            {
                return;
            }

            await _venderRepository.InsertAsync(new Vender
            (
                id: Guid.Parse("b5d44f56-eb3b-4ac5-b873-1453469ed73f"),
                verderName: "2ff66bce79ac4704b68e5fcd08c087ffa84f94445b684b728fc4ceedc108db35ead741bfa47a4b388f97b4a59872f0cd8237bd023be44cf19baa18b194ec51fb1dff90c49e234a6e97db3b89cac7d7f0acdc0987e72948b580f47bcb8ddbd18e7980518636f74f688423a78954dcc1495058e55da6ca408f87825459196abb6",
                address: "3d1d932d52bf45d784fcd3727a5aa82aba1df837a8024701bf5ea4be7fe3a63ea4d8b4c1a9c0484c8f3d884a11c0f79",
                email: "00442e4c959f44b69c833@1a03942c40254456893dd.com"
            ));

            await _venderRepository.InsertAsync(new Vender
            (
                id: Guid.Parse("7ce0c544-eb6b-49f3-afbf-9af52c5d7376"),
                verderName: "147df8b746414d44adc9c8b5ed843cd20b93fcb6398441dc860e6c7ad4e7e78cfadb82616055447eae1521a1e7fdecf9dd703d1c61b64c88989b5f8e68234107f10f1b131b284f19a3d6b4241efafec3820cd5b76b1d43f5a3539f4b7c8f9e5ceb66c9dfeb5e4b7ab10bc3bf1e74d2cbf7f83e729c7a495986689dd802a4ceb",
                address: "190f3c0e83754fc08251fcc7b25d7c6a9d0f0d03544e4cb581a30fc5ec4227038b8a0fe5c03d444389a1cd3c2946b37609d",
                email: "d94@8bb.com"
            ));

            await _unitOfWorkManager!.Current!.SaveChangesAsync();

            IsSeeded = true;
        }
    }
}