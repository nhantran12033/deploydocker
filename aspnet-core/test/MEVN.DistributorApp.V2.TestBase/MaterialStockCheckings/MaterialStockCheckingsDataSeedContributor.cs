using MEVN.DistributorApp.V2.Distributors;
using MEVN.DistributorApp.V2.Venders;
using MEVN.DistributorApp.V2.MaterialGroups;
using System;
using System.Threading.Tasks;
using Volo.Abp.Data;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Uow;
using MEVN.DistributorApp.V2.MaterialStockCheckings;

namespace MEVN.DistributorApp.V2.MaterialStockCheckings
{
    public class MaterialStockCheckingsDataSeedContributor : IDataSeedContributor, ISingletonDependency
    {
        private bool IsSeeded = false;
        private readonly IMaterialStockCheckingRepository _materialStockCheckingRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly MaterialGroupsDataSeedContributor _materialGroupsDataSeedContributor;

        private readonly VendersDataSeedContributor _vendersDataSeedContributor;

        private readonly DistributorsDataSeedContributor _distributorsDataSeedContributor;

        public MaterialStockCheckingsDataSeedContributor(IMaterialStockCheckingRepository materialStockCheckingRepository, IUnitOfWorkManager unitOfWorkManager, MaterialGroupsDataSeedContributor materialGroupsDataSeedContributor, VendersDataSeedContributor vendersDataSeedContributor, DistributorsDataSeedContributor distributorsDataSeedContributor)
        {
            _materialStockCheckingRepository = materialStockCheckingRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _materialGroupsDataSeedContributor = materialGroupsDataSeedContributor; _vendersDataSeedContributor = vendersDataSeedContributor; _distributorsDataSeedContributor = distributorsDataSeedContributor;
        }

        public async Task SeedAsync(DataSeedContext context)
        {
            if (IsSeeded)
            {
                return;
            }

            await _materialGroupsDataSeedContributor.SeedAsync(context);
            await _vendersDataSeedContributor.SeedAsync(context);
            await _distributorsDataSeedContributor.SeedAsync(context);

            await _materialStockCheckingRepository.InsertAsync(new MaterialStockChecking
            (
                id: Guid.Parse("259d7bf3-93d5-4185-a3d8-9920908859eb"),
                golfaCode: "ed5d02ebeaef416bbd8f4f2714cb4a1de00ee79",
                model: "3996bfd0c63b4075811f888933a9bd2d1209aa411f4f43a9b9d71322823",
                sAP_Code: "0829c462d5694fa587d453c299d4197d4381b4a27f3c4278",
                description_VN: "6919e3d725d7416a9f89f7866f4f0c615c57b688c5fa46d7b0156acce159c580b0d2eb00acef4af6819f239b315153dfb3c",
                description_Group: "758a992868b346dfaaa18aaa57aacfab20345883dc134799b658be3a7a94b3f59878583ab4d84163bd907af89a89",
                standard_Price: 1151968780,
                stockValueWarning: 1868845246,
                stockTmp: 481480555,
                spec1: "96ae6c75a9d7462084eb48cb45448206a022e91d6cc4449a82f5563699bdf100",
                spec2: "fe7e34ceec90499c9524c41722dfaea8015e98d854944b95b08468b7ebcc72721748",
                materialGroupId: Guid.Parse("82ad3d83-1826-4ddc-8525-7343c1771a06"),
                venderId: Guid.Parse("b5d44f56-eb3b-4ac5-b873-1453469ed73f"),
                distributorId: null
            ));

            await _materialStockCheckingRepository.InsertAsync(new MaterialStockChecking
            (
                id: Guid.Parse("cb229de2-2142-429d-8a06-82ccadd7e409"),
                golfaCode: "ac708df74d1a4ab193b33cd91637fa",
                model: "2d5c655cc21c413793ae8496f49758444ad7a35fe70d454fa2d90359253f0244b6a1",
                sAP_Code: "8e9b24e80",
                description_VN: "1c3c644459b4460e9e5681b1cc22bf8aa6e31dc3a3ef4ed3b0e5aec6067c30309bba7c59974d45829c49ea9604ee566d",
                description_Group: "bdb7309df65c4d6f887b86",
                standard_Price: 1843288689,
                stockValueWarning: 2059036762,
                stockTmp: 1396493679,
                spec1: "6fd6e4c24cb247c5b223fb4d6d2e748eb5ff8149112c434793e1e0770d360b9ddba90c1992d",
                spec2: "0e62aeaae21349f1b7c68264e9f7284c08c8e204842a4149bef",
                materialGroupId: Guid.Parse("82ad3d83-1826-4ddc-8525-7343c1771a06"),
                venderId: Guid.Parse("b5d44f56-eb3b-4ac5-b873-1453469ed73f"),
                distributorId: null
            ));

            await _unitOfWorkManager!.Current!.SaveChangesAsync();

            IsSeeded = true;
        }
    }
}