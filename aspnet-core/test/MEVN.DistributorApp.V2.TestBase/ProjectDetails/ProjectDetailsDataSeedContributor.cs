using System;
using System.Threading.Tasks;
using Volo.Abp.Data;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Uow;
using MEVN.DistributorApp.V2.ProjectDetails;

namespace MEVN.DistributorApp.V2.ProjectDetails
{
    public class ProjectDetailsDataSeedContributor : IDataSeedContributor, ISingletonDependency
    {
        private bool IsSeeded = false;
        private readonly IProjectDetailRepository _projectDetailRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public ProjectDetailsDataSeedContributor(IProjectDetailRepository projectDetailRepository, IUnitOfWorkManager unitOfWorkManager)
        {
            _projectDetailRepository = projectDetailRepository;
            _unitOfWorkManager = unitOfWorkManager;

        }

        public async Task SeedAsync(DataSeedContext context)
        {
            if (IsSeeded)
            {
                return;
            }

            await _projectDetailRepository.InsertAsync(new ProjectDetail
            (
                id: Guid.Parse("28538b4e-c068-4089-993e-60616e446271"),
                projectId: Guid.Parse("398c0945-21d7-4959-96a5-905178eeb4c3"),
                golfaCode: "5f65ad1d0dbc4d929895fd2e2c11c504f2fd377f",
                model: "444716ffce0a449591b36fc6f7d276e68b786315f3f0440e9d69cdf7b2684d8aacb1b7e5bda54c4e9f3d775c4f8f",
                qty: 534373572,
                dpoUsed: 555676145,
                requestStatus: "73d690",
                requestPrice: 2088406680,
                distRequestedPrice: 868936477,
                saleOfferPrice: 1945821941,
                saleAllowDiscountPrice: 1506903086,
                amountRequestedPrice: 1134411883
            ));

            await _projectDetailRepository.InsertAsync(new ProjectDetail
            (
                id: Guid.Parse("f6e93234-69e5-4863-bdce-dae9af04396a"),
                projectId: Guid.Parse("0803c3c5-8fb9-49f8-b302-7d8313bfdb9c"),
                golfaCode: "fe97ae0840ab4a07bce2cbd51bc74a107b590f32",
                model: "5ccf515f5a5f44788c2e40de9e79e6d654df6229e6de40d98f1fc0b1bfbbc79685f774af881e4b29af85003346883aa",
                qty: 764528467,
                dpoUsed: 1443801823,
                requestStatus: "dc0d04650add4b1f87b069",
                requestPrice: 1164176425,
                distRequestedPrice: 2082109174,
                saleOfferPrice: 1210526575,
                saleAllowDiscountPrice: 1838188553,
                amountRequestedPrice: 1383956781
            ));

            await _unitOfWorkManager!.Current!.SaveChangesAsync();

            IsSeeded = true;
        }
    }
}