using System;
using System.Threading.Tasks;
using Volo.Abp.Data;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Uow;
using MEVN.DistributorApp.V2.Customers;

namespace MEVN.DistributorApp.V2.Customers
{
    public class CustomersDataSeedContributor : IDataSeedContributor, ISingletonDependency
    {
        private bool IsSeeded = false;
        private readonly ICustomerRepository _customerRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public CustomersDataSeedContributor(ICustomerRepository customerRepository, IUnitOfWorkManager unitOfWorkManager)
        {
            _customerRepository = customerRepository;
            _unitOfWorkManager = unitOfWorkManager;

        }

        public async Task SeedAsync(DataSeedContext context)
        {
            if (IsSeeded)
            {
                return;
            }

            await _customerRepository.InsertAsync(new Customer
            (
                id: Guid.Parse("2d2fc3ba-d9e5-414d-afc8-ab6f8cde35dc"),
                taxCode: "bd3bec07b194400c93e87c6282cb952cbd691749dcd14b6d95",
                cusName: "dd4be371716549d6ab38eb914521eb355bf25d9a92674519afe2bb8003e05460ea5502f4db1f483094033338300a8d400dd63c8e906343e5b629658e4ead093b329f962d12704ee6966219bc26c00c6e85439ca1f0f8458cb52692b24b22b15877a915c8c07047b4b75303e361a1b4ef9a22701e56ba4c06951efce4ef91e79",
                address: "e0d2ae947afa4c3fb23f35657b2d693e438bc46da714421fbfc72acdad1106aa3d6b9b09d602409da14f45aa94bfa19349d533faf1bb4b9a987849624a942bcfb7b63a8c978f43ba904cff62f4ce08b5958e97ade2d64dcaa9b6ebe23fc5ac802cddf43a63fd464eb7bb9b01fdfaba8375960004b67f4009a66532a28114e35",
                phone: "841ad52993d14a39b1eef9924aa40e1b54aa8a008d8c4ba996603777f54732222ee4d2e2dfd842d2bda59743bfc08c2b8d96923e32c1485c8d4283f2d5c47a1345067de6b1844a968a1134f389d2b3064099eb852d7f4ff29a6792cf3aaf9a3fe02a56e818644594a1dc909c0aaee3e10de326c03cfa408fa670282e389b1d2",
                note: "94f6d866aa2b48d4b4f768b89a570d967301212b1efb47edb52c7602ecdf0"
            ));

            await _customerRepository.InsertAsync(new Customer
            (
                id: Guid.Parse("cf801891-f1ba-4d66-8c49-c40c5ada7619"),
                taxCode: "8610a0a9ac6c4067b8e77495fdfa6086f5f47c3668b647b393",
                cusName: "149f7dbbbae14e2eb939e46623a54b54c4036220219d4490af6944efb4b4e5725eb2c790b20347ebba863fcbdfc1b3c13ec3cc1c329148e8beae6d2a8cc4de92200a4f9a64bc42999076a1cf25dd764aea27d9781134429487097adee101a306686acd98da2f43afa6b7c005fec1fd4cded6922adacc49ecaa8eb69eb889c53",
                address: "d61a88eb4de242aa89b3ca39c45cbd32bb893ca7151345dd9209deb6040e16ca5058f9d139a54f6e968d09a95f563acd37159d68f505496ba5806fbd55833fceb15d3fe27e634f699a46586c1ffe2fd1dfab233008bc401aa78faa1d924d8dd97571d89bba1d42acabd3ee8d8b027e9b0f85faf96b6344e5aed6baae5bb83ce",
                phone: "ddf44f11fcc64227b3772096a5f42270c607c4ed3ebc44f9bbaa2cece66bbd08142bc6772aa44c469ef7bfdbdc8aa411ad7982c474994c90bd7c1ced075fa2c09491d8c83a9643e9b4f426fe962a5da80277057e68f244b894413a7921eef5a552a882a6d2d94735abc6ffce6e6535ff5db37ebe2bef4579b649a1267affd5e",
                note: "c033a843853e4ca19ff427489d334a8b2edf7b0be8964d6881a6a91f93c28bc3"
            ));

            await _unitOfWorkManager!.Current!.SaveChangesAsync();

            IsSeeded = true;
        }
    }
}