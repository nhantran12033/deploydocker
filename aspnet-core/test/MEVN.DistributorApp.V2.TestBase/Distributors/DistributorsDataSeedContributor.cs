using System;
using System.Threading.Tasks;
using Volo.Abp.Data;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Uow;
using MEVN.DistributorApp.V2.Distributors;

namespace MEVN.DistributorApp.V2.Distributors
{
    public class DistributorsDataSeedContributor : IDataSeedContributor, ISingletonDependency
    {
        private bool IsSeeded = false;
        private readonly IDistributorRepository _distributorRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public DistributorsDataSeedContributor(IDistributorRepository distributorRepository, IUnitOfWorkManager unitOfWorkManager)
        {
            _distributorRepository = distributorRepository;
            _unitOfWorkManager = unitOfWorkManager;

        }

        public async Task SeedAsync(DataSeedContext context)
        {
            if (IsSeeded)
            {
                return;
            }

            await _distributorRepository.InsertAsync(new Distributor
            (
                id: Guid.Parse("5495de11-951e-4437-963f-606bdd09b17e"),
                disCode: "fdce9478f4f4435193d7",
                disName: "c03706bb42fc4ce4af0e239a4102dfa8ca86fd27128d4ba49a1282f40f4900fb3cade53206204eb9bdfbddbc00b8da9b2400fcdabca64da7a3215d4dea351e97133c3324623d41359454140ce6aea0f7760a428c23e442e5ab17e4398da63e91834da2d4",
                address: "f0267a2c5a8d43e8a293ceaefa2fbf9c7f1a85e2779242f6b73e162024",
                contactInfo: "498ca4163e8947ab95ea83d632c8553fe719ad6b73a04d96879313d860e464606ab8f10f43d94170985e",
                node: "87a97864bf8941bbbbdfdf1cbc543d096e3b256c5b0e47f59b"
            ));

            await _distributorRepository.InsertAsync(new Distributor
            (
                id: Guid.Parse("de80851f-405e-45cd-852e-b972f1d251aa"),
                disCode: "89b56c32c02c4a1cb002",
                disName: "2e02aaec64954333a2bf1bc6dd40be77472524426eec40679782ba9b7418b34adad098f266b94436b6464129d69a009fb88aa004620c48a5a47ed1e447a339408b4a4730e9164e6fbaecd21653136dd9e88652fc82544ad1b89d9e8b1f606f99b9ac7e2b",
                address: "6326d63b4f0749fa8217d564acf6e8",
                contactInfo: "e53baa73407242b98e04dde65b13",
                node: "182ec5ea421a42d281aa96e164f746da9ee5c641de07430396c76ed5809452b94b"
            ));

            await _unitOfWorkManager!.Current!.SaveChangesAsync();

            IsSeeded = true;
        }
    }
}