using System;
using System.Threading.Tasks;
using Volo.Abp.Data;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Uow;
using MEVN.DistributorApp.V2.MaterialGroups;

namespace MEVN.DistributorApp.V2.MaterialGroups
{
    public class MaterialGroupsDataSeedContributor : IDataSeedContributor, ISingletonDependency
    {
        private bool IsSeeded = false;
        private readonly IMaterialGroupRepository _materialGroupRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public MaterialGroupsDataSeedContributor(IMaterialGroupRepository materialGroupRepository, IUnitOfWorkManager unitOfWorkManager)
        {
            _materialGroupRepository = materialGroupRepository;
            _unitOfWorkManager = unitOfWorkManager;

        }

        public async Task SeedAsync(DataSeedContext context)
        {
            if (IsSeeded)
            {
                return;
            }

            await _materialGroupRepository.InsertAsync(new MaterialGroup
            (
                id: Guid.Parse("82ad3d83-1826-4ddc-8525-7343c1771a06"),
                materialName: "214b5db15",
                disCode: "18942b2316794f8c8429d6529c4cf92b8cb5ab1fe09645"
            ));

            await _materialGroupRepository.InsertAsync(new MaterialGroup
            (
                id: Guid.Parse("a5aa95c7-ff2d-48ad-90a7-658f78041a29"),
                materialName: "5b1f048c6dd848298149ec5e33",
                disCode: "68c4c98a671a4be9b03752119cea0f3ccc6b35b81c524827917d2eee81ee11c7af20ed39935942bea7af9b389adc"
            ));

            await _unitOfWorkManager!.Current!.SaveChangesAsync();

            IsSeeded = true;
        }
    }
}