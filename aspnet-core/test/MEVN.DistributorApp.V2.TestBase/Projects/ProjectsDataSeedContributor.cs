using MEVN.DistributorApp.V2.Customers;
using MEVN.DistributorApp.V2.Distributors;
using System;
using System.Threading.Tasks;
using Volo.Abp.Data;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Uow;
using MEVN.DistributorApp.V2.Projects;

namespace MEVN.DistributorApp.V2.Projects
{
    public class ProjectsDataSeedContributor : IDataSeedContributor, ISingletonDependency
    {
        private bool IsSeeded = false;
        private readonly IProjectRepository _projectRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly DistributorsDataSeedContributor _distributorsDataSeedContributor;

        private readonly CustomersDataSeedContributor _customersDataSeedContributor;

        public ProjectsDataSeedContributor(IProjectRepository projectRepository, IUnitOfWorkManager unitOfWorkManager, DistributorsDataSeedContributor distributorsDataSeedContributor, CustomersDataSeedContributor customersDataSeedContributor)
        {
            _projectRepository = projectRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _distributorsDataSeedContributor = distributorsDataSeedContributor; _customersDataSeedContributor = customersDataSeedContributor;
        }

        public async Task SeedAsync(DataSeedContext context)
        {
            if (IsSeeded)
            {
                return;
            }

            await _distributorsDataSeedContributor.SeedAsync(context);
            await _customersDataSeedContributor.SeedAsync(context);

            await _projectRepository.InsertAsync(new Project
            (
                id: Guid.Parse("5b8c08c7-b622-441a-9d31-29bd2307def2"),
                projectCode: "6b98b6c21d004de7b318",
                projectName: "ee6a91d6001e4971a445a889dd051abf461677ac708640dea96bcb9894f56e19f03368d1efa94a5f9c6c0850bf58e23dc72701839c194811a349c38bf5e48874b80b14df2eb34950a1f6734e411ce418e99043f3bf254f368898986e0ad435bac2ae10e3d9554798a27cc1785382f1d7e801315bc7774a879e131bdb7d9bd3a",
                approvalStatus: "8ba729c634524fbc8cc7d4b978c7fd9c6b18609cd60b44dab836df8998344101cd82885a33da4622b567a334aa55f49811676d983dee4ea6a6408541776b83e8aa727e0bbc8f4869b6ec5c829927891380e72881a9d54ed797bfb9e5e0e8822f2cb47ad190b149959b10e36f8476c78f89eaca8198cc40ce8dc7b9dd8b6f414",
                accountNo: "44a949cf97724205b4e1ecb6adc7255609bdf01ecef348cc8175d43bea359b",
                projectType: "6270ab1d1c63401dbeaeb47c283811286f6aa8624",
                location: "ae50ef9fee084751a4f323fc3579497f9f5e3c6460f94f3b9d30c61ffc747f7517690c11852542ff9e7dbd2c878417",
                distributorMagin: "802bb5b4fc624ab2aec64d048ccf4f043fd73c160cb543cfa461e00ec9977ccf2476f87234824f8681921619ed2",
                pODate: new DateTime(2009, 2, 18),
                distributorId: Guid.Parse("5495de11-951e-4437-963f-606bdd09b17e"),
                customerId: null
            ));

            await _projectRepository.InsertAsync(new Project
            (
                id: Guid.Parse("6180ff50-79a0-455d-85f4-91b697f7e5b5"),
                projectCode: "7d4cc1b7e614490c9792",
                projectName: "a7ec8974a4b442d2b37a3304b2c49e35485dd417eaaf4e30b4063958ce7cf1bf3f867536c8ee4920932a86990d3579b5edb77a1106944931a7899c5fbaab36cab76b251c247a4c4599c14f55610163cf001dfe05a96345c1b81d01112408dd81971f499c962947b881bd3d4e375e0cae569adc696b244e8bbb5c5960e7232aa",
                approvalStatus: "b4d6cffb5d954aa7bd17d4414ee81f10b2ae852c986544b6948cae837de6d704703c8ce8f7a84990a89c46dd3773e1dc63682d134d984051894319e16a794ad5a81b2976af384f77b6ec100a5a35586ecad709492d0e400180b9ba9ab13fd5e26db8e58123f049cb8435b59786258bfa43a75dfd3d594eaea3e19ac9f57129e",
                accountNo: "a5d385974ceb4aa69d",
                projectType: "fe484c5979304238821666c2addb0c626a805e50f90b4755892943725096",
                location: "fc132342cf074197ae6bbd1f0ab4cec4edfcacad0fd34fc2934cf06",
                distributorMagin: "8b3524f7c28b43589f1013e22dc6c66989fc5f9323bd4fbd8250752c1783",
                pODate: new DateTime(2007, 1, 3),
                distributorId: Guid.Parse("5495de11-951e-4437-963f-606bdd09b17e"),
                customerId: null
            ));

            await _unitOfWorkManager!.Current!.SaveChangesAsync();

            IsSeeded = true;
        }
    }
}