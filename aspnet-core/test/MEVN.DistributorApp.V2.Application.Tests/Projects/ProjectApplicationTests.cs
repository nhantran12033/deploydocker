using System;
using System.Linq;
using Shouldly;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;
using Xunit;

namespace MEVN.DistributorApp.V2.Projects
{
    public class ProjectsAppServiceTests : V2ApplicationTestBase
    {
        private readonly IProjectsAppService _projectsAppService;
        private readonly IRepository<Project, Guid> _projectRepository;

        public ProjectsAppServiceTests()
        {
            _projectsAppService = GetRequiredService<IProjectsAppService>();
            _projectRepository = GetRequiredService<IRepository<Project, Guid>>();
        }

        [Fact]
        public async Task GetListAsync()
        {
            // Act
            var result = await _projectsAppService.GetListAsync(new GetProjectsInput());

            // Assert
            result.TotalCount.ShouldBe(2);
            result.Items.Count.ShouldBe(2);
            result.Items.Any(x => x.Project.Id == Guid.Parse("5b8c08c7-b622-441a-9d31-29bd2307def2")).ShouldBe(true);
            result.Items.Any(x => x.Project.Id == Guid.Parse("6180ff50-79a0-455d-85f4-91b697f7e5b5")).ShouldBe(true);
        }

        [Fact]
        public async Task GetAsync()
        {
            // Act
            var result = await _projectsAppService.GetAsync(Guid.Parse("5b8c08c7-b622-441a-9d31-29bd2307def2"));

            // Assert
            result.ShouldNotBeNull();
            result.Id.ShouldBe(Guid.Parse("5b8c08c7-b622-441a-9d31-29bd2307def2"));
        }

        [Fact]
        public async Task CreateAsync()
        {
            // Arrange
            var input = new ProjectCreateDto
            {
                ProjectCode = "d49d0a1c559d4fcb88ee",
                ProjectName = "2047d4f6e3af483cbc11022274468445e7a32171614f44c6ab3a133193b1e41d223009afdbea4e0bbaeade10c7e2c0d59e27411798044f67b37c898e294d8d0a75f33be21f914934b92d5c848c4d3dcf0199eefd699f47c68b2a2deb8cf41a62f3f6b669c675433d9fee375cb2eddedcbaf996be2e494544903ee9a70920f23",
                ApprovalStatus = "0b0823f53f144479a8c888b7b1a7426f918758b8afa0486c8e303ba0a6e559eee294712483f34d9587856de33a8a5e446e74f3b5cc1f4931bf3770cfaa6742314b8a58e5441049ea96c2cdc16a277cb4b2d73160e5394083a79d99a80449e4c5506b8c868b1d4f43b70a4769afe145b9c6ee9447316c4ab88ab0c3df09cb98f",
                AccountNo = "01af5fca0a44418f82c1ade8f512e9742127d1a5d2304fd4ab40be91dcc049c48dd607e44e024270ba4100",
                ProjectType = "3fef3ae6349e",
                Location = "d87febaab54f437d9929e8afd0de2c18e9d2c80f19df42b2acaa4f16d901b9153b7",
                DistributorMagin = "4d1f91dcaee546fd965c9e743fba",
                PODate = new DateTime(2010, 3, 17),
                DistributorId = Guid.Parse("5495de11-951e-4437-963f-606bdd09b17e"),

            };

            // Act
            var serviceResult = await _projectsAppService.CreateAsync(input);

            // Assert
            var result = await _projectRepository.FindAsync(c => c.Id == serviceResult.Id);

            result.ShouldNotBe(null);
            result.ProjectCode.ShouldBe("d49d0a1c559d4fcb88ee");
            result.ProjectName.ShouldBe("2047d4f6e3af483cbc11022274468445e7a32171614f44c6ab3a133193b1e41d223009afdbea4e0bbaeade10c7e2c0d59e27411798044f67b37c898e294d8d0a75f33be21f914934b92d5c848c4d3dcf0199eefd699f47c68b2a2deb8cf41a62f3f6b669c675433d9fee375cb2eddedcbaf996be2e494544903ee9a70920f23");
            result.ApprovalStatus.ShouldBe("0b0823f53f144479a8c888b7b1a7426f918758b8afa0486c8e303ba0a6e559eee294712483f34d9587856de33a8a5e446e74f3b5cc1f4931bf3770cfaa6742314b8a58e5441049ea96c2cdc16a277cb4b2d73160e5394083a79d99a80449e4c5506b8c868b1d4f43b70a4769afe145b9c6ee9447316c4ab88ab0c3df09cb98f");
            result.AccountNo.ShouldBe("01af5fca0a44418f82c1ade8f512e9742127d1a5d2304fd4ab40be91dcc049c48dd607e44e024270ba4100");
            result.ProjectType.ShouldBe("3fef3ae6349e");
            result.Location.ShouldBe("d87febaab54f437d9929e8afd0de2c18e9d2c80f19df42b2acaa4f16d901b9153b7");
            result.DistributorMagin.ShouldBe("4d1f91dcaee546fd965c9e743fba");
            result.PODate.ShouldBe(new DateTime(2010, 3, 17));
        }

        [Fact]
        public async Task UpdateAsync()
        {
            // Arrange
            var input = new ProjectUpdateDto()
            {
                ProjectCode = "76c0785d75b74e1593a8",
                ProjectName = "25872ba19cb646c2ac665bb84868538dd7609716234345df982d2e657ab0a3980d24f001ce5842f58d6b13b0b7830414578529690ad4480eba3ec040c3d89bb63657a58fa4454a7ab985d9cbdbf8b5a25fcecaaf26cc455ca698e5cc24aff32d12b164582f0d46458cd035fdd75fad44e77ec8004251446b85d0f9cec65f6ca",
                ApprovalStatus = "15672dea09b64bb783a45b3a7288ca4a3602149dca66434c9f7b903f4b2e11421cc6615f87874df5a0c44502386011c6248a14a02d26463d83f80c1426ef4c605bc2a98a989d4071b8793e5e758a509b5a0d95062dfa472095f63e733bf29d172a49776c25e54876872c0d49e6d4cfd13c4f9cd73028489a9cde78000814b4a",
                AccountNo = "66120f64b1ca4598899aba9788c5ec2a31b358e3d5134b3f87c6e3db3fa57621b1be5258c1",
                ProjectType = "846c4712734d4f3e85a6aee8a8cee46782b4aaf60fc14a7a8",
                Location = "3ff636e1e4094bd59d0597314b3a5cf3f8e9523cbce147b6b50a2c4be9a1084b83a10a9f059e4",
                DistributorMagin = "30a5056a9e9f4422bbbec213f3bb20a93310ba67c8fd4bb989c3e5e7e0b747af98159251ac9242e3b81969bec423d2a7",
                PODate = new DateTime(2002, 4, 26),
                DistributorId = Guid.Parse("5495de11-951e-4437-963f-606bdd09b17e"),

            };

            // Act
            var serviceResult = await _projectsAppService.UpdateAsync(Guid.Parse("5b8c08c7-b622-441a-9d31-29bd2307def2"), input);

            // Assert
            var result = await _projectRepository.FindAsync(c => c.Id == serviceResult.Id);

            result.ShouldNotBe(null);
            result.ProjectCode.ShouldBe("76c0785d75b74e1593a8");
            result.ProjectName.ShouldBe("25872ba19cb646c2ac665bb84868538dd7609716234345df982d2e657ab0a3980d24f001ce5842f58d6b13b0b7830414578529690ad4480eba3ec040c3d89bb63657a58fa4454a7ab985d9cbdbf8b5a25fcecaaf26cc455ca698e5cc24aff32d12b164582f0d46458cd035fdd75fad44e77ec8004251446b85d0f9cec65f6ca");
            result.ApprovalStatus.ShouldBe("15672dea09b64bb783a45b3a7288ca4a3602149dca66434c9f7b903f4b2e11421cc6615f87874df5a0c44502386011c6248a14a02d26463d83f80c1426ef4c605bc2a98a989d4071b8793e5e758a509b5a0d95062dfa472095f63e733bf29d172a49776c25e54876872c0d49e6d4cfd13c4f9cd73028489a9cde78000814b4a");
            result.AccountNo.ShouldBe("66120f64b1ca4598899aba9788c5ec2a31b358e3d5134b3f87c6e3db3fa57621b1be5258c1");
            result.ProjectType.ShouldBe("846c4712734d4f3e85a6aee8a8cee46782b4aaf60fc14a7a8");
            result.Location.ShouldBe("3ff636e1e4094bd59d0597314b3a5cf3f8e9523cbce147b6b50a2c4be9a1084b83a10a9f059e4");
            result.DistributorMagin.ShouldBe("30a5056a9e9f4422bbbec213f3bb20a93310ba67c8fd4bb989c3e5e7e0b747af98159251ac9242e3b81969bec423d2a7");
            result.PODate.ShouldBe(new DateTime(2002, 4, 26));
        }

        [Fact]
        public async Task DeleteAsync()
        {
            // Act
            await _projectsAppService.DeleteAsync(Guid.Parse("5b8c08c7-b622-441a-9d31-29bd2307def2"));

            // Assert
            var result = await _projectRepository.FindAsync(c => c.Id == Guid.Parse("5b8c08c7-b622-441a-9d31-29bd2307def2"));

            result.ShouldBeNull();
        }
    }
}