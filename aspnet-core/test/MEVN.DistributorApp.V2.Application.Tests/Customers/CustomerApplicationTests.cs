using System;
using System.Linq;
using Shouldly;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;
using Xunit;

namespace MEVN.DistributorApp.V2.Customers
{
    public class CustomersAppServiceTests : V2ApplicationTestBase
    {
        private readonly ICustomersAppService _customersAppService;
        private readonly IRepository<Customer, Guid> _customerRepository;

        public CustomersAppServiceTests()
        {
            _customersAppService = GetRequiredService<ICustomersAppService>();
            _customerRepository = GetRequiredService<IRepository<Customer, Guid>>();
        }

        [Fact]
        public async Task GetListAsync()
        {
            // Act
            var result = await _customersAppService.GetListAsync(new GetCustomersInput());

            // Assert
            result.TotalCount.ShouldBe(2);
            result.Items.Count.ShouldBe(2);
            result.Items.Any(x => x.Id == Guid.Parse("2d2fc3ba-d9e5-414d-afc8-ab6f8cde35dc")).ShouldBe(true);
            result.Items.Any(x => x.Id == Guid.Parse("cf801891-f1ba-4d66-8c49-c40c5ada7619")).ShouldBe(true);
        }

        [Fact]
        public async Task GetAsync()
        {
            // Act
            var result = await _customersAppService.GetAsync(Guid.Parse("2d2fc3ba-d9e5-414d-afc8-ab6f8cde35dc"));

            // Assert
            result.ShouldNotBeNull();
            result.Id.ShouldBe(Guid.Parse("2d2fc3ba-d9e5-414d-afc8-ab6f8cde35dc"));
        }

        [Fact]
        public async Task CreateAsync()
        {
            // Arrange
            var input = new CustomerCreateDto
            {
                TaxCode = "a1106fb37669411dae1f7ccd03d378cdd5a013066c964e2585",
                CusName = "1022de133c754538b9b492de1878a4acf646d2f0ebb942f6815bd176487401ae05c0a86867a8471ea424ca5f4016496fc430a4882b8349818b3f318738cc630268c0cb2ee3fc445d811be7e61623d693dd69867b92194978b0a3839a21269668fd9e7358c1144b148c8226e274c025e50996375e64e349edb5782aef50d785e",
                Address = "c19b061c7cc84e63895fdaf7d571470b2bcff738d62a44ca8331be8d1990b74c7996ddf9cee94f0bb3c192b7232bcdb29bf77adc02c845468ae0d0f8ce240893ed006bbb0f514221864455d0f5451e51ab30d8c61f364cbd8b68efe35078e5057374fbd288044d1a80d5d3b4664bd9bbbc9982c12607466abb5e6275cac0619",
                Phone = "9f711422183f4da6913b0e7b052430fe098b76c217f74614ad2c60e118ab20453f51fb055c9b4bf692aa83525e705fea74b9266dd43140208ffc088d951278742e52853bf45347b28569813a7ef3b4f1db384c1b6dfc4ef8ab4044923e352b32bdee62df18774ee488dbcdecdfcb8134adac91f5eb68481d80862f8a0f8fccf",
                Note = "2ce79a2dfee04a0681456c7afc2f36adb4a"
            };

            // Act
            var serviceResult = await _customersAppService.CreateAsync(input);

            // Assert
            var result = await _customerRepository.FindAsync(c => c.Id == serviceResult.Id);

            result.ShouldNotBe(null);
            result.TaxCode.ShouldBe("a1106fb37669411dae1f7ccd03d378cdd5a013066c964e2585");
            result.CusName.ShouldBe("1022de133c754538b9b492de1878a4acf646d2f0ebb942f6815bd176487401ae05c0a86867a8471ea424ca5f4016496fc430a4882b8349818b3f318738cc630268c0cb2ee3fc445d811be7e61623d693dd69867b92194978b0a3839a21269668fd9e7358c1144b148c8226e274c025e50996375e64e349edb5782aef50d785e");
            result.Address.ShouldBe("c19b061c7cc84e63895fdaf7d571470b2bcff738d62a44ca8331be8d1990b74c7996ddf9cee94f0bb3c192b7232bcdb29bf77adc02c845468ae0d0f8ce240893ed006bbb0f514221864455d0f5451e51ab30d8c61f364cbd8b68efe35078e5057374fbd288044d1a80d5d3b4664bd9bbbc9982c12607466abb5e6275cac0619");
            result.Phone.ShouldBe("9f711422183f4da6913b0e7b052430fe098b76c217f74614ad2c60e118ab20453f51fb055c9b4bf692aa83525e705fea74b9266dd43140208ffc088d951278742e52853bf45347b28569813a7ef3b4f1db384c1b6dfc4ef8ab4044923e352b32bdee62df18774ee488dbcdecdfcb8134adac91f5eb68481d80862f8a0f8fccf");
            result.Note.ShouldBe("2ce79a2dfee04a0681456c7afc2f36adb4a");
        }

        [Fact]
        public async Task UpdateAsync()
        {
            // Arrange
            var input = new CustomerUpdateDto()
            {
                TaxCode = "92d3af717ab14821979bee22182aacbb8e35fd3b7572404e95",
                CusName = "d641245cfba64bfdb830292b976ddbbdb91080aeb77f45289406362fa40f4feab5d11594d75046ad93d72e7f29ca2ef83f5e7f53642e48cc82ff34b97834bd6ef3839903a30f429e91a540a4932d9f0234930123568240f78c0e24a34f4773e561dc926c9c9341478022d317ed2027f9cf22fbefba454abba0f589f240d8f28",
                Address = "290caa5f50f946108bda6e4642162662d72a22083b1c4a69a2e6bad3a285cc2d6e3d60e4abfb4f4cb19e6df3c0b22b7377f4913ac2784da88f0f8f7d39fe22f4262b73d4bcf14adaa2452cc542200f174bf8057aef804973a74131d4d87572e0f99edd3bf4084b5cbb7c914214ff328b7a33b46039474829a33de4c3bd5e227",
                Phone = "a0b955b5aadf4b18a05060efbc3dc80f0727ccd5733a4be8b67b92b11cf01843ec26283c5ae14cdcb662bc9845928adc4891861c0f994ed69bbea65e6b6a9b29deaf7b3e7fb8469ca79ed33bd35ff08af1f1e41409934e63b00d068d97b800b518643e79f02c44a5b5d545dcfead9f52d457a7c7c30e4459ad643581b66e0d8",
                Note = "d048c8f25da44"
            };

            // Act
            var serviceResult = await _customersAppService.UpdateAsync(Guid.Parse("2d2fc3ba-d9e5-414d-afc8-ab6f8cde35dc"), input);

            // Assert
            var result = await _customerRepository.FindAsync(c => c.Id == serviceResult.Id);

            result.ShouldNotBe(null);
            result.TaxCode.ShouldBe("92d3af717ab14821979bee22182aacbb8e35fd3b7572404e95");
            result.CusName.ShouldBe("d641245cfba64bfdb830292b976ddbbdb91080aeb77f45289406362fa40f4feab5d11594d75046ad93d72e7f29ca2ef83f5e7f53642e48cc82ff34b97834bd6ef3839903a30f429e91a540a4932d9f0234930123568240f78c0e24a34f4773e561dc926c9c9341478022d317ed2027f9cf22fbefba454abba0f589f240d8f28");
            result.Address.ShouldBe("290caa5f50f946108bda6e4642162662d72a22083b1c4a69a2e6bad3a285cc2d6e3d60e4abfb4f4cb19e6df3c0b22b7377f4913ac2784da88f0f8f7d39fe22f4262b73d4bcf14adaa2452cc542200f174bf8057aef804973a74131d4d87572e0f99edd3bf4084b5cbb7c914214ff328b7a33b46039474829a33de4c3bd5e227");
            result.Phone.ShouldBe("a0b955b5aadf4b18a05060efbc3dc80f0727ccd5733a4be8b67b92b11cf01843ec26283c5ae14cdcb662bc9845928adc4891861c0f994ed69bbea65e6b6a9b29deaf7b3e7fb8469ca79ed33bd35ff08af1f1e41409934e63b00d068d97b800b518643e79f02c44a5b5d545dcfead9f52d457a7c7c30e4459ad643581b66e0d8");
            result.Note.ShouldBe("d048c8f25da44");
        }

        [Fact]
        public async Task DeleteAsync()
        {
            // Act
            await _customersAppService.DeleteAsync(Guid.Parse("2d2fc3ba-d9e5-414d-afc8-ab6f8cde35dc"));

            // Assert
            var result = await _customerRepository.FindAsync(c => c.Id == Guid.Parse("2d2fc3ba-d9e5-414d-afc8-ab6f8cde35dc"));

            result.ShouldBeNull();
        }
    }
}