using System;
using System.Linq;
using Shouldly;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;
using Xunit;

namespace MEVN.DistributorApp.V2.Distributors
{
    public class DistributorsAppServiceTests : V2ApplicationTestBase
    {
        private readonly IDistributorsAppService _distributorsAppService;
        private readonly IRepository<Distributor, Guid> _distributorRepository;

        public DistributorsAppServiceTests()
        {
            _distributorsAppService = GetRequiredService<IDistributorsAppService>();
            _distributorRepository = GetRequiredService<IRepository<Distributor, Guid>>();
        }

        [Fact]
        public async Task GetListAsync()
        {
            // Act
            var result = await _distributorsAppService.GetListAsync(new GetDistributorsInput());

            // Assert
            result.TotalCount.ShouldBe(2);
            result.Items.Count.ShouldBe(2);
            result.Items.Any(x => x.Id == Guid.Parse("5495de11-951e-4437-963f-606bdd09b17e")).ShouldBe(true);
            result.Items.Any(x => x.Id == Guid.Parse("de80851f-405e-45cd-852e-b972f1d251aa")).ShouldBe(true);
        }

        [Fact]
        public async Task GetAsync()
        {
            // Act
            var result = await _distributorsAppService.GetAsync(Guid.Parse("5495de11-951e-4437-963f-606bdd09b17e"));

            // Assert
            result.ShouldNotBeNull();
            result.Id.ShouldBe(Guid.Parse("5495de11-951e-4437-963f-606bdd09b17e"));
        }

        [Fact]
        public async Task CreateAsync()
        {
            // Arrange
            var input = new DistributorCreateDto
            {
                DisCode = "1063241265504311b15b",
                DisName = "605dee63d7374d75b0b2c0762f1a7f3501593a0d2e56427b861a777dcd391053029a36af06fa42029d400d1aaa30801e34460fd924b74aa8b04e7f19f6aa57e26c388106164245548e6ea22095a558c370d59cc8ad0e4cab9ee2d343c9831b2e2894767b",
                Address = "b5fb4411da0a4563b4c6de3c7e16e91252bc8afb12fe49a7b5bdf3c275ca5624a67a0e41f1704a968a4caf5ffac875b",
                ContactInfo = "42ddd3dbfcd94b92bb95aaf1f1f03f8cdd417bb6c85b429c842c2d067cb7b3c237",
                Node = "a1dabdb14ebf4850bc60fae64f59c4eac154aac38b5b438abd8c6f5bbc91d"
            };

            // Act
            var serviceResult = await _distributorsAppService.CreateAsync(input);

            // Assert
            var result = await _distributorRepository.FindAsync(c => c.Id == serviceResult.Id);

            result.ShouldNotBe(null);
            result.DisCode.ShouldBe("1063241265504311b15b");
            result.DisName.ShouldBe("605dee63d7374d75b0b2c0762f1a7f3501593a0d2e56427b861a777dcd391053029a36af06fa42029d400d1aaa30801e34460fd924b74aa8b04e7f19f6aa57e26c388106164245548e6ea22095a558c370d59cc8ad0e4cab9ee2d343c9831b2e2894767b");
            result.Address.ShouldBe("b5fb4411da0a4563b4c6de3c7e16e91252bc8afb12fe49a7b5bdf3c275ca5624a67a0e41f1704a968a4caf5ffac875b");
            result.ContactInfo.ShouldBe("42ddd3dbfcd94b92bb95aaf1f1f03f8cdd417bb6c85b429c842c2d067cb7b3c237");
            result.Node.ShouldBe("a1dabdb14ebf4850bc60fae64f59c4eac154aac38b5b438abd8c6f5bbc91d");
        }

        [Fact]
        public async Task UpdateAsync()
        {
            // Arrange
            var input = new DistributorUpdateDto()
            {
                DisCode = "6364c272743f457a96aa",
                DisName = "64cf8512547a4b63bc503bf8587b9dbc6525fd53bc80417ea50b78af8dfef4577171c8740a704558826ada79e3a97d1161b68bbec49d47e89644bf42d2eb5b8cd5d4e14f89aa45dd9e27bc1ce95d4f2fc1dcf0a342534e1c963a1d7bf77d61c9d707cf78",
                Address = "5b2cd13cb3d849c7984cc9370f41fd7972c80846807",
                ContactInfo = "568db1705a",
                Node = "3f050322e1954a778c778fa71da45dff2f66739ffabf4be5b92e663c1546ceafa8"
            };

            // Act
            var serviceResult = await _distributorsAppService.UpdateAsync(Guid.Parse("5495de11-951e-4437-963f-606bdd09b17e"), input);

            // Assert
            var result = await _distributorRepository.FindAsync(c => c.Id == serviceResult.Id);

            result.ShouldNotBe(null);
            result.DisCode.ShouldBe("6364c272743f457a96aa");
            result.DisName.ShouldBe("64cf8512547a4b63bc503bf8587b9dbc6525fd53bc80417ea50b78af8dfef4577171c8740a704558826ada79e3a97d1161b68bbec49d47e89644bf42d2eb5b8cd5d4e14f89aa45dd9e27bc1ce95d4f2fc1dcf0a342534e1c963a1d7bf77d61c9d707cf78");
            result.Address.ShouldBe("5b2cd13cb3d849c7984cc9370f41fd7972c80846807");
            result.ContactInfo.ShouldBe("568db1705a");
            result.Node.ShouldBe("3f050322e1954a778c778fa71da45dff2f66739ffabf4be5b92e663c1546ceafa8");
        }

        [Fact]
        public async Task DeleteAsync()
        {
            // Act
            await _distributorsAppService.DeleteAsync(Guid.Parse("5495de11-951e-4437-963f-606bdd09b17e"));

            // Assert
            var result = await _distributorRepository.FindAsync(c => c.Id == Guid.Parse("5495de11-951e-4437-963f-606bdd09b17e"));

            result.ShouldBeNull();
        }
    }
}