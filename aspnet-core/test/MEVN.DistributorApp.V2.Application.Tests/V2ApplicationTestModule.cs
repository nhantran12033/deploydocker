﻿using Volo.Abp.Modularity;

namespace MEVN.DistributorApp.V2;

[DependsOn(
    typeof(V2ApplicationModule),
    typeof(V2DomainTestModule)
    )]
public class V2ApplicationTestModule : AbpModule
{

}
