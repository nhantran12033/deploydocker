using System;
using System.Linq;
using Shouldly;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;
using Xunit;

namespace MEVN.DistributorApp.V2.MaterialGroups
{
    public class MaterialGroupsAppServiceTests : V2ApplicationTestBase
    {
        private readonly IMaterialGroupsAppService _materialGroupsAppService;
        private readonly IRepository<MaterialGroup, Guid> _materialGroupRepository;

        public MaterialGroupsAppServiceTests()
        {
            _materialGroupsAppService = GetRequiredService<IMaterialGroupsAppService>();
            _materialGroupRepository = GetRequiredService<IRepository<MaterialGroup, Guid>>();
        }

        [Fact]
        public async Task GetListAsync()
        {
            // Act
            var result = await _materialGroupsAppService.GetListAsync(new GetMaterialGroupsInput());

            // Assert
            result.TotalCount.ShouldBe(2);
            result.Items.Count.ShouldBe(2);
            result.Items.Any(x => x.Id == Guid.Parse("82ad3d83-1826-4ddc-8525-7343c1771a06")).ShouldBe(true);
            result.Items.Any(x => x.Id == Guid.Parse("a5aa95c7-ff2d-48ad-90a7-658f78041a29")).ShouldBe(true);
        }

        [Fact]
        public async Task GetAsync()
        {
            // Act
            var result = await _materialGroupsAppService.GetAsync(Guid.Parse("82ad3d83-1826-4ddc-8525-7343c1771a06"));

            // Assert
            result.ShouldNotBeNull();
            result.Id.ShouldBe(Guid.Parse("82ad3d83-1826-4ddc-8525-7343c1771a06"));
        }

        [Fact]
        public async Task CreateAsync()
        {
            // Arrange
            var input = new MaterialGroupCreateDto
            {
                MaterialName = "687f9c08fa434afa973e84621dbea824f61c0931522c41",
                DisCode = "4d09b3c9ddf14bb48c7bda5d6897ebabb2"
            };

            // Act
            var serviceResult = await _materialGroupsAppService.CreateAsync(input);

            // Assert
            var result = await _materialGroupRepository.FindAsync(c => c.Id == serviceResult.Id);

            result.ShouldNotBe(null);
            result.MaterialName.ShouldBe("687f9c08fa434afa973e84621dbea824f61c0931522c41");
            result.DisCode.ShouldBe("4d09b3c9ddf14bb48c7bda5d6897ebabb2");
        }

        [Fact]
        public async Task UpdateAsync()
        {
            // Arrange
            var input = new MaterialGroupUpdateDto()
            {
                MaterialName = "8974a1ec6f8b463aa753863f12480ee611fb2461d60e484c8979d1a9c55124ae6bb10869eea948d2b12ba323c3",
                DisCode = "775da2174aa44e22bdf3f8d511e37dced921b0748443472f898d6a0ebd45e2b45cc2c3"
            };

            // Act
            var serviceResult = await _materialGroupsAppService.UpdateAsync(Guid.Parse("82ad3d83-1826-4ddc-8525-7343c1771a06"), input);

            // Assert
            var result = await _materialGroupRepository.FindAsync(c => c.Id == serviceResult.Id);

            result.ShouldNotBe(null);
            result.MaterialName.ShouldBe("8974a1ec6f8b463aa753863f12480ee611fb2461d60e484c8979d1a9c55124ae6bb10869eea948d2b12ba323c3");
            result.DisCode.ShouldBe("775da2174aa44e22bdf3f8d511e37dced921b0748443472f898d6a0ebd45e2b45cc2c3");
        }

        [Fact]
        public async Task DeleteAsync()
        {
            // Act
            await _materialGroupsAppService.DeleteAsync(Guid.Parse("82ad3d83-1826-4ddc-8525-7343c1771a06"));

            // Assert
            var result = await _materialGroupRepository.FindAsync(c => c.Id == Guid.Parse("82ad3d83-1826-4ddc-8525-7343c1771a06"));

            result.ShouldBeNull();
        }
    }
}