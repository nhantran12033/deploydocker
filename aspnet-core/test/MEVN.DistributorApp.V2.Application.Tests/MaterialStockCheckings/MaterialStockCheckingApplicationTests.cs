using System;
using System.Linq;
using Shouldly;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;
using Xunit;

namespace MEVN.DistributorApp.V2.MaterialStockCheckings
{
    public class MaterialStockCheckingsAppServiceTests : V2ApplicationTestBase
    {
        private readonly IMaterialStockCheckingsAppService _materialStockCheckingsAppService;
        private readonly IRepository<MaterialStockChecking, Guid> _materialStockCheckingRepository;

        public MaterialStockCheckingsAppServiceTests()
        {
            _materialStockCheckingsAppService = GetRequiredService<IMaterialStockCheckingsAppService>();
            _materialStockCheckingRepository = GetRequiredService<IRepository<MaterialStockChecking, Guid>>();
        }

        [Fact]
        public async Task GetListAsync()
        {
            // Act
            var result = await _materialStockCheckingsAppService.GetListAsync(new GetMaterialStockCheckingsInput());

            // Assert
            result.TotalCount.ShouldBe(2);
            result.Items.Count.ShouldBe(2);
            result.Items.Any(x => x.MaterialStockChecking.Id == Guid.Parse("259d7bf3-93d5-4185-a3d8-9920908859eb")).ShouldBe(true);
            result.Items.Any(x => x.MaterialStockChecking.Id == Guid.Parse("cb229de2-2142-429d-8a06-82ccadd7e409")).ShouldBe(true);
        }

        [Fact]
        public async Task GetAsync()
        {
            // Act
            var result = await _materialStockCheckingsAppService.GetAsync(Guid.Parse("259d7bf3-93d5-4185-a3d8-9920908859eb"));

            // Assert
            result.ShouldNotBeNull();
            result.Id.ShouldBe(Guid.Parse("259d7bf3-93d5-4185-a3d8-9920908859eb"));
        }

        [Fact]
        public async Task CreateAsync()
        {
            // Arrange
            var input = new MaterialStockCheckingCreateDto
            {
                GolfaCode = "54b186b0adf34957931df",
                Model = "4da99ad7d8b54b9683e499698491018f9524af8e1de0413a8eccda1c2e0188",
                SAP_Code = "5f703fc0f81941138bafd094a57b80ee3486466e2d1143ec9767ef4dc8b05734479043b9d7fe4f989026c50ad7d7",
                Description_VN = "a20cae493c4a4bb3929b97282ee1",
                Description_Group = "86456b1ffce34a7c9ec5f3e99bfdeb8b8ea6fb",
                Standard_Price = 413840987,
                StockValueWarning = 971054363,
                StockTmp = 1318411240,
                Spec1 = "317d32d4639b47c0af6202222de051753b8fd62ccd51419285fa4118f61ff8e7ebf5f2",
                Spec2 = "0672fafbf6324a8e820",
                MaterialGroupId = Guid.Parse("82ad3d83-1826-4ddc-8525-7343c1771a06"),
                VenderId = Guid.Parse("b5d44f56-eb3b-4ac5-b873-1453469ed73f"),

            };

            // Act
            var serviceResult = await _materialStockCheckingsAppService.CreateAsync(input);

            // Assert
            var result = await _materialStockCheckingRepository.FindAsync(c => c.Id == serviceResult.Id);

            result.ShouldNotBe(null);
            result.GolfaCode.ShouldBe("54b186b0adf34957931df");
            result.Model.ShouldBe("4da99ad7d8b54b9683e499698491018f9524af8e1de0413a8eccda1c2e0188");
            result.SAP_Code.ShouldBe("5f703fc0f81941138bafd094a57b80ee3486466e2d1143ec9767ef4dc8b05734479043b9d7fe4f989026c50ad7d7");
            result.Description_VN.ShouldBe("a20cae493c4a4bb3929b97282ee1");
            result.Description_Group.ShouldBe("86456b1ffce34a7c9ec5f3e99bfdeb8b8ea6fb");
            result.Standard_Price.ShouldBe(413840987);
            result.StockValueWarning.ShouldBe(971054363);
            result.StockTmp.ShouldBe(1318411240);
            result.Spec1.ShouldBe("317d32d4639b47c0af6202222de051753b8fd62ccd51419285fa4118f61ff8e7ebf5f2");
            result.Spec2.ShouldBe("0672fafbf6324a8e820");
        }

        [Fact]
        public async Task UpdateAsync()
        {
            // Arrange
            var input = new MaterialStockCheckingUpdateDto()
            {
                ,
            MaterialGroupId = Guid.Parse("82ad3d83-1826-4ddc-8525-7343c1771a06"),
            VenderId = Guid.Parse("b5d44f56-eb3b-4ac5-b873-1453469ed73f"),

            };

            // Act
            var serviceResult = await _materialStockCheckingsAppService.UpdateAsync(Guid.Parse("259d7bf3-93d5-4185-a3d8-9920908859eb"), input);

            // Assert
            var result = await _materialStockCheckingRepository.FindAsync(c => c.Id == serviceResult.Id);

            result.ShouldNotBe(null);

        }

        [Fact]
        public async Task DeleteAsync()
        {
            // Act
            await _materialStockCheckingsAppService.DeleteAsync(Guid.Parse("259d7bf3-93d5-4185-a3d8-9920908859eb"));

            // Assert
            var result = await _materialStockCheckingRepository.FindAsync(c => c.Id == Guid.Parse("259d7bf3-93d5-4185-a3d8-9920908859eb"));

            result.ShouldBeNull();
        }
    }
}