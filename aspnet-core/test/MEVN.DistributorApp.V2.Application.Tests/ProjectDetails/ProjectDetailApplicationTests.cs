using System;
using System.Linq;
using Shouldly;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;
using Xunit;

namespace MEVN.DistributorApp.V2.ProjectDetails
{
    public class ProjectDetailsAppServiceTests : V2ApplicationTestBase
    {
        private readonly IProjectDetailsAppService _projectDetailsAppService;
        private readonly IRepository<ProjectDetail, Guid> _projectDetailRepository;

        public ProjectDetailsAppServiceTests()
        {
            _projectDetailsAppService = GetRequiredService<IProjectDetailsAppService>();
            _projectDetailRepository = GetRequiredService<IRepository<ProjectDetail, Guid>>();
        }

        [Fact]
        public async Task GetListAsync()
        {
            // Act
            var result = await _projectDetailsAppService.GetListAsync(new GetProjectDetailsInput());

            // Assert
            result.TotalCount.ShouldBe(2);
            result.Items.Count.ShouldBe(2);
            result.Items.Any(x => x.Id == Guid.Parse("28538b4e-c068-4089-993e-60616e446271")).ShouldBe(true);
            result.Items.Any(x => x.Id == Guid.Parse("f6e93234-69e5-4863-bdce-dae9af04396a")).ShouldBe(true);
        }

        [Fact]
        public async Task GetAsync()
        {
            // Act
            var result = await _projectDetailsAppService.GetAsync(Guid.Parse("28538b4e-c068-4089-993e-60616e446271"));

            // Assert
            result.ShouldNotBeNull();
            result.Id.ShouldBe(Guid.Parse("28538b4e-c068-4089-993e-60616e446271"));
        }

        [Fact]
        public async Task CreateAsync()
        {
            // Arrange
            var input = new ProjectDetailCreateDto
            {
                ProjectId = Guid.Parse("efd0dba4-c6b4-4050-a533-87ef5d7173ae"),
                GolfaCode = "ec1a6071b60c4cd99e086cbb756093562ca33706",
                Model = "004685990aa84e7fab2bbea",
                Qty = 532610734,
                DpoUsed = 436690780,
                RequestStatus = "5814c9965945431ab9c45a4a10135c151aa288be7b604ed9bb57a2e725882bdf3e1e8f8cc6",
                RequestPrice = 1957858311,
                DistRequestedPrice = 1189883454,
                SaleOfferPrice = 190036099,
                SaleAllowDiscountPrice = 594601494,
                AmountRequestedPrice = 103048909
            };

            // Act
            var serviceResult = await _projectDetailsAppService.CreateAsync(input);

            // Assert
            var result = await _projectDetailRepository.FindAsync(c => c.Id == serviceResult.Id);

            result.ShouldNotBe(null);
            result.ProjectId.ShouldBe(Guid.Parse("efd0dba4-c6b4-4050-a533-87ef5d7173ae"));
            result.GolfaCode.ShouldBe("ec1a6071b60c4cd99e086cbb756093562ca33706");
            result.Model.ShouldBe("004685990aa84e7fab2bbea");
            result.Qty.ShouldBe(532610734);
            result.DpoUsed.ShouldBe(436690780);
            result.RequestStatus.ShouldBe("5814c9965945431ab9c45a4a10135c151aa288be7b604ed9bb57a2e725882bdf3e1e8f8cc6");
            result.RequestPrice.ShouldBe(1957858311);
            result.DistRequestedPrice.ShouldBe(1189883454);
            result.SaleOfferPrice.ShouldBe(190036099);
            result.SaleAllowDiscountPrice.ShouldBe(594601494);
            result.AmountRequestedPrice.ShouldBe(103048909);
        }

        [Fact]
        public async Task UpdateAsync()
        {
            // Arrange
            var input = new ProjectDetailUpdateDto()
            {
                ProjectId = Guid.Parse("5d10e500-0acb-4dff-88dc-9a65d395cc44"),
                GolfaCode = "ad48a44a403349fd9426d070ed4881db9cb34291",
                Model = "fa70d9497d8c4429890d",
                Qty = 726790924,
                DpoUsed = 1070800628,
                RequestStatus = "352920faee32",
                RequestPrice = 2064164360,
                DistRequestedPrice = 213916567,
                SaleOfferPrice = 11316186,
                SaleAllowDiscountPrice = 231101687,
                AmountRequestedPrice = 1776952026
            };

            // Act
            var serviceResult = await _projectDetailsAppService.UpdateAsync(Guid.Parse("28538b4e-c068-4089-993e-60616e446271"), input);

            // Assert
            var result = await _projectDetailRepository.FindAsync(c => c.Id == serviceResult.Id);

            result.ShouldNotBe(null);
            result.ProjectId.ShouldBe(Guid.Parse("5d10e500-0acb-4dff-88dc-9a65d395cc44"));
            result.GolfaCode.ShouldBe("ad48a44a403349fd9426d070ed4881db9cb34291");
            result.Model.ShouldBe("fa70d9497d8c4429890d");
            result.Qty.ShouldBe(726790924);
            result.DpoUsed.ShouldBe(1070800628);
            result.RequestStatus.ShouldBe("352920faee32");
            result.RequestPrice.ShouldBe(2064164360);
            result.DistRequestedPrice.ShouldBe(213916567);
            result.SaleOfferPrice.ShouldBe(11316186);
            result.SaleAllowDiscountPrice.ShouldBe(231101687);
            result.AmountRequestedPrice.ShouldBe(1776952026);
        }

        [Fact]
        public async Task DeleteAsync()
        {
            // Act
            await _projectDetailsAppService.DeleteAsync(Guid.Parse("28538b4e-c068-4089-993e-60616e446271"));

            // Assert
            var result = await _projectDetailRepository.FindAsync(c => c.Id == Guid.Parse("28538b4e-c068-4089-993e-60616e446271"));

            result.ShouldBeNull();
        }
    }
}