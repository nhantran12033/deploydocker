using System;
using System.Linq;
using Shouldly;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;
using Xunit;

namespace MEVN.DistributorApp.V2.Venders
{
    public class VendersAppServiceTests : V2ApplicationTestBase
    {
        private readonly IVendersAppService _vendersAppService;
        private readonly IRepository<Vender, Guid> _venderRepository;

        public VendersAppServiceTests()
        {
            _vendersAppService = GetRequiredService<IVendersAppService>();
            _venderRepository = GetRequiredService<IRepository<Vender, Guid>>();
        }

        [Fact]
        public async Task GetListAsync()
        {
            // Act
            var result = await _vendersAppService.GetListAsync(new GetVendersInput());

            // Assert
            result.TotalCount.ShouldBe(2);
            result.Items.Count.ShouldBe(2);
            result.Items.Any(x => x.Id == Guid.Parse("b5d44f56-eb3b-4ac5-b873-1453469ed73f")).ShouldBe(true);
            result.Items.Any(x => x.Id == Guid.Parse("7ce0c544-eb6b-49f3-afbf-9af52c5d7376")).ShouldBe(true);
        }

        [Fact]
        public async Task GetAsync()
        {
            // Act
            var result = await _vendersAppService.GetAsync(Guid.Parse("b5d44f56-eb3b-4ac5-b873-1453469ed73f"));

            // Assert
            result.ShouldNotBeNull();
            result.Id.ShouldBe(Guid.Parse("b5d44f56-eb3b-4ac5-b873-1453469ed73f"));
        }

        [Fact]
        public async Task CreateAsync()
        {
            // Arrange
            var input = new VenderCreateDto
            {
                VerderName = "38f248db49f943078f75b510037329ce4d027f14918543268a3105a8223afbe933d24004b0ac4591bb61c0a2da94877bbd214d91a88e45a0b518f6c068a0deae11d8de98c94d48fdbd60cfca2a7b0aad2de7da8002c14f35b8d9af4758bc6ec1a506b0c836f44224bd1c27812f316d567bb79da9f72046fc822981ad568eb6d",
                Address = "e321ec1007064afa877bb5e70a875c7ebaed8d2653214640a1747073b791e55",
                Email = "d@b.com"
            };

            // Act
            var serviceResult = await _vendersAppService.CreateAsync(input);

            // Assert
            var result = await _venderRepository.FindAsync(c => c.Id == serviceResult.Id);

            result.ShouldNotBe(null);
            result.VerderName.ShouldBe("38f248db49f943078f75b510037329ce4d027f14918543268a3105a8223afbe933d24004b0ac4591bb61c0a2da94877bbd214d91a88e45a0b518f6c068a0deae11d8de98c94d48fdbd60cfca2a7b0aad2de7da8002c14f35b8d9af4758bc6ec1a506b0c836f44224bd1c27812f316d567bb79da9f72046fc822981ad568eb6d");
            result.Address.ShouldBe("e321ec1007064afa877bb5e70a875c7ebaed8d2653214640a1747073b791e55");
            result.Email.ShouldBe("d@b.com");
        }

        [Fact]
        public async Task UpdateAsync()
        {
            // Arrange
            var input = new VenderUpdateDto()
            {
                VerderName = "64557eed545543c6acdd3349555d5b20c6131e75d57148aea78356446dd087b2f5274b819d774567ae21f456c459284c3ff1a42921124dadacb88f938be227bf1a01c37cb1c1460990835db0f0616f65098c59ad70d94c28bc81404fcc5058aefe85f82fc8d5470fa73e69004ea4265f1c7ba61e90604c05bf1a7960f2aa113",
                Address = "579c9a1a04ae45ae8d5c6eefa25fbc6aaa687064ea2147f8bd9599e16dfd784eae8",
                Email = "252f8622e5c@8968bcf8c65.com"
            };

            // Act
            var serviceResult = await _vendersAppService.UpdateAsync(Guid.Parse("b5d44f56-eb3b-4ac5-b873-1453469ed73f"), input);

            // Assert
            var result = await _venderRepository.FindAsync(c => c.Id == serviceResult.Id);

            result.ShouldNotBe(null);
            result.VerderName.ShouldBe("64557eed545543c6acdd3349555d5b20c6131e75d57148aea78356446dd087b2f5274b819d774567ae21f456c459284c3ff1a42921124dadacb88f938be227bf1a01c37cb1c1460990835db0f0616f65098c59ad70d94c28bc81404fcc5058aefe85f82fc8d5470fa73e69004ea4265f1c7ba61e90604c05bf1a7960f2aa113");
            result.Address.ShouldBe("579c9a1a04ae45ae8d5c6eefa25fbc6aaa687064ea2147f8bd9599e16dfd784eae8");
            result.Email.ShouldBe("252f8622e5c@8968bcf8c65.com");
        }

        [Fact]
        public async Task DeleteAsync()
        {
            // Act
            await _vendersAppService.DeleteAsync(Guid.Parse("b5d44f56-eb3b-4ac5-b873-1453469ed73f"));

            // Assert
            var result = await _venderRepository.FindAsync(c => c.Id == Guid.Parse("b5d44f56-eb3b-4ac5-b873-1453469ed73f"));

            result.ShouldBeNull();
        }
    }
}