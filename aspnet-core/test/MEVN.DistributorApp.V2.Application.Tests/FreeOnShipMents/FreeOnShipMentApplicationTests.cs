using System;
using System.Linq;
using Shouldly;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;
using Xunit;

namespace MEVN.DistributorApp.V2.FreeOnShipMents
{
    public class FreeOnShipMentsAppServiceTests : V2ApplicationTestBase
    {
        private readonly IFreeOnShipMentsAppService _freeOnShipMentsAppService;
        private readonly IRepository<FreeOnShipMent, Guid> _freeOnShipMentRepository;

        public FreeOnShipMentsAppServiceTests()
        {
            _freeOnShipMentsAppService = GetRequiredService<IFreeOnShipMentsAppService>();
            _freeOnShipMentRepository = GetRequiredService<IRepository<FreeOnShipMent, Guid>>();
        }

        [Fact]
        public async Task GetListAsync()
        {
            // Act
            var result = await _freeOnShipMentsAppService.GetListAsync(new GetFreeOnShipMentsInput());

            // Assert
            result.TotalCount.ShouldBe(2);
            result.Items.Count.ShouldBe(2);
            result.Items.Any(x => x.Id == Guid.Parse("8ea924e2-f909-4254-a1df-7e72c0fe3d19")).ShouldBe(true);
            result.Items.Any(x => x.Id == Guid.Parse("03597530-e501-4157-96f9-6592f0a9a1aa")).ShouldBe(true);
        }

        [Fact]
        public async Task GetAsync()
        {
            // Act
            var result = await _freeOnShipMentsAppService.GetAsync(Guid.Parse("8ea924e2-f909-4254-a1df-7e72c0fe3d19"));

            // Assert
            result.ShouldNotBeNull();
            result.Id.ShouldBe(Guid.Parse("8ea924e2-f909-4254-a1df-7e72c0fe3d19"));
        }

        [Fact]
        public async Task CreateAsync()
        {
            // Arrange
            var input = new FreeOnShipMentCreateDto
            {
                MaterialID = Guid.Parse("479b598a-7c43-4a99-ac0d-3ee30c4faf0f"),
                PONo = "30170cfd10c64aae91600264f75fda2f88f8eebbb21845d2a772bfefde0774fd2ff2529165134490b43b8b3f97c6b9689313091ea06642369fcb82a8d77e1c3a2c0171cfc2164cb6b100ff9b0c619c18234903123723406780b4c748db07dce3df8c3b89bb8c49e29b3ca53394776b5cf79b967c7fbf436d8bd55a0be49a1bb",
                PODate = new DateTime(2007, 9, 15),
                Qty = 721562358,
                MachineNumber = "ff3cad47c31a4f088a0d"
            };

            // Act
            var serviceResult = await _freeOnShipMentsAppService.CreateAsync(input);

            // Assert
            var result = await _freeOnShipMentRepository.FindAsync(c => c.Id == serviceResult.Id);

            result.ShouldNotBe(null);
            result.MaterialID.ShouldBe(Guid.Parse("479b598a-7c43-4a99-ac0d-3ee30c4faf0f"));
            result.PONo.ShouldBe("30170cfd10c64aae91600264f75fda2f88f8eebbb21845d2a772bfefde0774fd2ff2529165134490b43b8b3f97c6b9689313091ea06642369fcb82a8d77e1c3a2c0171cfc2164cb6b100ff9b0c619c18234903123723406780b4c748db07dce3df8c3b89bb8c49e29b3ca53394776b5cf79b967c7fbf436d8bd55a0be49a1bb");
            result.PODate.ShouldBe(new DateTime(2007, 9, 15));
            result.Qty.ShouldBe(721562358);
            result.MachineNumber.ShouldBe("ff3cad47c31a4f088a0d");
        }

        [Fact]
        public async Task UpdateAsync()
        {
            // Arrange
            var input = new FreeOnShipMentUpdateDto()
            {
                MaterialID = Guid.Parse("dd187134-74a0-4e0f-869c-3fa6af6f79fc"),

            };

            // Act
            var serviceResult = await _freeOnShipMentsAppService.UpdateAsync(Guid.Parse("8ea924e2-f909-4254-a1df-7e72c0fe3d19"), input);

            // Assert
            var result = await _freeOnShipMentRepository.FindAsync(c => c.Id == serviceResult.Id);

            result.ShouldNotBe(null);
            result.MaterialID.ShouldBe(Guid.Parse("dd187134-74a0-4e0f-869c-3fa6af6f79fc"));

        }

        [Fact]
        public async Task DeleteAsync()
        {
            // Act
            await _freeOnShipMentsAppService.DeleteAsync(Guid.Parse("8ea924e2-f909-4254-a1df-7e72c0fe3d19"));

            // Assert
            var result = await _freeOnShipMentRepository.FindAsync(c => c.Id == Guid.Parse("8ea924e2-f909-4254-a1df-7e72c0fe3d19"));

            result.ShouldBeNull();
        }
    }
}