﻿using MEVN.DistributorApp.V2.EntityFrameworkCore;
using Volo.Abp.Modularity;

namespace MEVN.DistributorApp.V2;

[DependsOn(
    typeof(V2EntityFrameworkCoreTestModule)
    )]
public class V2DomainTestModule : AbpModule
{

}
