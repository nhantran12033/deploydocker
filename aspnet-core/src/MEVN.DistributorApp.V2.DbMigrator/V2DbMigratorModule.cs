﻿using MEVN.DistributorApp.V2.EntityFrameworkCore;
using Volo.Abp.Autofac;
using Volo.Abp.Modularity;

namespace MEVN.DistributorApp.V2.DbMigrator;

[DependsOn(
    typeof(AbpAutofacModule),
    typeof(V2EntityFrameworkCoreModule),
    typeof(V2ApplicationContractsModule)
)]
public class V2DbMigratorModule : AbpModule
{
}
