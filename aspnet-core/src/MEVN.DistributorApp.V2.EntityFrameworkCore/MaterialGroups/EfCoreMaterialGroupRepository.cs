using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;
using MEVN.DistributorApp.V2.EntityFrameworkCore;

namespace MEVN.DistributorApp.V2.MaterialGroups
{
    public abstract class EfCoreMaterialGroupRepositoryBase : EfCoreRepository<V2DbContext, MaterialGroup, Guid>
    {
        public EfCoreMaterialGroupRepositoryBase(IDbContextProvider<V2DbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        public virtual async Task<List<MaterialGroup>> GetListAsync(
            string? filterText = null,
            string? materialName = null,
            string? disCode = null,
            string? sorting = null,
            int maxResultCount = int.MaxValue,
            int skipCount = 0,
            CancellationToken cancellationToken = default)
        {
            var query = ApplyFilter((await GetQueryableAsync()), filterText, materialName, disCode);
            query = query.OrderBy(string.IsNullOrWhiteSpace(sorting) ? MaterialGroupConsts.GetDefaultSorting(false) : sorting);
            return await query.PageBy(skipCount, maxResultCount).ToListAsync(cancellationToken);
        }

        public virtual async Task<long> GetCountAsync(
            string? filterText = null,
            string? materialName = null,
            string? disCode = null,
            CancellationToken cancellationToken = default)
        {
            var query = ApplyFilter((await GetDbSetAsync()), filterText, materialName, disCode);
            return await query.LongCountAsync(GetCancellationToken(cancellationToken));
        }

        protected virtual IQueryable<MaterialGroup> ApplyFilter(
            IQueryable<MaterialGroup> query,
            string? filterText = null,
            string? materialName = null,
            string? disCode = null)
        {
            return query
                    .WhereIf(!string.IsNullOrWhiteSpace(filterText), e => e.MaterialName!.Contains(filterText!) || e.DisCode!.Contains(filterText!))
                    .WhereIf(!string.IsNullOrWhiteSpace(materialName), e => e.MaterialName.Contains(materialName))
                    .WhereIf(!string.IsNullOrWhiteSpace(disCode), e => e.DisCode.Contains(disCode));
        }
    }
}