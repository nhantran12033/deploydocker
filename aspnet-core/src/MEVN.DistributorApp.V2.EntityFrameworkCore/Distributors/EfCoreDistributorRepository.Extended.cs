using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;
using MEVN.DistributorApp.V2.EntityFrameworkCore;

namespace MEVN.DistributorApp.V2.Distributors
{
    public class EfCoreDistributorRepository : EfCoreDistributorRepositoryBase, IDistributorRepository
    {
        public EfCoreDistributorRepository(IDbContextProvider<V2DbContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }
    }
}