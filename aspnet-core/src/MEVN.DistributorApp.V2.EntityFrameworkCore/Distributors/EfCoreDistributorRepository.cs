using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;
using MEVN.DistributorApp.V2.EntityFrameworkCore;

namespace MEVN.DistributorApp.V2.Distributors
{
    public abstract class EfCoreDistributorRepositoryBase : EfCoreRepository<V2DbContext, Distributor, Guid>
    {
        public EfCoreDistributorRepositoryBase(IDbContextProvider<V2DbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        public virtual async Task<List<Distributor>> GetListAsync(
            string? filterText = null,
            string? disCode = null,
            string? disName = null,
            string? address = null,
            string? contactInfo = null,
            string? node = null,
            string? sorting = null,
            int maxResultCount = int.MaxValue,
            int skipCount = 0,
            CancellationToken cancellationToken = default)
        {
            var query = ApplyFilter((await GetQueryableAsync()), filterText, disCode, disName, address, contactInfo, node);
            query = query.OrderBy(string.IsNullOrWhiteSpace(sorting) ? DistributorConsts.GetDefaultSorting(false) : sorting);
            return await query.PageBy(skipCount, maxResultCount).ToListAsync(cancellationToken);
        }

        public virtual async Task<long> GetCountAsync(
            string? filterText = null,
            string? disCode = null,
            string? disName = null,
            string? address = null,
            string? contactInfo = null,
            string? node = null,
            CancellationToken cancellationToken = default)
        {
            var query = ApplyFilter((await GetDbSetAsync()), filterText, disCode, disName, address, contactInfo, node);
            return await query.LongCountAsync(GetCancellationToken(cancellationToken));
        }

        protected virtual IQueryable<Distributor> ApplyFilter(
            IQueryable<Distributor> query,
            string? filterText = null,
            string? disCode = null,
            string? disName = null,
            string? address = null,
            string? contactInfo = null,
            string? node = null)
        {
            return query
                    .WhereIf(!string.IsNullOrWhiteSpace(filterText), e => e.DisCode!.Contains(filterText!) || e.DisName!.Contains(filterText!) || e.Address!.Contains(filterText!) || e.ContactInfo!.Contains(filterText!) || e.Node!.Contains(filterText!))
                    .WhereIf(!string.IsNullOrWhiteSpace(disCode), e => e.DisCode.Contains(disCode))
                    .WhereIf(!string.IsNullOrWhiteSpace(disName), e => e.DisName.Contains(disName))
                    .WhereIf(!string.IsNullOrWhiteSpace(address), e => e.Address.Contains(address))
                    .WhereIf(!string.IsNullOrWhiteSpace(contactInfo), e => e.ContactInfo.Contains(contactInfo))
                    .WhereIf(!string.IsNullOrWhiteSpace(node), e => e.Node.Contains(node));
        }
    }
}