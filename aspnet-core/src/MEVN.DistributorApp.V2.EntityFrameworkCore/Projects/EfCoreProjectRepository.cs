using MEVN.DistributorApp.V2.Customers;
using MEVN.DistributorApp.V2.Distributors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;
using MEVN.DistributorApp.V2.EntityFrameworkCore;
using MEVN.DistributorApp.V2.ProjectDetails;

namespace MEVN.DistributorApp.V2.Projects
{
    public abstract class EfCoreProjectRepositoryBase : EfCoreRepository<V2DbContext, Project, Guid>, IProjectRepository
    {
        public EfCoreProjectRepositoryBase(IDbContextProvider<V2DbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }
        protected virtual IQueryable<Project> ApplyFilter(
            IQueryable<Project> query,
            string? projectCode = null,
            string? projectName = null,
            DateTime? pODateMin = null,
            DateTime? pODateMax = null,
            Guid? distributorId = null)
        {
            return query
                    .WhereIf(!string.IsNullOrWhiteSpace(projectCode), e => e.ProjectCode.Contains(projectCode))
                    .WhereIf(!string.IsNullOrWhiteSpace(projectName), e => e.ProjectName.Contains(projectName))
                    .WhereIf(pODateMin.HasValue, e => e.PODate >= pODateMin!.Value)
                    .WhereIf(pODateMax.HasValue, e => e.PODate <= pODateMax!.Value)
                    .WhereIf(distributorId != null && distributorId != Guid.Empty, e => e.DistributorId.Equals(distributorId)); 
        }

        public virtual async Task<ProjectWithNavigationProperties> GetWithNavigationPropertiesAsync(Guid id, CancellationToken cancellationToken = default)
        {
            var dbContext = await GetDbContextAsync();

            return (await GetDbSetAsync()).Where(b => b.Id == id)
                .Select(project => new ProjectWithNavigationProperties
                {
                    Project = project,
                    Distributor = dbContext.Set<Distributor>().FirstOrDefault(c => c.Id == project.DistributorId),
                    Customer = dbContext.Set<Customer>().FirstOrDefault(c => c.Id == project.CustomerId)
                }).FirstOrDefault();
        }

        public virtual async Task<List<ProjectWithNavigationProperties>> GetListWithNavigationPropertiesAsync(
            Guid? id = null,
            string? sorting = null,
            int maxResultCount = int.MaxValue,
            int skipCount = 0,
            CancellationToken cancellationToken = default)
        {
            var query = await GetQueryForNavigationPropertiesAsync();
            query = query.Where(b => b.Project.Id == id);
            query = query.OrderBy(string.IsNullOrWhiteSpace(sorting) ? ProjectConsts.GetDefaultSorting(true) : sorting);
            return await query.PageBy(skipCount, maxResultCount).ToListAsync(cancellationToken);
        }

        protected virtual async Task<IQueryable<ProjectWithNavigationProperties>> GetQueryForNavigationPropertiesAsync()
        {
            var dbContext = await GetDbContextAsync();

            var query = from project in (await GetDbSetAsync())
                        join distributor in dbContext.Set<Distributor>() on project.DistributorId equals distributor.Id into distributors
                        from distributor in distributors.DefaultIfEmpty()
                        join customer in dbContext.Set<Customer>() on project.CustomerId equals customer.Id into customers
                        from customer in customers.DefaultIfEmpty()
                        join projectDetail in dbContext.Set<ProjectDetail>() on project.Id equals projectDetail.ProjectId into projectDetails
                        select new ProjectWithNavigationProperties
                        {
                            Project = project,
                            ProjectDetail = projectDetails.ToList(),
                            Distributor = distributor,
                            Customer = customer,
                        };

            return query;
        }
       
        protected virtual IQueryable<ProjectWithNavigationProperties> ApplyFilter(
            IQueryable<ProjectWithNavigationProperties> query,
            string? projectCode = null,
            string? projectName = null,
            DateTime? pODateMin = null,
            DateTime? pODateMax = null,
            Guid? distributorId = null)
        {
            return query
                    .WhereIf(!string.IsNullOrWhiteSpace(projectCode), e => e.Project.ProjectCode.Contains(projectCode))
                    .WhereIf(pODateMin.HasValue, e => e.Project.PODate >= pODateMin!.Value)
                    .WhereIf(pODateMax.HasValue, e => e.Project.PODate <= pODateMax!.Value)
                    .WhereIf(distributorId != null && distributorId != Guid.Empty, e => e.Distributor != null && e.Distributor.Id == distributorId);
        }

        public virtual async Task<List<Project>> GetListAsync(
            string? projectCode = null,
            string? projectName = null,         
            DateTime? pODateMin = null,
            DateTime? pODateMax = null,
            Guid? distributorId = null,
            string? sorting = null,
            int maxResultCount = int.MaxValue,
            int skipCount = 0,
            CancellationToken cancellationToken = default)
        {
            var query = ApplyFilter((await GetQueryableAsync()), projectCode, projectName, pODateMin, pODateMax, distributorId);
            query = query.Include(e => e.ListProjectDetail);
            query = query.OrderBy(string.IsNullOrWhiteSpace(sorting) ? ProjectConsts.GetDefaultSorting(false) : sorting);
            
            return await query.PageBy(skipCount, maxResultCount).ToListAsync(cancellationToken);
        }

        public virtual async Task<long> GetCountAsync(
            string? projectCode = null,
            string? projectName = null,
            DateTime? pODateMin = null,
            DateTime? pODateMax = null,
            Guid? distributorId = null,
            CancellationToken cancellationToken = default)
        {
            var query = await GetQueryForNavigationPropertiesAsync();
            query = ApplyFilter(query, projectCode, projectName, pODateMin, pODateMax, distributorId);
            return await query.LongCountAsync(GetCancellationToken(cancellationToken));
        }

        
    }
}