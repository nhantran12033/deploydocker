using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;
using MEVN.DistributorApp.V2.EntityFrameworkCore;
using MEVN.DistributorApp.V2.ProjectDetails;

namespace MEVN.DistributorApp.V2.Projects
{
    public class EfCoreProjectRepository : EfCoreProjectRepositoryBase
    {
        public EfCoreProjectRepository(IDbContextProvider<V2DbContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }

       
    }
}