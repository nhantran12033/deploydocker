using MEVN.DistributorApp.V2.Distributors;
using MEVN.DistributorApp.V2.Venders;
using MEVN.DistributorApp.V2.MaterialGroups;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;
using MEVN.DistributorApp.V2.EntityFrameworkCore;
using MEVN.DistributorApp.V2.ProjectDetails;
using MEVN.DistributorApp.V2.Projects;
using Polly;
using MEVN.DistributorApp.V2.FreeOnShipMents;

namespace MEVN.DistributorApp.V2.MaterialStockCheckings
{
    public abstract class EfCoreMaterialStockCheckingRepositoryBase : EfCoreRepository<V2DbContext, MaterialStockChecking, Guid>, IMaterialStockCheckingRepository
    {
        public EfCoreMaterialStockCheckingRepositoryBase(IDbContextProvider<V2DbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        public virtual async Task<MaterialStockCheckingWithNavigationProperties> GetWithNavigationPropertiesAsync(Guid id, CancellationToken cancellationToken = default)
        {
            var dbContext = await GetDbContextAsync();

            return (await GetDbSetAsync()).Where(b => b.Id == id)
                .Select(materialStockChecking => new MaterialStockCheckingWithNavigationProperties
                {
                    MaterialStockChecking = materialStockChecking,
                    MaterialGroup = dbContext.Set<MaterialGroup>().FirstOrDefault(c => c.Id == materialStockChecking.MaterialGroupId),
                    Vender = dbContext.Set<Vender>().FirstOrDefault(c => c.Id == materialStockChecking.VenderId),
                    Distributor = dbContext.Set<Distributor>().FirstOrDefault(c => c.Id == materialStockChecking.DistributorId)
                }).FirstOrDefault();
        }

        public virtual async Task<List<MaterialStockCheckingWithNavigationProperties>> GetListWithNavigationPropertiesAsync(
              string? golfaCode = null,
            string? model = null,
            Guid? materialGroupId = null,
            Guid? venderId = null,
            Guid? distributorId = null,
            string? sorting = null,
            int maxResultCount = int.MaxValue,
            int skipCount = 0,
            CancellationToken cancellationToken = default)
        {
            var query = await GetQueryForNavigationPropertiesAsync();
            query = ApplyFilter(query, golfaCode, model, materialGroupId, venderId, distributorId);
            query = query.OrderBy(string.IsNullOrWhiteSpace(sorting) ? MaterialStockCheckingConsts.GetDefaultSorting(true) : sorting);
            return await query.PageBy(skipCount, maxResultCount).ToListAsync(cancellationToken);
        }

        protected virtual async Task<IQueryable<MaterialStockCheckingWithNavigationProperties>> GetQueryForNavigationPropertiesAsync()
        {
            var dbContext = await GetDbContextAsync();

            return from materialStockChecking in (await GetDbSetAsync())
                   join materialGroup in dbContext.Set<MaterialGroup>() on materialStockChecking.MaterialGroupId equals materialGroup.Id into materialGroups
                   from materialGroup in materialGroups.DefaultIfEmpty()
                   join vender in dbContext.Set<Vender>() on materialStockChecking.VenderId equals vender.Id into venders
                   from vender in venders.DefaultIfEmpty()
                   join distributor in dbContext.Set<Distributor>() on materialStockChecking.DistributorId equals distributor.Id into distributors
                   from distributor in distributors.DefaultIfEmpty()
                   join freeOnShipMent in dbContext.Set<FreeOnShipMent>() on materialStockChecking.Id equals freeOnShipMent.MaterialID into freeOnShipMents
                   select new MaterialStockCheckingWithNavigationProperties
                   {
                       MaterialStockChecking = materialStockChecking,
                       MaterialGroup = materialGroup,
                       Vender = vender,
                       Distributor = distributor,
                       FreeOnShipMent = freeOnShipMents.ToList()
                   };
        }


        protected virtual IQueryable<MaterialStockCheckingWithNavigationProperties> ApplyFilter(
            IQueryable<MaterialStockCheckingWithNavigationProperties> query,
           
            string? golfaCode = null,
            string? model = null,
            Guid? materialGroupId = null,
            Guid? venderId = null,
            Guid? distributorId = null)
        {
            return query
                .WhereIf(!string.IsNullOrWhiteSpace(golfaCode), e => e.MaterialStockChecking.GolfaCode.Contains(golfaCode))
                    .WhereIf(!string.IsNullOrWhiteSpace(model), e => e.MaterialStockChecking.Model.Contains(model))
                    .WhereIf(materialGroupId != null && materialGroupId != Guid.Empty, e => e.MaterialGroup != null && e.MaterialGroup.Id == materialGroupId)
                    .WhereIf(venderId != null && venderId != Guid.Empty, e => e.Vender != null && e.Vender.Id == venderId)
                    .WhereIf(distributorId != null && distributorId != Guid.Empty, e => e.Distributor != null && e.Distributor.Id == distributorId);
        }

        public virtual async Task<List<MaterialStockChecking>> GetListAsync(
            string? golfaCode = null,
            string? model = null,
            Guid? venderId = null,
            Guid? materialGroupId = null,
            Guid? distributorId = null,
            string? sorting = null,
            int maxResultCount = int.MaxValue,
            int skipCount = 0,
            CancellationToken cancellationToken = default)
        {
            var query = ApplyFilter((await GetQueryableAsync()), golfaCode, model, venderId, materialGroupId, distributorId);
            query = query.Include(e => e.ListFreeOnShipMent);
            query = query.OrderBy(string.IsNullOrWhiteSpace(sorting) ? MaterialStockCheckingConsts.GetDefaultSorting(false) : sorting);
            return await query.PageBy(skipCount, maxResultCount).ToListAsync(cancellationToken);
        }

        public virtual async Task<long> GetCountAsync(
            string? golfaCode = null,
            string? model = null,
            Guid? materialGroupId = null,
            Guid? venderId = null,
            Guid? distributorId = null,
            CancellationToken cancellationToken = default)
        {
            var query = await GetQueryForNavigationPropertiesAsync();
            query = ApplyFilter(query,  golfaCode, model,materialGroupId, venderId, distributorId);
            return await query.LongCountAsync(GetCancellationToken(cancellationToken));
        }

        protected virtual IQueryable<MaterialStockChecking> ApplyFilter(
            IQueryable<MaterialStockChecking> query,

            string? golfaCode = null,
            string? model = null,
            Guid? venderId = null,
            Guid? materialGroupId = null,
            Guid? distributorId = null

           )
        {
            return query
                    .WhereIf(!string.IsNullOrWhiteSpace(golfaCode), e => e.GolfaCode.Contains(golfaCode))
                    .WhereIf(!string.IsNullOrWhiteSpace(model), e => e.Model.Contains(model))
                    .WhereIf(materialGroupId != null && materialGroupId != Guid.Empty, e => e.MaterialGroupId != null && e.MaterialGroupId == materialGroupId)
                    .WhereIf(venderId != null && venderId != Guid.Empty, e => e.VenderId != null && e.VenderId == venderId)
                    .WhereIf(distributorId != null && distributorId != Guid.Empty, e => e.DistributorId != null && e.DistributorId == distributorId);
        }
    }
}