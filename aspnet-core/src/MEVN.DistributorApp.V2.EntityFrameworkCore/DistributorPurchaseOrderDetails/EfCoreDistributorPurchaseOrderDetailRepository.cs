using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;
using MEVN.DistributorApp.V2.EntityFrameworkCore;
using MEVN.DistributorApp.V2.listDistributorPurchaseOrderDetails;

namespace MEVN.DistributorApp.V2.DistributorPurchaseOrderDetails
{
    public abstract class EfCoreDistributorPurchaseOrderDetailRepositoryBase : EfCoreRepository<V2DbContext, DistributorPurchaseOrderDetail, Guid>
    {
        public EfCoreDistributorPurchaseOrderDetailRepositoryBase(IDbContextProvider<V2DbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        public virtual async Task<List<DistributorPurchaseOrderDetail>> GetListAsync(
            string? filterText = null,
            byte? statusMin = null,
            byte? statusMax = null,
            string? golfaCode = null,
            string? customer = null,
            string? model = null,
            int? qtyMin = null,
            int? qtyMax = null,
            float? priceMin = null,
            float? priceMax = null,
            float? amountMin = null,
            float? amountMax = null,
            DateTime? requestedETAMin = null,
            DateTime? requestedETAMax = null,
            int? deliveredMin = null,
            int? deliveredMax = null,
            int? inProgressMin = null,
            int? inProgressMax = null,
            string? note = null,
            byte? eTAMin = null,
            byte? eTAMax = null,
            string? customerName = null,
            string? statusCode = null,
            Guid? distributorPurchaseOrderId = null,
            string? sorting = null,
            int maxResultCount = int.MaxValue,
            int skipCount = 0,
            CancellationToken cancellationToken = default)
        {
            var query = ApplyFilter((await GetQueryableAsync()), filterText, statusMin, statusMax, golfaCode, customer, model, qtyMin, qtyMax, priceMin, priceMax, amountMin, amountMax, requestedETAMin, requestedETAMax, deliveredMin, deliveredMax, inProgressMin, inProgressMax, note, eTAMin, eTAMax, customerName, statusCode, distributorPurchaseOrderId);
            query = query.OrderBy(string.IsNullOrWhiteSpace(sorting) ? DistributorPurchaseOrderDetailConsts.GetDefaultSorting(false) : sorting);
            return await query.PageBy(skipCount, maxResultCount).ToListAsync(cancellationToken);
        }

        public virtual async Task<long> GetCountAsync(
            string? filterText = null,
            byte? statusMin = null,
            byte? statusMax = null,
            string? golfaCode = null,
            string? customer = null,
            string? model = null,
            int? qtyMin = null,
            int? qtyMax = null,
            float? priceMin = null,
            float? priceMax = null,
            float? amountMin = null,
            float? amountMax = null,
            DateTime? requestedETAMin = null,
            DateTime? requestedETAMax = null,
            int? deliveredMin = null,
            int? deliveredMax = null,
            int? inProgressMin = null,
            int? inProgressMax = null,
            string? note = null,
            byte? eTAMin = null,
            byte? eTAMax = null,
            string? customerName = null,
            string? statusCode = null,
            Guid? distributorPurchaseOrderId = null,
            CancellationToken cancellationToken = default)
        {
            var query = ApplyFilter((await GetDbSetAsync()), filterText, statusMin, statusMax, golfaCode, customer, model, qtyMin, qtyMax, priceMin, priceMax, amountMin, amountMax, requestedETAMin, requestedETAMax, deliveredMin, deliveredMax, inProgressMin, inProgressMax, note, eTAMin, eTAMax, customerName, statusCode, distributorPurchaseOrderId);
            return await query.LongCountAsync(GetCancellationToken(cancellationToken));
        }

        protected virtual IQueryable<DistributorPurchaseOrderDetail> ApplyFilter(
            IQueryable<DistributorPurchaseOrderDetail> query,
            string? filterText = null,
            byte? statusMin = null,
            byte? statusMax = null,
            string? golfaCode = null,
            string? customer = null,
            string? model = null,
            int? qtyMin = null,
            int? qtyMax = null,
            float? priceMin = null,
            float? priceMax = null,
            float? amountMin = null,
            float? amountMax = null,
            DateTime? requestedETAMin = null,
            DateTime? requestedETAMax = null,
            int? deliveredMin = null,
            int? deliveredMax = null,
            int? inProgressMin = null,
            int? inProgressMax = null,
            string? note = null,
            byte? eTAMin = null,
            byte? eTAMax = null,
            string? customerName = null,
            string? statusCode = null,
            Guid? distributorPurchaseOrderId = null)
        {
            return query
                    .WhereIf(!string.IsNullOrWhiteSpace(filterText), e => e.GolfaCode!.Contains(filterText!) || e.Customer!.Contains(filterText!) || e.Model!.Contains(filterText!) || e.Note!.Contains(filterText!) || e.CustomerName!.Contains(filterText!) || e.StatusCode!.Contains(filterText!))

                    .WhereIf(!string.IsNullOrWhiteSpace(golfaCode), e => e.GolfaCode.Contains(golfaCode))
                    .WhereIf(!string.IsNullOrWhiteSpace(customer), e => e.Customer.Contains(customer))
                    .WhereIf(!string.IsNullOrWhiteSpace(model), e => e.Model.Contains(model))
                    .WhereIf(qtyMin.HasValue, e => e.Qty >= qtyMin!.Value)
                    .WhereIf(qtyMax.HasValue, e => e.Qty <= qtyMax!.Value)
                    .WhereIf(priceMin.HasValue, e => e.Price >= priceMin!.Value)
                    .WhereIf(priceMax.HasValue, e => e.Price <= priceMax!.Value)
                    .WhereIf(amountMin.HasValue, e => e.Amount >= amountMin!.Value)
                    .WhereIf(amountMax.HasValue, e => e.Amount <= amountMax!.Value)
                    .WhereIf(requestedETAMin.HasValue, e => e.RequestedETA >= requestedETAMin!.Value)
                    .WhereIf(requestedETAMax.HasValue, e => e.RequestedETA <= requestedETAMax!.Value)
                    .WhereIf(deliveredMin.HasValue, e => e.Delivered >= deliveredMin!.Value)
                    .WhereIf(deliveredMax.HasValue, e => e.Delivered <= deliveredMax!.Value)
                    .WhereIf(inProgressMin.HasValue, e => e.InProgress >= inProgressMin!.Value)
                    .WhereIf(inProgressMax.HasValue, e => e.InProgress <= inProgressMax!.Value)
                    .WhereIf(!string.IsNullOrWhiteSpace(note), e => e.Note.Contains(note))

                    .WhereIf(!string.IsNullOrWhiteSpace(customerName), e => e.CustomerName.Contains(customerName))
                    .WhereIf(!string.IsNullOrWhiteSpace(statusCode), e => e.StatusCode.Contains(statusCode))
                    .WhereIf(distributorPurchaseOrderId.HasValue, e => e.DistributorPurchaseOrderId == distributorPurchaseOrderId);
        }
    }
}