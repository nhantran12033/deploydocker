using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;
using MEVN.DistributorApp.V2.EntityFrameworkCore;

namespace MEVN.DistributorApp.V2.DeliveryOrders
{
    public abstract class EfCoreDeliveryOrderRepositoryBase : EfCoreRepository<V2DbContext, DeliveryOrder, Guid>
    {
        public EfCoreDeliveryOrderRepositoryBase(IDbContextProvider<V2DbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        public virtual async Task<List<DeliveryOrder>> GetListAsync(
            string? filterText = null,
            byte? statusMin = null,
            byte? statusMax = null,
            DateTime? deliveryDateMin = null,
            DateTime? deliveryDateMax = null,
            string? deliveryCode = null,
            string? dOSAPNo = null,
            DateTime? invoiceDateMin = null,
            DateTime? invoiceDateMax = null,
            string? invoiceNo = null,
            string? billingNo = null,
            string? note = null,
            string? statusCode = null,
            int? qtyMin = null,
            int? qtyMax = null,
            Guid? distributorOrderDetailId = null,
            string? sorting = null,
            int maxResultCount = int.MaxValue,
            int skipCount = 0,
            CancellationToken cancellationToken = default)
        {
            var query = ApplyFilter((await GetQueryableAsync()), filterText, statusMin, statusMax, deliveryDateMin, deliveryDateMax, deliveryCode, dOSAPNo, invoiceDateMin, invoiceDateMax, invoiceNo, billingNo, note, statusCode, qtyMin, qtyMax, distributorOrderDetailId);
            query = query.OrderBy(string.IsNullOrWhiteSpace(sorting) ? DeliveryOrderConsts.GetDefaultSorting(false) : sorting);
            return await query.PageBy(skipCount, maxResultCount).ToListAsync(cancellationToken);
        }

        public virtual async Task<long> GetCountAsync(
            string? filterText = null,
            byte? statusMin = null,
            byte? statusMax = null,
            DateTime? deliveryDateMin = null,
            DateTime? deliveryDateMax = null,
            string? deliveryCode = null,
            string? dOSAPNo = null,
            DateTime? invoiceDateMin = null,
            DateTime? invoiceDateMax = null,
            string? invoiceNo = null,
            string? billingNo = null,
            string? note = null,
            string? statusCode = null,
            int? qtyMin = null,
            int? qtyMax = null,
            Guid? distributorOrderDetailId = null,
            CancellationToken cancellationToken = default)
        {
            var query = ApplyFilter((await GetDbSetAsync()), filterText, statusMin, statusMax, deliveryDateMin, deliveryDateMax, deliveryCode, dOSAPNo, invoiceDateMin, invoiceDateMax, invoiceNo, billingNo, note, statusCode, qtyMin, qtyMax, distributorOrderDetailId);
            return await query.LongCountAsync(GetCancellationToken(cancellationToken));
        }

        protected virtual IQueryable<DeliveryOrder> ApplyFilter(
            IQueryable<DeliveryOrder> query,
            string? filterText = null,
            byte? statusMin = null,
            byte? statusMax = null,
            DateTime? deliveryDateMin = null,
            DateTime? deliveryDateMax = null,
            string? deliveryCode = null,
            string? dOSAPNo = null,
            DateTime? invoiceDateMin = null,
            DateTime? invoiceDateMax = null,
            string? invoiceNo = null,
            string? billingNo = null,
            string? note = null,
            string? statusCode = null,
            int? qtyMin = null,
            int? qtyMax = null,
            Guid? distributorOrderDetailId = null)
        {
            return query
                    .WhereIf(!string.IsNullOrWhiteSpace(filterText), e => e.DeliveryCode!.Contains(filterText!) || e.DOSAPNo!.Contains(filterText!) || e.InvoiceNo!.Contains(filterText!) || e.BillingNo!.Contains(filterText!) || e.Note!.Contains(filterText!) || e.StatusCode!.Contains(filterText!))

                    .WhereIf(deliveryDateMin.HasValue, e => e.DeliveryDate >= deliveryDateMin!.Value)
                    .WhereIf(deliveryDateMax.HasValue, e => e.DeliveryDate <= deliveryDateMax!.Value)
                    .WhereIf(!string.IsNullOrWhiteSpace(deliveryCode), e => e.DeliveryCode.Contains(deliveryCode))
                    .WhereIf(!string.IsNullOrWhiteSpace(dOSAPNo), e => e.DOSAPNo.Contains(dOSAPNo))
                    .WhereIf(invoiceDateMin.HasValue, e => e.InvoiceDate >= invoiceDateMin!.Value)
                    .WhereIf(invoiceDateMax.HasValue, e => e.InvoiceDate <= invoiceDateMax!.Value)
                    .WhereIf(!string.IsNullOrWhiteSpace(invoiceNo), e => e.InvoiceNo.Contains(invoiceNo))
                    .WhereIf(!string.IsNullOrWhiteSpace(billingNo), e => e.BillingNo.Contains(billingNo))
                    .WhereIf(!string.IsNullOrWhiteSpace(note), e => e.Note.Contains(note))
                    .WhereIf(!string.IsNullOrWhiteSpace(statusCode), e => e.StatusCode.Contains(statusCode))
                    .WhereIf(qtyMin.HasValue, e => e.Qty >= qtyMin!.Value)
                    .WhereIf(qtyMax.HasValue, e => e.Qty <= qtyMax!.Value)
                    .WhereIf(distributorOrderDetailId.HasValue, e => e.DistributorOrderDetailId == distributorOrderDetailId);
        }
    }
}