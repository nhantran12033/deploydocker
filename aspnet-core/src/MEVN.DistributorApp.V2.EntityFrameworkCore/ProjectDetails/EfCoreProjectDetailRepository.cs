using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;
using MEVN.DistributorApp.V2.EntityFrameworkCore;

namespace MEVN.DistributorApp.V2.ProjectDetails
{
    public class EfCoreProjectDetailRepository : EfCoreRepository<V2DbContext, ProjectDetail, Guid>, IProjectDetailRepository
    {
        public EfCoreProjectDetailRepository(IDbContextProvider<V2DbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        public virtual async Task<List<ProjectDetail>> GetListAsync(
            string? filterText = null,
            Guid? projectId = null,
            string? golfaCode = null,
            string? model = null,
            int? qtyMin = null,
            int? qtyMax = null,
            int? dpoUsedMin = null,
            int? dpoUsedMax = null,
            string? requestStatus = null,
            float? requestPriceMin = null,
            float? requestPriceMax = null,
            float? distRequestedPriceMin = null,
            float? distRequestedPriceMax = null,
            float? saleOfferPriceMin = null,
            float? saleOfferPriceMax = null,
            float? saleAllowDiscountPriceMin = null,
            float? saleAllowDiscountPriceMax = null,
            float? amountRequestedPriceMin = null,
            float? amountRequestedPriceMax = null,
            string? statusCode = null,
            string? sorting = null,
            int maxResultCount = int.MaxValue,
            int skipCount = 0,
            CancellationToken cancellationToken = default)
        {
            var query = ApplyFilter((await GetQueryableAsync()), filterText, projectId, golfaCode, model, qtyMin, qtyMax, dpoUsedMin, dpoUsedMax, requestStatus, requestPriceMin, requestPriceMax, distRequestedPriceMin, distRequestedPriceMax, saleOfferPriceMin, saleOfferPriceMax, saleAllowDiscountPriceMin, saleAllowDiscountPriceMax, amountRequestedPriceMin, amountRequestedPriceMax, statusCode);
            query = query.OrderBy(string.IsNullOrWhiteSpace(sorting) ? ProjectDetailConsts.GetDefaultSorting(false) : sorting);
            return await query.PageBy(skipCount, maxResultCount).ToListAsync(cancellationToken);
        }

        public virtual async Task<long> GetCountAsync(
            string? filterText = null,
            Guid? projectId = null,
            string? golfaCode = null,
            string? model = null,
            int? qtyMin = null,
            int? qtyMax = null,
            int? dpoUsedMin = null,
            int? dpoUsedMax = null,
            string? requestStatus = null,
            float? requestPriceMin = null,
            float? requestPriceMax = null,
            float? distRequestedPriceMin = null,
            float? distRequestedPriceMax = null,
            float? saleOfferPriceMin = null,
            float? saleOfferPriceMax = null,
            float? saleAllowDiscountPriceMin = null,
            float? saleAllowDiscountPriceMax = null,
            float? amountRequestedPriceMin = null,
            float? amountRequestedPriceMax = null,
            string? statusCode = null,
            CancellationToken cancellationToken = default)
        {
            var query = ApplyFilter((await GetDbSetAsync()), filterText, projectId, golfaCode, model, qtyMin, qtyMax, dpoUsedMin, dpoUsedMax, requestStatus, requestPriceMin, requestPriceMax, distRequestedPriceMin, distRequestedPriceMax, saleOfferPriceMin, saleOfferPriceMax, saleAllowDiscountPriceMin, saleAllowDiscountPriceMax, amountRequestedPriceMin, amountRequestedPriceMax, statusCode);
            return await query.LongCountAsync(GetCancellationToken(cancellationToken));
        }

        protected virtual IQueryable<ProjectDetail> ApplyFilter(
            IQueryable<ProjectDetail> query,
            string? filterText = null,
            Guid? projectId = null,
            string? golfaCode = null,
            string? model = null,
            int? qtyMin = null,
            int? qtyMax = null,
            int? dpoUsedMin = null,
            int? dpoUsedMax = null,
            string? requestStatus = null,
            float? requestPriceMin = null,
            float? requestPriceMax = null,
            float? distRequestedPriceMin = null,
            float? distRequestedPriceMax = null,
            float? saleOfferPriceMin = null,
            float? saleOfferPriceMax = null,
            float? saleAllowDiscountPriceMin = null,
            float? saleAllowDiscountPriceMax = null,
            float? amountRequestedPriceMin = null,
            float? amountRequestedPriceMax = null,
            string? statusCode = null)
        {
            return query
                    .WhereIf(!string.IsNullOrWhiteSpace(filterText), e => e.GolfaCode!.Contains(filterText!) || e.Model!.Contains(filterText!) || e.RequestStatus!.Contains(filterText!) || e.StatusCode!.Contains(filterText!))
                    .WhereIf(projectId.HasValue, e => e.ProjectId == projectId)
                    .WhereIf(!string.IsNullOrWhiteSpace(golfaCode), e => e.GolfaCode.Contains(golfaCode))
                    .WhereIf(!string.IsNullOrWhiteSpace(model), e => e.Model.Contains(model))
                    .WhereIf(qtyMin.HasValue, e => e.Qty >= qtyMin!.Value)
                    .WhereIf(qtyMax.HasValue, e => e.Qty <= qtyMax!.Value)
                    .WhereIf(dpoUsedMin.HasValue, e => e.DpoUsed >= dpoUsedMin!.Value)
                    .WhereIf(dpoUsedMax.HasValue, e => e.DpoUsed <= dpoUsedMax!.Value)
                    .WhereIf(!string.IsNullOrWhiteSpace(requestStatus), e => e.RequestStatus.Contains(requestStatus))
                    .WhereIf(requestPriceMin.HasValue, e => e.RequestPrice >= requestPriceMin!.Value)
                    .WhereIf(requestPriceMax.HasValue, e => e.RequestPrice <= requestPriceMax!.Value)
                    .WhereIf(distRequestedPriceMin.HasValue, e => e.DistRequestedPrice >= distRequestedPriceMin!.Value)
                    .WhereIf(distRequestedPriceMax.HasValue, e => e.DistRequestedPrice <= distRequestedPriceMax!.Value)
                    .WhereIf(saleOfferPriceMin.HasValue, e => e.SaleOfferPrice >= saleOfferPriceMin!.Value)
                    .WhereIf(saleOfferPriceMax.HasValue, e => e.SaleOfferPrice <= saleOfferPriceMax!.Value)
                    .WhereIf(saleAllowDiscountPriceMin.HasValue, e => e.SaleAllowDiscountPrice >= saleAllowDiscountPriceMin!.Value)
                    .WhereIf(saleAllowDiscountPriceMax.HasValue, e => e.SaleAllowDiscountPrice <= saleAllowDiscountPriceMax!.Value)
                    .WhereIf(amountRequestedPriceMin.HasValue, e => e.AmountRequestedPrice >= amountRequestedPriceMin!.Value)
                    .WhereIf(amountRequestedPriceMax.HasValue, e => e.AmountRequestedPrice <= amountRequestedPriceMax!.Value)
                    .WhereIf(!string.IsNullOrWhiteSpace(statusCode), e => e.StatusCode.Contains(statusCode));
        }
    }
}