using MEVN.DistributorApp.V2.Distributors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;
using MEVN.DistributorApp.V2.EntityFrameworkCore;
using MEVN.DistributorApp.V2.listDistributorPurchaseOrderDetails;


namespace MEVN.DistributorApp.V2.DistributorPurchaseOrders
{
    public abstract class EfCoreDistributorPurchaseOrderRepositoryBase : EfCoreRepository<V2DbContext, DistributorPurchaseOrder, Guid>, IDistributorPurchaseOrderRepository
    {
        public EfCoreDistributorPurchaseOrderRepositoryBase(IDbContextProvider<V2DbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        public virtual async Task<DistributorPurchaseOrderWithNavigationProperties> GetWithNavigationPropertiesAsync(Guid id, CancellationToken cancellationToken = default)
        {
            var dbContext = await GetDbContextAsync();

            return (await GetDbSetAsync()).Where(b => b.Id == id)
                .Select(distributorPurchaseOrder => new DistributorPurchaseOrderWithNavigationProperties
                {
                    DistributorPurchaseOrder = distributorPurchaseOrder,
                    Distributor = dbContext.Set<Distributor>().FirstOrDefault(c => c.Id == distributorPurchaseOrder.DistributorId)
                }).FirstOrDefault();
        }

        public virtual async Task<List<DistributorPurchaseOrderWithNavigationProperties>> GetListWithNavigationPropertiesAsync(
           string? dPONo = null,
            DateTime? orderDate = null,
            string? fA_LVS = null,
            string? statusCode = null,
            Guid? distributorId = null,
            string? sorting = null,
            int maxResultCount = int.MaxValue,
            int skipCount = 0,
            CancellationToken cancellationToken = default)
        {
            var query = await GetQueryForNavigationPropertiesAsync();
            query = ApplyFilter(query, dPONo, orderDate,  fA_LVS,  statusCode, distributorId);
            query = query.OrderBy(string.IsNullOrWhiteSpace(sorting) ? DistributorPurchaseOrderConsts.GetDefaultSorting(true) : sorting);
            return await query.PageBy(skipCount, maxResultCount).ToListAsync(cancellationToken);
        }

        protected virtual async Task<IQueryable<DistributorPurchaseOrderWithNavigationProperties>> GetQueryForNavigationPropertiesAsync()
        {
            return from distributorPurchaseOrder in (await GetDbSetAsync())
                   join distributor in (await GetDbContextAsync()).Set<Distributor>() on distributorPurchaseOrder.DistributorId equals distributor.Id into distributors
                   from distributor in distributors.DefaultIfEmpty()
                   join distributorPurchaseOrderDetail in (await GetDbContextAsync()).Set<DistributorPurchaseOrderDetail>() on distributorPurchaseOrder.Id equals distributorPurchaseOrderDetail.DistributorPurchaseOrderId into distributorPurchaseOrderDetails
                   select new DistributorPurchaseOrderWithNavigationProperties
                   {
                       DistributorPurchaseOrder = distributorPurchaseOrder,
                       Distributor = distributor
                   };
        }

        protected virtual IQueryable<DistributorPurchaseOrderWithNavigationProperties> ApplyFilter(
            IQueryable<DistributorPurchaseOrderWithNavigationProperties> query,
            string? dPONo = null,
            DateTime? orderDate = null,
            string? fA_LVS = null,
            string? statusCode = null,
            Guid? distributorId = null)
        {
            return query
                .WhereIf(!string.IsNullOrWhiteSpace(dPONo), e => e.DistributorPurchaseOrder.DPONo.Contains(dPONo))

                    .WhereIf(orderDate.HasValue, e => e.DistributorPurchaseOrder.OrderDate == orderDate!.Value)

                    .WhereIf(!string.IsNullOrWhiteSpace(fA_LVS), e => e.DistributorPurchaseOrder.FA_LVS.Contains(fA_LVS))

                    .WhereIf(!string.IsNullOrWhiteSpace(statusCode), e => e.DistributorPurchaseOrder.StatusCode.Contains(statusCode))
                    .WhereIf(distributorId != null && distributorId != Guid.Empty, e => e.Distributor.Id.Equals(distributorId));

        }

        public virtual async Task<List<DistributorPurchaseOrder>> GetListAsync(
            string? dPONo = null,
            DateTime? orderDate = null,
            string? fA_LVS = null,
            string? statusCode = null,
            Guid? distributorId = null,
            string? sorting = null,
            int maxResultCount = int.MaxValue,
            int skipCount = 0,
            CancellationToken cancellationToken = default)
        {
            var query = ApplyFilter((await GetQueryableAsync()), dPONo,  orderDate,fA_LVS,  statusCode, distributorId);
            query = query.Include(x => x.ListDistributorPurchaseOrderDetail)
                  .ThenInclude(detail => detail.ListETADetail)
                  .Include(x => x.ListDistributorPurchaseOrderDetail)
                  .ThenInclude(detail => detail.ListDeliveryOrder);
            query = query.OrderBy(string.IsNullOrWhiteSpace(sorting) ? DistributorPurchaseOrderConsts.GetDefaultSorting(false) : sorting);
            return await query.PageBy(skipCount, maxResultCount).ToListAsync(cancellationToken);
        }

        public virtual async Task<long> GetCountAsync(
             string? dPONo = null,
            DateTime? orderDate = null,
            string? fA_LVS = null,
            string? statusCode = null,
            Guid? distributorId = null,
            string? sorting = null,
            CancellationToken cancellationToken = default)
        {
            var query = await GetQueryForNavigationPropertiesAsync();
            query = ApplyFilter(query, dPONo,orderDate, fA_LVS,  statusCode, distributorId);
            return await query.LongCountAsync(GetCancellationToken(cancellationToken));
        }

        protected virtual IQueryable<DistributorPurchaseOrder> ApplyFilter(
            IQueryable<DistributorPurchaseOrder> query,
            string? dPONo = null,
            DateTime? orderDate = null,
            string? fA_LVS = null,
            string? statusCode = null,
            Guid? distributorId = null)
        {
            return query
                    .WhereIf(!string.IsNullOrWhiteSpace(dPONo), e => e.DPONo.Contains(dPONo))

                    .WhereIf(orderDate.HasValue, e => (e.OrderDate.Value.Year == orderDate!.Value.Year) && (e.OrderDate.Value.Month == orderDate!.Value.Month))

                    .WhereIf(!string.IsNullOrWhiteSpace(fA_LVS), e => e.FA_LVS.Contains(fA_LVS))

                    .WhereIf(!string.IsNullOrWhiteSpace(statusCode), e => e.StatusCode.Contains(statusCode))
                    .WhereIf(distributorId.HasValue && distributorId != Guid.Empty, e => e.DistributorId == distributorId);

        }
    }
}