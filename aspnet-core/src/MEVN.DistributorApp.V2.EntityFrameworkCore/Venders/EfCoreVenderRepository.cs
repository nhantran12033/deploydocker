using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;
using MEVN.DistributorApp.V2.EntityFrameworkCore;

namespace MEVN.DistributorApp.V2.Venders
{
    public abstract class EfCoreVenderRepositoryBase : EfCoreRepository<V2DbContext, Vender, Guid>
    {
        public EfCoreVenderRepositoryBase(IDbContextProvider<V2DbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        public virtual async Task<List<Vender>> GetListAsync(
            string? filterText = null,
            string? verderName = null,
            string? address = null,
            string? email = null,
            string? sorting = null,
            int maxResultCount = int.MaxValue,
            int skipCount = 0,
            CancellationToken cancellationToken = default)
        {
            var query = ApplyFilter((await GetQueryableAsync()), filterText, verderName, address, email);
            query = query.OrderBy(string.IsNullOrWhiteSpace(sorting) ? VenderConsts.GetDefaultSorting(false) : sorting);
            return await query.PageBy(skipCount, maxResultCount).ToListAsync(cancellationToken);
        }

        public virtual async Task<long> GetCountAsync(
            string? filterText = null,
            string? verderName = null,
            string? address = null,
            string? email = null,
            CancellationToken cancellationToken = default)
        {
            var query = ApplyFilter((await GetDbSetAsync()), filterText, verderName, address, email);
            return await query.LongCountAsync(GetCancellationToken(cancellationToken));
        }

        protected virtual IQueryable<Vender> ApplyFilter(
            IQueryable<Vender> query,
            string? filterText = null,
            string? verderName = null,
            string? address = null,
            string? email = null)
        {
            return query
                    .WhereIf(!string.IsNullOrWhiteSpace(filterText), e => e.VerderName!.Contains(filterText!) || e.Address!.Contains(filterText!) || e.Email!.Contains(filterText!))
                    .WhereIf(!string.IsNullOrWhiteSpace(verderName), e => e.VerderName.Contains(verderName))
                    .WhereIf(!string.IsNullOrWhiteSpace(address), e => e.Address.Contains(address))
                    .WhereIf(!string.IsNullOrWhiteSpace(email), e => e.Email.Contains(email));
        }
    }
}