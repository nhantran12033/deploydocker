using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;
using MEVN.DistributorApp.V2.EntityFrameworkCore;

namespace MEVN.DistributorApp.V2.FreeOnShipMents
{
    public class EfCoreFreeOnShipMentRepository : EfCoreRepository<V2DbContext, FreeOnShipMent, Guid>, IFreeOnShipMentRepository
    {
        public EfCoreFreeOnShipMentRepository(IDbContextProvider<V2DbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        public virtual async Task<List<FreeOnShipMent>> GetListAsync(
            string? filterText = null,
            Guid? materialID = null,
            string? pONo = null,
            DateTime? pODateMin = null,
            DateTime? pODateMax = null,
            int? qtyMin = null,
            int? qtyMax = null,
            string? machineNumber = null,
            string? sorting = null,
            int maxResultCount = int.MaxValue,
            int skipCount = 0,
            CancellationToken cancellationToken = default)
        {
            var query = ApplyFilter((await GetQueryableAsync()), filterText, materialID, pONo, pODateMin, pODateMax, qtyMin, qtyMax, machineNumber);
            query = query.OrderBy(string.IsNullOrWhiteSpace(sorting) ? FreeOnShipMentConsts.GetDefaultSorting(false) : sorting);
            return await query.PageBy(skipCount, maxResultCount).ToListAsync(cancellationToken);
        }

        public virtual async Task<long> GetCountAsync(
            string? filterText = null,
            Guid? materialID = null,
            string? pONo = null,
            DateTime? pODateMin = null,
            DateTime? pODateMax = null,
            int? qtyMin = null,
            int? qtyMax = null,
            string? machineNumber = null,
            CancellationToken cancellationToken = default)
        {
            var query = ApplyFilter((await GetDbSetAsync()), filterText, materialID, pONo, pODateMin, pODateMax, qtyMin, qtyMax, machineNumber);
            return await query.LongCountAsync(GetCancellationToken(cancellationToken));
        }

        protected virtual IQueryable<FreeOnShipMent> ApplyFilter(
            IQueryable<FreeOnShipMent> query,
            string? filterText = null,
            Guid? materialID = null,
            string? pONo = null,
            DateTime? pODateMin = null,
            DateTime? pODateMax = null,
            int? qtyMin = null,
            int? qtyMax = null,
            string? machineNumber = null)
        {
            return query
                    .WhereIf(!string.IsNullOrWhiteSpace(filterText), e => e.PONo!.Contains(filterText!) || e.MachineNumber!.Contains(filterText!))
                    .WhereIf(materialID.HasValue, e => e.MaterialID == materialID)
                    .WhereIf(!string.IsNullOrWhiteSpace(pONo), e => e.PONo.Contains(pONo))
                    .WhereIf(pODateMin.HasValue, e => e.PODate >= pODateMin!.Value)
                    .WhereIf(pODateMax.HasValue, e => e.PODate <= pODateMax!.Value)
                    .WhereIf(qtyMin.HasValue, e => e.Qty >= qtyMin!.Value)
                    .WhereIf(qtyMax.HasValue, e => e.Qty <= qtyMax!.Value)
                    .WhereIf(!string.IsNullOrWhiteSpace(machineNumber), e => e.MachineNumber.Contains(machineNumber));
        }
    }
}