using MEVN.DistributorApp.V2.DistributorPurchaseOrderDetails;
using MEVN.DistributorApp.V2.ETADetails;
using MEVN.DistributorApp.V2.MaterialStockCheckings;
using MEVN.DistributorApp.V2.MaterialGroups;
using MEVN.DistributorApp.V2.Venders;
using MEVN.DistributorApp.V2.FreeOnShipMents;
using MEVN.DistributorApp.V2.DeliveryOrders;
using MEVN.DistributorApp.V2.listDistributorPurchaseOrderDetails;
using MEVN.DistributorApp.V2.DistributorPurchaseOrders;
using MEVN.DistributorApp.V2.ProjectDetails;

using MEVN.DistributorApp.V2.Projects;
using MEVN.DistributorApp.V2.Customers;
using MEVN.DistributorApp.V2.Distributors;
using Volo.Abp.EntityFrameworkCore.Modeling;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.AuditLogging.EntityFrameworkCore;
using Volo.Abp.BackgroundJobs.EntityFrameworkCore;
using Volo.Abp.BlobStoring.Database.EntityFrameworkCore;
using Volo.Abp.Data;
using Volo.Abp.DependencyInjection;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.FeatureManagement.EntityFrameworkCore;
using Volo.Abp.Identity;
using Volo.Abp.Identity.EntityFrameworkCore;
using Volo.Abp.LanguageManagement.EntityFrameworkCore;
using Volo.Abp.PermissionManagement.EntityFrameworkCore;
using Volo.Abp.SettingManagement.EntityFrameworkCore;
using Volo.Abp.TextTemplateManagement.EntityFrameworkCore;
using Volo.Saas.EntityFrameworkCore;
using Volo.Saas.Editions;
using Volo.Saas.Tenants;
using Volo.Abp.Gdpr;
using Volo.Abp.OpenIddict.EntityFrameworkCore;

namespace MEVN.DistributorApp.V2.EntityFrameworkCore;

[ReplaceDbContext(typeof(IIdentityProDbContext))]
[ReplaceDbContext(typeof(ISaasDbContext))]
[ConnectionStringName("Default")]
public class V2DbContext :
    AbpDbContext<V2DbContext>,
    IIdentityProDbContext,
    ISaasDbContext
{
    public DbSet<ETADetail> ETADetails { get; set; } = null!;

    public DbSet<MaterialStockChecking> MaterialStockCheckings { get; set; } = null!;
    public DbSet<MaterialGroup> MaterialGroups { get; set; } = null!;
    public DbSet<Vender> Venders { get; set; } = null!;
    public DbSet<FreeOnShipMent> FreeOnShipMents { get; set; } = null!;

    public DbSet<DeliveryOrder> DeliveryOrders { get; set; } = null!;
    public DbSet<DistributorPurchaseOrderDetail> DistributorPurchaseOrderDetails { get; set; } = null!;
    public DbSet<DistributorPurchaseOrder> DistributorPurchaseOrders { get; set; } = null!;
    public DbSet<ProjectDetail> ProjectDetails { get; set; } = null!;
    public DbSet<Project> Projects { get; set; } = null!;
    public DbSet<Customer> Customers { get; set; } = null!;
    public DbSet<Distributor> Distributors { get; set; } = null!;
    /* Add DbSet properties for your Aggregate Roots / Entities here. */

    #region Entities from the modules

    /* Notice: We only implemented IIdentityProDbContext and ISaasDbContext
     * and replaced them for this DbContext. This allows you to perform JOIN
     * queries for the entities of these modules over the repositories easily. You
     * typically don't need that for other modules. But, if you need, you can
     * implement the DbContext interface of the needed module and use ReplaceDbContext
     * attribute just like IIdentityProDbContext and ISaasDbContext.
     *
     * More info: Replacing a DbContext of a module ensures that the related module
     * uses this DbContext on runtime. Otherwise, it will use its own DbContext class.
     */

    // Identity
    public DbSet<IdentityUser> Users { get; set; }
    public DbSet<IdentityRole> Roles { get; set; }
    public DbSet<IdentityClaimType> ClaimTypes { get; set; }
    public DbSet<OrganizationUnit> OrganizationUnits { get; set; }
    public DbSet<IdentitySecurityLog> SecurityLogs { get; set; }
    public DbSet<IdentityLinkUser> LinkUsers { get; set; }
    public DbSet<IdentityUserDelegation> UserDelegations { get; set; }

    // SaaS
    public DbSet<Tenant> Tenants { get; set; }
    public DbSet<Edition> Editions { get; set; }
    public DbSet<TenantConnectionString> TenantConnectionStrings { get; set; }

    #endregion

    public V2DbContext(DbContextOptions<V2DbContext> options)
        : base(options)
    {

    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);

        /* Include modules to your migration db context */

        builder.ConfigurePermissionManagement();
        builder.ConfigureSettingManagement();
        builder.ConfigureBackgroundJobs();
        builder.ConfigureAuditLogging();
        builder.ConfigureIdentityPro();
        builder.ConfigureOpenIddictPro();
        builder.ConfigureFeatureManagement();
        builder.ConfigureLanguageManagement();
        builder.ConfigureSaas();
        builder.ConfigureTextTemplateManagement();
        builder.ConfigureBlobStoring();
        builder.ConfigureGdpr();

        /* Configure your own tables/entities inside here */

        //builder.Entity<YourEntity>(b =>
        //{
        //    b.ToTable(V2Consts.DbTablePrefix + "YourEntities", V2Consts.DbSchema);
        //    b.ConfigureByConvention(); //auto configure for the base class props
        //    //...
        //});
        if (builder.IsHostDatabase())
        {

        }
        if (builder.IsHostDatabase())
        {

        }
        if (builder.IsHostDatabase())
        {

        }
        if (builder.IsHostDatabase())
        {

        }
        if (builder.IsHostDatabase())
        {

        }
        if (builder.IsHostDatabase())
        {

        }
        if (builder.IsHostDatabase())
        {

        }
        if (builder.IsHostDatabase())
        {

        }
        if (builder.IsHostDatabase())
        {

        }
        if (builder.IsHostDatabase())
        {

        }
        if (builder.IsHostDatabase())
        {
            builder.Entity<Project>(b =>
{
    b.ToTable(V2Consts.DbTablePrefix + "Projects", V2Consts.DbSchema);
    b.ConfigureByConvention();
    b.Property(x => x.ProjectCode).HasColumnName(nameof(Project.ProjectCode)).IsRequired().HasMaxLength(ProjectConsts.ProjectCodeMaxLength);
    b.Property(x => x.ProjectName).HasColumnName(nameof(Project.ProjectName)).IsRequired().HasMaxLength(ProjectConsts.ProjectNameMaxLength);
    b.Property(x => x.ApprovalStatus).HasColumnName(nameof(Project.ApprovalStatus)).IsRequired().HasMaxLength(ProjectConsts.ApprovalStatusMaxLength);
    b.Property(x => x.AccountNo).HasColumnName(nameof(Project.AccountNo));
    b.Property(x => x.ProjectType).HasColumnName(nameof(Project.ProjectType));
    b.Property(x => x.Location).HasColumnName(nameof(Project.Location));
    b.Property(x => x.DistributorMagin).HasColumnName(nameof(Project.DistributorMagin));
    b.Property(x => x.PODate).HasColumnName(nameof(Project.PODate));
    b.HasOne<Distributor>().WithMany().IsRequired().HasForeignKey(x => x.DistributorId).OnDelete(DeleteBehavior.NoAction);
    b.HasOne<Customer>().WithMany().HasForeignKey(x => x.CustomerId).OnDelete(DeleteBehavior.NoAction);
    b.HasMany(x => x.ListProjectDetail).WithOne().IsRequired().HasForeignKey(x => x.ProjectId);
});

        }
        if (builder.IsHostDatabase())
        {

        }
        if (builder.IsHostDatabase())
        {

        }
        if (builder.IsHostDatabase())
        {

        }
        if (builder.IsHostDatabase())
        {

        }
        if (builder.IsHostDatabase())
        {

        }
        if (builder.IsHostDatabase())
        {

        }
        if (builder.IsHostDatabase())
        {

        }
        if (builder.IsHostDatabase())
        {

        }
        if (builder.IsHostDatabase())
        {

        }
        if (builder.IsHostDatabase())
        {
            builder.Entity<FreeOnShipMent>(b =>
{
    b.ToTable(V2Consts.DbTablePrefix + "FreeOnShipMents", V2Consts.DbSchema);
    b.ConfigureByConvention();
    b.Property(x => x.MaterialID).HasColumnName(nameof(FreeOnShipMent.MaterialID));
    b.Property(x => x.PONo).HasColumnName(nameof(FreeOnShipMent.PONo)).IsRequired().HasMaxLength(FreeOnShipMentConsts.PONoMaxLength);
    b.Property(x => x.PODate).HasColumnName(nameof(FreeOnShipMent.PODate));
    b.Property(x => x.Qty).HasColumnName(nameof(FreeOnShipMent.Qty));
    b.Property(x => x.MachineNumber).HasColumnName(nameof(FreeOnShipMent.MachineNumber));
});

        }
        if (builder.IsHostDatabase())
        {
            builder.Entity<Vender>(b =>
{
    b.ToTable(V2Consts.DbTablePrefix + "Venders", V2Consts.DbSchema);
    b.ConfigureByConvention();
    b.Property(x => x.VerderName).HasColumnName(nameof(Vender.VerderName)).IsRequired().HasMaxLength(VenderConsts.VerderNameMaxLength);
    b.Property(x => x.Address).HasColumnName(nameof(Vender.Address));
    b.Property(x => x.Email).HasColumnName(nameof(Vender.Email));
});

        }
        if (builder.IsHostDatabase())
        {
            builder.Entity<MaterialGroup>(b =>
{
    b.ToTable(V2Consts.DbTablePrefix + "MaterialGroups", V2Consts.DbSchema);
    b.ConfigureByConvention();
    b.Property(x => x.MaterialName).HasColumnName(nameof(MaterialGroup.MaterialName)).IsRequired();
    b.Property(x => x.DisCode).HasColumnName(nameof(MaterialGroup.DisCode)).IsRequired();
});

        }
        if (builder.IsHostDatabase())
        {
            builder.Entity<MaterialStockChecking>(b =>
{
    b.ToTable(V2Consts.DbTablePrefix + "MaterialStockCheckings", V2Consts.DbSchema);
    b.ConfigureByConvention();
    b.Property(x => x.GolfaCode).HasColumnName(nameof(MaterialStockChecking.GolfaCode)).IsRequired();
    b.Property(x => x.Model).HasColumnName(nameof(MaterialStockChecking.Model)).IsRequired();
    b.Property(x => x.SAP_Code).HasColumnName(nameof(MaterialStockChecking.SAP_Code));
    b.Property(x => x.Description_VN).HasColumnName(nameof(MaterialStockChecking.Description_VN)).IsRequired();
    b.Property(x => x.Description_Group).HasColumnName(nameof(MaterialStockChecking.Description_Group));
    b.Property(x => x.Standard_Price).HasColumnName(nameof(MaterialStockChecking.Standard_Price));
    b.Property(x => x.StockValueWarning).HasColumnName(nameof(MaterialStockChecking.StockValueWarning));
    b.Property(x => x.StockTmp).HasColumnName(nameof(MaterialStockChecking.StockTmp));
    b.Property(x => x.Spec1).HasColumnName(nameof(MaterialStockChecking.Spec1));
    b.Property(x => x.Spec2).HasColumnName(nameof(MaterialStockChecking.Spec2));
    b.HasOne<MaterialGroup>().WithMany().IsRequired().HasForeignKey(x => x.MaterialGroupId).OnDelete(DeleteBehavior.NoAction);
    b.HasOne<Vender>().WithMany().IsRequired().HasForeignKey(x => x.VenderId).OnDelete(DeleteBehavior.NoAction);
    b.HasOne<Distributor>().WithMany().HasForeignKey(x => x.DistributorId).OnDelete(DeleteBehavior.NoAction);
    b.HasMany(x => x.ListFreeOnShipMent).WithOne().IsRequired().HasForeignKey(x => x.MaterialID);
});

        }
        if (builder.IsHostDatabase())
        {

        }
        if (builder.IsHostDatabase())
        {

        }
        if (builder.IsHostDatabase())
        {

        }
        if (builder.IsHostDatabase())
        {

        }
        if (builder.IsHostDatabase())
        {

        }
        if (builder.IsHostDatabase())
        {

        }
        if (builder.IsHostDatabase())
        {
            builder.Entity<ProjectDetail>(b =>
{
    b.ToTable(V2Consts.DbTablePrefix + "ProjectDetails", V2Consts.DbSchema);
    b.ConfigureByConvention();
    b.Property(x => x.ProjectId).HasColumnName(nameof(ProjectDetail.ProjectId));
    b.Property(x => x.GolfaCode).HasColumnName(nameof(ProjectDetail.GolfaCode)).IsRequired().HasMaxLength(ProjectDetailConsts.GolfaCodeMaxLength);
    b.Property(x => x.Model).HasColumnName(nameof(ProjectDetail.Model));
    b.Property(x => x.Qty).HasColumnName(nameof(ProjectDetail.Qty));
    b.Property(x => x.DpoUsed).HasColumnName(nameof(ProjectDetail.DpoUsed));
    b.Property(x => x.RequestStatus).HasColumnName(nameof(ProjectDetail.RequestStatus));
    b.Property(x => x.RequestPrice).HasColumnName(nameof(ProjectDetail.RequestPrice));
    b.Property(x => x.DistRequestedPrice).HasColumnName(nameof(ProjectDetail.DistRequestedPrice));
    b.Property(x => x.SaleOfferPrice).HasColumnName(nameof(ProjectDetail.SaleOfferPrice));
    b.Property(x => x.SaleAllowDiscountPrice).HasColumnName(nameof(ProjectDetail.SaleAllowDiscountPrice));
    b.Property(x => x.AmountRequestedPrice).HasColumnName(nameof(ProjectDetail.AmountRequestedPrice));
    b.Property(x => x.StatusCode).HasColumnName(nameof(ProjectDetail.StatusCode)).IsRequired();
});

        }
        if (builder.IsHostDatabase())
        {
            builder.Entity<DistributorPurchaseOrder>(b =>
{
    b.ToTable(V2Consts.DbTablePrefix + "DistributorPurchaseOrders", V2Consts.DbSchema);
    b.ConfigureByConvention();
    b.Property(x => x.DPONo).HasColumnName(nameof(DistributorPurchaseOrder.DPONo)).HasMaxLength(DistributorPurchaseOrderConsts.DPONoMaxLength);
    b.Property(x => x.Status).HasColumnName(nameof(DistributorPurchaseOrder.Status));
    b.Property(x => x.OrderDate).HasColumnName(nameof(DistributorPurchaseOrder.OrderDate));
    b.Property(x => x.TotalAmount).HasColumnName(nameof(DistributorPurchaseOrder.TotalAmount));
    b.Property(x => x.FA_LVS).HasColumnName(nameof(DistributorPurchaseOrder.FA_LVS)).HasMaxLength(DistributorPurchaseOrderConsts.FA_LVSMaxLength);
    b.Property(x => x.Note).HasColumnName(nameof(DistributorPurchaseOrder.Note)).HasMaxLength(DistributorPurchaseOrderConsts.NoteMaxLength);
    b.Property(x => x.PaymentInfo).HasColumnName(nameof(DistributorPurchaseOrder.PaymentInfo)).HasMaxLength(DistributorPurchaseOrderConsts.PaymentInfoMaxLength);
    b.Property(x => x.ShipmentDate).HasColumnName(nameof(DistributorPurchaseOrder.ShipmentDate));
    b.Property(x => x.UserName).HasColumnName(nameof(DistributorPurchaseOrder.UserName)).HasMaxLength(DistributorPurchaseOrderConsts.UserNameMaxLength);
    b.Property(x => x.SPUser).HasColumnName(nameof(DistributorPurchaseOrder.SPUser)).HasMaxLength(DistributorPurchaseOrderConsts.SPUserMaxLength);
    b.Property(x => x.Remark).HasColumnName(nameof(DistributorPurchaseOrder.Remark)).HasMaxLength(DistributorPurchaseOrderConsts.RemarkMaxLength);
    b.Property(x => x.SPFileName).HasColumnName(nameof(DistributorPurchaseOrder.SPFileName)).HasMaxLength(DistributorPurchaseOrderConsts.SPFileNameMaxLength);
    b.Property(x => x.FileName).HasColumnName(nameof(DistributorPurchaseOrder.FileName)).HasMaxLength(DistributorPurchaseOrderConsts.FileNameMaxLength);
    b.Property(x => x.StatusCode).HasColumnName(nameof(DistributorPurchaseOrder.StatusCode)).HasMaxLength(DistributorPurchaseOrderConsts.StatusCodeMaxLength);
    b.Property(x => x.StockKeepingRequestNo).HasColumnName(nameof(DistributorPurchaseOrder.StockKeepingRequestNo)).HasMaxLength(DistributorPurchaseOrderConsts.StockKeepingRequestNoMaxLength);
    b.Property(x => x.IsKeepStock).HasColumnName(nameof(DistributorPurchaseOrder.IsKeepStock));
    b.Property(x => x.ValidDate).HasColumnName(nameof(DistributorPurchaseOrder.ValidDate));
    b.HasOne<Distributor>().WithMany().HasForeignKey(x => x.DistributorId).OnDelete(DeleteBehavior.NoAction);
    b.HasMany(x => x.ListDistributorPurchaseOrderDetail).WithOne().IsRequired().HasForeignKey(x => x.DistributorPurchaseOrderId);
});

        }
        if (builder.IsHostDatabase())
        {

        }
        if (builder.IsHostDatabase())
        {

        }
        if (builder.IsHostDatabase())
        {

        }
        if (builder.IsHostDatabase())
        {

        }
        if (builder.IsHostDatabase())
        {
            builder.Entity<Customer>(b =>
{
    b.ToTable(V2Consts.DbTablePrefix + "Customers", V2Consts.DbSchema);
    b.ConfigureByConvention();
    b.Property(x => x.TaxCode).HasColumnName(nameof(Customer.TaxCode)).IsRequired().HasMaxLength(CustomerConsts.TaxCodeMaxLength);
    b.Property(x => x.CusName).HasColumnName(nameof(Customer.CusName)).IsRequired().HasMaxLength(CustomerConsts.CusNameMaxLength);
    b.Property(x => x.Address).HasColumnName(nameof(Customer.Address)).HasMaxLength(CustomerConsts.AddressMaxLength);
    b.Property(x => x.Phone).HasColumnName(nameof(Customer.Phone)).HasMaxLength(CustomerConsts.PhoneMaxLength);
    b.Property(x => x.Note).HasColumnName(nameof(Customer.Note));
});

        }
        if (builder.IsHostDatabase())
        {

        }
        if (builder.IsHostDatabase())
        {

        }
        if (builder.IsHostDatabase())
        {

        }
        if (builder.IsHostDatabase())
        {

        }
        if (builder.IsHostDatabase())
        {

        }
        if (builder.IsHostDatabase())
        {

        }
        if (builder.IsHostDatabase())
        {
            builder.Entity<DistributorPurchaseOrderDetail>(b =>
{
    b.ToTable(V2Consts.DbTablePrefix + "DistributorPurchaseOrderDetails", V2Consts.DbSchema);
    b.ConfigureByConvention();
    b.Property(x => x.Status).HasColumnName(nameof(DistributorPurchaseOrderDetail.Status));
    b.Property(x => x.GolfaCode).HasColumnName(nameof(DistributorPurchaseOrderDetail.GolfaCode)).HasMaxLength(DistributorPurchaseOrderDetailConsts.GolfaCodeMaxLength);
    b.Property(x => x.Customer).HasColumnName(nameof(DistributorPurchaseOrderDetail.Customer)).HasMaxLength(DistributorPurchaseOrderDetailConsts.CustomerMaxLength);
    b.Property(x => x.Model).HasColumnName(nameof(DistributorPurchaseOrderDetail.Model)).HasMaxLength(DistributorPurchaseOrderDetailConsts.ModelMaxLength);
    b.Property(x => x.Qty).HasColumnName(nameof(DistributorPurchaseOrderDetail.Qty));
    b.Property(x => x.Price).HasColumnName(nameof(DistributorPurchaseOrderDetail.Price));
    b.Property(x => x.Amount).HasColumnName(nameof(DistributorPurchaseOrderDetail.Amount));
    b.Property(x => x.RequestedETA).HasColumnName(nameof(DistributorPurchaseOrderDetail.RequestedETA));
    b.Property(x => x.Delivered).HasColumnName(nameof(DistributorPurchaseOrderDetail.Delivered));
    b.Property(x => x.InProgress).HasColumnName(nameof(DistributorPurchaseOrderDetail.InProgress));
    b.Property(x => x.Note).HasColumnName(nameof(DistributorPurchaseOrderDetail.Note)).HasMaxLength(DistributorPurchaseOrderDetailConsts.NoteMaxLength);
    b.Property(x => x.ETA).HasColumnName(nameof(DistributorPurchaseOrderDetail.ETA));
    b.Property(x => x.CustomerName).HasColumnName(nameof(DistributorPurchaseOrderDetail.CustomerName)).HasMaxLength(DistributorPurchaseOrderDetailConsts.CustomerNameMaxLength);
    b.Property(x => x.StatusCode).HasColumnName(nameof(DistributorPurchaseOrderDetail.StatusCode)).HasMaxLength(DistributorPurchaseOrderDetailConsts.StatusCodeMaxLength);
    b.Property(x => x.DistributorPurchaseOrderId).HasColumnName(nameof(DistributorPurchaseOrderDetail.DistributorPurchaseOrderId));
    b.HasMany(x => x.ListETADetail).WithOne().IsRequired().HasForeignKey(x => x.DistributorOrderDetailId);
    b.HasMany(x => x.ListDeliveryOrder).WithOne().IsRequired().HasForeignKey(x => x.DistributorOrderDetailId);

});

        }
        if (builder.IsHostDatabase())
        {

        }
        if (builder.IsHostDatabase())
        {
            builder.Entity<ETADetail>(b =>
{
    b.ToTable(V2Consts.DbTablePrefix + "ETADetails", V2Consts.DbSchema);
    b.ConfigureByConvention();
    b.Property(x => x.StatusCode).HasColumnName(nameof(ETADetail.StatusCode)).IsRequired().HasMaxLength(ETADetailConsts.StatusCodeMaxLength);
    b.Property(x => x.InvoiceNo).HasColumnName(nameof(ETADetail.InvoiceNo)).IsRequired().HasMaxLength(ETADetailConsts.InvoiceNoMaxLength);
    b.Property(x => x.GolfaCode).HasColumnName(nameof(ETADetail.GolfaCode)).IsRequired().HasMaxLength(ETADetailConsts.GolfaCodeMaxLength);
    b.Property(x => x.Qty).HasColumnName(nameof(ETADetail.Qty));
    b.Property(x => x.ETD).HasColumnName(nameof(ETADetail.ETD));
    b.Property(x => x.ETA).HasColumnName(nameof(ETADetail.ETA));
    b.Property(x => x.DistributorOrderDetailId).HasColumnName(nameof(ETADetail.DistributorOrderDetailId));
});

        }
        if (builder.IsHostDatabase())
        {
            builder.Entity<DeliveryOrder>(b =>
{
    b.ToTable(V2Consts.DbTablePrefix + "DeliveryOrders", V2Consts.DbSchema);
    b.ConfigureByConvention();
    b.Property(x => x.Status).HasColumnName(nameof(DeliveryOrder.Status));
    b.Property(x => x.DeliveryDate).HasColumnName(nameof(DeliveryOrder.DeliveryDate));
    b.Property(x => x.DeliveryCode).HasColumnName(nameof(DeliveryOrder.DeliveryCode)).HasMaxLength(DeliveryOrderConsts.DeliveryCodeMaxLength);
    b.Property(x => x.DOSAPNo).HasColumnName(nameof(DeliveryOrder.DOSAPNo)).HasMaxLength(DeliveryOrderConsts.DOSAPNoMaxLength);
    b.Property(x => x.InvoiceDate).HasColumnName(nameof(DeliveryOrder.InvoiceDate));
    b.Property(x => x.InvoiceNo).HasColumnName(nameof(DeliveryOrder.InvoiceNo)).HasMaxLength(DeliveryOrderConsts.InvoiceNoMaxLength);
    b.Property(x => x.BillingNo).HasColumnName(nameof(DeliveryOrder.BillingNo)).HasMaxLength(DeliveryOrderConsts.BillingNoMaxLength);
    b.Property(x => x.Note).HasColumnName(nameof(DeliveryOrder.Note)).HasMaxLength(DeliveryOrderConsts.NoteMaxLength);
    b.Property(x => x.StatusCode).HasColumnName(nameof(DeliveryOrder.StatusCode)).HasMaxLength(DeliveryOrderConsts.StatusCodeMaxLength);
    b.Property(x => x.Qty).HasColumnName(nameof(DeliveryOrder.Qty));
    b.Property(x => x.DistributorOrderDetailId).HasColumnName(nameof(DeliveryOrder.DistributorOrderDetailId));
});

        }
        if (builder.IsHostDatabase())
        {
            builder.Entity<Distributor>(b =>
{
    b.ToTable(V2Consts.DbTablePrefix + "Distributors", V2Consts.DbSchema);
    b.ConfigureByConvention();
    b.Property(x => x.DisCode).HasColumnName(nameof(Distributor.DisCode)).IsRequired().HasMaxLength(DistributorConsts.DisCodeMaxLength);
    b.Property(x => x.DisName).HasColumnName(nameof(Distributor.DisName)).IsRequired().HasMaxLength(DistributorConsts.DisNameMaxLength);
    b.Property(x => x.Address).HasColumnName(nameof(Distributor.Address));
    b.Property(x => x.ContactInfo).HasColumnName(nameof(Distributor.ContactInfo));
    b.Property(x => x.Node).HasColumnName(nameof(Distributor.Node));
});

        }
    }
}