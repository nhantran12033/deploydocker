using MEVN.DistributorApp.V2.DistributorPurchaseOrderDetails;
using MEVN.DistributorApp.V2.ETADetails;

using MEVN.DistributorApp.V2.MaterialStockCheckings;
using MEVN.DistributorApp.V2.MaterialGroups;
using MEVN.DistributorApp.V2.Venders;
using MEVN.DistributorApp.V2.FreeOnShipMents;

using MEVN.DistributorApp.V2.DeliveryOrders;
using MEVN.DistributorApp.V2.listDistributorPurchaseOrderDetails;
using MEVN.DistributorApp.V2.DistributorPurchaseOrders;

using MEVN.DistributorApp.V2.ProjectDetails;

using MEVN.DistributorApp.V2.Projects;
using MEVN.DistributorApp.V2.Customers;
using MEVN.DistributorApp.V2.Distributors;
using System;
using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Uow;
using Volo.Abp.AuditLogging.EntityFrameworkCore;
using Volo.Abp.BackgroundJobs.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore.SqlServer;
using Volo.Abp.FeatureManagement.EntityFrameworkCore;
using Volo.Abp.Identity.EntityFrameworkCore;
using Volo.Abp.LanguageManagement.EntityFrameworkCore;
using Volo.Abp.Modularity;
using Volo.Abp.PermissionManagement.EntityFrameworkCore;
using Volo.Abp.SettingManagement.EntityFrameworkCore;
using Volo.Abp.TextTemplateManagement.EntityFrameworkCore;
using Volo.Saas.EntityFrameworkCore;
using Volo.Abp.BlobStoring.Database.EntityFrameworkCore;
using Volo.Abp.Gdpr;
using Volo.Abp.OpenIddict.EntityFrameworkCore;

namespace MEVN.DistributorApp.V2.EntityFrameworkCore;

[DependsOn(
    typeof(V2DomainModule),
    typeof(AbpIdentityProEntityFrameworkCoreModule),
    typeof(AbpOpenIddictProEntityFrameworkCoreModule),
    typeof(AbpPermissionManagementEntityFrameworkCoreModule),
    typeof(AbpSettingManagementEntityFrameworkCoreModule),
    typeof(AbpEntityFrameworkCoreSqlServerModule),
    typeof(AbpBackgroundJobsEntityFrameworkCoreModule),
    typeof(AbpAuditLoggingEntityFrameworkCoreModule),
    typeof(AbpFeatureManagementEntityFrameworkCoreModule),
    typeof(LanguageManagementEntityFrameworkCoreModule),
    typeof(SaasEntityFrameworkCoreModule),
    typeof(TextTemplateManagementEntityFrameworkCoreModule),
    typeof(AbpGdprEntityFrameworkCoreModule),
    typeof(BlobStoringDatabaseEntityFrameworkCoreModule)
    )]
public class V2EntityFrameworkCoreModule : AbpModule
{
    public override void PreConfigureServices(ServiceConfigurationContext context)
    {
        V2EfCoreEntityExtensionMappings.Configure();
    }

    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        context.Services.AddAbpDbContext<V2DbContext>(options =>
        {
            /* Remove "includeAllEntities: true" to create
             * default repositories only for aggregate roots */
            options.AddDefaultRepositories(includeAllEntities: true);
            options.AddRepository<Distributor, Distributors.EfCoreDistributorRepository>();

            options.AddRepository<Customer, Customers.EfCoreCustomerRepository>();

            options.AddRepository<Project, Projects.EfCoreProjectRepository>();

            options.AddRepository<ProjectDetail, ProjectDetails.EfCoreProjectDetailRepository>();

            options.AddRepository<FreeOnShipMent, FreeOnShipMents.EfCoreFreeOnShipMentRepository>();

            options.AddRepository<Vender, Venders.EfCoreVenderRepository>();

            options.AddRepository<MaterialGroup, MaterialGroups.EfCoreMaterialGroupRepository>();

            options.AddRepository<MaterialStockChecking, MaterialStockCheckings.EfCoreMaterialStockCheckingRepository>();

            options.AddRepository<DistributorPurchaseOrder, DistributorPurchaseOrders.EfCoreDistributorPurchaseOrderRepository>();

          
            options.AddRepository<DeliveryOrder, DeliveryOrders.EfCoreDeliveryOrderRepository>();

            options.AddRepository<ETADetail, ETADetails.EfCoreETADetailRepository>();

            
        });

        Configure<AbpDbContextOptions>(options =>
        {
            /* The main point to change your DBMS.
             * See also V2DbContextFactory for EF Core tooling. */
            options.UseSqlServer();
        });

    }
}