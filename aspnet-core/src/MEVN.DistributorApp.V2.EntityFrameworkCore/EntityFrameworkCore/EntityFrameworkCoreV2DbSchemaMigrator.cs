﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using MEVN.DistributorApp.V2.Data;
using Volo.Abp.DependencyInjection;

namespace MEVN.DistributorApp.V2.EntityFrameworkCore;

public class EntityFrameworkCoreV2DbSchemaMigrator
    : IV2DbSchemaMigrator, ITransientDependency
{
    private readonly IServiceProvider _serviceProvider;

    public EntityFrameworkCoreV2DbSchemaMigrator(IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
    }

    public async Task MigrateAsync()
    {
        /* We intentionally resolve the V2DbContext
         * from IServiceProvider (instead of directly injecting it)
         * to properly get the connection string of the current tenant in the
         * current scope.
         */

        await _serviceProvider
            .GetRequiredService<V2DbContext>()
            .Database
            .MigrateAsync();
    }
}
