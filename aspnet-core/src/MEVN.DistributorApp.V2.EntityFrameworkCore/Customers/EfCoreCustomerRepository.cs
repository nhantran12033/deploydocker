using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;
using MEVN.DistributorApp.V2.EntityFrameworkCore;

namespace MEVN.DistributorApp.V2.Customers
{
    public abstract class EfCoreCustomerRepositoryBase : EfCoreRepository<V2DbContext, Customer, Guid>
    {
        public EfCoreCustomerRepositoryBase(IDbContextProvider<V2DbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        public virtual async Task<List<Customer>> GetListAsync(
            string? filterText = null,
            string? taxCode = null,
            string? cusName = null,
            string? address = null,
            string? phone = null,
            string? note = null,
            string? sorting = null,
            int maxResultCount = int.MaxValue,
            int skipCount = 0,
            CancellationToken cancellationToken = default)
        {
            var query = ApplyFilter((await GetQueryableAsync()), filterText, taxCode, cusName, address, phone, note);
            query = query.OrderBy(string.IsNullOrWhiteSpace(sorting) ? CustomerConsts.GetDefaultSorting(false) : sorting);
            return await query.PageBy(skipCount, maxResultCount).ToListAsync(cancellationToken);
        }

        public virtual async Task<long> GetCountAsync(
            string? filterText = null,
            string? taxCode = null,
            string? cusName = null,
            string? address = null,
            string? phone = null,
            string? note = null,
            CancellationToken cancellationToken = default)
        {
            var query = ApplyFilter((await GetDbSetAsync()), filterText, taxCode, cusName, address, phone, note);
            return await query.LongCountAsync(GetCancellationToken(cancellationToken));
        }

        protected virtual IQueryable<Customer> ApplyFilter(
            IQueryable<Customer> query,
            string? filterText = null,
            string? taxCode = null,
            string? cusName = null,
            string? address = null,
            string? phone = null,
            string? note = null)
        {
            return query
                    .WhereIf(!string.IsNullOrWhiteSpace(filterText), e => e.TaxCode!.Contains(filterText!) || e.CusName!.Contains(filterText!) || e.Address!.Contains(filterText!) || e.Phone!.Contains(filterText!) || e.Note!.Contains(filterText!))
                    .WhereIf(!string.IsNullOrWhiteSpace(taxCode), e => e.TaxCode.Contains(taxCode))
                    .WhereIf(!string.IsNullOrWhiteSpace(cusName), e => e.CusName.Contains(cusName))
                    .WhereIf(!string.IsNullOrWhiteSpace(address), e => e.Address.Contains(address))
                    .WhereIf(!string.IsNullOrWhiteSpace(phone), e => e.Phone.Contains(phone))
                    .WhereIf(!string.IsNullOrWhiteSpace(note), e => e.Note.Contains(note));
        }
    }
}