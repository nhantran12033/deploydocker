using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;
using MEVN.DistributorApp.V2.EntityFrameworkCore;

namespace MEVN.DistributorApp.V2.ETADetails
{
    public abstract class EfCoreETADetailRepositoryBase : EfCoreRepository<V2DbContext, ETADetail, Guid>
    {
        public EfCoreETADetailRepositoryBase(IDbContextProvider<V2DbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        public virtual async Task<List<ETADetail>> GetListAsync(
            string? filterText = null,
            string? statusCode = null,
            string? invoiceNo = null,
            string? golfaCode = null,
            int? qtyMin = null,
            int? qtyMax = null,
            DateTime? eTDMin = null,
            DateTime? eTDMax = null,
            DateTime? eTAMin = null,
            DateTime? eTAMax = null,
            Guid? distributorOrderDetailId = null,
            string? sorting = null,
            int maxResultCount = int.MaxValue,
            int skipCount = 0,
            CancellationToken cancellationToken = default)
        {
            var query = ApplyFilter((await GetQueryableAsync()), filterText, statusCode, invoiceNo, golfaCode, qtyMin, qtyMax, eTDMin, eTDMax, eTAMin, eTAMax, distributorOrderDetailId);
            query = query.OrderBy(string.IsNullOrWhiteSpace(sorting) ? ETADetailConsts.GetDefaultSorting(false) : sorting);
            return await query.PageBy(skipCount, maxResultCount).ToListAsync(cancellationToken);
        }

        public virtual async Task<long> GetCountAsync(
            string? filterText = null,
            string? statusCode = null,
            string? invoiceNo = null,
            string? golfaCode = null,
            int? qtyMin = null,
            int? qtyMax = null,
            DateTime? eTDMin = null,
            DateTime? eTDMax = null,
            DateTime? eTAMin = null,
            DateTime? eTAMax = null,
            Guid? distributorOrderDetailId = null,
            CancellationToken cancellationToken = default)
        {
            var query = ApplyFilter((await GetDbSetAsync()), filterText, statusCode, invoiceNo, golfaCode, qtyMin, qtyMax, eTDMin, eTDMax, eTAMin, eTAMax, distributorOrderDetailId);
            return await query.LongCountAsync(GetCancellationToken(cancellationToken));
        }

        protected virtual IQueryable<ETADetail> ApplyFilter(
            IQueryable<ETADetail> query,
            string? filterText = null,
            string? statusCode = null,
            string? invoiceNo = null,
            string? golfaCode = null,
            int? qtyMin = null,
            int? qtyMax = null,
            DateTime? eTDMin = null,
            DateTime? eTDMax = null,
            DateTime? eTAMin = null,
            DateTime? eTAMax = null,
            Guid? distributorOrderDetailId = null)
        {
            return query
                    .WhereIf(!string.IsNullOrWhiteSpace(filterText), e => e.StatusCode!.Contains(filterText!) || e.InvoiceNo!.Contains(filterText!) || e.GolfaCode!.Contains(filterText!))
                    .WhereIf(!string.IsNullOrWhiteSpace(statusCode), e => e.StatusCode.Contains(statusCode))
                    .WhereIf(!string.IsNullOrWhiteSpace(invoiceNo), e => e.InvoiceNo.Contains(invoiceNo))
                    .WhereIf(!string.IsNullOrWhiteSpace(golfaCode), e => e.GolfaCode.Contains(golfaCode))
                    .WhereIf(qtyMin.HasValue, e => e.Qty >= qtyMin!.Value)
                    .WhereIf(qtyMax.HasValue, e => e.Qty <= qtyMax!.Value)
                    .WhereIf(eTDMin.HasValue, e => e.ETD >= eTDMin!.Value)
                    .WhereIf(eTDMax.HasValue, e => e.ETD <= eTDMax!.Value)
                    .WhereIf(eTAMin.HasValue, e => e.ETA >= eTAMin!.Value)
                    .WhereIf(eTAMax.HasValue, e => e.ETA <= eTAMax!.Value)
                    .WhereIf(distributorOrderDetailId.HasValue, e => e.DistributorOrderDetailId == distributorOrderDetailId);
        }
    }
}