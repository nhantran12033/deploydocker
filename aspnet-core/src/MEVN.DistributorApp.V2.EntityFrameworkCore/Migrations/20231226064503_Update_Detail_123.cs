﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MEVN.DistributorApp.V2.Migrations
{
    /// <inheritdoc />
    public partial class Update_Detail_123 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "DistributorOrderDetailId",
                table: "AppETADetails",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "DistributorOrderDetailId",
                table: "AppDeliveryOrders",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_AppETADetails_DistributorOrderDetailId",
                table: "AppETADetails",
                column: "DistributorOrderDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_AppDeliveryOrders_DistributorOrderDetailId",
                table: "AppDeliveryOrders",
                column: "DistributorOrderDetailId");

            migrationBuilder.AddForeignKey(
                name: "FK_AppDeliveryOrders_AppDistributorPurchaseOrderDetails_DistributorOrderDetailId",
                table: "AppDeliveryOrders",
                column: "DistributorOrderDetailId",
                principalTable: "AppDistributorPurchaseOrderDetails",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AppETADetails_AppDistributorPurchaseOrderDetails_DistributorOrderDetailId",
                table: "AppETADetails",
                column: "DistributorOrderDetailId",
                principalTable: "AppDistributorPurchaseOrderDetails",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AppDeliveryOrders_AppDistributorPurchaseOrderDetails_DistributorOrderDetailId",
                table: "AppDeliveryOrders");

            migrationBuilder.DropForeignKey(
                name: "FK_AppETADetails_AppDistributorPurchaseOrderDetails_DistributorOrderDetailId",
                table: "AppETADetails");

            migrationBuilder.DropIndex(
                name: "IX_AppETADetails_DistributorOrderDetailId",
                table: "AppETADetails");

            migrationBuilder.DropIndex(
                name: "IX_AppDeliveryOrders_DistributorOrderDetailId",
                table: "AppDeliveryOrders");

            migrationBuilder.DropColumn(
                name: "DistributorOrderDetailId",
                table: "AppETADetails");

            migrationBuilder.DropColumn(
                name: "DistributorOrderDetailId",
                table: "AppDeliveryOrders");
        }
    }
}
