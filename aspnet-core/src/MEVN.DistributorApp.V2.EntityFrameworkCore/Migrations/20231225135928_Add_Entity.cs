﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MEVN.DistributorApp.V2.Migrations
{
    /// <inheritdoc />
    public partial class Add_Entity : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            

            migrationBuilder.CreateTable(
                name: "AppDistributorPurchaseOrderDetails",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ExtraProperties = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ConcurrencyStamp = table.Column<string>(type: "nvarchar(40)", maxLength: 40, nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, defaultValue: false),
                    DeleterId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DistributorPurchaseOrderId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Status = table.Column<byte>(type: "tinyint", nullable: false),
                    GolfaCode = table.Column<string>(type: "nvarchar(40)", maxLength: 40, nullable: true),
                    Customer = table.Column<string>(type: "nvarchar(400)", maxLength: 400, nullable: true),
                    Model = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    DistributorSpec1 = table.Column<string>(type: "nvarchar(400)", maxLength: 400, nullable: true),
                    DistributorSpec2 = table.Column<string>(type: "nvarchar(400)", maxLength: 400, nullable: true),
                    ProjectCode = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    Qty = table.Column<int>(type: "int", nullable: true),
                    Price = table.Column<float>(type: "real", nullable: true),
                    Amount = table.Column<float>(type: "real", nullable: true),
                    RequestedETA = table.Column<DateTime>(type: "datetime2", nullable: true),
                    ShipmentMethod = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    Delivered = table.Column<int>(type: "int", nullable: true),
                    NeedDelivery = table.Column<int>(type: "int", nullable: true),
                    LockStock = table.Column<int>(type: "int", nullable: true),
                    LockStockSO = table.Column<int>(type: "int", nullable: true),
                    LockShipment = table.Column<int>(type: "int", nullable: true),
                    Note = table.Column<string>(type: "nvarchar(4000)", maxLength: 4000, nullable: true),
                    Priority = table.Column<byte>(type: "tinyint", nullable: true),
                    USDRate = table.Column<float>(type: "real", nullable: true),
                    CustomerCode = table.Column<string>(type: "nvarchar(125)", maxLength: 125, nullable: true),
                    ProjectV2DetailId = table.Column<int>(type: "int", nullable: true),
                    ErrorCode = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    StatusCode = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    LockStock_Keeping = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppDistributorPurchaseOrderDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AppDistributorPurchaseOrderDetails_AppDistributorPurchaseOrders_DistributorPurchaseOrderId",
                        column: x => x.DistributorPurchaseOrderId,
                        principalTable: "AppDistributorPurchaseOrders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            

            migrationBuilder.CreateIndex(
                name: "IX_AppDistributorPurchaseOrderDetails_DistributorPurchaseOrderId",
                table: "AppDistributorPurchaseOrderDetails",
                column: "DistributorPurchaseOrderId");

            
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
           

            migrationBuilder.DropTable(
                name: "AppDistributorPurchaseOrderDetails");

        }
    }
}
