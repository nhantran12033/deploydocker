﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MEVN.DistributorApp.V2.Migrations
{
    /// <inheritdoc />
    public partial class Update_Detail : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DistributorSpec1",
                table: "AppDistributorPurchaseOrderDetails");

            migrationBuilder.DropColumn(
                name: "DistributorSpec2",
                table: "AppDistributorPurchaseOrderDetails");

            migrationBuilder.DropColumn(
                name: "ErrorCode",
                table: "AppDistributorPurchaseOrderDetails");

            migrationBuilder.DropColumn(
                name: "LockShipment",
                table: "AppDistributorPurchaseOrderDetails");

            migrationBuilder.DropColumn(
                name: "LockStock",
                table: "AppDistributorPurchaseOrderDetails");

            migrationBuilder.DropColumn(
                name: "LockStockSO",
                table: "AppDistributorPurchaseOrderDetails");

            migrationBuilder.DropColumn(
                name: "LockStock_Keeping",
                table: "AppDistributorPurchaseOrderDetails");

            migrationBuilder.DropColumn(
                name: "NeedDelivery",
                table: "AppDistributorPurchaseOrderDetails");

            migrationBuilder.DropColumn(
                name: "ProjectCode",
                table: "AppDistributorPurchaseOrderDetails");

            migrationBuilder.DropColumn(
                name: "ShipmentMethod",
                table: "AppDistributorPurchaseOrderDetails");

            migrationBuilder.DropColumn(
                name: "USDRate",
                table: "AppDistributorPurchaseOrderDetails");

            migrationBuilder.RenameColumn(
                name: "ProjectV2DetailId",
                table: "AppDistributorPurchaseOrderDetails",
                newName: "InProgress");

            migrationBuilder.RenameColumn(
                name: "Priority",
                table: "AppDistributorPurchaseOrderDetails",
                newName: "ETA");

            migrationBuilder.RenameColumn(
                name: "CustomerCode",
                table: "AppDistributorPurchaseOrderDetails",
                newName: "CustomerName");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "InProgress",
                table: "AppDistributorPurchaseOrderDetails",
                newName: "ProjectV2DetailId");

            migrationBuilder.RenameColumn(
                name: "ETA",
                table: "AppDistributorPurchaseOrderDetails",
                newName: "Priority");

            migrationBuilder.RenameColumn(
                name: "CustomerName",
                table: "AppDistributorPurchaseOrderDetails",
                newName: "CustomerCode");

            migrationBuilder.AddColumn<string>(
                name: "DistributorSpec1",
                table: "AppDistributorPurchaseOrderDetails",
                type: "nvarchar(400)",
                maxLength: 400,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DistributorSpec2",
                table: "AppDistributorPurchaseOrderDetails",
                type: "nvarchar(400)",
                maxLength: 400,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ErrorCode",
                table: "AppDistributorPurchaseOrderDetails",
                type: "nvarchar(20)",
                maxLength: 20,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "LockShipment",
                table: "AppDistributorPurchaseOrderDetails",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "LockStock",
                table: "AppDistributorPurchaseOrderDetails",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "LockStockSO",
                table: "AppDistributorPurchaseOrderDetails",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "LockStock_Keeping",
                table: "AppDistributorPurchaseOrderDetails",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "NeedDelivery",
                table: "AppDistributorPurchaseOrderDetails",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ProjectCode",
                table: "AppDistributorPurchaseOrderDetails",
                type: "nvarchar(20)",
                maxLength: 20,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ShipmentMethod",
                table: "AppDistributorPurchaseOrderDetails",
                type: "nvarchar(20)",
                maxLength: 20,
                nullable: true);

            migrationBuilder.AddColumn<float>(
                name: "USDRate",
                table: "AppDistributorPurchaseOrderDetails",
                type: "real",
                nullable: true);
        }
    }
}
