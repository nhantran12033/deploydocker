using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Microsoft.AspNetCore.Authorization;
using Volo.Abp;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;
using MEVN.DistributorApp.V2.Permissions;
using MEVN.DistributorApp.V2.Distributors;

namespace MEVN.DistributorApp.V2.Distributors
{
    [RemoteService(IsEnabled = false)]
    [Authorize(V2Permissions.Distributors.Default)]
    public abstract class DistributorsAppServiceBase : ApplicationService
    {

        protected IDistributorRepository _distributorRepository;
        protected DistributorManager _distributorManager;

        public DistributorsAppServiceBase(IDistributorRepository distributorRepository, DistributorManager distributorManager)
        {

            _distributorRepository = distributorRepository;
            _distributorManager = distributorManager;
        }

        public virtual async Task<PagedResultDto<DistributorDto>> GetListAsync(GetDistributorsInput input)
        {
            var totalCount = await _distributorRepository.GetCountAsync(input.FilterText, input.DisCode, input.DisName, input.Address, input.ContactInfo, input.Node);
            var items = await _distributorRepository.GetListAsync(input.FilterText, input.DisCode, input.DisName, input.Address, input.ContactInfo, input.Node, input.Sorting, input.MaxResultCount, input.SkipCount);

            return new PagedResultDto<DistributorDto>
            {
                TotalCount = totalCount,
                Items = ObjectMapper.Map<List<Distributor>, List<DistributorDto>>(items)
            };
        }

        public virtual async Task<DistributorDto> GetAsync(Guid id)
        {
            return ObjectMapper.Map<Distributor, DistributorDto>(await _distributorRepository.GetAsync(id));
        }

        [Authorize(V2Permissions.Distributors.Delete)]
        public virtual async Task DeleteAsync(Guid id)
        {
            await _distributorRepository.DeleteAsync(id);
        }

        [Authorize(V2Permissions.Distributors.Create)]
        public virtual async Task<DistributorDto> CreateAsync(DistributorCreateDto input)
        {

            var distributor = await _distributorManager.CreateAsync(
            input.DisCode, input.DisName, input.Address, input.ContactInfo, input.Node
            );

            return ObjectMapper.Map<Distributor, DistributorDto>(distributor);
        }

        [Authorize(V2Permissions.Distributors.Edit)]
        public virtual async Task<DistributorDto> UpdateAsync(Guid id, DistributorUpdateDto input)
        {

            var distributor = await _distributorManager.UpdateAsync(
            id,
            input.DisCode, input.DisName, input.Address, input.ContactInfo, input.Node, input.ConcurrencyStamp
            );

            return ObjectMapper.Map<Distributor, DistributorDto>(distributor);
        }
    }
}