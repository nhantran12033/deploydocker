using System;

namespace MEVN.DistributorApp.V2.Distributors;

public abstract class DistributorExcelDownloadTokenCacheItemBase
{
    public string Token { get; set; } = null!;
}