using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Microsoft.AspNetCore.Authorization;
using Volo.Abp;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;
using MEVN.DistributorApp.V2.Permissions;
using MEVN.DistributorApp.V2.Customers;

namespace MEVN.DistributorApp.V2.Customers
{
    [RemoteService(IsEnabled = false)]
    [Authorize(V2Permissions.Customers.Default)]
    public abstract class CustomersAppServiceBase : ApplicationService
    {

        protected ICustomerRepository _customerRepository;
        protected CustomerManager _customerManager;

        public CustomersAppServiceBase(ICustomerRepository customerRepository, CustomerManager customerManager)
        {

            _customerRepository = customerRepository;
            _customerManager = customerManager;
        }

        public virtual async Task<PagedResultDto<CustomerDto>> GetListAsync(GetCustomersInput input)
        {
            var totalCount = await _customerRepository.GetCountAsync(input.FilterText, input.TaxCode, input.CusName, input.Address, input.Phone, input.Note);
            var items = await _customerRepository.GetListAsync(input.FilterText, input.TaxCode, input.CusName, input.Address, input.Phone, input.Note, input.Sorting, input.MaxResultCount, input.SkipCount);

            return new PagedResultDto<CustomerDto>
            {
                TotalCount = totalCount,
                Items = ObjectMapper.Map<List<Customer>, List<CustomerDto>>(items)
            };
        }

        public virtual async Task<CustomerDto> GetAsync(Guid id)
        {
            return ObjectMapper.Map<Customer, CustomerDto>(await _customerRepository.GetAsync(id));
        }

        [Authorize(V2Permissions.Customers.Delete)]
        public virtual async Task DeleteAsync(Guid id)
        {
            await _customerRepository.DeleteAsync(id);
        }

        [Authorize(V2Permissions.Customers.Create)]
        public virtual async Task<CustomerDto> CreateAsync(CustomerCreateDto input)
        {

            var customer = await _customerManager.CreateAsync(
            input.TaxCode, input.CusName, input.Address, input.Phone, input.Note
            );

            return ObjectMapper.Map<Customer, CustomerDto>(customer);
        }

        [Authorize(V2Permissions.Customers.Edit)]
        public virtual async Task<CustomerDto> UpdateAsync(Guid id, CustomerUpdateDto input)
        {

            var customer = await _customerManager.UpdateAsync(
            id,
            input.TaxCode, input.CusName, input.Address, input.Phone, input.Note, input.ConcurrencyStamp
            );

            return ObjectMapper.Map<Customer, CustomerDto>(customer);
        }
    }
}