using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Microsoft.AspNetCore.Authorization;
using Volo.Abp;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;
using MEVN.DistributorApp.V2.Permissions;
using MEVN.DistributorApp.V2.Customers;

namespace MEVN.DistributorApp.V2.Customers
{
    public class CustomersAppService : CustomersAppServiceBase, ICustomersAppService
    {
        //<suite-custom-code-autogenerated>
        public CustomersAppService(ICustomerRepository customerRepository, CustomerManager customerManager)
            : base(customerRepository, customerManager)
        {
        }
        //</suite-custom-code-autogenerated>

        //Write your custom code...
    }
}