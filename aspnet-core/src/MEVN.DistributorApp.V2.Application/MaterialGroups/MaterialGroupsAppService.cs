using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Microsoft.AspNetCore.Authorization;
using Volo.Abp;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;
using MEVN.DistributorApp.V2.Permissions;
using MEVN.DistributorApp.V2.MaterialGroups;

namespace MEVN.DistributorApp.V2.MaterialGroups
{
    [RemoteService(IsEnabled = false)]
    [Authorize(V2Permissions.MaterialGroups.Default)]
    public abstract class MaterialGroupsAppServiceBase : ApplicationService
    {

        protected IMaterialGroupRepository _materialGroupRepository;
        protected MaterialGroupManager _materialGroupManager;

        public MaterialGroupsAppServiceBase(IMaterialGroupRepository materialGroupRepository, MaterialGroupManager materialGroupManager)
        {

            _materialGroupRepository = materialGroupRepository;
            _materialGroupManager = materialGroupManager;
        }

        public virtual async Task<PagedResultDto<MaterialGroupDto>> GetListAsync(GetMaterialGroupsInput input)
        {
            var totalCount = await _materialGroupRepository.GetCountAsync(input.FilterText, input.MaterialName, input.DisCode);
            var items = await _materialGroupRepository.GetListAsync(input.FilterText, input.MaterialName, input.DisCode, input.Sorting, input.MaxResultCount, input.SkipCount);

            return new PagedResultDto<MaterialGroupDto>
            {
                TotalCount = totalCount,
                Items = ObjectMapper.Map<List<MaterialGroup>, List<MaterialGroupDto>>(items)
            };
        }

        public virtual async Task<MaterialGroupDto> GetAsync(Guid id)
        {
            return ObjectMapper.Map<MaterialGroup, MaterialGroupDto>(await _materialGroupRepository.GetAsync(id));
        }

        [Authorize(V2Permissions.MaterialGroups.Delete)]
        public virtual async Task DeleteAsync(Guid id)
        {
            await _materialGroupRepository.DeleteAsync(id);
        }

        [Authorize(V2Permissions.MaterialGroups.Create)]
        public virtual async Task<MaterialGroupDto> CreateAsync(MaterialGroupCreateDto input)
        {

            var materialGroup = await _materialGroupManager.CreateAsync(
            input.MaterialName, input.DisCode
            );

            return ObjectMapper.Map<MaterialGroup, MaterialGroupDto>(materialGroup);
        }

        [Authorize(V2Permissions.MaterialGroups.Edit)]
        public virtual async Task<MaterialGroupDto> UpdateAsync(Guid id, MaterialGroupUpdateDto input)
        {

            var materialGroup = await _materialGroupManager.UpdateAsync(
            id,
            input.MaterialName, input.DisCode, input.ConcurrencyStamp
            );

            return ObjectMapper.Map<MaterialGroup, MaterialGroupDto>(materialGroup);
        }
    }
}