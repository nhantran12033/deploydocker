using MEVN.DistributorApp.V2.DistributorPurchaseOrderDetails;
using MEVN.DistributorApp.V2.ETADetails;
using MEVN.DistributorApp.V2.MaterialStockCheckings;
using MEVN.DistributorApp.V2.MaterialGroups;
using MEVN.DistributorApp.V2.Venders;
using MEVN.DistributorApp.V2.FreeOnShipMents;

using MEVN.DistributorApp.V2.DeliveryOrders;
using MEVN.DistributorApp.V2.listDistributorPurchaseOrderDetails;
using MEVN.DistributorApp.V2.DistributorPurchaseOrders;

using MEVN.DistributorApp.V2.ProjectDetails;

using MEVN.DistributorApp.V2.Projects;
using MEVN.DistributorApp.V2.Customers;
using System;
using MEVN.DistributorApp.V2.Shared;
using Volo.Abp.AutoMapper;
using MEVN.DistributorApp.V2.Distributors;
using AutoMapper;
using System.Collections.Generic;
namespace MEVN.DistributorApp.V2;

public class V2ApplicationAutoMapperProfile : Profile
{
    public V2ApplicationAutoMapperProfile()
    {
        /* You can configure your AutoMapper mapping configuration here.
         * Alternatively, you can split your mapping configurations
         * into multiple profile classes for a better organization. */

        CreateMap<Distributor, DistributorDto>();
        CreateMap<Distributor, DistributorExcelDto>();

        CreateMap<Customer, CustomerDto>();

        CreateMap<Project, ProjectDto>();
        CreateMap<Project, ProjectExcelDto>();
        CreateMap<ProjectWithNavigationProperties, ProjectWithNavigationPropertiesDto>();
        CreateMap<Distributor, LookupDto<Guid>>().ForMember(dest => dest.DisplayName, opt => opt.MapFrom(src => src.DisName));
        CreateMap<Customer, LookupDto<Guid>>().ForMember(dest => dest.DisplayName, opt => opt.MapFrom(src => src.CusName));
        CreateMap<ProjectDetailCreateDto, ProjectDetail>();
        CreateMap<ProjectDetailUpdateDto, ProjectDetail>();
        CreateMap<ProjectDetail, ProjectDetailDto>();

        CreateMap<FreeOnShipMent, FreeOnShipMentDto>();
        CreateMap<FreeOnShipMentDto, FreeOnShipMent>();
        CreateMap<FreeOnShipMentCreateDto, FreeOnShipMent>();
        CreateMap<Vender, VenderDto>();

        CreateMap<MaterialGroup, MaterialGroupDto>();

        CreateMap<MaterialStockChecking, MaterialStockCheckingDto>();
        CreateMap<MaterialStockChecking, MaterialStockCheckingExcelDto>();
        CreateMap<MaterialStockCheckingCreateDto, MaterialStockChecking>();
        CreateMap<MaterialStockCheckingWithNavigationProperties, MaterialStockCheckingWithNavigationPropertiesDto>();
        CreateMap<FreeOnShipMent, FreeOnShipMentDto>();
        CreateMap<MaterialGroup, LookupDto<Guid>>().ForMember(dest => dest.DisplayName, opt => opt.MapFrom(src => src.MaterialName));
        CreateMap<Vender, LookupDto<Guid>>().ForMember(dest => dest.DisplayName, opt => opt.MapFrom(src => src.VerderName));
        CreateMap<Distributor, LookupDto<Guid>>().ForMember(dest => dest.DisplayName, opt => opt.MapFrom(src => src.DisCode));

        CreateMap<DistributorPurchaseOrder, DistributorPurchaseOrderDto>();
        CreateMap<DistributorPurchaseOrderDetail, DistributorPurchaseOrderDetailDto>();
        CreateMap<DistributorPurchaseOrderCreateDto, DistributorPurchaseOrder>();
        CreateMap<DistributorPurchaseOrderDetailCreateDto, DistributorPurchaseOrderDetail>();
        CreateMap<DistributorPurchaseOrder, DistributorPurchaseOrderExcelDto>();
        CreateMap<DistributorPurchaseOrderWithNavigationProperties, DistributorPurchaseOrderWithNavigationPropertiesDto>();

        CreateMap<DistributorPurchaseOrder, LookupDto<Guid>>().ForMember(dest => dest.DisplayName, opt => opt.MapFrom(src => src.Id));

        CreateMap<DeliveryOrder, DeliveryOrderDto>();
        CreateMap<DeliveryOrderCreateDto, DeliveryOrder>();
        CreateMap<ETADetail, ETADetailDto>();
        CreateMap<ETADetailCreateDto, ETADetail>();
    }

}