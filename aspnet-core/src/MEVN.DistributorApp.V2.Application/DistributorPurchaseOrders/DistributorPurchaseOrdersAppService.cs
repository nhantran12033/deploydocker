using MEVN.DistributorApp.V2.Shared;
using MEVN.DistributorApp.V2.Distributors;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Microsoft.AspNetCore.Authorization;
using Volo.Abp;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;
using MEVN.DistributorApp.V2.Permissions;
using MEVN.DistributorApp.V2.DistributorPurchaseOrders;
using MiniExcelLibs;
using Volo.Abp.Content;
using Volo.Abp.Authorization;
using Volo.Abp.Caching;
using Microsoft.Extensions.Caching.Distributed;
using MEVN.DistributorApp.V2.Shared;
using MEVN.DistributorApp.V2.listDistributorPurchaseOrderDetails;
using Volo.Abp.ObjectMapping;
using System.Collections.Generic;
using Volo.Abp.Gdpr;
using MEVN.DistributorApp.V2.DistributorPurchaseOrderDetails;

namespace MEVN.DistributorApp.V2.DistributorPurchaseOrders
{
    [RemoteService(IsEnabled = false)]
    [Authorize(V2Permissions.DistributorPurchaseOrders.Default)]
    public abstract class DistributorPurchaseOrdersAppServiceBase : ApplicationService
    {
        protected IDistributedCache<DistributorPurchaseOrderExcelDownloadTokenCacheItem, string> _excelDownloadTokenCache;
        protected IDistributorPurchaseOrderRepository _distributorPurchaseOrderRepository;
        protected DistributorPurchaseOrderManager _distributorPurchaseOrderManager;
        protected IRepository<Distributor, Guid> _distributorRepository;

        public DistributorPurchaseOrdersAppServiceBase(IDistributorPurchaseOrderRepository distributorPurchaseOrderRepository, DistributorPurchaseOrderManager distributorPurchaseOrderManager, IDistributedCache<DistributorPurchaseOrderExcelDownloadTokenCacheItem, string> excelDownloadTokenCache, IRepository<Distributor, Guid> distributorRepository)
        {
            _excelDownloadTokenCache = excelDownloadTokenCache;
            _distributorPurchaseOrderRepository = distributorPurchaseOrderRepository;
            _distributorPurchaseOrderManager = distributorPurchaseOrderManager; _distributorRepository = distributorRepository;
        }
        public virtual async Task<PagedResultDto<DistributorPurchaseOrderDto>> GetListAsync(GetDistributorPurchaseOrdersInput input)
        {
            var totalCount = await _distributorPurchaseOrderRepository.GetCountAsync(input.DPONo, input.OrderDate, input.FA_LVS, input.StatusCode, input.DistributorId);
            var items = await _distributorPurchaseOrderRepository.GetListAsync(input.DPONo, input.OrderDate, input.FA_LVS,input.StatusCode, input.DistributorId,input.Sorting, input.MaxResultCount, input.SkipCount);

            return new PagedResultDto<DistributorPurchaseOrderDto>
            {
                TotalCount = totalCount,
                Items = ObjectMapper.Map<List<DistributorPurchaseOrder>, List<DistributorPurchaseOrderDto>>(items)
            };
        }
        public virtual async Task<DistributorPurchaseOrderWithNavigationPropertiesDto> GetWithNavigationPropertiesAsync(Guid id)
        {
            return ObjectMapper.Map<DistributorPurchaseOrderWithNavigationProperties, DistributorPurchaseOrderWithNavigationPropertiesDto>
                (await _distributorPurchaseOrderRepository.GetWithNavigationPropertiesAsync(id));
        }
        public virtual async Task<DistributorPurchaseOrderDto> GetAsync(Guid id)
        {
            var objectDistributorPurchaseOrder = await _distributorPurchaseOrderRepository.GetListAsync();
            var distributorPurchaseOrderList = objectDistributorPurchaseOrder.FirstOrDefault(x => x.Id.Equals(id));
            return ObjectMapper.Map<DistributorPurchaseOrder, DistributorPurchaseOrderDto>(distributorPurchaseOrderList);
        }

        public virtual async Task<PagedResultDto<LookupDto<Guid>>> GetDistributorLookupAsync(LookupRequestDto input)
        {
            var query = (await _distributorRepository.GetQueryableAsync())
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter),
                    x => x.DisCode != null &&
                         x.DisCode.Contains(input.Filter));

            var lookupData = await query.PageBy(input.SkipCount, input.MaxResultCount).ToDynamicListAsync<Distributor>();
            var totalCount = query.Count();
            return new PagedResultDto<LookupDto<Guid>>
            {
                TotalCount = totalCount,
                Items = ObjectMapper.Map<List<Distributor>, List<LookupDto<Guid>>>(lookupData)
            };
        }

        [Authorize(V2Permissions.DistributorPurchaseOrders.Delete)]
        public virtual async Task DeleteAsync(Guid id)
        {
            await _distributorPurchaseOrderRepository.DeleteAsync(id);
        }

        [Authorize(V2Permissions.DistributorPurchaseOrders.Create)]
        public virtual async Task<DistributorPurchaseOrderDto> CreateAsync(DistributorPurchaseOrderCreateDto input)
        {
            if (input.DistributorId == default)
            {
                throw new UserFriendlyException(L["DistributorPurchaseOrderDistributorIdCanNotBeEmpty"]);
            }

            var distributorPurchaseOrder = await _distributorPurchaseOrderManager.CreateAsync(
            input.DistributorId, ObjectMapper.Map<List<DistributorPurchaseOrderDetailCreateDto>, List<DistributorPurchaseOrderDetail>>(input.ListDistributorPurchaseOrderDetail), input.Status, input.DPONo, input.OrderDate, input.TotalAmount, input.FA_LVS, input.Note, input.PaymentInfo, input.ShipmentDate, input.UserName, input.SPUser, input.Remark, input.SPFileName, input.FileName, input.StatusCode, input.StockKeepingRequestNo, input.IsKeepStock, input.ValidDate
            );
            

            return ObjectMapper.Map<DistributorPurchaseOrder, DistributorPurchaseOrderDto>(distributorPurchaseOrder);

        }
        



    }
}