using System;

namespace MEVN.DistributorApp.V2.DistributorPurchaseOrders;

public abstract class DistributorPurchaseOrderExcelDownloadTokenCacheItemBase
{
    public string Token { get; set; } = null!;
}