﻿using MEVN.DistributorApp.V2.Localization;
using Volo.Abp.Application.Services;

namespace MEVN.DistributorApp.V2;

/* Inherit your application services from this class.
 */
public abstract class V2AppService : ApplicationService
{
    protected V2AppService()
    {
        LocalizationResource = typeof(V2Resource);
    }
}
