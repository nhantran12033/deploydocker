using System;

namespace MEVN.DistributorApp.V2.MaterialStockCheckings;

public abstract class MaterialStockCheckingExcelDownloadTokenCacheItemBase
{
    public string Token { get; set; } = null!;
}