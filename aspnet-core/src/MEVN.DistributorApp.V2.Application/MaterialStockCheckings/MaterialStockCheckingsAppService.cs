using MEVN.DistributorApp.V2.Shared;
using MEVN.DistributorApp.V2.Distributors;
using MEVN.DistributorApp.V2.Venders;
using MEVN.DistributorApp.V2.MaterialGroups;
using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Microsoft.AspNetCore.Authorization;
using Volo.Abp;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;
using MEVN.DistributorApp.V2.Permissions;
using MiniExcelLibs;
using Volo.Abp.Content;
using Volo.Abp.Authorization;
using Volo.Abp.Caching;
using Microsoft.Extensions.Caching.Distributed;
using MEVN.DistributorApp.V2.FreeOnShipMents;
using System.Reflection.PortableExecutable;
using Volo.Abp.Gdpr;

namespace MEVN.DistributorApp.V2.MaterialStockCheckings
{
    [RemoteService(IsEnabled = false)]
    [Authorize(V2Permissions.MaterialStockCheckings.Default)]
    public abstract class MaterialStockCheckingsAppServiceBase : ApplicationService
    {
        protected IDistributedCache<MaterialStockCheckingExcelDownloadTokenCacheItem, string> _excelDownloadTokenCache;
        protected IMaterialStockCheckingRepository _materialStockCheckingRepository;
        protected MaterialStockCheckingManager _materialStockCheckingManager;
        protected IRepository<MaterialGroup, Guid> _materialGroupRepository;
        protected IRepository<Vender, Guid> _venderRepository;
        protected IRepository<Distributor, Guid> _distributorRepository;

        public MaterialStockCheckingsAppServiceBase(IMaterialStockCheckingRepository materialStockCheckingRepository, MaterialStockCheckingManager materialStockCheckingManager, IDistributedCache<MaterialStockCheckingExcelDownloadTokenCacheItem, string> excelDownloadTokenCache, IRepository<MaterialGroup, Guid> materialGroupRepository, IRepository<Vender, Guid> venderRepository, IRepository<Distributor, Guid> distributorRepository)
        {
            _excelDownloadTokenCache = excelDownloadTokenCache;
            _materialStockCheckingRepository = materialStockCheckingRepository;
            _materialStockCheckingManager = materialStockCheckingManager; 
            _materialGroupRepository = materialGroupRepository;
            _venderRepository = venderRepository;
            _distributorRepository = distributorRepository;
        }

        public virtual async Task<PagedResultDto<MaterialStockCheckingDto>> GetListAsync(GetMaterialStockCheckingsInput input)
        {
            var totalCount = await _materialStockCheckingRepository.GetCountAsync(input.GolfaCode, input.Model, input.MaterialGroupId, input.VenderId, input.DistributorId);
            var items = await _materialStockCheckingRepository.GetListAsync(input.GolfaCode, input.Model, input.VenderId, input.MaterialGroupId, input.DistributorId, input.Sorting, input.MaxResultCount, input.SkipCount);

            return new PagedResultDto<MaterialStockCheckingDto>
            {
                TotalCount = totalCount,
                Items = ObjectMapper.Map<List<MaterialStockChecking>, List<MaterialStockCheckingDto>>(items)
            };
        }

        public virtual async Task<MaterialStockCheckingDto> GetAsync(Guid id)
        {
            return ObjectMapper.Map<MaterialStockChecking, MaterialStockCheckingDto>(await _materialStockCheckingRepository.GetAsync(id));
        }

        public virtual async Task<PagedResultDto<LookupDto<Guid>>> GetMaterialGroupLookupAsync(LookupRequestDto input)
        {
            var query = (await _materialGroupRepository.GetQueryableAsync())
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter),
                    x => x.MaterialName != null &&
                         x.MaterialName.Contains(input.Filter));

            var lookupData = await query.PageBy(input.SkipCount, input.MaxResultCount).ToDynamicListAsync<MaterialGroup>();
            var totalCount = query.Count();
            return new PagedResultDto<LookupDto<Guid>>
            {
                TotalCount = totalCount,
                Items = ObjectMapper.Map<List<MaterialGroup>, List<LookupDto<Guid>>>(lookupData)
            };
        }

        public virtual async Task<PagedResultDto<LookupDto<Guid>>> GetVenderLookupAsync(LookupRequestDto input)
        {
            var query = (await _venderRepository.GetQueryableAsync())
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter),
                    x => x.VerderName != null &&
                         x.VerderName.Contains(input.Filter));

            var lookupData = await query.PageBy(input.SkipCount, input.MaxResultCount).ToDynamicListAsync<Vender>();
            var totalCount = query.Count();
            return new PagedResultDto<LookupDto<Guid>>
            {
                TotalCount = totalCount,
                Items = ObjectMapper.Map<List<Vender>, List<LookupDto<Guid>>>(lookupData)
            };
        }

        public virtual async Task<PagedResultDto<LookupDto<Guid>>> GetDistributorLookupAsync(LookupRequestDto input)
        {
            var query = (await _distributorRepository.GetQueryableAsync())
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter),
                    x => x.DisCode != null &&
                         x.DisCode.Contains(input.Filter));

            var lookupData = await query.PageBy(input.SkipCount, input.MaxResultCount).ToDynamicListAsync<Distributor>();
            var totalCount = query.Count();
            return new PagedResultDto<LookupDto<Guid>>
            {
                TotalCount = totalCount,
                Items = ObjectMapper.Map<List<Distributor>, List<LookupDto<Guid>>>(lookupData)
            };
        }

        [Authorize(V2Permissions.MaterialStockCheckings.Delete)]

        public virtual async Task DeleteAsync(Guid id)
        {
            await _materialStockCheckingRepository.DeleteAsync(id);
        }

        [Authorize(V2Permissions.MaterialStockCheckings.Create)]

        public virtual async Task<MaterialStockCheckingDto> CreateAsync(MaterialStockCheckingCreateDto input)
        {
            if (input.MaterialGroupId == default)
            {
                throw new UserFriendlyException(L["The {0} field is required.", L["MaterialGroup"]]);
            }
            if (input.VenderId == default)
            {
                throw new UserFriendlyException(L["The {0} field is required.", L["Vender"]]);
            }

            var materialStockChecking = await _materialStockCheckingManager.CreateAsync(
            input.MaterialGroupId, input.VenderId, input.DistributorId, ObjectMapper.Map<List<FreeOnShipMentCreateDto>, List<FreeOnShipMent>>(input.ListFreeOnShipMent),input.GolfaCode, input.Model, input.Description_VN, input.Standard_Price, input.SAP_Code, input.Description_Group, input.StockValueWarning, input.StockTmp, input.Spec1, input.Spec2
            );

            return ObjectMapper.Map<MaterialStockChecking, MaterialStockCheckingDto>(materialStockChecking);
        }

        public virtual async Task<IRemoteStreamContent> GetListAsExcelFileAsync(MaterialStockCheckingExcelDownloadDto input)
        {
            var downloadToken = await _excelDownloadTokenCache.GetAsync(input.DownloadToken);
            if (downloadToken == null || input.DownloadToken != downloadToken.Token)
            {
                throw new AbpAuthorizationException("Invalid download token: " + input.DownloadToken);
            }

            var materialStockCheckings = await _materialStockCheckingRepository.GetListWithNavigationPropertiesAsync(input.GolfaCode, input.Model,  input.MaterialGroupId, input.VenderId, input.DistributorId);
            var items = materialStockCheckings.Select(item => new
            {
                GolfaCode = item.MaterialStockChecking.GolfaCode,
                Model = item.MaterialStockChecking.Model,
                SAP_Code = item.MaterialStockChecking.SAP_Code,
                Description_VN = item.MaterialStockChecking.Description_VN,
                Description_Group = item.MaterialStockChecking.Description_Group,
                Standard_Price = item.MaterialStockChecking.Standard_Price,
                StockValueWarning = item.MaterialStockChecking.StockValueWarning,
                StockTmp = item.MaterialStockChecking.StockTmp,
                Spec1 = item.MaterialStockChecking.Spec1,
                Spec2 = item.MaterialStockChecking.Spec2,
                
                MaterialGroup = item.MaterialGroup?.MaterialName,
                Vender = item.Vender?.VerderName,
                Distributor = item.Distributor?.DisCode,
                FreeOnShipMentDetails = string.Join(", ", item.FreeOnShipMent.Select(fosm => $"{fosm.PONo} - {fosm.PODate} - {fosm.Qty} - {fosm.MachineNumber}"))

            });

            var memoryStream = new MemoryStream();
            await memoryStream.SaveAsAsync(items);
            memoryStream.Seek(0, SeekOrigin.Begin);

            return new RemoteStreamContent(memoryStream, "MaterialStockCheckings.xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        }
        [AllowAnonymous]
        public virtual async Task<DownloadTokenResultDto> GetDownloadTokenAsync()
        {
            var token = Guid.NewGuid().ToString("N");

            await _excelDownloadTokenCache.SetAsync(
                token,
                new MaterialStockCheckingExcelDownloadTokenCacheItem { Token = token },
                new DistributedCacheEntryOptions
                {
                    AbsoluteExpirationRelativeToNow = TimeSpan.FromSeconds(30)
                });

            return new DownloadTokenResultDto
            {
                Token = token
            };
        }
    }
}