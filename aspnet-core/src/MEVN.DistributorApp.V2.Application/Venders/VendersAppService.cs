using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Microsoft.AspNetCore.Authorization;
using Volo.Abp;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;
using MEVN.DistributorApp.V2.Permissions;
using MEVN.DistributorApp.V2.Venders;

namespace MEVN.DistributorApp.V2.Venders
{
    [RemoteService(IsEnabled = false)]
    [Authorize(V2Permissions.Venders.Default)]
    public abstract class VendersAppServiceBase : ApplicationService
    {

        protected IVenderRepository _venderRepository;
        protected VenderManager _venderManager;

        public VendersAppServiceBase(IVenderRepository venderRepository, VenderManager venderManager)
        {

            _venderRepository = venderRepository;
            _venderManager = venderManager;
        }

        public virtual async Task<PagedResultDto<VenderDto>> GetListAsync(GetVendersInput input)
        {
            var totalCount = await _venderRepository.GetCountAsync(input.FilterText, input.VerderName, input.Address, input.Email);
            var items = await _venderRepository.GetListAsync(input.FilterText, input.VerderName, input.Address, input.Email, input.Sorting, input.MaxResultCount, input.SkipCount);

            return new PagedResultDto<VenderDto>
            {
                TotalCount = totalCount,
                Items = ObjectMapper.Map<List<Vender>, List<VenderDto>>(items)
            };
        }

        public virtual async Task<VenderDto> GetAsync(Guid id)
        {
            return ObjectMapper.Map<Vender, VenderDto>(await _venderRepository.GetAsync(id));
        }

        [Authorize(V2Permissions.Venders.Delete)]
        public virtual async Task DeleteAsync(Guid id)
        {
            await _venderRepository.DeleteAsync(id);
        }

        [Authorize(V2Permissions.Venders.Create)]
        public virtual async Task<VenderDto> CreateAsync(VenderCreateDto input)
        {

            var vender = await _venderManager.CreateAsync(
            input.VerderName, input.Address, input.Email
            );

            return ObjectMapper.Map<Vender, VenderDto>(vender);
        }

        [Authorize(V2Permissions.Venders.Edit)]
        public virtual async Task<VenderDto> UpdateAsync(Guid id, VenderUpdateDto input)
        {

            var vender = await _venderManager.UpdateAsync(
            id,
            input.VerderName, input.Address, input.Email, input.ConcurrencyStamp
            );

            return ObjectMapper.Map<Vender, VenderDto>(vender);
        }
    }
}