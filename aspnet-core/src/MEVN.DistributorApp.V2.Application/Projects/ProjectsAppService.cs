using MEVN.DistributorApp.V2.Shared;
using MEVN.DistributorApp.V2.Customers;
using MEVN.DistributorApp.V2.Distributors;
using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Microsoft.AspNetCore.Authorization;
using Volo.Abp;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;
using MEVN.DistributorApp.V2.Permissions;
using MEVN.DistributorApp.V2.Projects;
using MiniExcelLibs;
using Volo.Abp.Content;
using Volo.Abp.Authorization;
using Volo.Abp.Caching;
using Microsoft.Extensions.Caching.Distributed;
using Volo.Abp.Gdpr;
using Volo.Abp.ObjectMapping;
using MEVN.DistributorApp.V2.ProjectDetails;


namespace MEVN.DistributorApp.V2.Projects
{
    [RemoteService(IsEnabled = false)]
    [Authorize(V2Permissions.Projects.Default)]
    public abstract class ProjectsAppServiceBase : ApplicationService
    {
        protected IDistributedCache<ProjectExcelDownloadTokenCacheItem, string> _excelDownloadTokenCache;
        protected IProjectRepository _projectRepository;
        protected ProjectManager _projectManager;
        protected IRepository<Distributor, Guid> _distributorRepository;
        protected IRepository<Customer, Guid> _customerRepository;

        public ProjectsAppServiceBase(IProjectRepository projectRepository, ProjectManager projectManager, IDistributedCache<ProjectExcelDownloadTokenCacheItem, string> excelDownloadTokenCache, IRepository<Distributor, Guid> distributorRepository, IRepository<Customer, Guid> customerRepository)
        {
            _excelDownloadTokenCache = excelDownloadTokenCache;
            _projectRepository = projectRepository;
            _projectManager = projectManager; _distributorRepository = distributorRepository;
            _customerRepository = customerRepository;
        }
    
        public virtual async Task<PagedResultDto<ProjectDto>> GetListAsync(GetProjectsInput input)
        {
            var totalCount = await _projectRepository.GetCountAsync(input.ProjectCode, input.ProjectName, input.PODateMin, input.PODateMax, input.DistributorId);
            var items = await _projectRepository.GetListAsync(input.ProjectCode, input.ProjectName, input.PODateMin, input.PODateMax, input.DistributorId);

            return new PagedResultDto<ProjectDto>
            {
                TotalCount = totalCount,
                Items = ObjectMapper.Map<List<Project>, List<ProjectDto>>(items)
            };
        }
  
        public virtual async Task<ProjectWithNavigationPropertiesDto> GetWithNavigationPropertiesAsync(Guid id)
        {
            return ObjectMapper.Map<ProjectWithNavigationProperties, ProjectWithNavigationPropertiesDto>
                (await _projectRepository.GetWithNavigationPropertiesAsync(id));
        }

        public virtual async Task<ProjectDto> GetAsync(Guid id)
        {
            var objectProject = await _projectRepository.GetListAsync();
            var projectList = objectProject.FirstOrDefault(e => e.Id.Equals(id));
            return ObjectMapper.Map<Project, ProjectDto>(projectList);

        }
    
        public virtual async Task<PagedResultDto<LookupDto<Guid>>> GetDistributorLookupAsync(LookupRequestDto input)
        {
            var query = (await _distributorRepository.GetQueryableAsync())
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter),
                    x => x.DisName != null &&
                         x.DisName.Contains(input.Filter));

            var lookupData = await query.PageBy(input.SkipCount, input.MaxResultCount).ToDynamicListAsync<Distributor>();
            var totalCount = query.Count();
            return new PagedResultDto<LookupDto<Guid>>
            {
                TotalCount = totalCount,
                Items = ObjectMapper.Map<List<Distributor>, List<LookupDto<Guid>>>(lookupData)
            };
        }
        
        public virtual async Task<PagedResultDto<LookupDto<Guid>>> GetCustomerLookupAsync(LookupRequestDto input)
        {
            var query = (await _customerRepository.GetQueryableAsync())
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter),
                    x => x.CusName != null &&
                         x.CusName.Contains(input.Filter));

            var lookupData = await query.PageBy(input.SkipCount, input.MaxResultCount).ToDynamicListAsync<Customer>();
            var totalCount = query.Count();
            return new PagedResultDto<LookupDto<Guid>>
            {
                TotalCount = totalCount,
                Items = ObjectMapper.Map<List<Customer>, List<LookupDto<Guid>>>(lookupData)
            };
        }

        [Authorize(V2Permissions.Projects.Delete)]
        public virtual async Task DeleteAsync(Guid id)
        {
            await _projectRepository.DeleteAsync(id);
        }

        [Authorize(V2Permissions.Projects.Create)]
        public virtual async Task<ProjectDto> CreateAsync(ProjectCreateDto input)
        {
            if (input.DistributorId == default)
            {
                throw new UserFriendlyException(L["The {0} field is required.", L["Distributor"]]);
            }

            var project = await _projectManager.CreateAsync(
            input.DistributorId, input.CustomerId , ObjectMapper.Map<List<ProjectDetailCreateDto>, List<ProjectDetail>>(input.ListProjectDetail),input.ProjectCode, input.ProjectName, input.ApprovalStatus, input.AccountNo, input.ProjectType, input.Location, input.DistributorMagin, input.PODate
            );
            return ObjectMapper.Map<Project, ProjectDto>(project);
        }

        public virtual async Task<IRemoteStreamContent> GetListAsExcelFileAsync(ProjectExcelDownloadDto input)
        {
            var downloadToken = await _excelDownloadTokenCache.GetAsync(input.DownloadToken);
            if (downloadToken == null || input.DownloadToken != downloadToken.Token)
            {
                throw new AbpAuthorizationException("Invalid download token: " + input.DownloadToken);
            }

            var projects = await _projectRepository.GetListWithNavigationPropertiesAsync(input.Id);
            var items = projects.Select(item => new
            {
                ProjectCode = item.Project.ProjectCode,
                ProjectName = item.Project.ProjectName,
                ApprovalStatus = item.Project.ApprovalStatus,
                AccountNo = item.Project.AccountNo,
                ProjectType = item.Project.ProjectType,
                Location = item.Project.Location,
                DistributorMagin = item.Project.DistributorMagin,
                PODate = item.Project.PODate,

                Distributor = item.Distributor?.DisName,
                Customer = item.Customer?.CusName,
                ProjectDetail = string.Join(", ", item.ProjectDetail.Select(fosm => $"{fosm.StatusCode} - {fosm.RequestStatus} - {fosm.GolfaCode} - {fosm.Model} - {fosm.Qty} - {fosm.AmountRequestedPrice}"))
            });

            var memoryStream = new MemoryStream();
            await memoryStream.SaveAsAsync(items);
            memoryStream.Seek(0, SeekOrigin.Begin);

            return new RemoteStreamContent(memoryStream, "Projects.xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        }
        [AllowAnonymous]
        public virtual async Task<DownloadTokenResultDto> GetDownloadTokenAsync()
        {
            var token = Guid.NewGuid().ToString("N");

            await _excelDownloadTokenCache.SetAsync(
                token,
                new ProjectExcelDownloadTokenCacheItem { Token = token },
                new DistributedCacheEntryOptions
                {
                    AbsoluteExpirationRelativeToNow = TimeSpan.FromSeconds(30)
                });

            return new DownloadTokenResultDto
            {
                Token = token
            };
        }
    }
}