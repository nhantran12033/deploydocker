using System;

namespace MEVN.DistributorApp.V2.Projects;

public abstract class ProjectExcelDownloadTokenCacheItemBase
{
    public string Token { get; set; } = null!;
}