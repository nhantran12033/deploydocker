﻿using System;

namespace MEVN.DistributorApp.V2.Models.Test;

public class TestModel
{
    public string? Name { get; set; }

    public DateTime BirthDate { get; set; }
}
