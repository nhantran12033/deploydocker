using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.Application.Dtos;
using MEVN.DistributorApp.V2.Distributors;

namespace MEVN.DistributorApp.V2.Controllers.Distributors
{
    [RemoteService]
    [Area("app")]
    [ControllerName("Distributor")]
    [Route("api/app/distributors")]

    public abstract class DistributorControllerBase : AbpController
    {
        protected IDistributorsAppService _distributorsAppService;

        public DistributorControllerBase(IDistributorsAppService distributorsAppService)
        {
            _distributorsAppService = distributorsAppService;
        }

        [HttpGet]
        public virtual Task<PagedResultDto<DistributorDto>> GetListAsync(GetDistributorsInput input)
        {
            return _distributorsAppService.GetListAsync(input);
        }

        [HttpGet]
        [Route("{id}")]
        public virtual Task<DistributorDto> GetAsync(Guid id)
        {
            return _distributorsAppService.GetAsync(id);
        }

        [HttpPost]
        public virtual Task<DistributorDto> CreateAsync(DistributorCreateDto input)
        {
            return _distributorsAppService.CreateAsync(input);
        }

        [HttpPut]
        [Route("{id}")]
        public virtual Task<DistributorDto> UpdateAsync(Guid id, DistributorUpdateDto input)
        {
            return _distributorsAppService.UpdateAsync(id, input);
        }

        [HttpDelete]
        [Route("{id}")]
        public virtual Task DeleteAsync(Guid id)
        {
            return _distributorsAppService.DeleteAsync(id);
        }
    }
}