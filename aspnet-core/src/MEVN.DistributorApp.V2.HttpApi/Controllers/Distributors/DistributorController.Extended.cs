using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.Application.Dtos;
using MEVN.DistributorApp.V2.Distributors;

namespace MEVN.DistributorApp.V2.Controllers.Distributors
{
    [RemoteService]
    [Area("app")]
    [ControllerName("Distributor")]
    [Route("api/app/distributors")]

    public class DistributorController : DistributorControllerBase, IDistributorsAppService
    {
        public DistributorController(IDistributorsAppService distributorsAppService) : base(distributorsAppService)
        {
        }
    }
}