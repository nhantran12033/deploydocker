using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.Application.Dtos;
using MEVN.DistributorApp.V2.Projects;

namespace MEVN.DistributorApp.V2.Controllers.Projects
{
    [RemoteService]
    [Area("app")]
    [ControllerName("Project")]
    [Route("api/app/projects")]

    public class ProjectController : ProjectControllerBase
    {
        public ProjectController(IProjectsAppService projectsAppService) : base(projectsAppService)
        {
        }
    }
}