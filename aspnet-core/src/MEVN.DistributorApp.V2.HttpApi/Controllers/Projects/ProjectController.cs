using MEVN.DistributorApp.V2.Shared;
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.Application.Dtos;
using MEVN.DistributorApp.V2.Projects;
using Volo.Abp.Content;
using MEVN.DistributorApp.V2.Shared;
using System.Collections.Generic;
using Volo.Abp.Gdpr;

namespace MEVN.DistributorApp.V2.Controllers.Projects
{
    [RemoteService]
    [Area("app")]
    [ControllerName("Project")]
    [Route("api/app/projects")]

    public abstract class ProjectControllerBase : AbpController, IProjectsAppService
    {
        protected IProjectsAppService _projectsAppService;

        public ProjectControllerBase(IProjectsAppService projectsAppService)
        {
            _projectsAppService = projectsAppService;
        }

        [HttpGet]
        public virtual Task<PagedResultDto<ProjectDto>> GetListAsync(GetProjectsInput input)
        {
            return _projectsAppService.GetListAsync(input);
        }

        [HttpGet]
        [Route("with-navigation-properties/{id}")]
        public virtual Task<ProjectWithNavigationPropertiesDto> GetWithNavigationPropertiesAsync(Guid id)
        {
            return _projectsAppService.GetWithNavigationPropertiesAsync(id);
        }

        [HttpGet]
        [Route("{id}")]
        public virtual Task<ProjectDto> GetAsync(Guid id)
        {
            return _projectsAppService.GetAsync(id);
        }

        [HttpGet]
        [Route("distributor-lookup")]
        public virtual Task<PagedResultDto<LookupDto<Guid>>> GetDistributorLookupAsync(LookupRequestDto input)
        {
            return _projectsAppService.GetDistributorLookupAsync(input);
        }

        [HttpGet]
        [Route("customer-lookup")]
        public virtual Task<PagedResultDto<LookupDto<Guid>>> GetCustomerLookupAsync(LookupRequestDto input)
        {
            return _projectsAppService.GetCustomerLookupAsync(input);
        }

        [HttpPost]
        public virtual Task<ProjectDto> CreateAsync(ProjectCreateDto input)
        {
            return _projectsAppService.CreateAsync(input);
        }
        [HttpDelete]
        [Route("{id}")]
        public virtual Task DeleteAsync(Guid id)
        {
            return _projectsAppService.DeleteAsync(id);
        }

        [HttpGet]
        [Route("as-excel-file")]
        public virtual Task<IRemoteStreamContent> GetListAsExcelFileAsync(ProjectExcelDownloadDto input)
        {
            return _projectsAppService.GetListAsExcelFileAsync(input);
        }

        [HttpGet]
        [Route("download-token")]
        public virtual Task<DownloadTokenResultDto> GetDownloadTokenAsync()
        {
            return _projectsAppService.GetDownloadTokenAsync();
        }
    }
}