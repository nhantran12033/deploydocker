using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.Application.Dtos;
using MEVN.DistributorApp.V2.DistributorPurchaseOrders;

namespace MEVN.DistributorApp.V2.Controllers.DistributorPurchaseOrders
{
    [RemoteService]
    [Area("app")]
    [ControllerName("DistributorPurchaseOrder")]
    [Route("api/app/distributor-purchase-orders")]

    public class DistributorPurchaseOrderController : DistributorPurchaseOrderControllerBase, IDistributorPurchaseOrdersAppService
    {
        public DistributorPurchaseOrderController(IDistributorPurchaseOrdersAppService distributorPurchaseOrdersAppService) : base(distributorPurchaseOrdersAppService)
        {
        }
    }
}