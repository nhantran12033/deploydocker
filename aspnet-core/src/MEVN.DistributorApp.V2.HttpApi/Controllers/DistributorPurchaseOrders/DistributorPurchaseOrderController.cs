using MEVN.DistributorApp.V2.Shared;
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.Application.Dtos;
using MEVN.DistributorApp.V2.DistributorPurchaseOrders;
using Volo.Abp.Content;
using MEVN.DistributorApp.V2.Shared;
using Volo.Abp.Gdpr;
using System.Collections.Generic;

namespace MEVN.DistributorApp.V2.Controllers.DistributorPurchaseOrders
{
    [RemoteService]
    [Area("app")]
    [ControllerName("DistributorPurchaseOrder")]
    [Route("api/app/distributor-purchase-orders")]

    public abstract class DistributorPurchaseOrderControllerBase : AbpController
    {
        protected IDistributorPurchaseOrdersAppService _distributorPurchaseOrdersAppService;


        public DistributorPurchaseOrderControllerBase(IDistributorPurchaseOrdersAppService distributorPurchaseOrdersAppService)
        {
            _distributorPurchaseOrdersAppService = distributorPurchaseOrdersAppService;
        }

        [HttpGet]
        public virtual Task<PagedResultDto<DistributorPurchaseOrderDto>> GetListAsync(GetDistributorPurchaseOrdersInput input)
        {
            return _distributorPurchaseOrdersAppService.GetListAsync(input);
        }

        [HttpGet]
        [Route("with-navigation-properties/{id}")]
        public virtual Task<DistributorPurchaseOrderWithNavigationPropertiesDto> GetWithNavigationPropertiesAsync(Guid id)
        {
            return _distributorPurchaseOrdersAppService.GetWithNavigationPropertiesAsync(id);
        }

        [HttpGet]
        [Route("{id}")]
        public virtual Task<DistributorPurchaseOrderDto> GetAsync(Guid id)
        {
            return _distributorPurchaseOrdersAppService.GetAsync(id);
        }

        [HttpGet]
        [Route("distributor-lookup")]
        public virtual Task<PagedResultDto<LookupDto<Guid>>> GetDistributorLookupAsync(LookupRequestDto input)
        {
            return _distributorPurchaseOrdersAppService.GetDistributorLookupAsync(input);
        }

        [HttpPost]
        public virtual Task<DistributorPurchaseOrderDto> CreateAsync(DistributorPurchaseOrderCreateDto input)
        {
            return _distributorPurchaseOrdersAppService.CreateAsync(input);
        }

       

        [HttpDelete]
        [Route("{id}")]
        public virtual Task DeleteAsync(Guid id)
        {
            return _distributorPurchaseOrdersAppService.DeleteAsync(id);
        }
       

    }
}