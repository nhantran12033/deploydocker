﻿using MEVN.DistributorApp.V2.Localization;
using Volo.Abp.AspNetCore.Mvc;

namespace MEVN.DistributorApp.V2.Controllers;

/* Inherit your controllers from this class.
 */
public abstract class V2Controller : AbpControllerBase
{
    protected V2Controller()
    {
        LocalizationResource = typeof(V2Resource);
    }
}
