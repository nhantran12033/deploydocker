using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.Application.Dtos;
using MEVN.DistributorApp.V2.Venders;

namespace MEVN.DistributorApp.V2.Controllers.Venders
{
    [RemoteService]
    [Area("app")]
    [ControllerName("Vender")]
    [Route("api/app/venders")]

    public abstract class VenderControllerBase : AbpController
    {
        protected IVendersAppService _vendersAppService;

        public VenderControllerBase(IVendersAppService vendersAppService)
        {
            _vendersAppService = vendersAppService;
        }

        [HttpGet]
        public virtual Task<PagedResultDto<VenderDto>> GetListAsync(GetVendersInput input)
        {
            return _vendersAppService.GetListAsync(input);
        }

        [HttpGet]
        [Route("{id}")]
        public virtual Task<VenderDto> GetAsync(Guid id)
        {
            return _vendersAppService.GetAsync(id);
        }

        [HttpPost]
        public virtual Task<VenderDto> CreateAsync(VenderCreateDto input)
        {
            return _vendersAppService.CreateAsync(input);
        }

        [HttpPut]
        [Route("{id}")]
        public virtual Task<VenderDto> UpdateAsync(Guid id, VenderUpdateDto input)
        {
            return _vendersAppService.UpdateAsync(id, input);
        }

        [HttpDelete]
        [Route("{id}")]
        public virtual Task DeleteAsync(Guid id)
        {
            return _vendersAppService.DeleteAsync(id);
        }
    }
}