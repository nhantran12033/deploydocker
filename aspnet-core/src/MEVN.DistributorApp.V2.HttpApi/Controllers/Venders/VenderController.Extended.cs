using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.Application.Dtos;
using MEVN.DistributorApp.V2.Venders;

namespace MEVN.DistributorApp.V2.Controllers.Venders
{
    [RemoteService]
    [Area("app")]
    [ControllerName("Vender")]
    [Route("api/app/venders")]

    public class VenderController : VenderControllerBase, IVendersAppService
    {
        public VenderController(IVendersAppService vendersAppService) : base(vendersAppService)
        {
        }
    }
}