using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.Application.Dtos;
using MEVN.DistributorApp.V2.Customers;

namespace MEVN.DistributorApp.V2.Controllers.Customers
{
    [RemoteService]
    [Area("app")]
    [ControllerName("Customer")]
    [Route("api/app/customers")]

    public abstract class CustomerControllerBase : AbpController
    {
        protected ICustomersAppService _customersAppService;

        public CustomerControllerBase(ICustomersAppService customersAppService)
        {
            _customersAppService = customersAppService;
        }

        [HttpGet]
        public virtual Task<PagedResultDto<CustomerDto>> GetListAsync(GetCustomersInput input)
        {
            return _customersAppService.GetListAsync(input);
        }

        [HttpGet]
        [Route("{id}")]
        public virtual Task<CustomerDto> GetAsync(Guid id)
        {
            return _customersAppService.GetAsync(id);
        }

        [HttpPost]
        public virtual Task<CustomerDto> CreateAsync(CustomerCreateDto input)
        {
            return _customersAppService.CreateAsync(input);
        }

        [HttpPut]
        [Route("{id}")]
        public virtual Task<CustomerDto> UpdateAsync(Guid id, CustomerUpdateDto input)
        {
            return _customersAppService.UpdateAsync(id, input);
        }

        [HttpDelete]
        [Route("{id}")]
        public virtual Task DeleteAsync(Guid id)
        {
            return _customersAppService.DeleteAsync(id);
        }
    }
}