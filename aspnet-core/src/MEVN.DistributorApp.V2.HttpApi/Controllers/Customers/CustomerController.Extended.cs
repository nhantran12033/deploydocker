using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.Application.Dtos;
using MEVN.DistributorApp.V2.Customers;

namespace MEVN.DistributorApp.V2.Controllers.Customers
{
    [RemoteService]
    [Area("app")]
    [ControllerName("Customer")]
    [Route("api/app/customers")]

    public class CustomerController : CustomerControllerBase, ICustomersAppService
    {
        public CustomerController(ICustomersAppService customersAppService) : base(customersAppService)
        {
        }
    }
}