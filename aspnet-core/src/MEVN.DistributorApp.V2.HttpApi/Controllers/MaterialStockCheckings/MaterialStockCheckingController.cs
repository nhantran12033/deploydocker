using MEVN.DistributorApp.V2.Shared;
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.Application.Dtos;
using MEVN.DistributorApp.V2.MaterialStockCheckings;
using Volo.Abp.Content;
using MEVN.DistributorApp.V2.Shared;
using Volo.Abp.Gdpr;

namespace MEVN.DistributorApp.V2.Controllers.MaterialStockCheckings
{
    [RemoteService]
    [Area("app")]
    [ControllerName("MaterialStockChecking")]
    [Route("api/app/material-stock-checkings")]

    public abstract class MaterialStockCheckingControllerBase : AbpController
    {
        protected IMaterialStockCheckingsAppService _materialStockCheckingsAppService;

        public MaterialStockCheckingControllerBase(IMaterialStockCheckingsAppService materialStockCheckingsAppService)
        {
            _materialStockCheckingsAppService = materialStockCheckingsAppService;
        }

        [HttpGet]
        public virtual Task<PagedResultDto<MaterialStockCheckingDto>> GetListAsync(GetMaterialStockCheckingsInput input)
        {
            return _materialStockCheckingsAppService.GetListAsync(input);
        }

        [HttpGet]
        [Route("{id}")]
        public virtual Task<MaterialStockCheckingDto> GetAsync(Guid id)
        {
            return _materialStockCheckingsAppService.GetAsync(id);
        }

        [HttpGet]
        [Route("material-group-lookup")]
        public virtual Task<PagedResultDto<LookupDto<Guid>>> GetMaterialGroupLookupAsync(LookupRequestDto input)
        {
            return _materialStockCheckingsAppService.GetMaterialGroupLookupAsync(input);
        }

        [HttpGet]
        [Route("vender-lookup")]
        public virtual Task<PagedResultDto<LookupDto<Guid>>> GetVenderLookupAsync(LookupRequestDto input)
        {
            return _materialStockCheckingsAppService.GetVenderLookupAsync(input);
        }

        [HttpGet]
        [Route("distributor-lookup")]
        public virtual Task<PagedResultDto<LookupDto<Guid>>> GetDistributorLookupAsync(LookupRequestDto input)
        {
            return _materialStockCheckingsAppService.GetDistributorLookupAsync(input);
        }

        [HttpPost]
        public virtual Task<MaterialStockCheckingDto> CreateAsync(MaterialStockCheckingCreateDto input)
        {
            return _materialStockCheckingsAppService.CreateAsync(input);
        }

        

        [HttpDelete]
        [Route("{id}")]
        public virtual Task DeleteAsync(Guid id)
        {
            return _materialStockCheckingsAppService.DeleteAsync(id);
        }

        [HttpGet]
        [Route("as-excel-file")]
        public virtual Task<IRemoteStreamContent> GetListAsExcelFileAsync(MaterialStockCheckingExcelDownloadDto input)
        {
            return _materialStockCheckingsAppService.GetListAsExcelFileAsync(input);
        }

        [HttpGet]
        [Route("download-token")]
        public virtual Task<DownloadTokenResultDto> GetDownloadTokenAsync()
        {
            return _materialStockCheckingsAppService.GetDownloadTokenAsync();
        }
    }
}