using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.Application.Dtos;
using MEVN.DistributorApp.V2.MaterialStockCheckings;

namespace MEVN.DistributorApp.V2.Controllers.MaterialStockCheckings
{
    [RemoteService]
    [Area("app")]
    [ControllerName("MaterialStockChecking")]
    [Route("api/app/material-stock-checkings")]

    public class MaterialStockCheckingController : MaterialStockCheckingControllerBase, IMaterialStockCheckingsAppService
    {
        public MaterialStockCheckingController(IMaterialStockCheckingsAppService materialStockCheckingsAppService) : base(materialStockCheckingsAppService)
        {
        }
    }
}