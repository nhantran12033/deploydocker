using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.Application.Dtos;
using MEVN.DistributorApp.V2.MaterialGroups;

namespace MEVN.DistributorApp.V2.Controllers.MaterialGroups
{
    [RemoteService]
    [Area("app")]
    [ControllerName("MaterialGroup")]
    [Route("api/app/material-groups")]

    public class MaterialGroupController : MaterialGroupControllerBase, IMaterialGroupsAppService
    {
        public MaterialGroupController(IMaterialGroupsAppService materialGroupsAppService) : base(materialGroupsAppService)
        {
        }
    }
}