using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.Application.Dtos;
using MEVN.DistributorApp.V2.MaterialGroups;

namespace MEVN.DistributorApp.V2.Controllers.MaterialGroups
{
    [RemoteService]
    [Area("app")]
    [ControllerName("MaterialGroup")]
    [Route("api/app/material-groups")]

    public abstract class MaterialGroupControllerBase : AbpController
    {
        protected IMaterialGroupsAppService _materialGroupsAppService;

        public MaterialGroupControllerBase(IMaterialGroupsAppService materialGroupsAppService)
        {
            _materialGroupsAppService = materialGroupsAppService;
        }

        [HttpGet]
        public virtual Task<PagedResultDto<MaterialGroupDto>> GetListAsync(GetMaterialGroupsInput input)
        {
            return _materialGroupsAppService.GetListAsync(input);
        }

        [HttpGet]
        [Route("{id}")]
        public virtual Task<MaterialGroupDto> GetAsync(Guid id)
        {
            return _materialGroupsAppService.GetAsync(id);
        }

        [HttpPost]
        public virtual Task<MaterialGroupDto> CreateAsync(MaterialGroupCreateDto input)
        {
            return _materialGroupsAppService.CreateAsync(input);
        }

        [HttpPut]
        [Route("{id}")]
        public virtual Task<MaterialGroupDto> UpdateAsync(Guid id, MaterialGroupUpdateDto input)
        {
            return _materialGroupsAppService.UpdateAsync(id, input);
        }

        [HttpDelete]
        [Route("{id}")]
        public virtual Task DeleteAsync(Guid id)
        {
            return _materialGroupsAppService.DeleteAsync(id);
        }
    }
}