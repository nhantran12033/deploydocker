using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace MEVN.DistributorApp.V2.Distributors
{
    public abstract class DistributorCreateDtoBase
    {
        [Required]
        [StringLength(DistributorConsts.DisCodeMaxLength, MinimumLength = DistributorConsts.DisCodeMinLength)]
        public string DisCode { get; set; } = null!;
        [Required]
        [StringLength(DistributorConsts.DisNameMaxLength, MinimumLength = DistributorConsts.DisNameMinLength)]
        public string DisName { get; set; } = null!;
        public string? Address { get; set; } = "NULL";
        public string? ContactInfo { get; set; } = "NULL";
        public string? Node { get; set; } = "NULL";
    }
}