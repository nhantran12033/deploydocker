using Volo.Abp.Application.Dtos;
using System;

namespace MEVN.DistributorApp.V2.Distributors
{
    public abstract class GetDistributorsInputBase : PagedAndSortedResultRequestDto
    {
        public string? FilterText { get; set; }

        public string? DisCode { get; set; }
        public string? DisName { get; set; }
        public string? Address { get; set; }
        public string? ContactInfo { get; set; }
        public string? Node { get; set; }

        public GetDistributorsInputBase()
        {

        }
    }
}