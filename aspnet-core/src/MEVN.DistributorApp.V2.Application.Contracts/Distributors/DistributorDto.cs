using System;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Domain.Entities;

namespace MEVN.DistributorApp.V2.Distributors
{
    public abstract class DistributorDtoBase : FullAuditedEntityDto<Guid>, IHasConcurrencyStamp
    {
        public Guid Id { get; set; }
        public string DisCode { get; set; } = null!;
        public string DisName { get; set; } = null!;
        public string? Address { get; set; }
        public string? ContactInfo { get; set; }
        public string? Node { get; set; }

        public string ConcurrencyStamp { get; set; } = null!;
    }
}