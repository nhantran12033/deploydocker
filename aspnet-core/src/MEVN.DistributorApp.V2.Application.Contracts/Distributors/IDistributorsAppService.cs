using System;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace MEVN.DistributorApp.V2.Distributors
{
    public partial interface IDistributorsAppService : IApplicationService
    {
        Task<PagedResultDto<DistributorDto>> GetListAsync(GetDistributorsInput input);

        Task<DistributorDto> GetAsync(Guid id);

        Task DeleteAsync(Guid id);

        Task<DistributorDto> CreateAsync(DistributorCreateDto input);

        Task<DistributorDto> UpdateAsync(Guid id, DistributorUpdateDto input);
    }
}