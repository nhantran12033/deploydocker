using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using Volo.Abp.Domain.Entities;

namespace MEVN.DistributorApp.V2.Distributors
{
    public abstract class DistributorUpdateDtoBase : IHasConcurrencyStamp
    {
        [Required]
        [StringLength(DistributorConsts.DisCodeMaxLength, MinimumLength = DistributorConsts.DisCodeMinLength)]
        public string DisCode { get; set; } = null!;
        [Required]
        [StringLength(DistributorConsts.DisNameMaxLength, MinimumLength = DistributorConsts.DisNameMinLength)]
        public string DisName { get; set; } = null!;
        public string? Address { get; set; }
        public string? ContactInfo { get; set; }
        public string? Node { get; set; }

        public string ConcurrencyStamp { get; set; } = null!;
    }
}