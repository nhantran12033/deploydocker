using System;

namespace MEVN.DistributorApp.V2.Distributors
{
    public abstract class DistributorExcelDtoBase
    {
        public string DisCode { get; set; } = null!;
        public string DisName { get; set; } = null!;
        public string Address { get; set; } = null!;
        public string ContactInfo { get; set; } = null!;
        public string Node { get; set; } = null!;
    }
}