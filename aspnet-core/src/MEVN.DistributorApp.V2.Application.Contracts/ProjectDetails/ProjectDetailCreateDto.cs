using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace MEVN.DistributorApp.V2.ProjectDetails
{
    public class ProjectDetailCreateDto
    {
        public Guid ProjectId { get; set; }
        [Required]
        [StringLength(ProjectDetailConsts.GolfaCodeMaxLength, MinimumLength = ProjectDetailConsts.GolfaCodeMinLength)]
        public string GolfaCode { get; set; } = null!;
        public string? Model { get; set; } = "NULL";
        public int Qty { get; set; }
        public int? DpoUsed { get; set; }
        public string? RequestStatus { get; set; }
        public float? RequestPrice { get; set; }
        public float DistRequestedPrice { get; set; }
        public float SaleOfferPrice { get; set; }
        public float SaleAllowDiscountPrice { get; set; }
        public float AmountRequestedPrice { get; set; }
        [Required]
        public string StatusCode { get; set; } = null!;
    }
}