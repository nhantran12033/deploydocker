using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using Volo.Abp.Domain.Entities;

namespace MEVN.DistributorApp.V2.ProjectDetails
{
    public class ProjectDetailUpdateDto : IHasConcurrencyStamp
    {
        public Guid ProjectId { get; set; }
        [Required]
        [StringLength(ProjectDetailConsts.GolfaCodeMaxLength, MinimumLength = ProjectDetailConsts.GolfaCodeMinLength)]
        public string GolfaCode { get; set; } = null!;
        public string? Model { get; set; }
        public int Qty { get; set; }
        public int? DpoUsed { get; set; }
        public string? RequestStatus { get; set; }
        public float? RequestPrice { get; set; }
        public float DistRequestedPrice { get; set; }
        public float SaleOfferPrice { get; set; }
        public float SaleAllowDiscountPrice { get; set; }
        public float AmountRequestedPrice { get; set; }

        public string ConcurrencyStamp { get; set; } = null!;
    }
}