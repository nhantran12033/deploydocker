using System;

namespace MEVN.DistributorApp.V2.ProjectDetails
{
    public abstract class ProjectDetailExcelDtoBase
    {
        public Guid ProjectId { get; set; }
        public string GolfaCode { get; set; } = null!;
        public string? Model { get; set; }
        public int Qty { get; set; }
        public int? DpoUsed { get; set; }
        public string? RequestStatus { get; set; }
        public float? RequestPrice { get; set; }
        public float DistRequestedPrice { get; set; }
        public float SaleOfferPrice { get; set; }
        public float SaleAllowDiscountPrice { get; set; }
        public float AmountRequestedPrice { get; set; }
    }
}