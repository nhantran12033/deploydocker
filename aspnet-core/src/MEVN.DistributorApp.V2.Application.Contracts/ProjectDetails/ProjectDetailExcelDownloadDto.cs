using Volo.Abp.Application.Dtos;
using System;

namespace MEVN.DistributorApp.V2.ProjectDetails
{
    public abstract class ProjectDetailExcelDownloadDtoBase
    {
        public string DownloadToken { get; set; } = null!;

        public Guid Id { get; set; }
        public ProjectDetailExcelDownloadDtoBase()
        {

        }
    }
}