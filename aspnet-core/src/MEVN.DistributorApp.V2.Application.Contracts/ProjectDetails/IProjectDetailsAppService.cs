using System;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace MEVN.DistributorApp.V2.ProjectDetails
{
    public interface IProjectDetailsAppService : IApplicationService
    {
        Task<PagedResultDto<ProjectDetailDto>> GetListAsync(GetProjectDetailsInput input);

        Task<ProjectDetailDto> GetAsync(Guid id);

        Task DeleteAsync(Guid id);

        Task<ProjectDetailDto> CreateAsync(ProjectDetailCreateDto input);

        Task<ProjectDetailDto> UpdateAsync(Guid id, ProjectDetailUpdateDto input);
    }
}