using Volo.Abp.Application.Dtos;
using System;

namespace MEVN.DistributorApp.V2.ProjectDetails
{
    public class GetProjectDetailsInput : PagedAndSortedResultRequestDto
    {
        public string? FilterText { get; set; }

        public Guid? ProjectId { get; set; }
        public string? GolfaCode { get; set; }
        public string? Model { get; set; }
        public int? QtyMin { get; set; }
        public int? QtyMax { get; set; }
        public int? DpoUsedMin { get; set; }
        public int? DpoUsedMax { get; set; }
        public string? RequestStatus { get; set; }
        public float? RequestPriceMin { get; set; }
        public float? RequestPriceMax { get; set; }
        public float? DistRequestedPriceMin { get; set; }
        public float? DistRequestedPriceMax { get; set; }
        public float? SaleOfferPriceMin { get; set; }
        public float? SaleOfferPriceMax { get; set; }
        public float? SaleAllowDiscountPriceMin { get; set; }
        public float? SaleAllowDiscountPriceMax { get; set; }
        public float? AmountRequestedPriceMin { get; set; }
        public float? AmountRequestedPriceMax { get; set; }
        public string? StatusCode { get; set; }

        public GetProjectDetailsInput()
        {

        }
    }
}