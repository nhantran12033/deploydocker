namespace MEVN.DistributorApp.V2.Shared;

public abstract class DownloadTokenResultDtoBase
{
    public string Token { get; set; } = null!;
}