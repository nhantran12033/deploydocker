using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace MEVN.DistributorApp.V2.MaterialGroups
{
    public abstract class MaterialGroupCreateDtoBase
    {
        [Required]
        public string MaterialName { get; set; } = null!;
        [Required]
        public string DisCode { get; set; } = null!;
    }
}