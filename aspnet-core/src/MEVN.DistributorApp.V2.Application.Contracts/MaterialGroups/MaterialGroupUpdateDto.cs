using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using Volo.Abp.Domain.Entities;

namespace MEVN.DistributorApp.V2.MaterialGroups
{
    public abstract class MaterialGroupUpdateDtoBase : IHasConcurrencyStamp
    {
        [Required]
        public string MaterialName { get; set; } = null!;
        [Required]
        public string DisCode { get; set; } = null!;

        public string ConcurrencyStamp { get; set; } = null!;
    }
}