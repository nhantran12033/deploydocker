using Volo.Abp.Application.Dtos;
using System;

namespace MEVN.DistributorApp.V2.MaterialGroups
{
    public abstract class GetMaterialGroupsInputBase : PagedAndSortedResultRequestDto
    {
        public string? FilterText { get; set; }

        public string? MaterialName { get; set; }
        public string? DisCode { get; set; }

        public GetMaterialGroupsInputBase()
        {

        }
    }
}