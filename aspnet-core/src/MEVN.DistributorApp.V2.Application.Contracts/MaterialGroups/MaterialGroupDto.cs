using System;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Domain.Entities;

namespace MEVN.DistributorApp.V2.MaterialGroups
{
    public abstract class MaterialGroupDtoBase : FullAuditedEntityDto<Guid>, IHasConcurrencyStamp
    {
        public Guid Id { get; set; }
        public string MaterialName { get; set; } = null!;
        public string DisCode { get; set; } = null!;

        public string ConcurrencyStamp { get; set; } = null!;
    }
}