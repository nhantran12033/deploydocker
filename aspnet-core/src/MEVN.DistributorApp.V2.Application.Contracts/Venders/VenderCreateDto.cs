using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace MEVN.DistributorApp.V2.Venders
{
    public abstract class VenderCreateDtoBase
    {
        [Required]
        [StringLength(VenderConsts.VerderNameMaxLength, MinimumLength = VenderConsts.VerderNameMinLength)]
        public string VerderName { get; set; } = null!;
        public string? Address { get; set; }
        [EmailAddress]
        public string? Email { get; set; }
    }
}