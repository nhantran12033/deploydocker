using System;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Domain.Entities;

namespace MEVN.DistributorApp.V2.Venders
{
    public abstract class VenderDtoBase : FullAuditedEntityDto<Guid>, IHasConcurrencyStamp
    {
        public Guid Id { get; set; }
        public string VerderName { get; set; } = null!;
        public string? Address { get; set; }
        public string? Email { get; set; }

        public string ConcurrencyStamp { get; set; } = null!;
    }
}