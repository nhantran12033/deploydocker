using System;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace MEVN.DistributorApp.V2.Venders
{
    public partial interface IVendersAppService : IApplicationService
    {
        Task<PagedResultDto<VenderDto>> GetListAsync(GetVendersInput input);

        Task<VenderDto> GetAsync(Guid id);

        Task DeleteAsync(Guid id);

        Task<VenderDto> CreateAsync(VenderCreateDto input);

        Task<VenderDto> UpdateAsync(Guid id, VenderUpdateDto input);
    }
}