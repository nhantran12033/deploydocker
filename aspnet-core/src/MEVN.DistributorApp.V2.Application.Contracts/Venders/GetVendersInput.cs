using Volo.Abp.Application.Dtos;
using System;

namespace MEVN.DistributorApp.V2.Venders
{
    public abstract class GetVendersInputBase : PagedAndSortedResultRequestDto
    {
        public string? FilterText { get; set; }

        public string? VerderName { get; set; }
        public string? Address { get; set; }
        public string? Email { get; set; }

        public GetVendersInputBase()
        {

        }
    }
}