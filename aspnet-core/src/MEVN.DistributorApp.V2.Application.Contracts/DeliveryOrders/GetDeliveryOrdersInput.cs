using Volo.Abp.Application.Dtos;
using System;

namespace MEVN.DistributorApp.V2.DeliveryOrders
{
    public abstract class GetDeliveryOrdersInputBase : PagedAndSortedResultRequestDto
    {
        public string? FilterText { get; set; }

        public byte? StatusMin { get; set; }
        public byte? StatusMax { get; set; }
        public DateTime? DeliveryDateMin { get; set; }
        public DateTime? DeliveryDateMax { get; set; }
        public string? DeliveryCode { get; set; }
        public string? DOSAPNo { get; set; }
        public DateTime? InvoiceDateMin { get; set; }
        public DateTime? InvoiceDateMax { get; set; }
        public string? InvoiceNo { get; set; }
        public string? BillingNo { get; set; }
        public string? Note { get; set; }
        public string? StatusCode { get; set; }
        public int? QtyMin { get; set; }
        public int? QtyMax { get; set; }
        public Guid? DistributorOrderDetailId { get; set; }

        public GetDeliveryOrdersInputBase()
        {

        }
    }
}