using System;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace MEVN.DistributorApp.V2.DeliveryOrders
{
    public partial interface IDeliveryOrdersAppService : IApplicationService
    {
        Task<PagedResultDto<DeliveryOrderDto>> GetListAsync(GetDeliveryOrdersInput input);

        Task<DeliveryOrderDto> GetAsync(Guid id);

        Task DeleteAsync(Guid id);

        Task<DeliveryOrderDto> CreateAsync(DeliveryOrderCreateDto input);

        Task<DeliveryOrderDto> UpdateAsync(Guid id, DeliveryOrderUpdateDto input);
    }
}