using System;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Domain.Entities;

namespace MEVN.DistributorApp.V2.DeliveryOrders
{
    public abstract class DeliveryOrderDtoBase : FullAuditedEntityDto<Guid>, IHasConcurrencyStamp
    {
        public byte Status { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public string? DeliveryCode { get; set; }
        public string? DOSAPNo { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public string? InvoiceNo { get; set; }
        public string? BillingNo { get; set; }
        public string? Note { get; set; }
        public string? StatusCode { get; set; }
        public int Qty { get; set; }
        public Guid DistributorOrderDetailId { get; set; }

        public string ConcurrencyStamp { get; set; } = null!;
    }
}