using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace MEVN.DistributorApp.V2.DeliveryOrders
{
    public abstract class DeliveryOrderCreateDtoBase
    {
        public byte Status { get; set; }
        public DateTime? DeliveryDate { get; set; }
        [StringLength(DeliveryOrderConsts.DeliveryCodeMaxLength)]
        public string? DeliveryCode { get; set; }
        [StringLength(DeliveryOrderConsts.DOSAPNoMaxLength)]
        public string? DOSAPNo { get; set; }
        public DateTime? InvoiceDate { get; set; }
        [StringLength(DeliveryOrderConsts.InvoiceNoMaxLength)]
        public string? InvoiceNo { get; set; }
        [StringLength(DeliveryOrderConsts.BillingNoMaxLength)]
        public string? BillingNo { get; set; }
        [StringLength(DeliveryOrderConsts.NoteMaxLength)]
        public string? Note { get; set; }
        [StringLength(DeliveryOrderConsts.StatusCodeMaxLength)]
        public string? StatusCode { get; set; }
        public int Qty { get; set; }
    }
}