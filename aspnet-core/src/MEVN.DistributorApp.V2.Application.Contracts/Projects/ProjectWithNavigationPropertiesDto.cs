using MEVN.DistributorApp.V2.Distributors;
using MEVN.DistributorApp.V2.Customers;

using System;
using Volo.Abp.Application.Dtos;
using System.Collections.Generic;
using MEVN.DistributorApp.V2.ProjectDetails;

namespace MEVN.DistributorApp.V2.Projects
{
    public abstract class ProjectWithNavigationPropertiesDtoBase
    {
        public ProjectDtoBase Project { get; set; } = null!;
        public List<ProjectDetailDto> ProjectDetail { get; set; } = null!;
        public DistributorDto Distributor { get; set; } = null!;
        public CustomerDto Customer { get; set; } = null!;

    }
}