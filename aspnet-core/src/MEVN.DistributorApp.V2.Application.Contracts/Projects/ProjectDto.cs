using MEVN.DistributorApp.V2.ProjectDetails;
using System;
using System.Collections.Generic;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Domain.Entities;

namespace MEVN.DistributorApp.V2.Projects
{
    public abstract class ProjectDtoBase : FullAuditedEntityDto<Guid>, IHasConcurrencyStamp
    {
        public string ProjectCode { get; set; } = null!;
        public string ProjectName { get; set; } = null!;
        public string ApprovalStatus { get; set; } = null!;
        public string? AccountNo { get; set; }
        public string? ProjectType { get; set; }
        public string? Location { get; set; }
        public string? DistributorMagin { get; set; }
        public DateTime? PODate { get; set; }
        public Guid DistributorId { get; set; }
        public Guid? CustomerId { get; set; }
        public List<ProjectDetailDto>? ListProjectDetail { get; set; }
        public string ConcurrencyStamp { get; set; } = null!;
    }
}