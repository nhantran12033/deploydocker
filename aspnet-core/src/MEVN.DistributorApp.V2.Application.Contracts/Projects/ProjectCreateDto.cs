using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using MEVN.DistributorApp.V2.ProjectDetails;


namespace MEVN.DistributorApp.V2.Projects
{
    public abstract class ProjectCreateDtoBase
    {
        [Required]
        [StringLength(ProjectConsts.ProjectCodeMaxLength, MinimumLength = ProjectConsts.ProjectCodeMinLength)]
        public string ProjectCode { get; set; } = null!;
        [Required]
        [StringLength(ProjectConsts.ProjectNameMaxLength, MinimumLength = ProjectConsts.ProjectNameMinLength)]
        public string ProjectName { get; set; } = null!;
        [Required]
        [StringLength(ProjectConsts.ApprovalStatusMaxLength, MinimumLength = ProjectConsts.ApprovalStatusMinLength)]
        public string ApprovalStatus { get; set; } = null!;
        public string? AccountNo { get; set; } = "NULL";
        public string? ProjectType { get; set; } = "NULL";
        public string? Location { get; set; } = "NULL";
        public string? DistributorMagin { get; set; } = "NULL";
        public DateTime? PODate { get; set; }
        public Guid DistributorId { get; set; }
        public Guid? CustomerId { get; set; }
        public List<ProjectDetailCreateDto>? ListProjectDetail { get; set; }

    }
}