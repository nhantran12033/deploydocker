using Volo.Abp.Application.Dtos;
using System;

namespace MEVN.DistributorApp.V2.Projects
{
    public abstract class GetProjectsInputBase : PagedAndSortedResultRequestDto
    {

        public string? ProjectCode { get; set; }
        public string? ProjectName { get; set; }
        public DateTime? PODateMin { get; set; }
        public DateTime? PODateMax { get; set; }
        public Guid? DistributorId { get; set; }


        public GetProjectsInputBase()
        {

        }
    }
}