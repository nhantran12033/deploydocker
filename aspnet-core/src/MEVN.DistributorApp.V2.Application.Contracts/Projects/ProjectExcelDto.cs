using System;

namespace MEVN.DistributorApp.V2.Projects
{
    public abstract class ProjectExcelDtoBase
    {
        public string ProjectCode { get; set; } = null!;
        public string ProjectName { get; set; } = null!;
        public string ApprovalStatus { get; set; } = null!;
        public string? ProjectType { get; set; }
    }
}