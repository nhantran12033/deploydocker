using Volo.Abp.Application.Dtos;
using System;

namespace MEVN.DistributorApp.V2.Projects
{
    public abstract class ProjectExcelDownloadDtoBase
    {
        public string DownloadToken { get; set; } = null!;

        public Guid Id { get; set; } 

        public ProjectExcelDownloadDtoBase()
        {

        }
    }
}