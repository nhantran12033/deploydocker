using MEVN.DistributorApp.V2.Shared;
using System;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Content;
using Volo.Abp.Gdpr;

namespace MEVN.DistributorApp.V2.Projects
{
    public partial interface IProjectsAppService : IApplicationService
    {
        Task<PagedResultDto<ProjectDto>> GetListAsync(GetProjectsInput input);

        Task<ProjectWithNavigationPropertiesDto> GetWithNavigationPropertiesAsync(Guid id);

        Task<ProjectDto> GetAsync(Guid id);

        Task<PagedResultDto<LookupDto<Guid>>> GetDistributorLookupAsync(LookupRequestDto input);

        Task<PagedResultDto<LookupDto<Guid>>> GetCustomerLookupAsync(LookupRequestDto input);

        Task DeleteAsync(Guid id);

        Task<ProjectDto> CreateAsync(ProjectCreateDto input);

        Task<IRemoteStreamContent> GetListAsExcelFileAsync(ProjectExcelDownloadDto input);

        Task<DownloadTokenResultDto> GetDownloadTokenAsync();
    }
}