using System;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Domain.Entities;

namespace MEVN.DistributorApp.V2.ETADetails
{
    public abstract class ETADetailDtoBase : FullAuditedEntityDto<Guid>, IHasConcurrencyStamp
    {
        public string StatusCode { get; set; } = null!;
        public string InvoiceNo { get; set; } = null!;
        public string GolfaCode { get; set; } = null!;
        public int Qty { get; set; }
        public DateTime ETD { get; set; }
        public DateTime ETA { get; set; }
        public Guid DistributorOrderDetailId { get; set; }

        public string ConcurrencyStamp { get; set; } = null!;
    }
}