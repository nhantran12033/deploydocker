using Volo.Abp.Application.Dtos;
using System;

namespace MEVN.DistributorApp.V2.ETADetails
{
    public abstract class GetETADetailsInputBase : PagedAndSortedResultRequestDto
    {
        public string? FilterText { get; set; }

        public string? StatusCode { get; set; }
        public string? InvoiceNo { get; set; }
        public string? GolfaCode { get; set; }
        public int? QtyMin { get; set; }
        public int? QtyMax { get; set; }
        public DateTime? ETDMin { get; set; }
        public DateTime? ETDMax { get; set; }
        public DateTime? ETAMin { get; set; }
        public DateTime? ETAMax { get; set; }
        public Guid? DistributorOrderDetailId { get; set; }

        public GetETADetailsInputBase()
        {

        }
    }
}