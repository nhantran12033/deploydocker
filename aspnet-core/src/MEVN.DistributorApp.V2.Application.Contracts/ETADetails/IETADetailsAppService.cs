using System;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace MEVN.DistributorApp.V2.ETADetails
{
    public partial interface IETADetailsAppService : IApplicationService
    {
        Task<PagedResultDto<ETADetailDto>> GetListAsync(GetETADetailsInput input);

        Task<ETADetailDto> GetAsync(Guid id);

        Task DeleteAsync(Guid id);

        Task<ETADetailDto> CreateAsync(ETADetailCreateDto input);

        Task<ETADetailDto> UpdateAsync(Guid id, ETADetailUpdateDto input);
    }
}