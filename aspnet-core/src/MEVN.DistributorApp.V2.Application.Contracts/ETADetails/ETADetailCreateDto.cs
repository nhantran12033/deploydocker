using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace MEVN.DistributorApp.V2.ETADetails
{
    public abstract class ETADetailCreateDtoBase
    {
        [Required]
        [StringLength(ETADetailConsts.StatusCodeMaxLength)]
        public string StatusCode { get; set; } = null!;
        [Required]
        [StringLength(ETADetailConsts.InvoiceNoMaxLength)]
        public string InvoiceNo { get; set; } = null!;
        [Required]
        [StringLength(ETADetailConsts.GolfaCodeMaxLength)]
        public string GolfaCode { get; set; } = null!;
        public int Qty { get; set; }
        public DateTime ETD { get; set; }
        public DateTime ETA { get; set; }
        public Guid DistributorOrderDetailId { get; set; }
    }
}