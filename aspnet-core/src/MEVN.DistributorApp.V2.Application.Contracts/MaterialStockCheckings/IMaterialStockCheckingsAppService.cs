using MEVN.DistributorApp.V2.Shared;
using System;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Content;
using Volo.Abp.Gdpr;

namespace MEVN.DistributorApp.V2.MaterialStockCheckings
{
    public partial interface IMaterialStockCheckingsAppService : IApplicationService
    {
        Task<PagedResultDto<MaterialStockCheckingDto>> GetListAsync(GetMaterialStockCheckingsInput input);

        Task<MaterialStockCheckingDto> GetAsync(Guid id);

        Task<PagedResultDto<LookupDto<Guid>>> GetMaterialGroupLookupAsync(LookupRequestDto input);

        Task<PagedResultDto<LookupDto<Guid>>> GetVenderLookupAsync(LookupRequestDto input);

        Task<PagedResultDto<LookupDto<Guid>>> GetDistributorLookupAsync(LookupRequestDto input);

        Task DeleteAsync(Guid id);

        Task<MaterialStockCheckingDto> CreateAsync(MaterialStockCheckingCreateDto input);


        Task<IRemoteStreamContent> GetListAsExcelFileAsync(MaterialStockCheckingExcelDownloadDto input);

        Task<DownloadTokenResultDto> GetDownloadTokenAsync();
    }
}