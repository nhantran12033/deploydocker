using System;

namespace MEVN.DistributorApp.V2.MaterialStockCheckings
{
    public abstract class MaterialStockCheckingExcelDtoBase
    {
        public string GolfaCode { get; set; } = null!;
        public string Model { get; set; } = null!;
        public string? SAP_Code { get; set; }
        public string Description_VN { get; set; } = null!;
        public string? Description_Group { get; set; }
        public int Standard_Price { get; set; }
        public int? StockValueWarning { get; set; }
        public int? StockTmp { get; set; }
        public string? Spec1 { get; set; }
        public string? Spec2 { get; set; }
    }
}