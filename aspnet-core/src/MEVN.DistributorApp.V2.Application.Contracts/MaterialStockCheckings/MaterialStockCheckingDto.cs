using MEVN.DistributorApp.V2.FreeOnShipMents;
using System;
using System.Collections.Generic;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Domain.Entities;

namespace MEVN.DistributorApp.V2.MaterialStockCheckings
{
    public abstract class MaterialStockCheckingDtoBase : FullAuditedEntityDto<Guid>, IHasConcurrencyStamp
    {
        public string GolfaCode { get; set; } = null!;
        public string Model { get; set; } = null!;
        public string? SAP_Code { get; set; }
        public string Description_VN { get; set; } = null!;
        public string? Description_Group { get; set; }
        public int Standard_Price { get; set; }
        public int? StockValueWarning { get; set; }
        public int? StockTmp { get; set; }
        public string? Spec1 { get; set; }
        public string? Spec2 { get; set; }
        public Guid MaterialGroupId { get; set; }
        public Guid VenderId { get; set; }
        public Guid? DistributorId { get; set; }
        public List<FreeOnShipMentDto>? ListFreeOnShipMent { get; set; }
        public string ConcurrencyStamp { get; set; } = null!;
    }
}