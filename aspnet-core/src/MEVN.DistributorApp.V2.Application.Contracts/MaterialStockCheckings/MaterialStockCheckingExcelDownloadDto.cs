using Volo.Abp.Application.Dtos;
using System;

namespace MEVN.DistributorApp.V2.MaterialStockCheckings
{
    public abstract class MaterialStockCheckingExcelDownloadDtoBase
    {
        public string DownloadToken { get; set; } = null!;

        public string? GolfaCode { get; set; }
        public string? Model { get; set; }
 
        public Guid? MaterialGroupId { get; set; }
        public Guid? VenderId { get; set; }
        public Guid? DistributorId { get; set; }

        public MaterialStockCheckingExcelDownloadDtoBase()
        {

        }
    }
}