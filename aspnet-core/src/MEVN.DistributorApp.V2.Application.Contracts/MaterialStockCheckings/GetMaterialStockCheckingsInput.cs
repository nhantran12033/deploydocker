using Volo.Abp.Application.Dtos;
using System;

namespace MEVN.DistributorApp.V2.MaterialStockCheckings
{
    public abstract class GetMaterialStockCheckingsInputBase : PagedAndSortedResultRequestDto
    {
        public string? GolfaCode { get; set; }
        public string? Model { get; set; }
        public Guid? MaterialGroupId { get; set; }
        public Guid? VenderId { get; set; }
        public Guid? DistributorId { get; set; }

        public GetMaterialStockCheckingsInputBase()
        {

        }
    }
}