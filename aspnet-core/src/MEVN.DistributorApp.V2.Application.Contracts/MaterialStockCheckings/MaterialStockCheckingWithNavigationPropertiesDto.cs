using MEVN.DistributorApp.V2.MaterialGroups;
using MEVN.DistributorApp.V2.Venders;
using MEVN.DistributorApp.V2.Distributors;

using System;
using Volo.Abp.Application.Dtos;
using System.Collections.Generic;
using MEVN.DistributorApp.V2.FreeOnShipMents;

namespace MEVN.DistributorApp.V2.MaterialStockCheckings
{
    public abstract class MaterialStockCheckingWithNavigationPropertiesDtoBase
    {
        public MaterialStockCheckingDto MaterialStockChecking { get; set; } = null!;
        public List<FreeOnShipMentDto> FreeOnShipMent { get; set; } = null!;
        public MaterialGroupDto MaterialGroup { get; set; } = null!;
        public VenderDto Vender { get; set; } = null!;
        public DistributorDto Distributor { get; set; } = null!;

    }
}