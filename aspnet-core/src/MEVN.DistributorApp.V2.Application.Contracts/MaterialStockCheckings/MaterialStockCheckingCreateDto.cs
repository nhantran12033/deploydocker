using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using MEVN.DistributorApp.V2.FreeOnShipMents;

namespace MEVN.DistributorApp.V2.MaterialStockCheckings
{
    public abstract class MaterialStockCheckingCreateDtoBase
    {
        [Required]
        public string GolfaCode { get; set; } = null!;
        [Required]
        public string Model { get; set; } = null!;
        public string? SAP_Code { get; set; }
        [Required]
        public string Description_VN { get; set; } = null!;
        public string? Description_Group { get; set; }
        public int Standard_Price { get; set; }
        public int? StockValueWarning { get; set; }
        public int? StockTmp { get; set; }
        public string? Spec1 { get; set; } = "NULL";
        public string? Spec2 { get; set; } = "NULL";
        public Guid MaterialGroupId { get; set; }
        public Guid VenderId { get; set; }
        public Guid? DistributorId { get; set; }
        public List<FreeOnShipMentCreateDto>? ListFreeOnShipMent { get; set; }
    }
}