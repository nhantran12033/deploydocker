using MEVN.DistributorApp.V2.Localization;
using Volo.Abp.Authorization.Permissions;
using Volo.Abp.Localization;
using Volo.Abp.MultiTenancy;

namespace MEVN.DistributorApp.V2.Permissions;

public class V2PermissionDefinitionProvider : PermissionDefinitionProvider
{
    public override void Define(IPermissionDefinitionContext context)
    {
        var myGroup = context.AddGroup(V2Permissions.GroupName);

        myGroup.AddPermission(V2Permissions.Dashboard.Host, L("Permission:Dashboard"), MultiTenancySides.Host);
        myGroup.AddPermission(V2Permissions.Dashboard.Tenant, L("Permission:Dashboard"), MultiTenancySides.Tenant);

        //Define your own permissions here. Example:
        //myGroup.AddPermission(V2Permissions.MyPermission1, L("Permission:MyPermission1"));

        var distributorPermission = myGroup.AddPermission(V2Permissions.Distributors.Default, L("Permission:Distributors"));
        distributorPermission.AddChild(V2Permissions.Distributors.Create, L("Permission:Create"));
        distributorPermission.AddChild(V2Permissions.Distributors.Edit, L("Permission:Edit"));
        distributorPermission.AddChild(V2Permissions.Distributors.Delete, L("Permission:Delete"));

        var customerPermission = myGroup.AddPermission(V2Permissions.Customers.Default, L("Permission:Customers"));
        customerPermission.AddChild(V2Permissions.Customers.Create, L("Permission:Create"));
        customerPermission.AddChild(V2Permissions.Customers.Edit, L("Permission:Edit"));
        customerPermission.AddChild(V2Permissions.Customers.Delete, L("Permission:Delete"));

        var projectPermission = myGroup.AddPermission(V2Permissions.Projects.Default, L("Permission:Projects"));
        projectPermission.AddChild(V2Permissions.Projects.Create, L("Permission:Create"));
        projectPermission.AddChild(V2Permissions.Projects.Edit, L("Permission:Edit"));
        projectPermission.AddChild(V2Permissions.Projects.Delete, L("Permission:Delete"));

        var projectDetailPermission = myGroup.AddPermission(V2Permissions.ProjectDetails.Default, L("Permission:ProjectDetails"));
        projectDetailPermission.AddChild(V2Permissions.ProjectDetails.Create, L("Permission:Create"));
        projectDetailPermission.AddChild(V2Permissions.ProjectDetails.Edit, L("Permission:Edit"));
        projectDetailPermission.AddChild(V2Permissions.ProjectDetails.Delete, L("Permission:Delete"));

        var freeOnShipMentPermission = myGroup.AddPermission(V2Permissions.FreeOnShipMents.Default, L("Permission:FreeOnShipMents"));
        freeOnShipMentPermission.AddChild(V2Permissions.FreeOnShipMents.Create, L("Permission:Create"));
        freeOnShipMentPermission.AddChild(V2Permissions.FreeOnShipMents.Edit, L("Permission:Edit"));
        freeOnShipMentPermission.AddChild(V2Permissions.FreeOnShipMents.Delete, L("Permission:Delete"));

        var venderPermission = myGroup.AddPermission(V2Permissions.Venders.Default, L("Permission:Venders"));
        venderPermission.AddChild(V2Permissions.Venders.Create, L("Permission:Create"));
        venderPermission.AddChild(V2Permissions.Venders.Edit, L("Permission:Edit"));
        venderPermission.AddChild(V2Permissions.Venders.Delete, L("Permission:Delete"));

        var materialGroupPermission = myGroup.AddPermission(V2Permissions.MaterialGroups.Default, L("Permission:MaterialGroups"));
        materialGroupPermission.AddChild(V2Permissions.MaterialGroups.Create, L("Permission:Create"));
        materialGroupPermission.AddChild(V2Permissions.MaterialGroups.Edit, L("Permission:Edit"));
        materialGroupPermission.AddChild(V2Permissions.MaterialGroups.Delete, L("Permission:Delete"));

        var materialStockCheckingPermission = myGroup.AddPermission(V2Permissions.MaterialStockCheckings.Default, L("Permission:MaterialStockCheckings"));
        materialStockCheckingPermission.AddChild(V2Permissions.MaterialStockCheckings.Create, L("Permission:Create"));
        materialStockCheckingPermission.AddChild(V2Permissions.MaterialStockCheckings.Edit, L("Permission:Edit"));
        materialStockCheckingPermission.AddChild(V2Permissions.MaterialStockCheckings.Delete, L("Permission:Delete"));

        var distributorPurchaseOrderPermission = myGroup.AddPermission(V2Permissions.DistributorPurchaseOrders.Default, L("Permission:DistributorPurchaseOrders"));
        distributorPurchaseOrderPermission.AddChild(V2Permissions.DistributorPurchaseOrders.Create, L("Permission:Create"));
        distributorPurchaseOrderPermission.AddChild(V2Permissions.DistributorPurchaseOrders.Edit, L("Permission:Edit"));
        distributorPurchaseOrderPermission.AddChild(V2Permissions.DistributorPurchaseOrders.Delete, L("Permission:Delete"));

        var distributorPurchaseOrderDetailPermission = myGroup.AddPermission(V2Permissions.DistributorPurchaseOrderDetails.Default, L("Permission:DistributorPurchaseOrderDetails"));
        distributorPurchaseOrderDetailPermission.AddChild(V2Permissions.DistributorPurchaseOrderDetails.Create, L("Permission:Create"));
        distributorPurchaseOrderDetailPermission.AddChild(V2Permissions.DistributorPurchaseOrderDetails.Edit, L("Permission:Edit"));
        distributorPurchaseOrderDetailPermission.AddChild(V2Permissions.DistributorPurchaseOrderDetails.Delete, L("Permission:Delete"));

        var deliveryOrderPermission = myGroup.AddPermission(V2Permissions.DeliveryOrders.Default, L("Permission:DeliveryOrders"));
        deliveryOrderPermission.AddChild(V2Permissions.DeliveryOrders.Create, L("Permission:Create"));
        deliveryOrderPermission.AddChild(V2Permissions.DeliveryOrders.Edit, L("Permission:Edit"));
        deliveryOrderPermission.AddChild(V2Permissions.DeliveryOrders.Delete, L("Permission:Delete"));

        var eTADetailPermission = myGroup.AddPermission(V2Permissions.ETADetails.Default, L("Permission:ETADetails"));
        eTADetailPermission.AddChild(V2Permissions.ETADetails.Create, L("Permission:Create"));
        eTADetailPermission.AddChild(V2Permissions.ETADetails.Edit, L("Permission:Edit"));
        eTADetailPermission.AddChild(V2Permissions.ETADetails.Delete, L("Permission:Delete"));
    }

    private static LocalizableString L(string name)
    {
        return LocalizableString.Create<V2Resource>(name);
    }
}