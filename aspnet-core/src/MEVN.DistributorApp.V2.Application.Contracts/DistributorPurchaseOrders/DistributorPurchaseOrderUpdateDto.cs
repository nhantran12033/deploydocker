using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using Volo.Abp.Domain.Entities;

namespace MEVN.DistributorApp.V2.DistributorPurchaseOrders
{
    public abstract class DistributorPurchaseOrderUpdateDtoBase : IHasConcurrencyStamp
    {
        [StringLength(DistributorPurchaseOrderConsts.DPONoMaxLength)]
        public string? DPONo { get; set; }
        public byte Status { get; set; }
        public DateTime? OrderDate { get; set; }
        public float? TotalAmount { get; set; }
        [StringLength(DistributorPurchaseOrderConsts.FA_LVSMaxLength)]
        public string? FA_LVS { get; set; }
        [StringLength(DistributorPurchaseOrderConsts.NoteMaxLength)]
        public string? Note { get; set; }
        [StringLength(DistributorPurchaseOrderConsts.PaymentInfoMaxLength)]
        public string? PaymentInfo { get; set; }
        public DateTime? ShipmentDate { get; set; }
        [StringLength(DistributorPurchaseOrderConsts.UserNameMaxLength)]
        public string? UserName { get; set; }
        [StringLength(DistributorPurchaseOrderConsts.SPUserMaxLength)]
        public string? SPUser { get; set; }
        [StringLength(DistributorPurchaseOrderConsts.RemarkMaxLength)]
        public string? Remark { get; set; }
        [StringLength(DistributorPurchaseOrderConsts.SPFileNameMaxLength)]
        public string? SPFileName { get; set; }
        [StringLength(DistributorPurchaseOrderConsts.FileNameMaxLength)]
        public string? FileName { get; set; }
        [StringLength(DistributorPurchaseOrderConsts.StatusCodeMaxLength)]
        public string? StatusCode { get; set; }
        [StringLength(DistributorPurchaseOrderConsts.StockKeepingRequestNoMaxLength)]
        public string? StockKeepingRequestNo { get; set; }
        public byte? IsKeepStock { get; set; }
        public string? ValidDate { get; set; }
        public Guid? DistributorId { get; set; }

        public string ConcurrencyStamp { get; set; } = null!;
    }
}