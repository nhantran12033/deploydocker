using System;

namespace MEVN.DistributorApp.V2.DistributorPurchaseOrders
{
    public abstract class DistributorPurchaseOrderExcelDtoBase
    {
        public string? DPONo { get; set; }
        public byte Status { get; set; }
        public DateTime? OrderDate { get; set; }
        public float? TotalAmount { get; set; }
        public string? FA_LVS { get; set; }
        public string? Note { get; set; }
        public string? PaymentInfo { get; set; }
        public DateTime? ShipmentDate { get; set; }
        public string? UserName { get; set; }
        public string? SPUser { get; set; }
        public string? Remark { get; set; }
        public string? SPFileName { get; set; }
        public string? FileName { get; set; }
        public string? StatusCode { get; set; }
        public string? StockKeepingRequestNo { get; set; }
        public byte? IsKeepStock { get; set; }
        public string? ValidDate { get; set; }
    }
}