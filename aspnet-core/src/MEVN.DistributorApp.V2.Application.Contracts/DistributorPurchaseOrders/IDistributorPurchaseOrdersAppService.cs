using MEVN.DistributorApp.V2.Shared;
using System;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Content;
using Volo.Abp.Gdpr;

namespace MEVN.DistributorApp.V2.DistributorPurchaseOrders
{
    public partial interface IDistributorPurchaseOrdersAppService : IApplicationService
    {
        Task<PagedResultDto<DistributorPurchaseOrderDto>> GetListAsync(GetDistributorPurchaseOrdersInput input);

        Task<DistributorPurchaseOrderWithNavigationPropertiesDto> GetWithNavigationPropertiesAsync(Guid id);

        Task<DistributorPurchaseOrderDto> GetAsync(Guid id);

        Task<PagedResultDto<LookupDto<Guid>>> GetDistributorLookupAsync(LookupRequestDto input);

        Task DeleteAsync(Guid id);

        Task<DistributorPurchaseOrderDto> CreateAsync(DistributorPurchaseOrderCreateDto input);

    }
}