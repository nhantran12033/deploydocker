using Volo.Abp.Application.Dtos;
using System;

namespace MEVN.DistributorApp.V2.DistributorPurchaseOrders
{
    public abstract class DistributorPurchaseOrderExcelDownloadDtoBase
    {
        public string DownloadToken { get; set; } = null!;

       
        public string? DPONo { get; set; }
      
        public DateTime? OrderDate { get; set; }
    
        public string? FA_LVS { get; set; }
  
   
     
      
        public string? StatusCode { get; set; }
       
        public Guid? DistributorId { get; set; }

        public DistributorPurchaseOrderExcelDownloadDtoBase()
        {

        }
    }
}