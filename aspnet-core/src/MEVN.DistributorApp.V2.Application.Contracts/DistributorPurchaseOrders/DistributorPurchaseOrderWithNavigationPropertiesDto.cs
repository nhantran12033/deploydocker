using MEVN.DistributorApp.V2.Distributors;

using System;
using Volo.Abp.Application.Dtos;
using System.Collections.Generic;
using MEVN.DistributorApp.V2.listDistributorPurchaseOrderDetails;

namespace MEVN.DistributorApp.V2.DistributorPurchaseOrders
{
    public abstract class DistributorPurchaseOrderWithNavigationPropertiesDtoBase
    {
        public DistributorPurchaseOrderDto DistributorPurchaseOrder { get; set; } = null!;
        public DistributorDto Distributor { get; set; } = null!;

    }
}