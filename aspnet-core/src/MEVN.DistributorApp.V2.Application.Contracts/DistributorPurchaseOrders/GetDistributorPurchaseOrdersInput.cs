using Volo.Abp.Application.Dtos;
using System;
using System.Collections.Generic;

namespace MEVN.DistributorApp.V2.DistributorPurchaseOrders
{
    public abstract class GetDistributorPurchaseOrdersInputBase : PagedAndSortedResultRequestDto
    {
      

        public string? DPONo { get; set; }
    
        public DateTime? OrderDate { get; set; }
     
        public string? FA_LVS { get; set; }
       
        public string? StatusCode { get; set; }
    
        public Guid? DistributorId { get; set; }

        public GetDistributorPurchaseOrdersInputBase()
        {

        }
    }
}