using MEVN.DistributorApp.V2.DistributorPurchaseOrderDetails;
using MEVN.DistributorApp.V2.listDistributorPurchaseOrderDetails;
using System;
using System.Collections.Generic;
using Volo.Abp;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Domain.Entities;

namespace MEVN.DistributorApp.V2.DistributorPurchaseOrders
{
    public class DistributorPurchaseOrderDtoBase : FullAuditedEntityDto<Guid>, IHasConcurrencyStamp
    {
        public string? DPONo { get; set; }
        public byte Status { get; set; }
        public DateTime? OrderDate { get; set; }
        public float? TotalAmount { get; set; }
        public string? FA_LVS { get; set; }
        public string? Note { get; set; }
        public string? PaymentInfo { get; set; }
        public DateTime? ShipmentDate { get; set; }
        public string? UserName { get; set; }
        public string? SPUser { get; set; }
        public string? Remark { get; set; }
        public string? SPFileName { get; set; }
        public string? FileName { get; set; }
        public string? StatusCode { get; set; }
        public string? StockKeepingRequestNo { get; set; }
        public byte? IsKeepStock { get; set; }
        public string? ValidDate { get; set; }
        public Guid? DistributorId { get; set; }
        public List<DistributorPurchaseOrderDetailDto>? ListDistributorPurchaseOrderDetail { get; set; }
        public string ConcurrencyStamp { get; set; } = null!;


    }
}
