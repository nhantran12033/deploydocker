using System;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Domain.Entities;

namespace MEVN.DistributorApp.V2.FreeOnShipMents
{
    public class FreeOnShipMentDto : FullAuditedEntityDto<Guid>, IHasConcurrencyStamp
    {
        public Guid MaterialID { get; set; }
        public string PONo { get; set; } = null!;
        public DateTime PODate { get; set; }
        public int? Qty { get; set; }
        public string? MachineNumber { get; set; }

        public string ConcurrencyStamp { get; set; } = null!;
    }
}