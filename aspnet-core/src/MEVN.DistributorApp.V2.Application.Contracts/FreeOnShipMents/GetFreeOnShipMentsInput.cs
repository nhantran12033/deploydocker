using Volo.Abp.Application.Dtos;
using System;

namespace MEVN.DistributorApp.V2.FreeOnShipMents
{
    public class GetFreeOnShipMentsInput : PagedAndSortedResultRequestDto
    {
        public string? FilterText { get; set; }

        public Guid? MaterialID { get; set; }
        public string? PONo { get; set; }
        public DateTime? PODateMin { get; set; }
        public DateTime? PODateMax { get; set; }
        public int? QtyMin { get; set; }
        public int? QtyMax { get; set; }
        public string? MachineNumber { get; set; }

        public GetFreeOnShipMentsInput()
        {

        }
    }
}