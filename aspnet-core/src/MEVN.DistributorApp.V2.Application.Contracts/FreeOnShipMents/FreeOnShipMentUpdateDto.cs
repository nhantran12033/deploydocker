using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using Volo.Abp.Domain.Entities;

namespace MEVN.DistributorApp.V2.FreeOnShipMents
{
    public class FreeOnShipMentUpdateDto : IHasConcurrencyStamp
    {
        public Guid MaterialID { get; set; }

        public string ConcurrencyStamp { get; set; } = null!;
    }
}