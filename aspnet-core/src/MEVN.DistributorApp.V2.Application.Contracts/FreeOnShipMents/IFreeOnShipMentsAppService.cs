using System;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace MEVN.DistributorApp.V2.FreeOnShipMents
{
    public interface IFreeOnShipMentsAppService : IApplicationService
    {
        Task<PagedResultDto<FreeOnShipMentDto>> GetListAsync(GetFreeOnShipMentsInput input);

        Task<FreeOnShipMentDto> GetAsync(Guid id);

        Task DeleteAsync(Guid id);

        Task<FreeOnShipMentDto> CreateAsync(FreeOnShipMentCreateDto input);

        Task<FreeOnShipMentDto> UpdateAsync(Guid id, FreeOnShipMentUpdateDto input);
    }
}