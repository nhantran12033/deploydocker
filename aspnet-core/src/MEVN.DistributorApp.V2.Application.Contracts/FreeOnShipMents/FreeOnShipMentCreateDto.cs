using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace MEVN.DistributorApp.V2.FreeOnShipMents
{
    public class FreeOnShipMentCreateDto
    {
        [Required(AllowEmptyStrings = true)]
        [StringLength(FreeOnShipMentConsts.PONoMaxLength, MinimumLength = FreeOnShipMentConsts.PONoMinLength)]
        public string PONo { get; set; } = null!;
        public DateTime PODate { get; set; }
        public int? Qty { get; set; }
        public string? MachineNumber { get; set; } = "NULL";
    }
}