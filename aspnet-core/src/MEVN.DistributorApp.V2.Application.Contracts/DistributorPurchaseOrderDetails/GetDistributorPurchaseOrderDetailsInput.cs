using Volo.Abp.Application.Dtos;
using System;

namespace MEVN.DistributorApp.V2.DistributorPurchaseOrderDetails
{
    public abstract class GetDistributorPurchaseOrderDetailsInputBase : PagedAndSortedResultRequestDto
    {
        public string? FilterText { get; set; }

        public byte? StatusMin { get; set; }
        public byte? StatusMax { get; set; }
        public string? GolfaCode { get; set; }
        public string? Customer { get; set; }
        public string? Model { get; set; }
        public int? QtyMin { get; set; }
        public int? QtyMax { get; set; }
        public float? PriceMin { get; set; }
        public float? PriceMax { get; set; }
        public float? AmountMin { get; set; }
        public float? AmountMax { get; set; }
        public DateTime? RequestedETAMin { get; set; }
        public DateTime? RequestedETAMax { get; set; }
        public int? DeliveredMin { get; set; }
        public int? DeliveredMax { get; set; }
        public int? InProgressMin { get; set; }
        public int? InProgressMax { get; set; }
        public string? Note { get; set; }
        public byte? ETAMin { get; set; }
        public byte? ETAMax { get; set; }
        public string? CustomerName { get; set; }
        public string? StatusCode { get; set; }
        public Guid? DistributorPurchaseOrderId { get; set; }

        public GetDistributorPurchaseOrderDetailsInputBase()
        {

        }
    }
}