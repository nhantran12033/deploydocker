using MEVN.DistributorApp.V2.DistributorPurchaseOrders;

using System;
using Volo.Abp.Application.Dtos;
using System.Collections.Generic;
using MEVN.DistributorApp.V2.DeliveryOrders;

namespace MEVN.DistributorApp.V2.DistributorPurchaseOrderDetails
{
    public abstract class DistributorPurchaseOrderDetailWithNavigationPropertiesDtoBase
    {
        public DistributorPurchaseOrderDetailDto DistributorPurchaseOrderDetail { get; set; } = null!;
        public DistributorPurchaseOrderDto DistributorPurchaseOrder { get; set; } = null!;

    }
}