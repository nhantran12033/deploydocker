using System;

namespace MEVN.DistributorApp.V2.DistributorPurchaseOrderDetails
{
    public abstract class DistributorPurchaseOrderDetailExcelDtoBase
    {
        public byte Status { get; set; }
        public string? GolfaCode { get; set; }
        public string? Customer { get; set; }
        public string? Model { get; set; }
        public string? DistributorSpec1 { get; set; }
        public string? DistributorSpec2 { get; set; }
        public string? ProjectCode { get; set; }
        public int? Qty { get; set; }
        public float? Price { get; set; }
        public float? Amount { get; set; }
        public DateTime? RequestedETA { get; set; }
        public string? ShipmentMethod { get; set; }
        public int? Delivered { get; set; }
        public int? NeedDelivery { get; set; }
        public int? LockStock { get; set; }
        public int? LockStockSO { get; set; }
        public int? LockShipment { get; set; }
        public string? Note { get; set; }
        public byte? Priority { get; set; }
        public float? USDRate { get; set; }
        public string? CustomerCode { get; set; }
        public int? ProjectV2DetailId { get; set; }
        public string? ErrorCode { get; set; }
        public string? StatusCode { get; set; }
        public int? LockStock_Keeping { get; set; }
    }
}