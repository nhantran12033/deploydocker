using MEVN.DistributorApp.V2.DeliveryOrders;
using MEVN.DistributorApp.V2.ETADetails;
using System;
using System.Collections.Generic;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Domain.Entities;

namespace MEVN.DistributorApp.V2.DistributorPurchaseOrderDetails
{
    public abstract class DistributorPurchaseOrderDetailDtoBase : FullAuditedEntityDto<Guid>, IHasConcurrencyStamp
    {
        public byte Status { get; set; }
        public string? GolfaCode { get; set; }
        public string? Customer { get; set; }
        public string? Model { get; set; }
        public int? Qty { get; set; }
        public float? Price { get; set; }
        public float? Amount { get; set; }
        public DateTime? RequestedETA { get; set; }
        public int? Delivered { get; set; }
        public int? InProgress { get; set; }
        public string? Note { get; set; }
        public byte? ETA { get; set; }
        public string? CustomerName { get; set; }
        public string? StatusCode { get; set; }
        public Guid DistributorPurchaseOrderId { get; set; }
        public List<ETADetailDto>? ListETADetail { get; set; }
        public List<DeliveryOrderDto>? ListDeliveryOrder { get; set; }
        public string ConcurrencyStamp { get; set; } = null!;
    }
}