using MEVN.DistributorApp.V2.listDistributorPurchaseOrderDetails;
using System;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace MEVN.DistributorApp.V2.DistributorPurchaseOrderDetails
{
    public partial interface IDistributorPurchaseOrderDetailsAppService : IApplicationService
    {
        Task<PagedResultDto<DistributorPurchaseOrderDetailDto>> GetListAsync(GetDistributorPurchaseOrderDetailsInput input);

        Task<DistributorPurchaseOrderDetailDto> GetAsync(Guid id);

        Task DeleteAsync(Guid id);

        Task<DistributorPurchaseOrderDetailDto> CreateAsync(DistributorPurchaseOrderDetailCreateDto input);

   
    }
}