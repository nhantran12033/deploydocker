using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using MEVN.DistributorApp.V2.ETADetails;
using MEVN.DistributorApp.V2.DeliveryOrders;

namespace MEVN.DistributorApp.V2.DistributorPurchaseOrderDetails
{
    public abstract class DistributorPurchaseOrderDetailCreateDtoBase
    {
        public byte Status { get; set; }
        [StringLength(DistributorPurchaseOrderDetailConsts.GolfaCodeMaxLength)]
        public string? GolfaCode { get; set; }
        [StringLength(DistributorPurchaseOrderDetailConsts.CustomerMaxLength)]
        public string? Customer { get; set; }
        [StringLength(DistributorPurchaseOrderDetailConsts.ModelMaxLength)]
        public string? Model { get; set; }
        public int? Qty { get; set; }
        public float? Price { get; set; }
        public float? Amount { get; set; }
        public DateTime? RequestedETA { get; set; }
        public int? Delivered { get; set; }
        public int? InProgress { get; set; }
        [StringLength(DistributorPurchaseOrderDetailConsts.NoteMaxLength)]
        public string? Note { get; set; }
        public byte? ETA { get; set; }
        [StringLength(DistributorPurchaseOrderDetailConsts.CustomerNameMaxLength)]
        public string? CustomerName { get; set; }
        [StringLength(DistributorPurchaseOrderDetailConsts.StatusCodeMaxLength)]
        public string? StatusCode { get; set; }
        public List<ETADetailCreateDto>? ListETADetail { get; set; }
        public List<DeliveryOrderCreateDto>? ListDeliveryOrder { get; set; } 
    }
}