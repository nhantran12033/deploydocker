using Volo.Abp.Application.Dtos;
using System;

namespace MEVN.DistributorApp.V2.DistributorPurchaseOrderDetails
{
    public abstract class DistributorPurchaseOrderDetailExcelDownloadDtoBase
    {
        public string DownloadToken { get; set; } = null!;

        public string? FilterText { get; set; }

        public byte? StatusMin { get; set; }
        public byte? StatusMax { get; set; }
        public string? GolfaCode { get; set; }
        public string? Customer { get; set; }
        public string? Model { get; set; }
        public string? DistributorSpec1 { get; set; }
        public string? DistributorSpec2 { get; set; }
        public string? ProjectCode { get; set; }
        public int? QtyMin { get; set; }
        public int? QtyMax { get; set; }
        public float? PriceMin { get; set; }
        public float? PriceMax { get; set; }
        public float? AmountMin { get; set; }
        public float? AmountMax { get; set; }
        public DateTime? RequestedETAMin { get; set; }
        public DateTime? RequestedETAMax { get; set; }
        public string? ShipmentMethod { get; set; }
        public int? DeliveredMin { get; set; }
        public int? DeliveredMax { get; set; }
        public int? NeedDeliveryMin { get; set; }
        public int? NeedDeliveryMax { get; set; }
        public int? LockStockMin { get; set; }
        public int? LockStockMax { get; set; }
        public int? LockStockSOMin { get; set; }
        public int? LockStockSOMax { get; set; }
        public int? LockShipmentMin { get; set; }
        public int? LockShipmentMax { get; set; }
        public string? Note { get; set; }
        public byte? PriorityMin { get; set; }
        public byte? PriorityMax { get; set; }
        public float? USDRateMin { get; set; }
        public float? USDRateMax { get; set; }
        public string? CustomerCode { get; set; }
        public int? ProjectV2DetailIdMin { get; set; }
        public int? ProjectV2DetailIdMax { get; set; }
        public string? ErrorCode { get; set; }
        public string? StatusCode { get; set; }
        public int? LockStock_KeepingMin { get; set; }
        public int? LockStock_KeepingMax { get; set; }
        public Guid? DistributorPurchaseOrderId { get; set; }

        public DistributorPurchaseOrderDetailExcelDownloadDtoBase()
        {

        }
    }
}