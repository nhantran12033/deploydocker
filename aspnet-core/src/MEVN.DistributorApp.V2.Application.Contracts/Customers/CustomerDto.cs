using System;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Domain.Entities;

namespace MEVN.DistributorApp.V2.Customers
{
    public abstract class CustomerDtoBase : FullAuditedEntityDto<Guid>, IHasConcurrencyStamp
    {
        public Guid Id { get; set; }
        public string TaxCode { get; set; } = null!;
        public string CusName { get; set; } = null!;
        public string? Address { get; set; }
        public string? Phone { get; set; }
        public string? Note { get; set; }

        public string ConcurrencyStamp { get; set; } = null!;
    }
}