using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace MEVN.DistributorApp.V2.Customers
{
    public abstract class CustomerCreateDtoBase
    {
        [Required]
        [StringLength(CustomerConsts.TaxCodeMaxLength, MinimumLength = CustomerConsts.TaxCodeMinLength)]
        public string TaxCode { get; set; } = null!;
        [Required]
        [StringLength(CustomerConsts.CusNameMaxLength, MinimumLength = CustomerConsts.CusNameMinLength)]
        public string CusName { get; set; } = null!;
        [StringLength(CustomerConsts.AddressMaxLength, MinimumLength = CustomerConsts.AddressMinLength)]
        public string? Address { get; set; } = "NULL";
        [StringLength(CustomerConsts.PhoneMaxLength, MinimumLength = CustomerConsts.PhoneMinLength)]
        public string? Phone { get; set; } = "NULL";
        public string? Note { get; set; } = "NULL";
    }
}