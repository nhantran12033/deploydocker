using Volo.Abp.Application.Dtos;
using System;

namespace MEVN.DistributorApp.V2.Customers
{
    public abstract class GetCustomersInputBase : PagedAndSortedResultRequestDto
    {
        public string? FilterText { get; set; }

        public string? TaxCode { get; set; }
        public string? CusName { get; set; }
        public string? Address { get; set; }
        public string? Phone { get; set; }
        public string? Note { get; set; }

        public GetCustomersInputBase()
        {

        }
    }
}