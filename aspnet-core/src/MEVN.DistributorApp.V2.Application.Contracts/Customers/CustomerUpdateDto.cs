using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using Volo.Abp.Domain.Entities;

namespace MEVN.DistributorApp.V2.Customers
{
    public abstract class CustomerUpdateDtoBase : IHasConcurrencyStamp
    {
        [Required]
        [StringLength(CustomerConsts.TaxCodeMaxLength, MinimumLength = CustomerConsts.TaxCodeMinLength)]
        public string TaxCode { get; set; } = null!;
        [Required]
        [StringLength(CustomerConsts.CusNameMaxLength, MinimumLength = CustomerConsts.CusNameMinLength)]
        public string CusName { get; set; } = null!;
        [StringLength(CustomerConsts.AddressMaxLength, MinimumLength = CustomerConsts.AddressMinLength)]
        public string? Address { get; set; }
        [StringLength(CustomerConsts.PhoneMaxLength, MinimumLength = CustomerConsts.PhoneMinLength)]
        public string? Phone { get; set; }
        public string? Note { get; set; }

        public string ConcurrencyStamp { get; set; } = null!;
    }
}