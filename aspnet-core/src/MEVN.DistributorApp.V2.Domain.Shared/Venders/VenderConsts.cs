namespace MEVN.DistributorApp.V2.Venders
{
    public static class VenderConsts
    {
        private const string DefaultSorting = "{0}VerderName asc";

        public static string GetDefaultSorting(bool withEntityName)
        {
            return string.Format(DefaultSorting, withEntityName ? "Vender." : string.Empty);
        }

        public const int VerderNameMinLength = 1;
        public const int VerderNameMaxLength = 255;
    }
}