namespace MEVN.DistributorApp.V2.DistributorPurchaseOrders
{
    public static class DistributorPurchaseOrderConsts
    {
        private const string DefaultSorting = "{0}DPONo asc";

        public static string GetDefaultSorting(bool withEntityName)
        {
            return string.Format(DefaultSorting, withEntityName ? "DistributorPurchaseOrder." : string.Empty);
        }

        public const int DPONoMaxLength = 50;
        public const int FA_LVSMaxLength = 10;
        public const int NoteMaxLength = 4000;
        public const int PaymentInfoMaxLength = 255;
        public const int UserNameMaxLength = 50;
        public const int SPUserMaxLength = 100;
        public const int RemarkMaxLength = 255;
        public const int SPFileNameMaxLength = 255;
        public const int FileNameMaxLength = 255;
        public const int StatusCodeMaxLength = 50;
        public const int StockKeepingRequestNoMaxLength = 500;
    }
}