namespace MEVN.DistributorApp.V2.Customers
{
    public static class CustomerConsts
    {
        private const string DefaultSorting = "{0}TaxCode asc";

        public static string GetDefaultSorting(bool withEntityName)
        {
            return string.Format(DefaultSorting, withEntityName ? "Customer." : string.Empty);
        }

        public const int TaxCodeMinLength = 1;
        public const int TaxCodeMaxLength = 50;
        public const int CusNameMinLength = 1;
        public const int CusNameMaxLength = 255;
        public const int AddressMinLength = 1;
        public const int AddressMaxLength = 255;
        public const int PhoneMinLength = 1;
        public const int PhoneMaxLength = 255;
    }
}