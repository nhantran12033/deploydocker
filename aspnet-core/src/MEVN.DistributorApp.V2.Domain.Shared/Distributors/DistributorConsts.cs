namespace MEVN.DistributorApp.V2.Distributors
{
    public static class DistributorConsts
    {
        private const string DefaultSorting = "{0}DisCode asc";

        public static string GetDefaultSorting(bool withEntityName)
        {
            return string.Format(DefaultSorting, withEntityName ? "Distributor." : string.Empty);
        }

        public const int DisCodeMinLength = 1;
        public const int DisCodeMaxLength = 20;
        public const int DisNameMinLength = 1;
        public const int DisNameMaxLength = 200;
    }
}