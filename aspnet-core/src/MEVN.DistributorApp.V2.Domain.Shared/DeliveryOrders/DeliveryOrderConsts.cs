namespace MEVN.DistributorApp.V2.DeliveryOrders
{
    public static class DeliveryOrderConsts
    {
        private const string DefaultSorting = "{0}Status asc";

        public static string GetDefaultSorting(bool withEntityName)
        {
            return string.Format(DefaultSorting, withEntityName ? "DeliveryOrder." : string.Empty);
        }

        public const int DeliveryCodeMaxLength = 20;
        public const int DOSAPNoMaxLength = 50;
        public const int InvoiceNoMaxLength = 50;
        public const int BillingNoMaxLength = 50;
        public const int NoteMaxLength = 4000;
        public const int StatusCodeMaxLength = 50;
    }
}