﻿using Volo.Abp.Localization;

namespace MEVN.DistributorApp.V2.Localization;

[LocalizationResourceName("V2")]
public class V2Resource
{

}
