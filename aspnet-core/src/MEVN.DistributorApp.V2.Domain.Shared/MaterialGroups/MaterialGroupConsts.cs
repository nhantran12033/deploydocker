namespace MEVN.DistributorApp.V2.MaterialGroups
{
    public static class MaterialGroupConsts
    {
        private const string DefaultSorting = "{0}MaterialName asc";

        public static string GetDefaultSorting(bool withEntityName)
        {
            return string.Format(DefaultSorting, withEntityName ? "MaterialGroup." : string.Empty);
        }

    }
}