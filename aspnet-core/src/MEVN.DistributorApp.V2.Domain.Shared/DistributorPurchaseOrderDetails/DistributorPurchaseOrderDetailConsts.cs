namespace MEVN.DistributorApp.V2.DistributorPurchaseOrderDetails
{
    public static class DistributorPurchaseOrderDetailConsts
    {
        private const string DefaultSorting = "{0}Status asc";

        public static string GetDefaultSorting(bool withEntityName)
        {
            return string.Format(DefaultSorting, withEntityName ? "DistributorPurchaseOrderDetail." : string.Empty);
        }

        public const int GolfaCodeMaxLength = 40;
        public const int CustomerMaxLength = 400;
        public const int ModelMaxLength = 100;
        public const int NoteMaxLength = 4000;
        public const int CustomerNameMaxLength = 125;
        public const int StatusCodeMaxLength = 50;
    }
}