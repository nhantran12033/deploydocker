namespace MEVN.DistributorApp.V2.FreeOnShipMents
{
    public static class FreeOnShipMentConsts
    {
        private const string DefaultSorting = "{0}MaterialID asc";

        public static string GetDefaultSorting(bool withEntityName)
        {
            return string.Format(DefaultSorting, withEntityName ? "FreeOnShipMent." : string.Empty);
        }

        public const int PONoMinLength = 1;
        public const int PONoMaxLength = 255;
    }
}