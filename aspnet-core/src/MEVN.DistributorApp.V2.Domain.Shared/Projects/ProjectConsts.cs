namespace MEVN.DistributorApp.V2.Projects
{
    public static class ProjectConsts
    {
        private const string DefaultSorting = "{0}ProjectCode asc";

        public static string GetDefaultSorting(bool withEntityName)
        {
            return string.Format(DefaultSorting, withEntityName ? "Project." : string.Empty);
        }

        public const int ProjectCodeMinLength = 1;
        public const int ProjectCodeMaxLength = 20;
        public const int ProjectNameMinLength = 1;
        public const int ProjectNameMaxLength = 255;
        public const int ApprovalStatusMinLength = 1;
        public const int ApprovalStatusMaxLength = 255;
    }
}