namespace MEVN.DistributorApp.V2.MaterialStockCheckings
{
    public static class MaterialStockCheckingConsts
    {
        private const string DefaultSorting = "{0}GolfaCode asc";

        public static string GetDefaultSorting(bool withEntityName)
        {
            return string.Format(DefaultSorting, withEntityName ? "MaterialStockChecking." : string.Empty);
        }

    }
}