namespace MEVN.DistributorApp.V2.ProjectDetails
{
    public static class ProjectDetailConsts
    {
        private const string DefaultSorting = "{0}ProjectId asc";

        public static string GetDefaultSorting(bool withEntityName)
        {
            return string.Format(DefaultSorting, withEntityName ? "ProjectDetail." : string.Empty);
        }

        public const int GolfaCodeMinLength = 1;
        public const int GolfaCodeMaxLength = 40;
    }
}