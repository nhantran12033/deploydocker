namespace MEVN.DistributorApp.V2.ETADetails
{
    public static class ETADetailConsts
    {
        private const string DefaultSorting = "{0}StatusCode asc";

        public static string GetDefaultSorting(bool withEntityName)
        {
            return string.Format(DefaultSorting, withEntityName ? "ETADetail." : string.Empty);
        }

        public const int StatusCodeMaxLength = 50;
        public const int InvoiceNoMaxLength = 50;
        public const int GolfaCodeMaxLength = 50;
    }
}