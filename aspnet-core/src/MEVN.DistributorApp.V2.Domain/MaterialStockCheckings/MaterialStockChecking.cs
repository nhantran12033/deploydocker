using MEVN.DistributorApp.V2.MaterialGroups;
using MEVN.DistributorApp.V2.Venders;
using MEVN.DistributorApp.V2.Distributors;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Volo.Abp.Domain.Entities;
using Volo.Abp.Domain.Entities.Auditing;
using Volo.Abp.MultiTenancy;
using JetBrains.Annotations;

using Volo.Abp;
using MEVN.DistributorApp.V2.FreeOnShipMents;

namespace MEVN.DistributorApp.V2.MaterialStockCheckings
{
    public abstract class MaterialStockCheckingBase : FullAuditedAggregateRoot<Guid>
    {

        [NotNull]
        public virtual string GolfaCode { get; set; }

        [NotNull]
        public virtual string Model { get; set; }

        [CanBeNull]
        public virtual string? SAP_Code { get; set; }

        [NotNull]
        public virtual string Description_VN { get; set; }

        [CanBeNull]
        public virtual string? Description_Group { get; set; }

        public virtual int Standard_Price { get; set; }

        public virtual int? StockValueWarning { get; set; }

        public virtual int? StockTmp { get; set; }

        [CanBeNull]
        public virtual string? Spec1 { get; set; }

        [CanBeNull]
        public virtual string? Spec2 { get; set; }
        public Guid MaterialGroupId { get; set; }
        public Guid VenderId { get; set; }
        public Guid? DistributorId { get; set; }
        public List<FreeOnShipMent> ListFreeOnShipMent { get; set; }
        protected MaterialStockCheckingBase()
        {

        }

        public MaterialStockCheckingBase(Guid id, Guid materialGroupId, Guid venderId, Guid? distributorId, List<FreeOnShipMent> listFreeOnShipMent, string golfaCode, string model, string description_VN, int standard_Price, string? sAP_Code = null, string? description_Group = null, int? stockValueWarning = null, int? stockTmp = null, string? spec1 = null, string? spec2 = null)
        {

            Id = id;
            Check.NotNull(golfaCode, nameof(golfaCode));
            Check.NotNull(model, nameof(model));
            Check.NotNull(description_VN, nameof(description_VN));
            GolfaCode = golfaCode;
            Model = model;
            Description_VN = description_VN;
            Standard_Price = standard_Price;
            SAP_Code = sAP_Code;
            Description_Group = description_Group;
            StockValueWarning = stockValueWarning;
            StockTmp = stockTmp;
            Spec1 = spec1;
            Spec2 = spec2;
            MaterialGroupId = materialGroupId;
            VenderId = venderId;
            DistributorId = distributorId;
            ListFreeOnShipMent = listFreeOnShipMent;
        }

    }
}