using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Volo.Abp;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.Domain.Services;
using Volo.Abp.Data;
using MEVN.DistributorApp.V2.FreeOnShipMents;

namespace MEVN.DistributorApp.V2.MaterialStockCheckings
{
    public abstract class MaterialStockCheckingManagerBase : DomainService
    {
        protected IMaterialStockCheckingRepository _materialStockCheckingRepository;

        public MaterialStockCheckingManagerBase(IMaterialStockCheckingRepository materialStockCheckingRepository)
        {
            _materialStockCheckingRepository = materialStockCheckingRepository;
        }

        public virtual async Task<MaterialStockChecking> CreateAsync(
        Guid materialGroupId, Guid venderId, Guid? distributorId, List<FreeOnShipMent> listFreeOnShipMent, string golfaCode, string model, string description_VN, int standard_Price, string? sAP_Code = null, string? description_Group = null, int? stockValueWarning = null, int? stockTmp = null, string? spec1 = null, string? spec2 = null)
        {
            Check.NotNull(materialGroupId, nameof(materialGroupId));
            Check.NotNull(venderId, nameof(venderId));
            Check.NotNullOrWhiteSpace(golfaCode, nameof(golfaCode));
            Check.NotNullOrWhiteSpace(model, nameof(model));
            Check.NotNullOrWhiteSpace(description_VN, nameof(description_VN));

            var materialStockChecking = new MaterialStockChecking(
             GuidGenerator.Create(),
             materialGroupId, venderId, distributorId,listFreeOnShipMent,golfaCode, model, description_VN, standard_Price, sAP_Code, description_Group, stockValueWarning, stockTmp, spec1, spec2
             );
            materialStockChecking.ListFreeOnShipMent = listFreeOnShipMent;
            return await _materialStockCheckingRepository.InsertAsync(materialStockChecking);
        }

    }
}