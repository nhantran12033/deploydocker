using MEVN.DistributorApp.V2.MaterialGroups;
using MEVN.DistributorApp.V2.Venders;
using MEVN.DistributorApp.V2.Distributors;

using System;
using System.Collections.Generic;
using MEVN.DistributorApp.V2.FreeOnShipMents;

namespace MEVN.DistributorApp.V2.MaterialStockCheckings
{
    public abstract class MaterialStockCheckingWithNavigationPropertiesBase
    {
        public MaterialStockChecking MaterialStockChecking { get; set; } = null!;
        public List<FreeOnShipMent> FreeOnShipMent { get; set; } = null!;
        public MaterialGroup MaterialGroup { get; set; } = null!;
        public Vender Vender { get; set; } = null!;
        public Distributor Distributor { get; set; } = null!;
        

        
    }
}