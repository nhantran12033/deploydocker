using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace MEVN.DistributorApp.V2.MaterialStockCheckings
{
    public partial interface IMaterialStockCheckingRepository : IRepository<MaterialStockChecking, Guid>
    {
        Task<MaterialStockCheckingWithNavigationProperties> GetWithNavigationPropertiesAsync(
    Guid id,
    CancellationToken cancellationToken = default
);

        Task<List<MaterialStockCheckingWithNavigationProperties>> GetListWithNavigationPropertiesAsync(
            string? golfaCode = null,
            string? model = null,
            Guid? materialGroupId = null,
            Guid? venderId = null,
            Guid? distributorId = null,
            string? sorting = null,
            int maxResultCount = int.MaxValue,
            int skipCount = 0,
            CancellationToken cancellationToken = default
        );

        Task<List<MaterialStockChecking>> GetListAsync(
                    string? golfaCode = null,
                    string? model = null,
                     Guid? materialGroupId = null,
                    Guid? venderId = null,
                    Guid? distributorId = null,
                    string? sorting = null,
                    int maxResultCount = int.MaxValue,
                    int skipCount = 0,
                    CancellationToken cancellationToken = default
                );

        Task<long> GetCountAsync(
            string? golfaCode = null,
            string? model = null,
            Guid? materialGroupId = null,
            Guid? venderId = null,
            Guid? distributorId = null,
            CancellationToken cancellationToken = default);
    }
}