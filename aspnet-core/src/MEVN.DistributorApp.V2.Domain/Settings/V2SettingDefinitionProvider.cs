﻿using Volo.Abp.Settings;

namespace MEVN.DistributorApp.V2.Settings;

public class V2SettingDefinitionProvider : SettingDefinitionProvider
{
    public override void Define(ISettingDefinitionContext context)
    {
        //Define your own settings here. Example:
        //context.Add(new SettingDefinition(V2Settings.MySetting1));
    }
}
