﻿using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;

namespace MEVN.DistributorApp.V2.Data;

/* This is used if database provider does't define
 * IV2DbSchemaMigrator implementation.
 */
public class NullV2DbSchemaMigrator : IV2DbSchemaMigrator, ITransientDependency
{
    public Task MigrateAsync()
    {
        return Task.CompletedTask;
    }
}
