﻿using System.Threading.Tasks;

namespace MEVN.DistributorApp.V2.Data;

public interface IV2DbSchemaMigrator
{
    Task MigrateAsync();
}
