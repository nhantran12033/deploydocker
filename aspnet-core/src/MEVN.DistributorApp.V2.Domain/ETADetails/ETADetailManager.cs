using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Volo.Abp;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.Domain.Services;
using Volo.Abp.Data;

namespace MEVN.DistributorApp.V2.ETADetails
{
    public abstract class ETADetailManagerBase : DomainService
    {
        protected IETADetailRepository _eTADetailRepository;

        public ETADetailManagerBase(IETADetailRepository eTADetailRepository)
        {
            _eTADetailRepository = eTADetailRepository;
        }

        public virtual async Task<ETADetail> CreateAsync(
        string statusCode, string invoiceNo, string golfaCode, int qty, DateTime eTD, DateTime eTA, Guid distributorOrderDetailId)
        {
            Check.NotNullOrWhiteSpace(statusCode, nameof(statusCode));
            Check.Length(statusCode, nameof(statusCode), ETADetailConsts.StatusCodeMaxLength);
            Check.NotNullOrWhiteSpace(invoiceNo, nameof(invoiceNo));
            Check.Length(invoiceNo, nameof(invoiceNo), ETADetailConsts.InvoiceNoMaxLength);
            Check.NotNullOrWhiteSpace(golfaCode, nameof(golfaCode));
            Check.Length(golfaCode, nameof(golfaCode), ETADetailConsts.GolfaCodeMaxLength);
            Check.NotNull(eTD, nameof(eTD));
            Check.NotNull(eTA, nameof(eTA));

            var eTADetail = new ETADetail(
             GuidGenerator.Create(),
             statusCode, invoiceNo, golfaCode, qty, eTD, eTA, distributorOrderDetailId
             );

            return await _eTADetailRepository.InsertAsync(eTADetail);
        }

        public virtual async Task<ETADetail> UpdateAsync(
            Guid id,
            string statusCode, string invoiceNo, string golfaCode, int qty, DateTime eTD, DateTime eTA, Guid distributorOrderDetailId, [CanBeNull] string? concurrencyStamp = null
        )
        {
            Check.NotNullOrWhiteSpace(statusCode, nameof(statusCode));
            Check.Length(statusCode, nameof(statusCode), ETADetailConsts.StatusCodeMaxLength);
            Check.NotNullOrWhiteSpace(invoiceNo, nameof(invoiceNo));
            Check.Length(invoiceNo, nameof(invoiceNo), ETADetailConsts.InvoiceNoMaxLength);
            Check.NotNullOrWhiteSpace(golfaCode, nameof(golfaCode));
            Check.Length(golfaCode, nameof(golfaCode), ETADetailConsts.GolfaCodeMaxLength);
            Check.NotNull(eTD, nameof(eTD));
            Check.NotNull(eTA, nameof(eTA));

            var eTADetail = await _eTADetailRepository.GetAsync(id);

            eTADetail.StatusCode = statusCode;
            eTADetail.InvoiceNo = invoiceNo;
            eTADetail.GolfaCode = golfaCode;
            eTADetail.Qty = qty;
            eTADetail.ETD = eTD;
            eTADetail.ETA = eTA;
            eTADetail.DistributorOrderDetailId = distributorOrderDetailId;

            eTADetail.SetConcurrencyStampIfNotNull(concurrencyStamp);
            return await _eTADetailRepository.UpdateAsync(eTADetail);
        }

    }
}