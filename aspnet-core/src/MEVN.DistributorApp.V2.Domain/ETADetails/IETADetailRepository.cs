using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace MEVN.DistributorApp.V2.ETADetails
{
    public partial interface IETADetailRepository : IRepository<ETADetail, Guid>
    {
        Task<List<ETADetail>> GetListAsync(
            string? filterText = null,
            string? statusCode = null,
            string? invoiceNo = null,
            string? golfaCode = null,
            int? qtyMin = null,
            int? qtyMax = null,
            DateTime? eTDMin = null,
            DateTime? eTDMax = null,
            DateTime? eTAMin = null,
            DateTime? eTAMax = null,
            Guid? distributorOrderDetailId = null,
            string? sorting = null,
            int maxResultCount = int.MaxValue,
            int skipCount = 0,
            CancellationToken cancellationToken = default
        );

        Task<long> GetCountAsync(
            string? filterText = null,
            string? statusCode = null,
            string? invoiceNo = null,
            string? golfaCode = null,
            int? qtyMin = null,
            int? qtyMax = null,
            DateTime? eTDMin = null,
            DateTime? eTDMax = null,
            DateTime? eTAMin = null,
            DateTime? eTAMax = null,
            Guid? distributorOrderDetailId = null,
            CancellationToken cancellationToken = default);
    }
}