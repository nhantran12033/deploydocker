using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Volo.Abp.Domain.Entities;
using Volo.Abp.Domain.Entities.Auditing;
using Volo.Abp.MultiTenancy;
using JetBrains.Annotations;

using Volo.Abp;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace MEVN.DistributorApp.V2.ETADetails
{
    public abstract class ETADetailBase : FullAuditedAggregateRoot<Guid>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual Guid Id { get; set; }
        [NotNull]
        public virtual string StatusCode { get; set; }

        [NotNull]
        public virtual string InvoiceNo { get; set; }

        [NotNull]
        public virtual string GolfaCode { get; set; }

        public virtual int Qty { get; set; }

        public virtual DateTime ETD { get; set; }

        public virtual DateTime ETA { get; set; }

        public virtual Guid DistributorOrderDetailId { get; set; }

        protected ETADetailBase()
        {

        }

        public ETADetailBase(Guid id, string statusCode, string invoiceNo, string golfaCode, int qty, DateTime eTD, DateTime eTA, Guid distributorOrderDetailId)
        {

            Id = id;
            Check.NotNull(statusCode, nameof(statusCode));
            Check.Length(statusCode, nameof(statusCode), ETADetailConsts.StatusCodeMaxLength, 0);
            Check.NotNull(invoiceNo, nameof(invoiceNo));
            Check.Length(invoiceNo, nameof(invoiceNo), ETADetailConsts.InvoiceNoMaxLength, 0);
            Check.NotNull(golfaCode, nameof(golfaCode));
            Check.Length(golfaCode, nameof(golfaCode), ETADetailConsts.GolfaCodeMaxLength, 0);
            StatusCode = statusCode;
            InvoiceNo = invoiceNo;
            GolfaCode = golfaCode;
            Qty = qty;
            ETD = eTD;
            ETA = eTA;
            DistributorOrderDetailId = distributorOrderDetailId;
        }

    }
}