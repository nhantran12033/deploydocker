using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace MEVN.DistributorApp.V2.Distributors
{
    public partial interface IDistributorRepository : IRepository<Distributor, Guid>
    {
        Task<List<Distributor>> GetListAsync(
            string? filterText = null,
            string? disCode = null,
            string? disName = null,
            string? address = null,
            string? contactInfo = null,
            string? node = null,
            string? sorting = null,
            int maxResultCount = int.MaxValue,
            int skipCount = 0,
            CancellationToken cancellationToken = default
        );

        Task<long> GetCountAsync(
            string? filterText = null,
            string? disCode = null,
            string? disName = null,
            string? address = null,
            string? contactInfo = null,
            string? node = null,
            CancellationToken cancellationToken = default);
    }
}