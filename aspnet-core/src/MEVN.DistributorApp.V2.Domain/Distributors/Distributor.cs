using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Volo.Abp.Domain.Entities;
using Volo.Abp.Domain.Entities.Auditing;
using Volo.Abp.MultiTenancy;
using JetBrains.Annotations;

using Volo.Abp;

namespace MEVN.DistributorApp.V2.Distributors
{
    public abstract class DistributorBase : FullAuditedAggregateRoot<Guid>
    {
        [NotNull]
        public virtual string DisCode { get; set; }

        [NotNull]
        public virtual string DisName { get; set; }

        [CanBeNull]
        public virtual string? Address { get; set; }

        [CanBeNull]
        public virtual string? ContactInfo { get; set; }

        [CanBeNull]
        public virtual string? Node { get; set; }

        protected DistributorBase()
        {

        }

        public DistributorBase(Guid id, string disCode, string disName, string? address = null, string? contactInfo = null, string? node = null)
        {

            Id = id;
            Check.NotNull(disCode, nameof(disCode));
            Check.Length(disCode, nameof(disCode), DistributorConsts.DisCodeMaxLength, DistributorConsts.DisCodeMinLength);
            Check.NotNull(disName, nameof(disName));
            Check.Length(disName, nameof(disName), DistributorConsts.DisNameMaxLength, DistributorConsts.DisNameMinLength);
            DisCode = disCode;
            DisName = disName;
            Address = address;
            ContactInfo = contactInfo;
            Node = node;
        }

    }
}