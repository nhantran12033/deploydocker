using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Volo.Abp;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.Domain.Services;
using Volo.Abp.Data;

namespace MEVN.DistributorApp.V2.Distributors
{
    public abstract class DistributorManagerBase : DomainService
    {
        protected IDistributorRepository _distributorRepository;

        public DistributorManagerBase(IDistributorRepository distributorRepository)
        {
            _distributorRepository = distributorRepository;
        }

        public virtual async Task<Distributor> CreateAsync(
        string disCode, string disName, string? address = null, string? contactInfo = null, string? node = null)
        {
            Check.NotNullOrWhiteSpace(disCode, nameof(disCode));
            Check.Length(disCode, nameof(disCode), DistributorConsts.DisCodeMaxLength, DistributorConsts.DisCodeMinLength);
            Check.NotNullOrWhiteSpace(disName, nameof(disName));
            Check.Length(disName, nameof(disName), DistributorConsts.DisNameMaxLength, DistributorConsts.DisNameMinLength);

            var distributor = new Distributor(
             GuidGenerator.Create(),
             disCode, disName, address, contactInfo, node
             );

            return await _distributorRepository.InsertAsync(distributor);
        }

        public virtual async Task<Distributor> UpdateAsync(
            Guid id,
            string disCode, string disName, string? address = null, string? contactInfo = null, string? node = null, [CanBeNull] string? concurrencyStamp = null
        )
        {
            Check.NotNullOrWhiteSpace(disCode, nameof(disCode));
            Check.Length(disCode, nameof(disCode), DistributorConsts.DisCodeMaxLength, DistributorConsts.DisCodeMinLength);
            Check.NotNullOrWhiteSpace(disName, nameof(disName));
            Check.Length(disName, nameof(disName), DistributorConsts.DisNameMaxLength, DistributorConsts.DisNameMinLength);

            var distributor = await _distributorRepository.GetAsync(id);

            distributor.DisCode = disCode;
            distributor.DisName = disName;
            distributor.Address = address;
            distributor.ContactInfo = contactInfo;
            distributor.Node = node;

            distributor.SetConcurrencyStampIfNotNull(concurrencyStamp);
            return await _distributorRepository.UpdateAsync(distributor);
        }

    }
}