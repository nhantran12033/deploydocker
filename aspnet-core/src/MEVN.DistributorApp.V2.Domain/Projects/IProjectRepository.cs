using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace MEVN.DistributorApp.V2.Projects
{
    public partial interface IProjectRepository : IRepository<Project, Guid>
    {
        Task<ProjectWithNavigationProperties> GetWithNavigationPropertiesAsync(
    Guid id,
    CancellationToken cancellationToken = default
);

        Task<List<ProjectWithNavigationProperties>> GetListWithNavigationPropertiesAsync(
            Guid? id = null,
            string? sorting = null,
            int maxResultCount = int.MaxValue,
            int skipCount = 0,
            CancellationToken cancellationToken = default
        );
        Task<List<Project>> GetListAsync(

                    string? projectCode = null,
                    string? projectName = null,
                   
                    DateTime? pODateMin = null,
                    DateTime? pODateMax = null,
                    Guid? distributorId = null,
                    string? sorting = null,
                    int maxResultCount = int.MaxValue,
                    int skipCount = 0,
                    CancellationToken cancellationToken = default
                );

        Task<long> GetCountAsync(

            string? projectCode = null,
            string? projectName = null,
           
            DateTime? pODateMin = null,
            DateTime? pODateMax = null,
            Guid? distributorId = null,

            CancellationToken cancellationToken = default);
    }
}