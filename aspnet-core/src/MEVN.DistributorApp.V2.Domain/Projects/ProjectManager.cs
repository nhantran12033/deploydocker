using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Volo.Abp;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.Domain.Services;
using Volo.Abp.Data;
using MEVN.DistributorApp.V2.ProjectDetails;
using Microsoft.Extensions.Logging;


namespace MEVN.DistributorApp.V2.Projects
{
    public abstract class ProjectManagerBase : DomainService
    {
        protected IProjectRepository _projectRepository;
        protected IProjectDetailRepository _projectDetailRepository;
        public ProjectManagerBase(IProjectRepository projectRepository, IProjectDetailRepository projectDetailRepository)
        {
            _projectRepository = projectRepository;
            _projectDetailRepository = projectDetailRepository;
        }

        public virtual async Task<Project> CreateAsync(
        Guid distributorId, Guid? customerId,  List<ProjectDetail> listProjectDetail,string projectCode, string projectName, string approvalStatus, string? accountNo = null, string? projectType = null, string? location = null, string? distributorMagin = null, DateTime? pODate = null)
        {
            Check.NotNull(distributorId, nameof(distributorId));
            Check.NotNullOrWhiteSpace(projectCode, nameof(projectCode));
            Check.Length(projectCode, nameof(projectCode), ProjectConsts.ProjectCodeMaxLength, ProjectConsts.ProjectCodeMinLength);
            Check.NotNullOrWhiteSpace(projectName, nameof(projectName));
            Check.Length(projectName, nameof(projectName), ProjectConsts.ProjectNameMaxLength, ProjectConsts.ProjectNameMinLength);
            Check.NotNullOrWhiteSpace(approvalStatus, nameof(approvalStatus));
            Check.Length(approvalStatus, nameof(approvalStatus), ProjectConsts.ApprovalStatusMaxLength, ProjectConsts.ApprovalStatusMinLength);

            var project = new Project(
             GuidGenerator.Create(),
             distributorId, customerId, projectCode, projectName, approvalStatus, accountNo, projectType, location, distributorMagin, pODate
             );
            project.ListProjectDetail = listProjectDetail;
            return await _projectRepository.InsertAsync(project);
        }

    

    }
}