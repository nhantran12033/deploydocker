using MEVN.DistributorApp.V2.Distributors;
using MEVN.DistributorApp.V2.Customers;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Volo.Abp.Domain.Entities;
using Volo.Abp.Domain.Entities.Auditing;
using Volo.Abp.MultiTenancy;
using JetBrains.Annotations;

using Volo.Abp;
using MEVN.DistributorApp.V2.ProjectDetails;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace MEVN.DistributorApp.V2.Projects
{
    public abstract class ProjectBase : FullAuditedAggregateRoot<Guid>
    {
        [NotNull]
        public virtual string ProjectCode { get; set; }

        [NotNull]
        public virtual string ProjectName { get; set; }

        [NotNull]
        public virtual string ApprovalStatus { get; set; }

        [CanBeNull]
        public virtual string? AccountNo { get; set; }

        [CanBeNull]
        public virtual string? ProjectType { get; set; }

        [CanBeNull]
        public virtual string? Location { get; set; }

        [CanBeNull]
        public virtual string? DistributorMagin { get; set; }

        public virtual DateTime? PODate { get; set; }
        public Guid DistributorId { get; set; }
        public Guid? CustomerId { get; set; }
     
        public List<ProjectDetail> ListProjectDetail { get; set; }
        protected ProjectBase(Guid id)
        {

        }

        public ProjectBase(Guid id, Guid distributorId, Guid? customerId, string projectCode, string projectName, string approvalStatus, string? accountNo = null, string? projectType = null, string? location = null, string? distributorMagin = null, DateTime? pODate = null)
        {

            Id = id;
            Check.NotNull(projectCode, nameof(projectCode));
            Check.Length(projectCode, nameof(projectCode), ProjectConsts.ProjectCodeMaxLength, ProjectConsts.ProjectCodeMinLength);
            Check.NotNull(projectName, nameof(projectName));
            Check.Length(projectName, nameof(projectName), ProjectConsts.ProjectNameMaxLength, ProjectConsts.ProjectNameMinLength);
            Check.NotNull(approvalStatus, nameof(approvalStatus));
            Check.Length(approvalStatus, nameof(approvalStatus), ProjectConsts.ApprovalStatusMaxLength, ProjectConsts.ApprovalStatusMinLength);
            ProjectCode = projectCode;
            ProjectName = projectName;
            ApprovalStatus = approvalStatus;
            AccountNo = accountNo;
            ProjectType = projectType;
            Location = location;
            DistributorMagin = distributorMagin;
            PODate = pODate;
            DistributorId = distributorId;
            CustomerId = customerId;
        }

    }
}