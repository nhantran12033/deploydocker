using MEVN.DistributorApp.V2.Distributors;
using MEVN.DistributorApp.V2.Customers;

using System;
using System.Collections.Generic;
using MEVN.DistributorApp.V2.ProjectDetails;

namespace MEVN.DistributorApp.V2.Projects
{
    public abstract class ProjectWithNavigationPropertiesBase
    {
        public Project Project { get; set; } = null!;
        public List<ProjectDetail> ProjectDetail { get; set; } = null!;
        public Distributor Distributor { get; set; } = null!;
        public Customer Customer { get; set; } = null!;
        

        
    }
}