using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace MEVN.DistributorApp.V2.DistributorPurchaseOrders
{
    public partial interface IDistributorPurchaseOrderRepository : IRepository<DistributorPurchaseOrder, Guid>
    {
        Task<DistributorPurchaseOrderWithNavigationProperties> GetWithNavigationPropertiesAsync(
            Guid id,
            CancellationToken cancellationToken = default
        );

        Task<List<DistributorPurchaseOrderWithNavigationProperties>> GetListWithNavigationPropertiesAsync(
            string? dPONo = null,
            DateTime? orderDate = null,
            string? fA_LVS = null,
            string? statusCode = null,
            Guid? distributorId = null,
            string? sorting = null,
            int maxResultCount = int.MaxValue,
            int skipCount = 0,
            CancellationToken cancellationToken = default
        );

        Task<List<DistributorPurchaseOrder>> GetListAsync(
                 string? dPONo = null,
            DateTime? orderDate = null,
            string? fA_LVS = null,
            string? statusCode = null,
            Guid? distributorId = null,
            string? sorting = null,
            int maxResultCount = int.MaxValue,
            int skipCount = 0,
            CancellationToken cancellationToken = default
                );

        Task<long> GetCountAsync(

            string? dPONo = null,
            DateTime? orderDate = null,
            string? fA_LVS = null,
            string? statusCode = null,
            Guid? distributorId = null,
            string? sorting = null,
            CancellationToken cancellationToken = default);
    }
}