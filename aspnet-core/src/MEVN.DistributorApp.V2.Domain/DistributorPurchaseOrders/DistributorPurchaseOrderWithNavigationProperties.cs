using MEVN.DistributorApp.V2.listDistributorPurchaseOrderDetails;
using MEVN.DistributorApp.V2.Distributors;

using System;
using System.Collections.Generic;

namespace MEVN.DistributorApp.V2.DistributorPurchaseOrders
{
    public abstract class DistributorPurchaseOrderWithNavigationPropertiesBase
    {
        public DistributorPurchaseOrder DistributorPurchaseOrder { get; set; } = null!;
        public Distributor Distributor { get; set; } = null!;
        

        
    }
}