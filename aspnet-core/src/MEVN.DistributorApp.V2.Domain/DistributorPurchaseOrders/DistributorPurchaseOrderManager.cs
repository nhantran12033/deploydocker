﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Volo.Abp;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.Domain.Services;
using Volo.Abp.Data;
using MEVN.DistributorApp.V2.listDistributorPurchaseOrderDetails;
using MEVN.DistributorApp.V2.DistributorPurchaseOrderDetails;

namespace MEVN.DistributorApp.V2.DistributorPurchaseOrders
{
    public abstract class DistributorPurchaseOrderManagerBase : DomainService
    {
        protected IDistributorPurchaseOrderRepository _distributorPurchaseOrderRepository;

        public DistributorPurchaseOrderManagerBase(IDistributorPurchaseOrderRepository distributorPurchaseOrderRepository)
        {
            _distributorPurchaseOrderRepository = distributorPurchaseOrderRepository;
        }

        public virtual async Task<DistributorPurchaseOrder> CreateAsync(
        Guid? distributorId, List<DistributorPurchaseOrderDetail> listDistributorPurchaseOrderDetail, byte status, string? dPONo = null, DateTime? orderDate = null, float? totalAmount = null, string? fA_LVS = null, string? note = null, string? paymentInfo = null, DateTime? shipmentDate = null, string? userName = null, string? sPUser = null, string? remark = null, string? sPFileName = null, string? fileName = null, string? statusCode = null, string? stockKeepingRequestNo = null, byte? isKeepStock = null, string? validDate = null)
        {
            Check.Length(dPONo, nameof(dPONo), DistributorPurchaseOrderConsts.DPONoMaxLength);
            Check.Length(fA_LVS, nameof(fA_LVS), DistributorPurchaseOrderConsts.FA_LVSMaxLength);
            Check.Length(note, nameof(note), DistributorPurchaseOrderConsts.NoteMaxLength);
            Check.Length(paymentInfo, nameof(paymentInfo), DistributorPurchaseOrderConsts.PaymentInfoMaxLength);
            Check.Length(userName, nameof(userName), DistributorPurchaseOrderConsts.UserNameMaxLength);
            Check.Length(sPUser, nameof(sPUser), DistributorPurchaseOrderConsts.SPUserMaxLength);
            Check.Length(remark, nameof(remark), DistributorPurchaseOrderConsts.RemarkMaxLength);
            Check.Length(sPFileName, nameof(sPFileName), DistributorPurchaseOrderConsts.SPFileNameMaxLength);
            Check.Length(fileName, nameof(fileName), DistributorPurchaseOrderConsts.FileNameMaxLength);
            Check.Length(statusCode, nameof(statusCode), DistributorPurchaseOrderConsts.StatusCodeMaxLength);
            Check.Length(stockKeepingRequestNo, nameof(stockKeepingRequestNo), DistributorPurchaseOrderConsts.StockKeepingRequestNoMaxLength);

            var distributorPurchaseOrder = new DistributorPurchaseOrder(
             GuidGenerator.Create(),
             distributorId, listDistributorPurchaseOrderDetail,status, dPONo, orderDate, totalAmount, fA_LVS, note, paymentInfo, shipmentDate, userName, sPUser, remark, sPFileName, fileName, statusCode, stockKeepingRequestNo, isKeepStock, validDate
             );

            distributorPurchaseOrder.ListDistributorPurchaseOrderDetail = listDistributorPurchaseOrderDetail;
            return await _distributorPurchaseOrderRepository.InsertAsync(distributorPurchaseOrder);
        }

        //public virtual async Task<DistributorPurchaseOrder> UpdateAsync(
        //    Guid id,
        //    Guid? distributorId, byte status, string? dPONo = null, DateTime? orderDate = null, float? totalAmount = null, string? fA_LVS = null, string? note = null, string? paymentInfo = null, DateTime? shipmentDate = null, string? userName = null, string? sPUser = null, string? remark = null, string? sPFileName = null, string? fileName = null, string? statusCode = null, string? stockKeepingRequestNo = null, byte? isKeepStock = null, string? validDate = null, [CanBeNull] string? concurrencyStamp = null
        //)
        //{
        //    Check.Length(dPONo, nameof(dPONo), DistributorPurchaseOrderConsts.DPONoMaxLength);
        //    Check.Length(fA_LVS, nameof(fA_LVS), DistributorPurchaseOrderConsts.FA_LVSMaxLength);
        //    Check.Length(note, nameof(note), DistributorPurchaseOrderConsts.NoteMaxLength);
        //    Check.Length(paymentInfo, nameof(paymentInfo), DistributorPurchaseOrderConsts.PaymentInfoMaxLength);
        //    Check.Length(userName, nameof(userName), DistributorPurchaseOrderConsts.UserNameMaxLength);
        //    Check.Length(sPUser, nameof(sPUser), DistributorPurchaseOrderConsts.SPUserMaxLength);
        //    Check.Length(remark, nameof(remark), DistributorPurchaseOrderConsts.RemarkMaxLength);
        //    Check.Length(sPFileName, nameof(sPFileName), DistributorPurchaseOrderConsts.SPFileNameMaxLength);
        //    Check.Length(fileName, nameof(fileName), DistributorPurchaseOrderConsts.FileNameMaxLength);
        //    Check.Length(statusCode, nameof(statusCode), DistributorPurchaseOrderConsts.StatusCodeMaxLength);
        //    Check.Length(stockKeepingRequestNo, nameof(stockKeepingRequestNo), DistributorPurchaseOrderConsts.StockKeepingRequestNoMaxLength);

        //    var distributorPurchaseOrder = await _distributorPurchaseOrderRepository.GetAsync(id);

        //    distributorPurchaseOrder.DistributorId = distributorId;
        //    distributorPurchaseOrder.Status = status;
        //    distributorPurchaseOrder.DPONo = dPONo;
        //    distributorPurchaseOrder.OrderDate = orderDate;
        //    distributorPurchaseOrder.TotalAmount = totalAmount;
        //    distributorPurchaseOrder.FA_LVS = fA_LVS;
        //    distributorPurchaseOrder.Note = note;
        //    distributorPurchaseOrder.PaymentInfo = paymentInfo;
        //    distributorPurchaseOrder.ShipmentDate = shipmentDate;
        //    distributorPurchaseOrder.UserName = userName;
        //    distributorPurchaseOrder.SPUser = sPUser;
        //    distributorPurchaseOrder.Remark = remark;
        //    distributorPurchaseOrder.SPFileName = sPFileName;
        //    distributorPurchaseOrder.FileName = fileName;
        //    distributorPurchaseOrder.StatusCode = statusCode;
        //    distributorPurchaseOrder.StockKeepingRequestNo = stockKeepingRequestNo;
        //    distributorPurchaseOrder.IsKeepStock = isKeepStock;
        //    distributorPurchaseOrder.ValidDate = validDate;

        //    distributorPurchaseOrder.SetConcurrencyStampIfNotNull(concurrencyStamp);
        //    return await _distributorPurchaseOrderRepository.UpdateAsync(distributorPurchaseOrder);
        //}

    }
}
