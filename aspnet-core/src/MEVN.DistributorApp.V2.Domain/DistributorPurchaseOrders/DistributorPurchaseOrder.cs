using MEVN.DistributorApp.V2.Distributors;
using MEVN.DistributorApp.V2.listDistributorPurchaseOrderDetails;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Volo.Abp.Domain.Entities;
using Volo.Abp.Domain.Entities.Auditing;
using Volo.Abp.MultiTenancy;
using JetBrains.Annotations;

using Volo.Abp;

namespace MEVN.DistributorApp.V2.DistributorPurchaseOrders
{
    public abstract class DistributorPurchaseOrderBase : FullAuditedAggregateRoot<Guid>
    {
        [CanBeNull]
        public virtual string? DPONo { get; set; }

        public virtual byte Status { get; set; }

        public virtual DateTime? OrderDate { get; set; }

        public virtual float? TotalAmount { get; set; }

        [CanBeNull]
        public virtual string? FA_LVS { get; set; }

        [CanBeNull]
        public virtual string? Note { get; set; }

        [CanBeNull]
        public virtual string? PaymentInfo { get; set; }

        public virtual DateTime? ShipmentDate { get; set; }

        [CanBeNull]
        public virtual string? UserName { get; set; }

        [CanBeNull]
        public virtual string? SPUser { get; set; }

        [CanBeNull]
        public virtual string? Remark { get; set; }

        [CanBeNull]
        public virtual string? SPFileName { get; set; }

        [CanBeNull]
        public virtual string? FileName { get; set; }

        [CanBeNull]
        public virtual string? StatusCode { get; set; }

        [CanBeNull]
        public virtual string? StockKeepingRequestNo { get; set; }

        public virtual byte? IsKeepStock { get; set; }

        [CanBeNull]
        public virtual string? ValidDate { get; set; }
        public Guid? DistributorId { get; set; }
        public List<DistributorPurchaseOrderDetail> ListDistributorPurchaseOrderDetail { get; set; }
        protected DistributorPurchaseOrderBase()
        {

        }

        public DistributorPurchaseOrderBase(Guid id, Guid? distributorId, List<DistributorPurchaseOrderDetail> listDistributorPurchaseOrderDetail, byte status, string? dPONo = null, DateTime? orderDate = null, float? totalAmount = null, string? fA_LVS = null, string? note = null, string? paymentInfo = null, DateTime? shipmentDate = null, string? userName = null, string? sPUser = null, string? remark = null, string? sPFileName = null, string? fileName = null, string? statusCode = null, string? stockKeepingRequestNo = null, byte? isKeepStock = null, string? validDate = null)
        {

            Id = id;
            Check.Length(dPONo, nameof(dPONo), DistributorPurchaseOrderConsts.DPONoMaxLength, 0);
            Check.Length(fA_LVS, nameof(fA_LVS), DistributorPurchaseOrderConsts.FA_LVSMaxLength, 0);
            Check.Length(note, nameof(note), DistributorPurchaseOrderConsts.NoteMaxLength, 0);
            Check.Length(paymentInfo, nameof(paymentInfo), DistributorPurchaseOrderConsts.PaymentInfoMaxLength, 0);
            Check.Length(userName, nameof(userName), DistributorPurchaseOrderConsts.UserNameMaxLength, 0);
            Check.Length(sPUser, nameof(sPUser), DistributorPurchaseOrderConsts.SPUserMaxLength, 0);
            Check.Length(remark, nameof(remark), DistributorPurchaseOrderConsts.RemarkMaxLength, 0);
            Check.Length(sPFileName, nameof(sPFileName), DistributorPurchaseOrderConsts.SPFileNameMaxLength, 0);
            Check.Length(fileName, nameof(fileName), DistributorPurchaseOrderConsts.FileNameMaxLength, 0);
            Check.Length(statusCode, nameof(statusCode), DistributorPurchaseOrderConsts.StatusCodeMaxLength, 0);
            Check.Length(stockKeepingRequestNo, nameof(stockKeepingRequestNo), DistributorPurchaseOrderConsts.StockKeepingRequestNoMaxLength, 0);
            Status = status;
            DPONo = dPONo;
            OrderDate = orderDate;
            TotalAmount = totalAmount;
            FA_LVS = fA_LVS;
            Note = note;
            PaymentInfo = paymentInfo;
            ShipmentDate = shipmentDate;
            UserName = userName;
            SPUser = sPUser;
            Remark = remark;
            SPFileName = sPFileName;
            FileName = fileName;
            StatusCode = statusCode;
            StockKeepingRequestNo = stockKeepingRequestNo;
            IsKeepStock = isKeepStock;
            ValidDate = validDate;
            DistributorId = distributorId;
            ListDistributorPurchaseOrderDetail = listDistributorPurchaseOrderDetail;
        }

    }
}