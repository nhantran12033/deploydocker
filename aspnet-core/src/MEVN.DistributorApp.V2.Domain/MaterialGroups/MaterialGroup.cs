using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Volo.Abp.Domain.Entities;
using Volo.Abp.Domain.Entities.Auditing;
using Volo.Abp.MultiTenancy;
using JetBrains.Annotations;

using Volo.Abp;

namespace MEVN.DistributorApp.V2.MaterialGroups
{
    public abstract class MaterialGroupBase : FullAuditedAggregateRoot<Guid>
    {
        [NotNull]
        public virtual string MaterialName { get; set; }

        [NotNull]
        public virtual string DisCode { get; set; }

        protected MaterialGroupBase()
        {

        }

        public MaterialGroupBase(Guid id, string materialName, string disCode)
        {

            Id = id;
            Check.NotNull(materialName, nameof(materialName));
            Check.NotNull(disCode, nameof(disCode));
            MaterialName = materialName;
            DisCode = disCode;
        }

    }
}