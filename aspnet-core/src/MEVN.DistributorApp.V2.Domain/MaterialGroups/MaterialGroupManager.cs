using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Volo.Abp;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.Domain.Services;
using Volo.Abp.Data;

namespace MEVN.DistributorApp.V2.MaterialGroups
{
    public abstract class MaterialGroupManagerBase : DomainService
    {
        protected IMaterialGroupRepository _materialGroupRepository;

        public MaterialGroupManagerBase(IMaterialGroupRepository materialGroupRepository)
        {
            _materialGroupRepository = materialGroupRepository;
        }

        public virtual async Task<MaterialGroup> CreateAsync(
        string materialName, string disCode)
        {
            Check.NotNullOrWhiteSpace(materialName, nameof(materialName));
            Check.NotNullOrWhiteSpace(disCode, nameof(disCode));

            var materialGroup = new MaterialGroup(
             GuidGenerator.Create(),
             materialName, disCode
             );

            return await _materialGroupRepository.InsertAsync(materialGroup);
        }

        public virtual async Task<MaterialGroup> UpdateAsync(
            Guid id,
            string materialName, string disCode, [CanBeNull] string? concurrencyStamp = null
        )
        {
            Check.NotNullOrWhiteSpace(materialName, nameof(materialName));
            Check.NotNullOrWhiteSpace(disCode, nameof(disCode));

            var materialGroup = await _materialGroupRepository.GetAsync(id);

            materialGroup.MaterialName = materialName;
            materialGroup.DisCode = disCode;

            materialGroup.SetConcurrencyStampIfNotNull(concurrencyStamp);
            return await _materialGroupRepository.UpdateAsync(materialGroup);
        }

    }
}