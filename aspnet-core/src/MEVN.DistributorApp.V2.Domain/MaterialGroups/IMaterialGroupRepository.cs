using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace MEVN.DistributorApp.V2.MaterialGroups
{
    public partial interface IMaterialGroupRepository : IRepository<MaterialGroup, Guid>
    {
        Task<List<MaterialGroup>> GetListAsync(
            string? filterText = null,
            string? materialName = null,
            string? disCode = null,
            string? sorting = null,
            int maxResultCount = int.MaxValue,
            int skipCount = 0,
            CancellationToken cancellationToken = default
        );

        Task<long> GetCountAsync(
            string? filterText = null,
            string? materialName = null,
            string? disCode = null,
            CancellationToken cancellationToken = default);
    }
}