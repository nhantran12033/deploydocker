using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Volo.Abp.Domain.Entities;
using Volo.Abp.Domain.Entities.Auditing;
using Volo.Abp.MultiTenancy;
using JetBrains.Annotations;

using Volo.Abp;

namespace MEVN.DistributorApp.V2.Customers
{
    public abstract class CustomerBase : FullAuditedAggregateRoot<Guid>
    {
        [NotNull]
        public virtual string TaxCode { get; set; }

        [NotNull]
        public virtual string CusName { get; set; }

        [CanBeNull]
        public virtual string? Address { get; set; }

        [CanBeNull]
        public virtual string? Phone { get; set; }

        [CanBeNull]
        public virtual string? Note { get; set; }

        protected CustomerBase()
        {

        }

        public CustomerBase(Guid id, string taxCode, string cusName, string? address = null, string? phone = null, string? note = null)
        {

            Id = id;
            Check.NotNull(taxCode, nameof(taxCode));
            Check.Length(taxCode, nameof(taxCode), CustomerConsts.TaxCodeMaxLength, CustomerConsts.TaxCodeMinLength);
            Check.NotNull(cusName, nameof(cusName));
            Check.Length(cusName, nameof(cusName), CustomerConsts.CusNameMaxLength, CustomerConsts.CusNameMinLength);
            Check.Length(address, nameof(address), CustomerConsts.AddressMaxLength, CustomerConsts.AddressMinLength);
            Check.Length(phone, nameof(phone), CustomerConsts.PhoneMaxLength, CustomerConsts.PhoneMinLength);
            TaxCode = taxCode;
            CusName = cusName;
            Address = address;
            Phone = phone;
            Note = note;
        }

    }
}