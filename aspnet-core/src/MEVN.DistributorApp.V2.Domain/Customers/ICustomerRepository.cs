using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace MEVN.DistributorApp.V2.Customers
{
    public partial interface ICustomerRepository : IRepository<Customer, Guid>
    {
        Task<List<Customer>> GetListAsync(
            string? filterText = null,
            string? taxCode = null,
            string? cusName = null,
            string? address = null,
            string? phone = null,
            string? note = null,
            string? sorting = null,
            int maxResultCount = int.MaxValue,
            int skipCount = 0,
            CancellationToken cancellationToken = default
        );

        Task<long> GetCountAsync(
            string? filterText = null,
            string? taxCode = null,
            string? cusName = null,
            string? address = null,
            string? phone = null,
            string? note = null,
            CancellationToken cancellationToken = default);
    }
}