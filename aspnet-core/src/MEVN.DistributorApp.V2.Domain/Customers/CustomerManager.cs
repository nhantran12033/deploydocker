using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Volo.Abp;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.Domain.Services;
using Volo.Abp.Data;

namespace MEVN.DistributorApp.V2.Customers
{
    public abstract class CustomerManagerBase : DomainService
    {
        protected ICustomerRepository _customerRepository;

        public CustomerManagerBase(ICustomerRepository customerRepository)
        {
            _customerRepository = customerRepository;
        }

        public virtual async Task<Customer> CreateAsync(
        string taxCode, string cusName, string? address = null, string? phone = null, string? note = null)
        {
            Check.NotNullOrWhiteSpace(taxCode, nameof(taxCode));
            Check.Length(taxCode, nameof(taxCode), CustomerConsts.TaxCodeMaxLength, CustomerConsts.TaxCodeMinLength);
            Check.NotNullOrWhiteSpace(cusName, nameof(cusName));
            Check.Length(cusName, nameof(cusName), CustomerConsts.CusNameMaxLength, CustomerConsts.CusNameMinLength);
            Check.Length(address, nameof(address), CustomerConsts.AddressMaxLength, CustomerConsts.AddressMinLength);
            Check.Length(phone, nameof(phone), CustomerConsts.PhoneMaxLength, CustomerConsts.PhoneMinLength);

            var customer = new Customer(
             GuidGenerator.Create(),
             taxCode, cusName, address, phone, note
             );

            return await _customerRepository.InsertAsync(customer);
        }

        public virtual async Task<Customer> UpdateAsync(
            Guid id,
            string taxCode, string cusName, string? address = null, string? phone = null, string? note = null, [CanBeNull] string? concurrencyStamp = null
        )
        {
            Check.NotNullOrWhiteSpace(taxCode, nameof(taxCode));
            Check.Length(taxCode, nameof(taxCode), CustomerConsts.TaxCodeMaxLength, CustomerConsts.TaxCodeMinLength);
            Check.NotNullOrWhiteSpace(cusName, nameof(cusName));
            Check.Length(cusName, nameof(cusName), CustomerConsts.CusNameMaxLength, CustomerConsts.CusNameMinLength);
            Check.Length(address, nameof(address), CustomerConsts.AddressMaxLength, CustomerConsts.AddressMinLength);
            Check.Length(phone, nameof(phone), CustomerConsts.PhoneMaxLength, CustomerConsts.PhoneMinLength);

            var customer = await _customerRepository.GetAsync(id);

            customer.TaxCode = taxCode;
            customer.CusName = cusName;
            customer.Address = address;
            customer.Phone = phone;
            customer.Note = note;

            customer.SetConcurrencyStampIfNotNull(concurrencyStamp);
            return await _customerRepository.UpdateAsync(customer);
        }

    }
}