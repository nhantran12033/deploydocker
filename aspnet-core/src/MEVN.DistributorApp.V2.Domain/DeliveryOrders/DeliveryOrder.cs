using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Volo.Abp.Domain.Entities;
using Volo.Abp.Domain.Entities.Auditing;
using Volo.Abp.MultiTenancy;
using JetBrains.Annotations;

using Volo.Abp;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace MEVN.DistributorApp.V2.DeliveryOrders
{
    public abstract class DeliveryOrderBase : FullAuditedAggregateRoot<Guid>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual Guid Id { get; set; }
        public virtual byte Status { get; set; }

        public virtual DateTime? DeliveryDate { get; set; }

        [CanBeNull]
        public virtual string? DeliveryCode { get; set; }

        [CanBeNull]
        public virtual string? DOSAPNo { get; set; }

        public virtual DateTime? InvoiceDate { get; set; }

        [CanBeNull]
        public virtual string? InvoiceNo { get; set; }

        [CanBeNull]
        public virtual string? BillingNo { get; set; }

        [CanBeNull]
        public virtual string? Note { get; set; }

        [CanBeNull]
        public virtual string? StatusCode { get; set; }

        public virtual int Qty { get; set; }

        public virtual Guid DistributorOrderDetailId { get; set; }

        protected DeliveryOrderBase()
        {

        }

        public DeliveryOrderBase(Guid id, byte status, int qty, Guid distributorOrderDetailId, DateTime? deliveryDate = null, string? deliveryCode = null, string? dOSAPNo = null, DateTime? invoiceDate = null, string? invoiceNo = null, string? billingNo = null, string? note = null, string? statusCode = null)
        {

            Id = id;
            Check.Length(deliveryCode, nameof(deliveryCode), DeliveryOrderConsts.DeliveryCodeMaxLength, 0);
            Check.Length(dOSAPNo, nameof(dOSAPNo), DeliveryOrderConsts.DOSAPNoMaxLength, 0);
            Check.Length(invoiceNo, nameof(invoiceNo), DeliveryOrderConsts.InvoiceNoMaxLength, 0);
            Check.Length(billingNo, nameof(billingNo), DeliveryOrderConsts.BillingNoMaxLength, 0);
            Check.Length(note, nameof(note), DeliveryOrderConsts.NoteMaxLength, 0);
            Check.Length(statusCode, nameof(statusCode), DeliveryOrderConsts.StatusCodeMaxLength, 0);
            Status = status;
            Qty = qty;
            DistributorOrderDetailId = distributorOrderDetailId;
            DeliveryDate = deliveryDate;
            DeliveryCode = deliveryCode;
            DOSAPNo = dOSAPNo;
            InvoiceDate = invoiceDate;
            InvoiceNo = invoiceNo;
            BillingNo = billingNo;
            Note = note;
            StatusCode = statusCode;
        }

    }
}