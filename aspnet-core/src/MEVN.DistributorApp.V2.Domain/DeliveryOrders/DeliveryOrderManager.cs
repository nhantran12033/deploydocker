using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Volo.Abp;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.Domain.Services;
using Volo.Abp.Data;

namespace MEVN.DistributorApp.V2.DeliveryOrders
{
    public abstract class DeliveryOrderManagerBase : DomainService
    {
        protected IDeliveryOrderRepository _deliveryOrderRepository;

        public DeliveryOrderManagerBase(IDeliveryOrderRepository deliveryOrderRepository)
        {
            _deliveryOrderRepository = deliveryOrderRepository;
        }

        public virtual async Task<DeliveryOrder> CreateAsync(
        byte status, int qty, Guid distributorOrderDetailId, DateTime? deliveryDate = null, string? deliveryCode = null, string? dOSAPNo = null, DateTime? invoiceDate = null, string? invoiceNo = null, string? billingNo = null, string? note = null, string? statusCode = null)
        {
            Check.Length(deliveryCode, nameof(deliveryCode), DeliveryOrderConsts.DeliveryCodeMaxLength);
            Check.Length(dOSAPNo, nameof(dOSAPNo), DeliveryOrderConsts.DOSAPNoMaxLength);
            Check.Length(invoiceNo, nameof(invoiceNo), DeliveryOrderConsts.InvoiceNoMaxLength);
            Check.Length(billingNo, nameof(billingNo), DeliveryOrderConsts.BillingNoMaxLength);
            Check.Length(note, nameof(note), DeliveryOrderConsts.NoteMaxLength);
            Check.Length(statusCode, nameof(statusCode), DeliveryOrderConsts.StatusCodeMaxLength);

            var deliveryOrder = new DeliveryOrder(
             GuidGenerator.Create(),
             status, qty, distributorOrderDetailId, deliveryDate, deliveryCode, dOSAPNo, invoiceDate, invoiceNo, billingNo, note, statusCode
             );

            return await _deliveryOrderRepository.InsertAsync(deliveryOrder);
        }

        public virtual async Task<DeliveryOrder> UpdateAsync(
            Guid id,
            byte status, int qty, Guid distributorOrderDetailId, DateTime? deliveryDate = null, string? deliveryCode = null, string? dOSAPNo = null, DateTime? invoiceDate = null, string? invoiceNo = null, string? billingNo = null, string? note = null, string? statusCode = null, [CanBeNull] string? concurrencyStamp = null
        )
        {
            Check.Length(deliveryCode, nameof(deliveryCode), DeliveryOrderConsts.DeliveryCodeMaxLength);
            Check.Length(dOSAPNo, nameof(dOSAPNo), DeliveryOrderConsts.DOSAPNoMaxLength);
            Check.Length(invoiceNo, nameof(invoiceNo), DeliveryOrderConsts.InvoiceNoMaxLength);
            Check.Length(billingNo, nameof(billingNo), DeliveryOrderConsts.BillingNoMaxLength);
            Check.Length(note, nameof(note), DeliveryOrderConsts.NoteMaxLength);
            Check.Length(statusCode, nameof(statusCode), DeliveryOrderConsts.StatusCodeMaxLength);

            var deliveryOrder = await _deliveryOrderRepository.GetAsync(id);

            deliveryOrder.Status = status;
            deliveryOrder.Qty = qty;
            deliveryOrder.DistributorOrderDetailId = distributorOrderDetailId;
            deliveryOrder.DeliveryDate = deliveryDate;
            deliveryOrder.DeliveryCode = deliveryCode;
            deliveryOrder.DOSAPNo = dOSAPNo;
            deliveryOrder.InvoiceDate = invoiceDate;
            deliveryOrder.InvoiceNo = invoiceNo;
            deliveryOrder.BillingNo = billingNo;
            deliveryOrder.Note = note;
            deliveryOrder.StatusCode = statusCode;

            deliveryOrder.SetConcurrencyStampIfNotNull(concurrencyStamp);
            return await _deliveryOrderRepository.UpdateAsync(deliveryOrder);
        }

    }
}