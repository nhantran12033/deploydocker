using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace MEVN.DistributorApp.V2.DeliveryOrders
{
    public partial interface IDeliveryOrderRepository : IRepository<DeliveryOrder, Guid>
    {
        Task<List<DeliveryOrder>> GetListAsync(
            string? filterText = null,
            byte? statusMin = null,
            byte? statusMax = null,
            DateTime? deliveryDateMin = null,
            DateTime? deliveryDateMax = null,
            string? deliveryCode = null,
            string? dOSAPNo = null,
            DateTime? invoiceDateMin = null,
            DateTime? invoiceDateMax = null,
            string? invoiceNo = null,
            string? billingNo = null,
            string? note = null,
            string? statusCode = null,
            int? qtyMin = null,
            int? qtyMax = null,
            Guid? distributorOrderDetailId = null,
            string? sorting = null,
            int maxResultCount = int.MaxValue,
            int skipCount = 0,
            CancellationToken cancellationToken = default
        );

        Task<long> GetCountAsync(
            string? filterText = null,
            byte? statusMin = null,
            byte? statusMax = null,
            DateTime? deliveryDateMin = null,
            DateTime? deliveryDateMax = null,
            string? deliveryCode = null,
            string? dOSAPNo = null,
            DateTime? invoiceDateMin = null,
            DateTime? invoiceDateMax = null,
            string? invoiceNo = null,
            string? billingNo = null,
            string? note = null,
            string? statusCode = null,
            int? qtyMin = null,
            int? qtyMax = null,
            Guid? distributorOrderDetailId = null,
            CancellationToken cancellationToken = default);
    }
}