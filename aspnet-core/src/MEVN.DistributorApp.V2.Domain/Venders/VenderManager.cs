using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Volo.Abp;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.Domain.Services;
using Volo.Abp.Data;

namespace MEVN.DistributorApp.V2.Venders
{
    public abstract class VenderManagerBase : DomainService
    {
        protected IVenderRepository _venderRepository;

        public VenderManagerBase(IVenderRepository venderRepository)
        {
            _venderRepository = venderRepository;
        }

        public virtual async Task<Vender> CreateAsync(
        string verderName, string? address = null, string? email = null)
        {
            Check.NotNullOrWhiteSpace(verderName, nameof(verderName));
            Check.Length(verderName, nameof(verderName), VenderConsts.VerderNameMaxLength, VenderConsts.VerderNameMinLength);

            var vender = new Vender(
             GuidGenerator.Create(),
             verderName, address, email
             );

            return await _venderRepository.InsertAsync(vender);
        }

        public virtual async Task<Vender> UpdateAsync(
            Guid id,
            string verderName, string? address = null, string? email = null, [CanBeNull] string? concurrencyStamp = null
        )
        {
            Check.NotNullOrWhiteSpace(verderName, nameof(verderName));
            Check.Length(verderName, nameof(verderName), VenderConsts.VerderNameMaxLength, VenderConsts.VerderNameMinLength);

            var vender = await _venderRepository.GetAsync(id);

            vender.VerderName = verderName;
            vender.Address = address;
            vender.Email = email;

            vender.SetConcurrencyStampIfNotNull(concurrencyStamp);
            return await _venderRepository.UpdateAsync(vender);
        }

    }
}