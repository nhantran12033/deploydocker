using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace MEVN.DistributorApp.V2.Venders
{
    public partial interface IVenderRepository : IRepository<Vender, Guid>
    {
        Task<List<Vender>> GetListAsync(
            string? filterText = null,
            string? verderName = null,
            string? address = null,
            string? email = null,
            string? sorting = null,
            int maxResultCount = int.MaxValue,
            int skipCount = 0,
            CancellationToken cancellationToken = default
        );

        Task<long> GetCountAsync(
            string? filterText = null,
            string? verderName = null,
            string? address = null,
            string? email = null,
            CancellationToken cancellationToken = default);
    }
}