using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Volo.Abp.Domain.Entities;
using Volo.Abp.Domain.Entities.Auditing;
using Volo.Abp.MultiTenancy;
using JetBrains.Annotations;

using Volo.Abp;

namespace MEVN.DistributorApp.V2.Venders
{
    public abstract class VenderBase : FullAuditedAggregateRoot<Guid>
    {
        [NotNull]
        public virtual string VerderName { get; set; }

        [CanBeNull]
        public virtual string? Address { get; set; }

        [CanBeNull]
        public virtual string? Email { get; set; }

        protected VenderBase()
        {

        }

        public VenderBase(Guid id, string verderName, string? address = null, string? email = null)
        {

            Id = id;
            Check.NotNull(verderName, nameof(verderName));
            Check.Length(verderName, nameof(verderName), VenderConsts.VerderNameMaxLength, VenderConsts.VerderNameMinLength);
            VerderName = verderName;
            Address = address;
            Email = email;
        }

    }
}