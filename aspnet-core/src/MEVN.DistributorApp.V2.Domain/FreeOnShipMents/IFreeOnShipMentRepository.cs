using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace MEVN.DistributorApp.V2.FreeOnShipMents
{
    public interface IFreeOnShipMentRepository : IRepository<FreeOnShipMent, Guid>
    {
        Task<List<FreeOnShipMent>> GetListAsync(
            string? filterText = null,
            Guid? materialID = null,
            string? pONo = null,
            DateTime? pODateMin = null,
            DateTime? pODateMax = null,
            int? qtyMin = null,
            int? qtyMax = null,
            string? machineNumber = null,
            string? sorting = null,
            int maxResultCount = int.MaxValue,
            int skipCount = 0,
            CancellationToken cancellationToken = default
        );

        Task<long> GetCountAsync(
            string? filterText = null,
            Guid? materialID = null,
            string? pONo = null,
            DateTime? pODateMin = null,
            DateTime? pODateMax = null,
            int? qtyMin = null,
            int? qtyMax = null,
            string? machineNumber = null,
            CancellationToken cancellationToken = default);
    }
}