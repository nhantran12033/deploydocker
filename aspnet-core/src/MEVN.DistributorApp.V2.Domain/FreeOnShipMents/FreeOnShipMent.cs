using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Volo.Abp.Domain.Entities;
using Volo.Abp.Domain.Entities.Auditing;
using Volo.Abp.MultiTenancy;
using JetBrains.Annotations;

using Volo.Abp;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace MEVN.DistributorApp.V2.FreeOnShipMents
{
    public class FreeOnShipMent : FullAuditedAggregateRoot<Guid>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual Guid Id { get; set; }
        public virtual Guid MaterialID { get; set; }

        [NotNull]
        public virtual string PONo { get; set; }

        public virtual DateTime PODate { get; set; }

        public virtual int? Qty { get; set; }

        [CanBeNull]
        public virtual string? MachineNumber { get; set; }

        protected FreeOnShipMent()
        {

        }

        public FreeOnShipMent(Guid id, Guid materialID, string pONo, DateTime pODate, int? qty = null, string? machineNumber = null)
        {

            Id = id;
            Check.NotNull(pONo, nameof(pONo));
            Check.Length(pONo, nameof(pONo), FreeOnShipMentConsts.PONoMaxLength, FreeOnShipMentConsts.PONoMinLength);
            MaterialID = materialID;
            PONo = pONo;
            PODate = pODate;
            Qty = qty;
            MachineNumber = machineNumber;
        }

    }
}