using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Volo.Abp;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.Domain.Services;
using Volo.Abp.Data;

namespace MEVN.DistributorApp.V2.FreeOnShipMents
{
    public class FreeOnShipMentManager : DomainService
    {
        protected IFreeOnShipMentRepository _freeOnShipMentRepository;

        public FreeOnShipMentManager(IFreeOnShipMentRepository freeOnShipMentRepository)
        {
            _freeOnShipMentRepository = freeOnShipMentRepository;
        }

        public virtual async Task<FreeOnShipMent> CreateAsync(
        Guid materialID, string pONo, DateTime pODate, int? qty = null, string? machineNumber = null)
        {
            Check.NotNullOrWhiteSpace(pONo, nameof(pONo));
            Check.Length(pONo, nameof(pONo), FreeOnShipMentConsts.PONoMaxLength, FreeOnShipMentConsts.PONoMinLength);
            Check.NotNull(pODate, nameof(pODate));

            var freeOnShipMent = new FreeOnShipMent(
             GuidGenerator.Create(),
             materialID, pONo, pODate, qty, machineNumber
             );

            return await _freeOnShipMentRepository.InsertAsync(freeOnShipMent);
        }

        public virtual async Task<FreeOnShipMent> UpdateAsync(
            Guid id,
            Guid materialID, [CanBeNull] string? concurrencyStamp = null
        )
        {

            var freeOnShipMent = await _freeOnShipMentRepository.GetAsync(id);

            freeOnShipMent.MaterialID = materialID;

            freeOnShipMent.SetConcurrencyStampIfNotNull(concurrencyStamp);
            return await _freeOnShipMentRepository.UpdateAsync(freeOnShipMent);
        }

    }
}