using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Volo.Abp;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.Domain.Services;
using Volo.Abp.Data;
using MEVN.DistributorApp.V2.listDistributorPurchaseOrderDetails;
using MEVN.DistributorApp.V2.DeliveryOrders;
using MEVN.DistributorApp.V2.ETADetails;

namespace MEVN.DistributorApp.V2.DistributorPurchaseOrderDetails
{
    public abstract class DistributorPurchaseOrderDetailManagerBase : DomainService
    {
        protected IDistributorPurchaseOrderDetailRepository _distributorPurchaseOrderDetailRepository;

        public DistributorPurchaseOrderDetailManagerBase(IDistributorPurchaseOrderDetailRepository distributorPurchaseOrderDetailRepository)
        {
            _distributorPurchaseOrderDetailRepository = distributorPurchaseOrderDetailRepository;
        }

        public virtual async Task<DistributorPurchaseOrderDetail> CreateAsync(
        byte status, List<ETADetail> listETADetail, List<DeliveryOrder> listDeliveryOrder, Guid distributorPurchaseOrderId, string? golfaCode = null, string? customer = null, string? model = null, int? qty = null, float? price = null, float? amount = null, DateTime? requestedETA = null, int? delivered = null, int? inProgress = null, string? note = null, byte? eTA = null, string? customerName = null, string? statusCode = null)
        {
            Check.Length(golfaCode, nameof(golfaCode), DistributorPurchaseOrderDetailConsts.GolfaCodeMaxLength);
            Check.Length(customer, nameof(customer), DistributorPurchaseOrderDetailConsts.CustomerMaxLength);
            Check.Length(model, nameof(model), DistributorPurchaseOrderDetailConsts.ModelMaxLength);
            Check.Length(note, nameof(note), DistributorPurchaseOrderDetailConsts.NoteMaxLength);
            Check.Length(customerName, nameof(customerName), DistributorPurchaseOrderDetailConsts.CustomerNameMaxLength);
            Check.Length(statusCode, nameof(statusCode), DistributorPurchaseOrderDetailConsts.StatusCodeMaxLength);

            var distributorPurchaseOrderDetail = new DistributorPurchaseOrderDetail(
             GuidGenerator.Create(),
             listETADetail, listDeliveryOrder, status,distributorPurchaseOrderId, golfaCode, customer, model, qty, price, amount, requestedETA, delivered, inProgress, note, eTA, customerName, statusCode
             );
            distributorPurchaseOrderDetail.ListDeliveryOrder = listDeliveryOrder;
            distributorPurchaseOrderDetail.ListETADetail = listETADetail;
            return await _distributorPurchaseOrderDetailRepository.InsertAsync(distributorPurchaseOrderDetail);
        }

       

    }
}