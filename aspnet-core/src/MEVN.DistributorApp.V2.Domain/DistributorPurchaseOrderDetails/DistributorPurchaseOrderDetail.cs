using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Volo.Abp.Domain.Entities;
using Volo.Abp.Domain.Entities.Auditing;
using Volo.Abp.MultiTenancy;
using JetBrains.Annotations;

using Volo.Abp;
using MEVN.DistributorApp.V2.ETADetails;
using MEVN.DistributorApp.V2.DeliveryOrders;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace MEVN.DistributorApp.V2.DistributorPurchaseOrderDetails
{
    public abstract class DistributorPurchaseOrderDetailBase : FullAuditedAggregateRoot<Guid>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual Guid Id { get; set; }
        public virtual byte Status { get; set; }

        [CanBeNull]
        public virtual string? GolfaCode { get; set; }

        [CanBeNull]
        public virtual string? Customer { get; set; }

        [CanBeNull]
        public virtual string? Model { get; set; }

        public virtual int? Qty { get; set; }

        public virtual float? Price { get; set; }

        public virtual float? Amount { get; set; }

        public virtual DateTime? RequestedETA { get; set; }

        public virtual int? Delivered { get; set; }

        public virtual int? InProgress { get; set; }

        [CanBeNull]
        public virtual string? Note { get; set; }

        public virtual byte? ETA { get; set; }

        [CanBeNull]
        public virtual string? CustomerName { get; set; }

        [CanBeNull]
        public virtual string? StatusCode { get; set; }

        public virtual Guid DistributorPurchaseOrderId { get; set; }
        public List<ETADetail> ListETADetail { get; set; }
        public List<DeliveryOrder> ListDeliveryOrder { get; set; }

        protected DistributorPurchaseOrderDetailBase()
        {

        }

        public DistributorPurchaseOrderDetailBase(Guid id, List<ETADetail> listETADetail, List<DeliveryOrder> listDeliveryOrder,byte status, Guid distributorPurchaseOrderId, string? golfaCode = null, string? customer = null, string? model = null, int? qty = null, float? price = null, float? amount = null, DateTime? requestedETA = null, int? delivered = null, int? inProgress = null, string? note = null, byte? eTA = null, string? customerName = null, string? statusCode = null)
        {

            Id = id;
            Check.Length(golfaCode, nameof(golfaCode), DistributorPurchaseOrderDetailConsts.GolfaCodeMaxLength, 0);
            Check.Length(customer, nameof(customer), DistributorPurchaseOrderDetailConsts.CustomerMaxLength, 0);
            Check.Length(model, nameof(model), DistributorPurchaseOrderDetailConsts.ModelMaxLength, 0);
            Check.Length(note, nameof(note), DistributorPurchaseOrderDetailConsts.NoteMaxLength, 0);
            Check.Length(customerName, nameof(customerName), DistributorPurchaseOrderDetailConsts.CustomerNameMaxLength, 0);
            Check.Length(statusCode, nameof(statusCode), DistributorPurchaseOrderDetailConsts.StatusCodeMaxLength, 0);
            Status = status;
            DistributorPurchaseOrderId = distributorPurchaseOrderId;
            GolfaCode = golfaCode;
            Customer = customer;
            Model = model;
            Qty = qty;
            Price = price;
            Amount = amount;
            RequestedETA = requestedETA;
            Delivered = delivered;
            InProgress = inProgress;
            Note = note;
            ETA = eTA;
            CustomerName = customerName;
            StatusCode = statusCode;
            ListETADetail = listETADetail;
            ListDeliveryOrder = listDeliveryOrder;
        }

    }
}