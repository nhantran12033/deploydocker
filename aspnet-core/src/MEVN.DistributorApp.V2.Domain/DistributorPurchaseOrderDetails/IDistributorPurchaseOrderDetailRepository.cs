using MEVN.DistributorApp.V2.listDistributorPurchaseOrderDetails;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace MEVN.DistributorApp.V2.DistributorPurchaseOrderDetails
{
    public partial interface IDistributorPurchaseOrderDetailRepository : IRepository<DistributorPurchaseOrderDetail, Guid>
    {
        Task<List<DistributorPurchaseOrderDetail>> GetListAsync(
            string? filterText = null,
            byte? statusMin = null,
            byte? statusMax = null,
            string? golfaCode = null,
            string? customer = null,
            string? model = null,
            int? qtyMin = null,
            int? qtyMax = null,
            float? priceMin = null,
            float? priceMax = null,
            float? amountMin = null,
            float? amountMax = null,
            DateTime? requestedETAMin = null,
            DateTime? requestedETAMax = null,
            int? deliveredMin = null,
            int? deliveredMax = null,
            int? inProgressMin = null,
            int? inProgressMax = null,
            string? note = null,
            byte? eTAMin = null,
            byte? eTAMax = null,
            string? customerName = null,
            string? statusCode = null,
            Guid? distributorPurchaseOrderId = null,
            string? sorting = null,
            int maxResultCount = int.MaxValue,
            int skipCount = 0,
            CancellationToken cancellationToken = default
        );

        Task<long> GetCountAsync(
            string? filterText = null,
            byte? statusMin = null,
            byte? statusMax = null,
            string? golfaCode = null,
            string? customer = null,
            string? model = null,
            int? qtyMin = null,
            int? qtyMax = null,
            float? priceMin = null,
            float? priceMax = null,
            float? amountMin = null,
            float? amountMax = null,
            DateTime? requestedETAMin = null,
            DateTime? requestedETAMax = null,
            int? deliveredMin = null,
            int? deliveredMax = null,
            int? inProgressMin = null,
            int? inProgressMax = null,
            string? note = null,
            byte? eTAMin = null,
            byte? eTAMax = null,
            string? customerName = null,
            string? statusCode = null,
            Guid? distributorPurchaseOrderId = null,
            CancellationToken cancellationToken = default);
    }
}