using MEVN.DistributorApp.V2.DeliveryOrders;
using MEVN.DistributorApp.V2.DistributorPurchaseOrders;
using MEVN.DistributorApp.V2.listDistributorPurchaseOrderDetails;
using System;
using System.Collections.Generic;

namespace MEVN.DistributorApp.V2.DistributorPurchaseOrderDetails
{
    public abstract class DistributorPurchaseOrderDetailWithNavigationPropertiesBase
    {
        public DistributorPurchaseOrderDetail DistributorPurchaseOrderDetail { get; set; } = null!;
        public List<DeliveryOrder> DeliveryOrder { get; set; } = null!;

        public DistributorPurchaseOrder DistributorPurchaseOrder { get; set; } = null!;
        

        
    }
}