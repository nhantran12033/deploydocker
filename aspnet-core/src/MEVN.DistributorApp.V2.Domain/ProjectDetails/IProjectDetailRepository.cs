using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace MEVN.DistributorApp.V2.ProjectDetails
{
    public interface IProjectDetailRepository : IRepository<ProjectDetail, Guid>
    {
        Task<List<ProjectDetail>> GetListAsync(
            string? filterText = null,
            Guid? projectId = null,
            string? golfaCode = null,
            string? model = null,
            int? qtyMin = null,
            int? qtyMax = null,
            int? dpoUsedMin = null,
            int? dpoUsedMax = null,
            string? requestStatus = null,
            float? requestPriceMin = null,
            float? requestPriceMax = null,
            float? distRequestedPriceMin = null,
            float? distRequestedPriceMax = null,
            float? saleOfferPriceMin = null,
            float? saleOfferPriceMax = null,
            float? saleAllowDiscountPriceMin = null,
            float? saleAllowDiscountPriceMax = null,
            float? amountRequestedPriceMin = null,
            float? amountRequestedPriceMax = null,
            string? statusCode = null,
            string? sorting = null,
            int maxResultCount = int.MaxValue,
            int skipCount = 0,
            CancellationToken cancellationToken = default
        );

        Task<long> GetCountAsync(
            string? filterText = null,
            Guid? projectId = null,
            string? golfaCode = null,
            string? model = null,
            int? qtyMin = null,
            int? qtyMax = null,
            int? dpoUsedMin = null,
            int? dpoUsedMax = null,
            string? requestStatus = null,
            float? requestPriceMin = null,
            float? requestPriceMax = null,
            float? distRequestedPriceMin = null,
            float? distRequestedPriceMax = null,
            float? saleOfferPriceMin = null,
            float? saleOfferPriceMax = null,
            float? saleAllowDiscountPriceMin = null,
            float? saleAllowDiscountPriceMax = null,
            float? amountRequestedPriceMin = null,
            float? amountRequestedPriceMax = null,
            string? statusCode = null,
            CancellationToken cancellationToken = default);
    }
}