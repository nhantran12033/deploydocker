using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Volo.Abp;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.Domain.Services;
using Volo.Abp.Data;

namespace MEVN.DistributorApp.V2.ProjectDetails
{
    public class ProjectDetailManager : DomainService
    {
        protected IProjectDetailRepository _projectDetailRepository;

        public ProjectDetailManager(IProjectDetailRepository projectDetailRepository)
        {
            _projectDetailRepository = projectDetailRepository;
        }

        public virtual async Task<ProjectDetail> CreateAsync(
        Guid projectId, string golfaCode, int qty, float distRequestedPrice, float saleOfferPrice, float saleAllowDiscountPrice, float amountRequestedPrice, string statusCode, string? model = null, int? dpoUsed = null, string? requestStatus = null, float? requestPrice = null)
        {
            Check.NotNullOrWhiteSpace(golfaCode, nameof(golfaCode));
            Check.Length(golfaCode, nameof(golfaCode), ProjectDetailConsts.GolfaCodeMaxLength, ProjectDetailConsts.GolfaCodeMinLength);
            Check.NotNullOrWhiteSpace(statusCode, nameof(statusCode));

            var projectDetail = new ProjectDetail(
             GuidGenerator.Create(),
             projectId, golfaCode, qty, distRequestedPrice, saleOfferPrice, saleAllowDiscountPrice, amountRequestedPrice, statusCode, model, dpoUsed, requestStatus, requestPrice
             );

            return await _projectDetailRepository.InsertAsync(projectDetail);
        }

        public virtual async Task<ProjectDetail> UpdateAsync(
            Guid id,
            Guid projectId, string golfaCode, int qty, float distRequestedPrice, float saleOfferPrice, float saleAllowDiscountPrice, float amountRequestedPrice, string? model = null, int? dpoUsed = null, string? requestStatus = null, float? requestPrice = null, [CanBeNull] string? concurrencyStamp = null
        )
        {
            Check.NotNullOrWhiteSpace(golfaCode, nameof(golfaCode));
            Check.Length(golfaCode, nameof(golfaCode), ProjectDetailConsts.GolfaCodeMaxLength, ProjectDetailConsts.GolfaCodeMinLength);

            var projectDetail = await _projectDetailRepository.GetAsync(id);

            projectDetail.ProjectId = projectId;
            projectDetail.GolfaCode = golfaCode;
            projectDetail.Qty = qty;
            projectDetail.DistRequestedPrice = distRequestedPrice;
            projectDetail.SaleOfferPrice = saleOfferPrice;
            projectDetail.SaleAllowDiscountPrice = saleAllowDiscountPrice;
            projectDetail.AmountRequestedPrice = amountRequestedPrice;
            projectDetail.Model = model;
            projectDetail.DpoUsed = dpoUsed;
            projectDetail.RequestStatus = requestStatus;
            projectDetail.RequestPrice = requestPrice;

            projectDetail.SetConcurrencyStampIfNotNull(concurrencyStamp);
            return await _projectDetailRepository.UpdateAsync(projectDetail);
        }

    }
}