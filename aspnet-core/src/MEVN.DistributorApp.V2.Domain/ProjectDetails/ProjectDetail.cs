using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Volo.Abp.Domain.Entities;
using Volo.Abp.Domain.Entities.Auditing;
using Volo.Abp.MultiTenancy;
using JetBrains.Annotations;

using Volo.Abp;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace MEVN.DistributorApp.V2.ProjectDetails
{
    public class ProjectDetail : FullAuditedAggregateRoot<Guid>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual Guid Id { get; set; }
        public virtual Guid ProjectId { get; set; }

        [NotNull]
        public virtual string GolfaCode { get; set; }

        [CanBeNull]
        public virtual string? Model { get; set; }

        public virtual int Qty { get; set; }

        public virtual int? DpoUsed { get; set; }

        [CanBeNull]
        public virtual string? RequestStatus { get; set; }

        public virtual float? RequestPrice { get; set; }

        public virtual float DistRequestedPrice { get; set; }

        public virtual float SaleOfferPrice { get; set; }

        public virtual float SaleAllowDiscountPrice { get; set; }

        public virtual float AmountRequestedPrice { get; set; }

        [NotNull]
        public virtual string StatusCode { get; set; }

        protected ProjectDetail()
        {

        }

        public ProjectDetail(Guid id, Guid projectId, string golfaCode, int qty, float distRequestedPrice, float saleOfferPrice, float saleAllowDiscountPrice, float amountRequestedPrice, string statusCode, string? model = null, int? dpoUsed = null, string? requestStatus = null, float? requestPrice = null)
        {

            Id = id;
            Check.NotNull(golfaCode, nameof(golfaCode));
            Check.Length(golfaCode, nameof(golfaCode), ProjectDetailConsts.GolfaCodeMaxLength, ProjectDetailConsts.GolfaCodeMinLength);
            Check.NotNull(statusCode, nameof(statusCode));
            ProjectId = projectId;
            GolfaCode = golfaCode;
            Qty = qty;
            DistRequestedPrice = distRequestedPrice;
            SaleOfferPrice = saleOfferPrice;
            SaleAllowDiscountPrice = saleAllowDiscountPrice;
            AmountRequestedPrice = amountRequestedPrice;
            StatusCode = statusCode;
            Model = model;
            DpoUsed = dpoUsed;
            RequestStatus = requestStatus;
            RequestPrice = requestPrice;
        }

    }
}