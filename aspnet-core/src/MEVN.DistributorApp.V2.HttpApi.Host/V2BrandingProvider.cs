﻿using Volo.Abp.DependencyInjection;
using Volo.Abp.Ui.Branding;

namespace MEVN.DistributorApp.V2;

[Dependency(ReplaceServices = true)]
public class V2BrandingProvider : DefaultBrandingProvider
{
    public override string AppName => "V2";
}
