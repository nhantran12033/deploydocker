import { NgModule } from '@angular/core';
import { AuthGuard, PermissionGuard } from '@abp/ng.core';
import { MitsubishiElectricComponent } from './mitsubishi-electric.component';
import { RouterModule, Routes, mapToCanActivate } from '@angular/router';
const routes: Routes = [{ path: '', component: MitsubishiElectricComponent, canActivate: mapToCanActivate([AuthGuard, PermissionGuard]), }];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MitsubishiElectricRoutingModule { }
