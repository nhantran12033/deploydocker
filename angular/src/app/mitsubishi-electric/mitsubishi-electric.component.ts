import { DistributorCreateDto, DistributorDto, GetDistributorsInput } from './../proxy/distributors/models';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { DistributorService } from '@proxy/distributors';
import { GetMaterialGroupsInput, MaterialGroupDto, MaterialGroupService } from '@proxy/material-groups';
import { GetMaterialStockCheckingsInput, MaterialStockCheckingDto, MaterialStockCheckingExcelDownloadDto, MaterialStockCheckingExcelDownloadDtoBase, MaterialStockCheckingService } from '@proxy/material-stock-checkings';
import { LookupRequestDto } from '@proxy/shared';
import { GetVendersInput, VenderDto, VenderService } from '@proxy/venders';
import { DownloadTokenResultDto } from '@proxy/volo/abp/gdpr';
import { MatSnackBar } from '@angular/material/snack-bar';


@Component({
  selector: 'app-mitsubishi-electric',
  templateUrl: './mitsubishi-electric.component.html',
  styleUrls: ['./mitsubishi-electric.component.scss']
})
export class MitsubishiElectricComponent implements OnInit {

  Distributor: string = '';
  Vender: string = '';
  Material_Group: string = '';
  Golfacode: string;
  Model: string = '';

  get_data: any;
  data: any;
  display_select_vender: any;
  venderIdToNameMap: any;
  groupMaterialIdToNameMap: any;
  get_data_Distributor: DistributorDto[];
  get_data_venders: VenderDto[];
  get_data_Material_Group: MaterialGroupDto[];
  materialStockCheckings: MaterialStockCheckingDto[] = [];
  formValid: boolean = false;

  constructor(
    private MaterialStockCheckingService: MaterialStockCheckingService,
    private VenderService: VenderService,
    private MaterialGroupService: MaterialGroupService,
    private DistributorService: DistributorService,
    private snackBar: MatSnackBar,

  ) { }

  checkFormValidity(): void {
    this.formValid = false;

    if (this.Distributor && this.Vender && this.Material_Group) {
      this.formValid = true;
    }
  }

  private showSnackbar(message: string): void {
    this.snackBar.open(message, 'Close', {
      duration: 5000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
      panelClass: ['custom-snackbar'],
    });
  }

  ngOnInit(): void {

    this.VenderService.getList(this.vender_input).subscribe((venders) => {
      this.get_data_venders = venders.items
      this.venderIdToNameMap = {};
      venders.items.forEach(vender => {
        this.venderIdToNameMap[vender.id] = vender.verderName;
      });
    })

    this.MaterialGroupService.getList(this.materiaGroup_input).subscribe((groupMaterial) => {
      this.get_data_Material_Group = groupMaterial.items;
      this.groupMaterialIdToNameMap = {};
      groupMaterial.items.forEach(groupMaterial => {
        this.groupMaterialIdToNameMap[groupMaterial.id] = groupMaterial.materialName;
      })
    })

    this.DistributorService.getList(this.Distributor_input).subscribe((Distributor) => {
      this.get_data_Distributor = Distributor.items;
    })

  }

  Distributor_input: GetDistributorsInput = {
    filterText: '',
    disCode: '',
    disName: '',
    address: '',
    contactInfo: '',
    node: '',
    sorting: '',
    skipCount: 0,
    maxResultCount: 10
  }

  vender_input: GetVendersInput = {
    filterText: '',
    verderName: '',
    address: '',
    email: '',
    sorting: '',
    skipCount: 0,
    maxResultCount: 10
  }

  materiaGroup_input: GetMaterialGroupsInput = {
    filterText: '',
    materialName: '',
    disCode: '',
    sorting: '',
    skipCount: 0,
    maxResultCount: 10
  }

  getMateriaGroup(material_Id: string): string {
    return this.groupMaterialIdToNameMap[material_Id] || material_Id;
  }

  getVenderName(venderId: string): string {
    return this.venderIdToNameMap[venderId] || venderId;
  }

  searchParams: GetMaterialStockCheckingsInput = {
    golfaCode: '',
    model: '',
    distributorId: '',
    venderId: '',
    materialGroupId: '',
    maxResultCount: 10,
    sorting: '',
    skipCount: 0
  }

  onClickLoad(): void {

    this.checkFormValidity();

    if (this.formValid) {
      const distributorid = this.get_data_Distributor.find(d => d.disName === this.Distributor)
      this.searchParams.distributorId = distributorid.id;

      const venderid = this.get_data_venders.find(d => d.verderName === this.Vender)
      this.searchParams.venderId = venderid.id;

      const materialid = this.get_data_Material_Group.find(d => d.materialName === this.Material_Group)
      this.searchParams.materialGroupId = materialid.id;

      this.searchParams.golfaCode = this.Golfacode;
      this.searchParams.model = this.Model
      this.MaterialStockCheckingService.getList(this.searchParams).subscribe((result) => {
        this.dataSource.data = result.items;
      });

    } else {
      // console.log('Form is not valid. Please fill in all required fields.');
      this.showSnackbar('Please select Distributor, Vender and Material Group');
    }


    // const distributorid = this.get_data_Distributor.find(d => d.disName === this.Distributor)
    // this.searchParams.distributorId = distributorid.id;

    // // const venderid = this.get_data_venders.find(d => d.verderName === this.Vender)
    // // this.searchParams.venderId = venderid.id;

    // const materialid = this.get_data_Material_Group.find(d => d.materialName === this.Material_Group)
    // this.searchParams.materialGroupId = materialid.id;

    // this.searchParams.golfaCode = this.Golfacode;
    // this.searchParams.model = this.Model
    // // console.log(this.searchParams)
    // this.MaterialStockCheckingService.getList(this.searchParams).subscribe((result) => {
    //   this.dataSource.data = result.items;
    // });


  }

  downloadInput: MaterialStockCheckingExcelDownloadDto = {
    downloadToken: '',
    golfaCode: '',
    model: '',
    materialGroupId: '',
    venderId: '',
    distributorId: '',
  };
  distributorid: string

  // downloadExcel(): void {

  //   this.MaterialStockCheckingService.getDownloadToken().subscribe((result) => {
  //     this.downloadInput.downloadToken = result.token
  //     const distributorid = this.get_data_Distributor.find(d => d.disName === this.Distributor)
  //     this.downloadInput.distributorId = distributorid.id;

  //     //const venderid = this.get_data_venders.find(d => d.verderName === this.Vender)
  //     //this.downloadInput.venderId = venderid.id;

  //     const materialid = this.get_data_Material_Group.find(d => d.materialName === this.Material_Group)
  //     this.downloadInput.materialGroupId = materialid.id;

  //     this.downloadInput.golfaCode = this.Golfacode;

  //     this.downloadInput.model = this.Model
  //     this.MaterialStockCheckingService.getListAsExcelFile(this.downloadInput).subscribe(
  //       (data: Blob) => {
  //         this.downloadFile(data);
  //       },
  //       (error) => {
  //         console.error('Error downloading Excel file', error);
  //         // Handle error if needed
  //       }
  //     );
  //   })

  // }

  downloadExcel(): void {

    if (this.formValid) {
      this.MaterialStockCheckingService.getDownloadToken().subscribe((result) => {
        this.downloadInput.downloadToken = result.token
        const distributorid = this.get_data_Distributor.find(d => d.disName === this.Distributor)
        this.downloadInput.distributorId = distributorid.id;

        //const venderid = this.get_data_venders.find(d => d.verderName === this.Vender)
        //this.downloadInput.venderId = venderid.id;

        const materialid = this.get_data_Material_Group.find(d => d.materialName === this.Material_Group)
        this.downloadInput.materialGroupId = materialid.id;

        this.downloadInput.golfaCode = this.Golfacode;

        this.downloadInput.model = this.Model
        this.MaterialStockCheckingService.getListAsExcelFile(this.downloadInput).subscribe(
          (data: Blob) => {
            this.downloadFile(data);
          },
          (error) => {
            console.error('Error downloading Excel file', error);
          }
        );
      })
    } else {
      this.showSnackbar('Please select Distributor, Vender and Material Group');

    }

  }

  private downloadFile(blobData: Blob): void {
    const blobUrl = URL.createObjectURL(blobData);
    const link = document.createElement('a');
    link.href = blobUrl;
    link.download = 'material_stock_checkings.xlsx'; // Set the file name
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }

  displayedColumns: string[] = ['golfaCode', 'model', 'description_VN', 'venderId', 'materialGroupId', 'stockValueWarning', 'stockTmp', 'standard_Price', 'spec1', 'spec2'];
  dataSource = new MatTableDataSource<MaterialStockCheckingDto>();

  @ViewChild(MatPaginator) paginator: MatPaginator;

  // eslint-disable-next-line @angular-eslint/use-lifecycle-interface
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  // private showSnackbar(message: string): void {
  //   this.snackBar.open(message, 'Close', {
  //     duration: 5000, // 5 seconds
  //     horizontalPosition: 'center', // 'start' | 'center' | 'end' | 'left' | 'right'
  //     verticalPosition: 'top', // 'top' | 'bottom' | 'center'
  //     panelClass: ['custom-snackbar'], // Custom class for styling
  //   });
  // }

}
