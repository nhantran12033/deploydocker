import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MitsubishiElectricRoutingModule } from './mitsubishi-electric-routing.module';
import { MitsubishiElectricComponent } from './mitsubishi-electric.component';
import { FormsModule } from '@angular/forms';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';


@NgModule({
  declarations: [
    MitsubishiElectricComponent
  ],
  imports: [
    CommonModule,
    MitsubishiElectricRoutingModule,
    MatSnackBarModule,

    MatFormFieldModule,
    MatSelectModule,
    MatButtonModule,
    MatInputModule,
    MatPaginatorModule,
    MatTableModule,
    FormsModule
  ]
})
export class MitsubishiElectricModule { }
