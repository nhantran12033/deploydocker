import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MitsubishiElectricComponent } from './mitsubishi-electric.component';

describe('MitsubishiElectricComponent', () => {
  let component: MitsubishiElectricComponent;
  let fixture: ComponentFixture<MitsubishiElectricComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MitsubishiElectricComponent]
    });
    fixture = TestBed.createComponent(MitsubishiElectricComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
