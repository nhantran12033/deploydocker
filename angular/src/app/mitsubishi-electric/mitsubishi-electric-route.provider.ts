import { eLayoutType, RoutesService } from '@abp/ng.core';
import { APP_INITIALIZER } from '@angular/core';

export const MITSUBISHI_ELECTRIC_ROUTE_PROVIDER = [
    { provide: APP_INITIALIZER, useFactory: configureRoutes, deps: [RoutesService], multi: true },
];

function configureRoutes(routes: RoutesService) {
    return () => {
        routes.add([
            {
                path: '/material-stock-checking',
                iconClass: 'fas fa-briefcase',
                name: 'Material Stock Checking',
                layout: eLayoutType.application,
                requiredPolicy: 'V2.MaterialStockCheckings',
            },
        ]);
    };
}