import { AuthGuard, PermissionGuard } from '@abp/ng.core';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    loadChildren: () => import('./home/home.module').then(m => m.HomeModule),
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule),
    canActivate: [AuthGuard, PermissionGuard],
  },
  {
    path: 'account',
    loadChildren: () =>
      import('@volo/abp.ng.account/public').then(m => m.AccountPublicModule.forLazy()),
  },
  {
    path: 'gdpr',
    loadChildren: () => import('@volo/abp.ng.gdpr').then(m => m.GdprModule.forLazy()),
  },
  {
    path: 'identity',
    loadChildren: () => import('@volo/abp.ng.identity').then(m => m.IdentityModule.forLazy()),
  },
  {
    path: 'language-management',
    loadChildren: () =>
      import('@volo/abp.ng.language-management').then(m => m.LanguageManagementModule.forLazy()),
  },
  {
    path: 'saas',
    loadChildren: () => import('@volo/abp.ng.saas').then(m => m.SaasModule.forLazy()),
  },
  {
    path: 'audit-logs',
    loadChildren: () =>
      import('@volo/abp.ng.audit-logging').then(m => m.AuditLoggingModule.forLazy()),
  },
  {
    path: 'openiddict',
    loadChildren: () =>
      import('@volo/abp.ng.openiddictpro').then(m => m.OpeniddictproModule.forLazy()),
  },
  {
    path: 'text-template-management',
    loadChildren: () =>
      import('@volo/abp.ng.text-template-management').then(m =>
        m.TextTemplateManagementModule.forLazy()
      ),
  },
  {
    path: 'setting-management',
    loadChildren: () =>
      import('@abp/ng.setting-management').then(m => m.SettingManagementModule.forLazy()),
  },
  {
    path: 'gdpr-cookie-consent',
    loadChildren: () =>
      import('./gdpr-cookie-consent/gdpr-cookie-consent.module').then(
        m => m.GdprCookieConsentModule
      ),
  },


  { path: 'distributor-purchase-orders', loadChildren: () => import('./DPO/distributor-purchase-order/distributor-purchase-order.module').then(m => m.DistributorPurchaseOrderModule) },
  { path: 'eta-etd', loadChildren: () => import('./DPO/eta-etd/eta-etd.module').then(m => m.EtaEtdModule) },
  { path: 'dodetails', loadChildren: () => import('./DPO/dodetails/dodetails.module').then(m => m.DodetailsModule) },
  { path: 'material-stock-checking', loadChildren: () => import('./mitsubishi-electric/mitsubishi-electric.module').then(m => m.MitsubishiElectricModule) },
  { path: 'project', loadChildren: () => import('./project/project.module').then(m => m.ProjectModule) },



  // { path: 'distributor-purchase-orders', loadChildren: () => import('./distributor-purchase-orders/distributor-purchase-order/distributor-purchase-order.module').then(m => m.DistributorPurchaseOrderModule) },
  // { path: 'distributor-purchase-order-details', loadChildren: () => import('./distributor-purchase-order-details/distributor-purchase-order-detail/distributor-purchase-order-detail.module').then(m => m.DistributorPurchaseOrderDetailModule) },
  // { path: 'delivery-orders', loadChildren: () => import('./delivery-orders/delivery-order/delivery-order.module').then(m => m.DeliveryOrderModule) },

  // { path: 'dpo', loadChildren: () => import('./distributor-purchase-order/distributor-purchase-order.module').then(m => m.DistributorPurchaseOrderModule) },
  // { path: 'eta-etd', loadChildren: () => import('./eta-etd/eta-etd.module').then(m => m.EtaEtdModule) },
  // { path: 'do-details', loadChildren: () => import('./do-details/do-details.module').then(m => m.DoDetailsModule) },




];

@NgModule({
  imports: [RouterModule.forRoot(routes, {})],
  exports: [RouterModule],
})
export class AppRoutingModule { }
