import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface FreeOnShipMentCreateDto {
  poNo: string;
  poDate?: string;
  qty?: number;
  machineNumber?: string;
}

export interface FreeOnShipMentDto extends FullAuditedEntityDto<string> {
  materialID?: string;
  poNo?: string;
  poDate?: string;
  qty?: number;
  machineNumber?: string;
  concurrencyStamp?: string;
}
