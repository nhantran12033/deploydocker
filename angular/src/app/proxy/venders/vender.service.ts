import type { GetVendersInput, VenderCreateDto, VenderDto, VenderUpdateDto } from './models';
import { RestService, Rest } from '@abp/ng.core';
import type { PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class VenderService {
  apiName = 'Default';
  

  create = (input: VenderCreateDto, config?: Partial<Rest.Config>) =>
    this.restService.request<any, VenderDto>({
      method: 'POST',
      url: '/api/app/venders',
      body: input,
    },
    { apiName: this.apiName,...config });
  

  delete = (id: string, config?: Partial<Rest.Config>) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/venders/${id}`,
    },
    { apiName: this.apiName,...config });
  

  get = (id: string, config?: Partial<Rest.Config>) =>
    this.restService.request<any, VenderDto>({
      method: 'GET',
      url: `/api/app/venders/${id}`,
    },
    { apiName: this.apiName,...config });
  

  getList = (input: GetVendersInput, config?: Partial<Rest.Config>) =>
    this.restService.request<any, PagedResultDto<VenderDto>>({
      method: 'GET',
      url: '/api/app/venders',
      params: { filterText: input.filterText, verderName: input.verderName, address: input.address, email: input.email, sorting: input.sorting, skipCount: input.skipCount, maxResultCount: input.maxResultCount },
    },
    { apiName: this.apiName,...config });
  

  update = (id: string, input: VenderUpdateDto, config?: Partial<Rest.Config>) =>
    this.restService.request<any, VenderDto>({
      method: 'PUT',
      url: `/api/app/venders/${id}`,
      body: input,
    },
    { apiName: this.apiName,...config });

  constructor(private restService: RestService) {}
}
