import type { FullAuditedEntityDto, PagedAndSortedResultRequestDto } from '@abp/ng.core';

export interface GetVendersInput extends GetVendersInputBase {
}

export interface GetVendersInputBase extends PagedAndSortedResultRequestDto {
  filterText?: string;
  verderName?: string;
  address?: string;
  email?: string;
}

export interface VenderCreateDto extends VenderCreateDtoBase {
}

export interface VenderCreateDtoBase {
  verderName: string;
  address?: string;
  email?: string;
}

export interface VenderDto extends VenderDtoBase {
}

export interface VenderDtoBase extends FullAuditedEntityDto<string> {
  id?: string;
  verderName?: string;
  address?: string;
  email?: string;
  concurrencyStamp?: string;
}

export interface VenderUpdateDto extends VenderUpdateDtoBase {
}

export interface VenderUpdateDtoBase {
  verderName: string;
  address?: string;
  email?: string;
  concurrencyStamp?: string;
}
