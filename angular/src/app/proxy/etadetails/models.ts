import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface ETADetailCreateDto extends ETADetailCreateDtoBase {
}

export interface ETADetailCreateDtoBase {
  statusCode: string;
  invoiceNo: string;
  golfaCode: string;
  qty: number;
  etd?: string;
  eta?: string;
  distributorOrderDetailId?: string;
}

export interface ETADetailDto extends ETADetailDtoBase {
}

export interface ETADetailDtoBase extends FullAuditedEntityDto<string> {
  statusCode?: string;
  invoiceNo?: string;
  golfaCode?: string;
  qty: number;
  etd?: string;
  eta?: string;
  distributorOrderDetailId?: string;
  concurrencyStamp?: string;
}
