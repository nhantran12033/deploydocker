import type { GetMaterialStockCheckingsInput, MaterialStockCheckingCreateDto, MaterialStockCheckingDto, MaterialStockCheckingExcelDownloadDto } from './models';
import { RestService, Rest } from '@abp/ng.core';
import type { PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';
import type { LookupDto, LookupRequestDto } from '../shared/models';
import type { DownloadTokenResultDto } from '../volo/abp/gdpr/models';

@Injectable({
  providedIn: 'root',
})
export class MaterialStockCheckingService {
  apiName = 'Default';
  

  create = (input: MaterialStockCheckingCreateDto, config?: Partial<Rest.Config>) =>
    this.restService.request<any, MaterialStockCheckingDto>({
      method: 'POST',
      url: '/api/app/material-stock-checkings',
      body: input,
    },
    { apiName: this.apiName,...config });
  

  delete = (id: string, config?: Partial<Rest.Config>) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/material-stock-checkings/${id}`,
    },
    { apiName: this.apiName,...config });
  

  get = (id: string, config?: Partial<Rest.Config>) =>
    this.restService.request<any, MaterialStockCheckingDto>({
      method: 'GET',
      url: `/api/app/material-stock-checkings/${id}`,
    },
    { apiName: this.apiName,...config });
  

  getDistributorLookup = (input: LookupRequestDto, config?: Partial<Rest.Config>) =>
    this.restService.request<any, PagedResultDto<LookupDto<string>>>({
      method: 'GET',
      url: '/api/app/material-stock-checkings/distributor-lookup',
      params: { filter: input.filter, skipCount: input.skipCount, maxResultCount: input.maxResultCount },
    },
    { apiName: this.apiName,...config });
  

  getDownloadToken = (config?: Partial<Rest.Config>) =>
    this.restService.request<any, DownloadTokenResultDto>({
      method: 'GET',
      url: '/api/app/material-stock-checkings/download-token',
    },
    { apiName: this.apiName,...config });
  

  getList = (input: GetMaterialStockCheckingsInput, config?: Partial<Rest.Config>) =>
    this.restService.request<any, PagedResultDto<MaterialStockCheckingDto>>({
      method: 'GET',
      url: '/api/app/material-stock-checkings',
      params: { golfaCode: input.golfaCode, model: input.model, materialGroupId: input.materialGroupId, venderId: input.venderId, distributorId: input.distributorId, sorting: input.sorting, skipCount: input.skipCount, maxResultCount: input.maxResultCount },
    },
    { apiName: this.apiName,...config });
  

  getListAsExcelFile = (input: MaterialStockCheckingExcelDownloadDto, config?: Partial<Rest.Config>) =>
    this.restService.request<any, Blob>({
      method: 'GET',
      responseType: 'blob',
      url: '/api/app/material-stock-checkings/as-excel-file',
      params: { downloadToken: input.downloadToken, golfaCode: input.golfaCode, model: input.model, materialGroupId: input.materialGroupId, venderId: input.venderId, distributorId: input.distributorId },
    },
    { apiName: this.apiName,...config });
  

  getMaterialGroupLookup = (input: LookupRequestDto, config?: Partial<Rest.Config>) =>
    this.restService.request<any, PagedResultDto<LookupDto<string>>>({
      method: 'GET',
      url: '/api/app/material-stock-checkings/material-group-lookup',
      params: { filter: input.filter, skipCount: input.skipCount, maxResultCount: input.maxResultCount },
    },
    { apiName: this.apiName,...config });
  

  getVenderLookup = (input: LookupRequestDto, config?: Partial<Rest.Config>) =>
    this.restService.request<any, PagedResultDto<LookupDto<string>>>({
      method: 'GET',
      url: '/api/app/material-stock-checkings/vender-lookup',
      params: { filter: input.filter, skipCount: input.skipCount, maxResultCount: input.maxResultCount },
    },
    { apiName: this.apiName,...config });

  constructor(private restService: RestService) {}
}
