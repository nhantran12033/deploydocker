import type { FullAuditedEntityDto, PagedAndSortedResultRequestDto } from '@abp/ng.core';
import type { FreeOnShipMentCreateDto, FreeOnShipMentDto } from '../free-on-ship-ments/models';

export interface GetMaterialStockCheckingsInput extends GetMaterialStockCheckingsInputBase {
}

export interface GetMaterialStockCheckingsInputBase extends PagedAndSortedResultRequestDto {
  golfaCode?: string;
  model?: string;
  materialGroupId?: string;
  venderId?: string;
  distributorId?: string;
}

export interface MaterialStockCheckingCreateDto extends MaterialStockCheckingCreateDtoBase {
}

export interface MaterialStockCheckingCreateDtoBase {
  golfaCode: string;
  model: string;
  sap_Code?: string;
  description_VN: string;
  description_Group?: string;
  standard_Price: number;
  stockValueWarning?: number;
  stockTmp?: number;
  spec1?: string;
  spec2?: string;
  materialGroupId?: string;
  venderId?: string;
  distributorId?: string;
  listFreeOnShipMent: FreeOnShipMentCreateDto[];
}

export interface MaterialStockCheckingDto extends MaterialStockCheckingDtoBase {
}

export interface MaterialStockCheckingDtoBase extends FullAuditedEntityDto<string> {
  golfaCode?: string;
  model?: string;
  sap_Code?: string;
  description_VN?: string;
  description_Group?: string;
  standard_Price: number;
  stockValueWarning?: number;
  stockTmp?: number;
  spec1?: string;
  spec2?: string;
  materialGroupId?: string;
  venderId?: string;
  distributorId?: string;
  listFreeOnShipMent: FreeOnShipMentDto[];
  concurrencyStamp?: string;
}

export interface MaterialStockCheckingExcelDownloadDto extends MaterialStockCheckingExcelDownloadDtoBase {
}

export interface MaterialStockCheckingExcelDownloadDtoBase {
  downloadToken?: string;
  golfaCode?: string;
  model?: string;
  materialGroupId?: string;
  venderId?: string;
  distributorId?: string;
}
