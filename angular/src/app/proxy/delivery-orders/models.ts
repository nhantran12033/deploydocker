import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface DeliveryOrderCreateDto extends DeliveryOrderCreateDtoBase {
}

export interface DeliveryOrderCreateDtoBase {
  status: number;
  deliveryDate?: string;
  deliveryCode?: string;
  dosapNo?: string;
  invoiceDate?: string;
  invoiceNo?: string;
  billingNo?: string;
  note?: string;
  statusCode?: string;
  qty: number;
}

export interface DeliveryOrderDto extends DeliveryOrderDtoBase {
}

export interface DeliveryOrderDtoBase extends FullAuditedEntityDto<string> {
  status: number;
  deliveryDate?: string;
  deliveryCode?: string;
  dosapNo?: string;
  invoiceDate?: string;
  invoiceNo?: string;
  billingNo?: string;
  note?: string;
  statusCode?: string;
  qty: number;
  distributorOrderDetailId?: string;
  concurrencyStamp?: string;
}
