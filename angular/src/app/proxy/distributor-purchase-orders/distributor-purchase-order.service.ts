import type { DistributorPurchaseOrderCreateDto, DistributorPurchaseOrderDto, DistributorPurchaseOrderWithNavigationPropertiesDto, GetDistributorPurchaseOrdersInput } from './models';
import { RestService, Rest } from '@abp/ng.core';
import type { PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';
import type { LookupDto, LookupRequestDto } from '../shared/models';

@Injectable({
  providedIn: 'root',
})
export class DistributorPurchaseOrderService {
  apiName = 'Default';
  

  create = (input: DistributorPurchaseOrderCreateDto, config?: Partial<Rest.Config>) =>
    this.restService.request<any, DistributorPurchaseOrderDto>({
      method: 'POST',
      url: '/api/app/distributor-purchase-orders',
      body: input,
    },
    { apiName: this.apiName,...config });
  

  delete = (id: string, config?: Partial<Rest.Config>) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/distributor-purchase-orders/${id}`,
    },
    { apiName: this.apiName,...config });
  

  get = (id: string, config?: Partial<Rest.Config>) =>
    this.restService.request<any, DistributorPurchaseOrderDto>({
      method: 'GET',
      url: `/api/app/distributor-purchase-orders/${id}`,
    },
    { apiName: this.apiName,...config });
  

  getDistributorLookup = (input: LookupRequestDto, config?: Partial<Rest.Config>) =>
    this.restService.request<any, PagedResultDto<LookupDto<string>>>({
      method: 'GET',
      url: '/api/app/distributor-purchase-orders/distributor-lookup',
      params: { filter: input.filter, skipCount: input.skipCount, maxResultCount: input.maxResultCount },
    },
    { apiName: this.apiName,...config });
  

  getList = (input: GetDistributorPurchaseOrdersInput, config?: Partial<Rest.Config>) =>
    this.restService.request<any, PagedResultDto<DistributorPurchaseOrderDto>>({
      method: 'GET',
      url: '/api/app/distributor-purchase-orders',
      params: { dpoNo: input.dpoNo, orderDate: input.orderDate, fa_LVS: input.fa_LVS, statusCode: input.statusCode, distributorId: input.distributorId, sorting: input.sorting, skipCount: input.skipCount, maxResultCount: input.maxResultCount },
    },
    { apiName: this.apiName,...config });
  

  getWithNavigationProperties = (id: string, config?: Partial<Rest.Config>) =>
    this.restService.request<any, DistributorPurchaseOrderWithNavigationPropertiesDto>({
      method: 'GET',
      url: `/api/app/distributor-purchase-orders/with-navigation-properties/${id}`,
    },
    { apiName: this.apiName,...config });

  constructor(private restService: RestService) {}
}
