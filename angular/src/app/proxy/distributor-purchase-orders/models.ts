import type { DistributorPurchaseOrderDetailCreateDto, DistributorPurchaseOrderDetailDto } from '../distributor-purchase-order-details/models';
import type { FullAuditedEntityDto, PagedAndSortedResultRequestDto } from '@abp/ng.core';
import type { DistributorDto } from '../distributors/models';

export interface DistributorPurchaseOrderCreateDto extends DistributorPurchaseOrderCreateDtoBase {
}

export interface DistributorPurchaseOrderCreateDtoBase {
  dpoNo?: string;
  status: number;
  orderDate?: string;
  totalAmount?: number;
  fa_LVS?: string;
  note?: string;
  paymentInfo?: string;
  shipmentDate?: string;
  userName?: string;
  spUser?: string;
  remark?: string;
  spFileName?: string;
  fileName?: string;
  statusCode?: string;
  stockKeepingRequestNo?: string;
  isKeepStock?: number;
  validDate?: string;
  distributorId?: string;
  listDistributorPurchaseOrderDetail: DistributorPurchaseOrderDetailCreateDto[];
}

export interface DistributorPurchaseOrderDto extends DistributorPurchaseOrderDtoBase {
}

export interface DistributorPurchaseOrderDtoBase extends FullAuditedEntityDto<string> {
  dpoNo?: string;
  status: number;
  orderDate?: string;
  totalAmount?: number;
  fa_LVS?: string;
  note?: string;
  paymentInfo?: string;
  shipmentDate?: string;
  userName?: string;
  spUser?: string;
  remark?: string;
  spFileName?: string;
  fileName?: string;
  statusCode?: string;
  stockKeepingRequestNo?: string;
  isKeepStock?: number;
  validDate?: string;
  distributorId?: string;
  listDistributorPurchaseOrderDetail: DistributorPurchaseOrderDetailDto[];
  concurrencyStamp?: string;
  expanded?: boolean;
}

export interface DistributorPurchaseOrderWithNavigationPropertiesDto extends DistributorPurchaseOrderWithNavigationPropertiesDtoBase {
}

export interface DistributorPurchaseOrderWithNavigationPropertiesDtoBase {
  distributorPurchaseOrder: DistributorPurchaseOrderDto;
  distributor: DistributorDto;
}

export interface GetDistributorPurchaseOrdersInput extends GetDistributorPurchaseOrdersInputBase {
}

export interface GetDistributorPurchaseOrdersInputBase extends PagedAndSortedResultRequestDto {
  dpoNo?: string;
  orderDate?: string;
  fa_LVS?: string;
  statusCode?: string;
  distributorId?: string;
}
