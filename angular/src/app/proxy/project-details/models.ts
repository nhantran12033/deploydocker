import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface ProjectDetailCreateDto {
  projectId?: string;
  golfaCode: string;
  model?: string;
  qty: number;
  dpoUsed?: number;
  requestStatus?: string;
  requestPrice?: number;
  distRequestedPrice: number;
  saleOfferPrice: number;
  saleAllowDiscountPrice: number;
  amountRequestedPrice: number;
  statusCode: string;
}

export interface ProjectDetailDto extends FullAuditedEntityDto<string> {
  projectId?: string;
  golfaCode?: string;
  model?: string;
  qty: number;
  dpoUsed?: number;
  requestStatus?: string;
  requestPrice?: number;
  distRequestedPrice: number;
  saleOfferPrice: number;
  saleAllowDiscountPrice: number;
  amountRequestedPrice: number;
  statusCode?: string;
  concurrencyStamp?: string;
}
