import type { ETADetailCreateDto, ETADetailDto } from '../etadetails/models';
import type { DeliveryOrderCreateDto, DeliveryOrderDto } from '../delivery-orders/models';
import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface DistributorPurchaseOrderDetailCreateDto extends DistributorPurchaseOrderDetailCreateDtoBase {
}

export interface DistributorPurchaseOrderDetailCreateDtoBase {
  status: number;
  golfaCode?: string;
  customer?: string;
  model?: string;
  qty?: number;
  price?: number;
  amount?: number;
  requestedETA?: string;
  delivered?: number;
  inProgress?: number;
  note?: string;
  eta?: number;
  customerName?: string;
  statusCode?: string;
  listETADetail: ETADetailCreateDto[];
  listDeliveryOrder: DeliveryOrderCreateDto[];
}

export interface DistributorPurchaseOrderDetailDto extends DistributorPurchaseOrderDetailDtoBase {
}

export interface DistributorPurchaseOrderDetailDtoBase extends FullAuditedEntityDto<string> {
  status: number;
  golfaCode?: string;
  customer?: string;
  model?: string;
  qty?: number;
  price?: number;
  amount?: number;
  requestedETA?: string;
  delivered?: number;
  inProgress?: number;
  note?: string;
  eta?: number;
  customerName?: string;
  statusCode?: string;
  distributorPurchaseOrderId?: string;
  listETADetail: ETADetailDto[];
  listDeliveryOrder: DeliveryOrderDto[];
  concurrencyStamp?: string;
}
