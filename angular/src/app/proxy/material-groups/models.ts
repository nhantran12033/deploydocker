import type { FullAuditedEntityDto, PagedAndSortedResultRequestDto } from '@abp/ng.core';

export interface GetMaterialGroupsInput extends GetMaterialGroupsInputBase {
}

export interface GetMaterialGroupsInputBase extends PagedAndSortedResultRequestDto {
  filterText?: string;
  materialName?: string;
  disCode?: string;
}

export interface MaterialGroupCreateDto extends MaterialGroupCreateDtoBase {
}

export interface MaterialGroupCreateDtoBase {
  materialName: string;
  disCode: string;
}

export interface MaterialGroupDto extends MaterialGroupDtoBase {
}

export interface MaterialGroupDtoBase extends FullAuditedEntityDto<string> {
  id?: string;
  materialName?: string;
  disCode?: string;
  concurrencyStamp?: string;
}

export interface MaterialGroupUpdateDto extends MaterialGroupUpdateDtoBase {
}

export interface MaterialGroupUpdateDtoBase {
  materialName: string;
  disCode: string;
  concurrencyStamp?: string;
}
