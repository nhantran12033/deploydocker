import type { DistributorCreateDto, DistributorDto, DistributorUpdateDto, GetDistributorsInput } from './models';
import { RestService, Rest } from '@abp/ng.core';
import type { PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class DistributorService {
  apiName = 'Default';
  

  create = (input: DistributorCreateDto, config?: Partial<Rest.Config>) =>
    this.restService.request<any, DistributorDto>({
      method: 'POST',
      url: '/api/app/distributors',
      body: input,
    },
    { apiName: this.apiName,...config });
  

  delete = (id: string, config?: Partial<Rest.Config>) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/distributors/${id}`,
    },
    { apiName: this.apiName,...config });
  

  get = (id: string, config?: Partial<Rest.Config>) =>
    this.restService.request<any, DistributorDto>({
      method: 'GET',
      url: `/api/app/distributors/${id}`,
    },
    { apiName: this.apiName,...config });
  

  getList = (input: GetDistributorsInput, config?: Partial<Rest.Config>) =>
    this.restService.request<any, PagedResultDto<DistributorDto>>({
      method: 'GET',
      url: '/api/app/distributors',
      params: { filterText: input.filterText, disCode: input.disCode, disName: input.disName, address: input.address, contactInfo: input.contactInfo, node: input.node, sorting: input.sorting, skipCount: input.skipCount, maxResultCount: input.maxResultCount },
    },
    { apiName: this.apiName,...config });
  

  update = (id: string, input: DistributorUpdateDto, config?: Partial<Rest.Config>) =>
    this.restService.request<any, DistributorDto>({
      method: 'PUT',
      url: `/api/app/distributors/${id}`,
      body: input,
    },
    { apiName: this.apiName,...config });

  constructor(private restService: RestService) {}
}
