import type { FullAuditedEntityDto, PagedAndSortedResultRequestDto } from '@abp/ng.core';

export interface DistributorCreateDto extends DistributorCreateDtoBase {
}

export interface DistributorCreateDtoBase {
  disCode: string;
  disName: string;
  address?: string;
  contactInfo?: string;
  node?: string;
}

export interface DistributorDto extends DistributorDtoBase {
}

export interface DistributorDtoBase extends FullAuditedEntityDto<string> {
  id?: string;
  disCode?: string;
  disName?: string;
  address?: string;
  contactInfo?: string;
  node?: string;
  concurrencyStamp?: string;
}

export interface DistributorUpdateDto extends DistributorUpdateDtoBase {
}

export interface DistributorUpdateDtoBase {
  disCode: string;
  disName: string;
  address?: string;
  contactInfo?: string;
  node?: string;
  concurrencyStamp?: string;
}

export interface GetDistributorsInput extends GetDistributorsInputBase {
}

export interface GetDistributorsInputBase extends PagedAndSortedResultRequestDto {
  filterText?: string;
  disCode?: string;
  disName?: string;
  address?: string;
  contactInfo?: string;
  node?: string;
}
