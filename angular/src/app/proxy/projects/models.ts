import type { FullAuditedEntityDto, PagedAndSortedResultRequestDto } from '@abp/ng.core';
import type { ProjectDetailCreateDto, ProjectDetailDto } from '../project-details/models';
import type { DistributorDto } from '../distributors/models';
import type { CustomerDto } from '../customers/models';

export interface GetProjectsInput extends GetProjectsInputBase {
}

export interface GetProjectsInputBase extends PagedAndSortedResultRequestDto {
  projectCode?: string;
  projectName?: string;
  poDateMin?: string;
  poDateMax?: string;
  distributorId?: string;
}

export interface ProjectCreateDto extends ProjectCreateDtoBase {
}

export interface ProjectCreateDtoBase {
  projectCode: string;
  projectName: string;
  approvalStatus: string;
  accountNo?: string;
  projectType?: string;
  location?: string;
  distributorMagin?: string;
  poDate?: string;
  distributorId?: string;
  customerId?: string;
  listProjectDetail: ProjectDetailCreateDto[];
}

export interface ProjectDto extends ProjectDtoBase {
}

export interface ProjectDtoBase extends FullAuditedEntityDto<string> {
  projectCode?: string;
  projectName?: string;
  approvalStatus?: string;
  accountNo?: string;
  projectType?: string;
  location?: string;
  distributorMagin?: string;
  poDate?: string;
  distributorId?: string;
  customerId?: string;
  listProjectDetail: ProjectDetailDto[];
  concurrencyStamp?: string;
}

export interface ProjectExcelDownloadDto extends ProjectExcelDownloadDtoBase {
}

export interface ProjectExcelDownloadDtoBase {
  downloadToken?: string;
  id?: string;
}

export interface ProjectWithNavigationPropertiesDto extends ProjectWithNavigationPropertiesDtoBase {
}

export interface ProjectWithNavigationPropertiesDtoBase {
  project: ProjectDtoBase;
  projectDetail: ProjectDetailDto[];
  distributor: DistributorDto;
  customer: CustomerDto;
}
