import type { FullAuditedEntityDto, PagedAndSortedResultRequestDto } from '@abp/ng.core';

export interface CustomerCreateDto extends CustomerCreateDtoBase {
}

export interface CustomerCreateDtoBase {
  taxCode: string;
  cusName: string;
  address?: string;
  phone?: string;
  note?: string;
}

export interface CustomerDto extends CustomerDtoBase {
}

export interface CustomerDtoBase extends FullAuditedEntityDto<string> {
  id?: string;
  taxCode?: string;
  cusName?: string;
  address?: string;
  phone?: string;
  note?: string;
  concurrencyStamp?: string;
}

export interface CustomerUpdateDto extends CustomerUpdateDtoBase {
}

export interface CustomerUpdateDtoBase {
  taxCode: string;
  cusName: string;
  address?: string;
  phone?: string;
  note?: string;
  concurrencyStamp?: string;
}

export interface GetCustomersInput extends GetCustomersInputBase {
}

export interface GetCustomersInputBase extends PagedAndSortedResultRequestDto {
  filterText?: string;
  taxCode?: string;
  cusName?: string;
  address?: string;
  phone?: string;
  note?: string;
}
