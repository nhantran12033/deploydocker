import { DistributorService } from './../proxy/distributors/distributor.service';
import { Component, OnInit } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MatTableDataSource } from '@angular/material/table';
import { GetProjectsInput, ProjectDto, ProjectDtoBase, ProjectExcelDownloadDto, ProjectService } from '@proxy/projects';
import { DistributorDto, GetDistributorsInput } from '@proxy/distributors';
import { CustomerDto, CustomerService, GetCustomersInput } from '@proxy/customers';
import { ProjectDetailDto } from '@proxy/project-details';
import { Data } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';


@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ProjectComponent implements OnInit {

  Distributor: string = '';
  get_data_Distributor: DistributorDto[];
  get_data_Customer: CustomerDto[];
  Project_Code: string = '';
  Project_Name: string = '';
  customerIdToNameMap: any;
  Date_From: Date;
  Date_To: Date;
  formValid: boolean = false;
  // dataSource: MatTableDataSource<GetProjectsInput> = new MatTableDataSource([]);
  dataSource = new MatTableDataSource<ProjectDto>();
  columnsToDisplay: string[] = ['status', 'project_Code', 'project_Name', 'account_Code', 'project_Type', 'customer'];
  // childColumnsToDisplay: string[] = ['status', 'golfa_Code', 'model', 'qty', 'used_In_DPO', 'requested_Price', 'sale_Offer_Price', 'amount_Requested_Price'];
  childColumnsToDisplay: string[] = ['status', 'golfa_Code', 'model', 'qty', 'used_In_DPO', 'requested_Price', 'sale_Offer_Price', 'amount_Requested_Price'];


  constructor(
    private customer_service: CustomerService,
    private project_Service: ProjectService,
    private Distributor_Service: DistributorService,
    private snackBar: MatSnackBar,
  ) { }

  ngOnInit(): void {
    this.Distributor_Service.getList(this.Distributor_input).subscribe((Distributor) => {
      this.get_data_Distributor = Distributor.items;
    });

    this.customer_service.getList(this.Customer_input).subscribe((Customer) => {
      this.get_data_Customer = Customer.items;
      this.customerIdToNameMap = {};
      Customer.items.forEach(vender => {
        this.customerIdToNameMap[vender.id] = vender.cusName;
      });
    });
  }
  checkFormValidity(): void {
    this.formValid = false;

    if (this.Distributor && this.Date_From && this.Date_To) {
      this.formValid = true;
    }
  }
  getCustomerName(customerId: string): string {
    return this.customerIdToNameMap[customerId] || customerId;
  }

  Customer_input: GetCustomersInput = {
    filterText: '',
    taxCode: '',
    cusName: '',
    address: '',
    phone: '',
    note: '',
    sorting: '',
    skipCount: 0,
    maxResultCount: 10
  }

  Distributor_input: GetDistributorsInput = {
    filterText: '',
    disCode: '',
    disName: '',
    address: '',
    contactInfo: '',
    node: '',
    sorting: '',
    skipCount: 0,
    maxResultCount: 10
  }

  searchParams: GetProjectsInput = {
    projectCode: '',
    projectName: '',
    poDateMin: '',
    poDateMax: '',
    distributorId: '',
    sorting: '',
    skipCount: 0,
    maxResultCount: 10
  }
  private showSnackbar(message: string): void {
    this.snackBar.open(message, 'Close', {
      duration: 5000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
      panelClass: ['custom-snackbar'],
    });
  }
  onClickLoad(): void {
    this.checkFormValidity();

    if (this.formValid) {
      const distributorid = this.get_data_Distributor.find(d => d.disName === this.Distributor)
      this.searchParams.distributorId = distributorid.id;

      this.searchParams.projectCode = this.Project_Code;
      this.searchParams.projectName = this.Project_Name;



      const YearMin = this.Date_From.getFullYear();
      const MonthMin = this.Date_From.getMonth();
      const DayMin = this.Date_From.getDate();
      const formattedDateMin = `${YearMin}-${MonthMin + 1}-${DayMin}`;
      this.searchParams.poDateMin = formattedDateMin;
      console.log(this.searchParams);
      const Year = this.Date_To.getFullYear();
      const Month = this.Date_To.getMonth();
      const Day = this.Date_To.getDate();
      const formattedDate = `${Year}-${Month + 1}-${Day}`;
      this.searchParams.poDateMax = formattedDate;



      this.project_Service.getList(this.searchParams).subscribe((result) => {
        this.dataSource.data = result.items;
        console.log(this.dataSource)
      });
    }
    else {
      this.showSnackbar('Please select Distributor, Date From and Date To');
    }

  }

  toggleRow(element: any): void {
    element.expanded = !element.expanded;
  }

  downloadInput: ProjectExcelDownloadDto = {
    downloadToken: '',
    id: ''
  };

  get_project_code: ProjectExcelDownloadDto[];
  getId: string;
  downloadExcel(id_projects: string): void {

    this.project_Service.getDownloadToken().subscribe((result) => {
      this.downloadInput.downloadToken = result.token
      this.downloadInput.id = id_projects
      this.project_Service.getListAsExcelFile(this.downloadInput).subscribe(
        (data: Blob) => {
          this.downloadFile(data);
        },
        (error) => {
          console.error('Error downloading Excel file', error);
        }
      );
    })

  }

  private downloadFile(blobData: Blob): void {
    const blobUrl = URL.createObjectURL(blobData);
    const link = document.createElement('a');
    link.href = blobUrl;
    link.download = 'projects.xlsx';
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }
}


