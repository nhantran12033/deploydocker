import { NgModule } from '@angular/core';
import { RouterModule, Routes, mapToCanActivate } from '@angular/router';
import { DistributorPurchaseOrderComponent } from './distributor-purchase-order.component';
import { AuthGuard, PermissionGuard } from '@abp/ng.core';
const routes: Routes = [{ path: '', component: DistributorPurchaseOrderComponent, canActivate: mapToCanActivate([AuthGuard, PermissionGuard]), }];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DistributorPurchaseOrderRoutingModule { }
