import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
// import { startWith, map } from 'rxjs/operators';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { DPO_Service } from '../../API/DPO/dpo.service';
import { DO_Details, DPO, DPO_Details, ETD_ETA } from '../../API/DPO/models';
import { Router } from '@angular/router';

// import { SelectionModel } from '@angular/cdk/collections';

import { animate, state, style, transition, trigger } from '@angular/animations';

import { MatSnackBar } from '@angular/material/snack-bar';

import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { EtaEtdComponent } from '../eta-etd/eta-etd.component';
import { DodetailsComponent } from '../dodetails/dodetails.component';
import { DistributorPurchaseOrderService } from '@proxy/distributor-purchase-orders';
import { GetDistributorPurchaseOrdersInput } from '@proxy/distributor-purchase-orders';
import { DistributorPurchaseOrderDto } from '@proxy/distributor-purchase-orders';
import { DistributorPurchaseOrderDetailDto } from '@proxy/distributor-purchase-order-details';
import { DeliveryOrderDto } from '@proxy/delivery-orders'
import { ETADetailDto } from '@proxy/etadetails';
import { DistributorService, GetDistributorsInput } from '@proxy/distributors';
import { DistributorDto } from '@proxy/distributors';
import { DatePipe } from '@angular/common';


@Component({
  selector: 'app-distributor-purchase-order',
  templateUrl: './distributor-purchase-order.component.html',
  styleUrls: ['./distributor-purchase-order.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class DistributorPurchaseOrderComponent implements OnInit, AfterViewInit {

  selectedDistributor: string;
  selectedDate: Date;
  selectedDPO: string;
  selectedStatus: string;
  selectedMaterialType: string;

  isSearchClicked: boolean = false; // Thêm biến này

  distributors = ['Distributor 1', 'Distributor 2', 'Distributor 3'];
  statuses = ['Close', 'In Progress'];
  materialTypes = ['FA', 'LVS'];

  options = ['DPO 1', 'DPO 2', 'DPO 3'];

  myControl = new FormControl();
  filteredOptions: Observable<string[]>;
  dto: DistributorPurchaseOrderDto[];
  get_Data: any;
  distributor: any;
  dataSource = new MatTableDataSource<DistributorPurchaseOrderDto>();
  dataSource2 = new MatTableDataSource<DistributorPurchaseOrderDetailDto>();
  dataSource3 = new MatTableDataSource<ETD_ETA>();
  dataSource4 = new MatTableDataSource<DeliveryOrderDto>();
  dialogRefDODetails: MatDialogRef<DodetailsComponent>;
  dialogRefETDDialog: MatDialogRef<EtaEtdComponent>;
  columnsToDisplay: string[] = ['expandArrow', 'statusCode', 'dpoNo', 'fA_LVS', 'orderDate', 'totalAmount', 'note'];
  columnsToDisplay2: string[] = ['statusCode', 'golfaCode', 'model', 'requestedETA', 'qty', 'price', 'amount', 'customerName', 'eta', 'delivered', 'inProgress'];
  columnsToDisplay3: string[] = ['status', 'invoice no', 'Golfa Code', 'Qty', 'ETD', 'ETA'];
  columnsToDisplay4: string[] = ['status', 'deliveryDate', 'invoiceNo', 'invoiceDate', 'qty'];

  distributorDto: DistributorDto[]; // You need to declare this array



  @ViewChild(MatPaginator) paginator: MatPaginator;
  datasd: string;





  constructor(
    private snackBar: MatSnackBar,
    private dpoService: DPO_Service,
    private dialog: MatDialog,
    private router: Router,
    private distributorPurchaseOrderService: DistributorPurchaseOrderService,
    private distributorService: DistributorService,
    private datePipe: DatePipe

  ) { }

  ngOnInit(): void {
    this.getDistributor(this.searchDistributor);
    this.statuses
    this.materialTypes

  }

  ngAfterViewInit() {
    // Set the paginators for the main table and nested table
    this.dataSource.paginator = this.paginator;
  }

  getDistributor(searchDistributor: GetDistributorsInput) {
    this.distributorService.getList(searchDistributor).subscribe((result) => {
      this.distributorDto = result.items;

    })
  }
  searchDistributor: GetDistributorsInput = {
    disCode: '',
    disName: '',
    filterText: '',
    address: '',
    contactInfo: '',
    node: '',
    maxResultCount: 10,
    skipCount: 0,
  }



  getDPO(searchParams: GetDistributorPurchaseOrdersInput) {
    // Call the service method with the search parameters
    this.distributorPurchaseOrderService.getList(searchParams)
      .subscribe((result) => {
        this.dataSource.data = result.items;

        this.get_Data = result.items;
      });
  }
  searchDistributorParams: GetDistributorsInput = {
    filterText: '',
    disCode: '',
    disName: '',
    address: '',
    contactInfo: '',
    node: '',
    maxResultCount: 10
  };
  searchParams: GetDistributorPurchaseOrdersInput = {
    distributorId: '',
    orderDate: '',
    fa_LVS: '',
    statusCode: '',
    dpoNo: '',
    maxResultCount: 10, // Adjust the default value as needed
    skipCount: 0, // Adjust the default value as needed

  };
  onSearch() {

    this.onButtonClick();
    const matchingDistributor = this.distributorDto.find(d => d.disName === this.selectedDistributor);
    this.searchParams.distributorId = matchingDistributor.id;



    this.searchParams.dpoNo = this.selectedDPO;
    this.searchParams.statusCode = this.selectedStatus;

    const Year = this.selectedDate.getFullYear();
    const Month = this.selectedDate.getMonth();
    const formattedDate = `${Year}-${Month + 1}`;
    this.searchParams.orderDate = formattedDate;
    this.searchParams.fa_LVS = this.selectedMaterialType;
    this.isSearchClicked = true;
    this.getDPO(this.searchParams);



  }
  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }
  onButtonClick() {
    if (!this.selectedDate && !this.selectedDPO) {
      this.showSnackbar('Please select Month/Year or input DPO Number');
    } else {
      // Proceed with your logic when both fields are filled
      // For example, load data
    }
  }

  private showSnackbar(message: string): void {
    this.snackBar.open(message, 'Close', {
      duration: 5000, // 5 seconds
      horizontalPosition: 'center', // 'start' | 'center' | 'end' | 'left' | 'right'
      verticalPosition: 'top', // 'top' | 'bottom' | 'center'
      panelClass: ['custom-snackbar'], // Custom class for styling
    });
  }

  toggleRow(element: DistributorPurchaseOrderDto) {
    element.expanded = !element.expanded;
    this.dataSource2.data = element.listDistributorPurchaseOrderDetail;

    }
  deliveryDetailDto(element: DistributorPurchaseOrderDto) {
    for (const detail of element.listDistributorPurchaseOrderDetail) {

      const listDeliveryDto = detail.listDeliveryOrder;

      if (listDeliveryDto && listDeliveryDto.length > 0) {

        this.openDODetailsDialog(listDeliveryDto);
      }
    }
  }

  ETADetailsDto(element: DistributorPurchaseOrderDto) {
    for (const detail of element.listDistributorPurchaseOrderDetail) {
      const listETADetail = detail.listETADetail;
      
      if (listETADetail && listETADetail.length > 0) {
        this.openETDDialog(listETADetail);
      }
    }
  }
  getIconColorDelivery(element: DistributorPurchaseOrderDto): string {
    // Kiểm tra xem có dữ liệu delivery hay không
    const hasDelivery = element.listDistributorPurchaseOrderDetail.some(detail =>
      detail.listDeliveryOrder && detail.listDeliveryOrder.length > 0
    );

    // Thêm style vào chuỗi kết quả
    return hasDelivery ? 'blue' : 'black';
  }

  getIconColorETA(element: DistributorPurchaseOrderDto): string {
    // Kiểm tra xem có dữ liệu delivery hay không
    const hasETA = element.listDistributorPurchaseOrderDetail.some(detail =>
      detail.listETADetail && detail.listETADetail.length > 0
    );

    return hasETA ? 'blue' : 'black';
  }




  // manageAllRows(expand: boolean) {
  //   this.dataSource.data.forEach((row) => (row.expanded = expand));
  // }

  toggleArrow(element: DPO): void {
    element.expanded = !element.expanded;
  }

  openDODetailsDialog(doData: DeliveryOrderDto[]): void {
  
    if (!this.dialogRefDODetails || !this.dialogRefDODetails.componentInstance) {
      this.dialogRefDODetails = this.dialog.open(DodetailsComponent, {
        data: { doData: doData }
      });

      // Subscribe to the afterClosed event to perform actions after dialog is closed
      this.dialogRefDODetails.afterClosed().subscribe(result => {
        // Perform any actions needed after dialog is closed
        console.log('Delivery Order Details Dialog closed:', result);
      });
    }

   

  }


  openETDDialog(etdData: ETADetailDto[]): void {
   
    if (!this.dialogRefETDDialog || !this.dialogRefETDDialog.componentInstance) {
      this.dialogRefETDDialog = this.dialog.open(EtaEtdComponent, {
        data: { etdData: etdData }
      });

      // Subscribe to the afterClosed event to perform actions after dialog is closed
      this.dialogRefETDDialog.afterClosed().subscribe(result => {
        // Perform any actions needed after dialog is closed
        console.log('ETA/ETD Dialog closed:', result);
      });
    }
   }






}



