import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DistributorPurchaseOrderRoutingModule } from './distributor-purchase-order-routing.module';
import { DistributorPurchaseOrderComponent } from './distributor-purchase-order.component';
import { SharedModule } from '../../shared/shared.module';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatSelectModule } from '@angular/material/select';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatNativeDateModule } from '@angular/material/core';
import { MatButtonModule } from '@angular/material/button';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatPaginatorModule} from '@angular/material/paginator';
import { MatTableModule} from '@angular/material/table';
import { MatDialogModule} from '@angular/material/dialog';



import { PageModule } from '@abp/ng.components/page';


@NgModule({
  declarations: [
    DistributorPurchaseOrderComponent
  ],
  imports: [
    CommonModule,
    DistributorPurchaseOrderRoutingModule,
    SharedModule,

    ReactiveFormsModule,
    MatInputModule,
    MatIconModule,
    PageModule,
    MatFormFieldModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatButtonModule,
    MatSnackBarModule,
    MatPaginatorModule,
    MatTableModule,
    MatDialogModule,
    FormsModule
    
  
   
    
    
    
  ]
})
export class DistributorPurchaseOrderModule { }
