import { eLayoutType, RoutesService } from '@abp/ng.core';
import { APP_INITIALIZER } from '@angular/core';

export const DISTRIBUTOR_PURCHASE_ORDERS_DISTRIBUTOR_PURCHASE_ORDER_ROUTE_PROVIDER = [
  { provide: APP_INITIALIZER, useFactory: configureRoutes, deps: [RoutesService], multi: true },
];

function configureRoutes(routes: RoutesService) {
  return () => {
    routes.add([
      {
        path: '/distributor-purchase-orders',
        iconClass: 'fas fa-file-alt',
        name: 'Distributor Purchase Orders',
        layout: eLayoutType.application,
        requiredPolicy: 'V2.DistributorPurchaseOrders',
      },
    ]);
  };
}