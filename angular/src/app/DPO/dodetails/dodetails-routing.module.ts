import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DodetailsComponent } from './dodetails.component';

const routes: Routes = [{ path: '', component: DodetailsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DodetailsRoutingModule { }
