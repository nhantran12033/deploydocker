import { Component , Inject, OnInit,  ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DO_Details } from '../../API/DPO/models';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator} from '@angular/material/paginator';
import { DeliveryOrderDto } from '@proxy/delivery-orders';

@Component({
  selector: 'app-dodetails',
  templateUrl: './dodetails.component.html',
  styleUrls: ['./dodetails.component.scss']
})
export class DodetailsComponent implements OnInit {

  columnsToDisplay: string[] = ['status', 'deliveryDate', 'invoiceNo', 'invoiceDate', 'qty' ];

  dataSource4 = new MatTableDataSource<DeliveryOrderDto>();
  get_Data: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any ,
  ){}
  ngOnInit(): void {
    console.log('Received Data in DodetailsComponent:', this.data);

    if (Array.isArray(this.data?.doData)) {
      this.dataSource4.data = this.data.doData;
    } else {
      console.error('Invalid or missing data in DodetailsComponent:', this.data);
    }
  }
  ngAfterViewInit() {
    this.dataSource4.paginator = this.paginator;
  }


}
