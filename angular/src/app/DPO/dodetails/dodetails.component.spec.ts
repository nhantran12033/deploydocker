import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DodetailsComponent } from './dodetails.component';

describe('DodetailsComponent', () => {
  let component: DodetailsComponent;
  let fixture: ComponentFixture<DodetailsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DodetailsComponent]
    });
    fixture = TestBed.createComponent(DodetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
