import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DodetailsRoutingModule } from './dodetails-routing.module';
import { DodetailsComponent } from './dodetails.component';
import { SharedModule } from '../../shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule} from '@angular/material/paginator';

@NgModule({
  declarations: [
    DodetailsComponent
  ],
  imports: [
    CommonModule,
    DodetailsRoutingModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatTableModule,
    SharedModule,
    MatPaginatorModule
    
  ]
})
export class DodetailsModule { }
