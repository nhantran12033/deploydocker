import { Component , Inject, OnInit, ViewChild} from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ETD_ETA } from 'src/app/API/DPO/models';
import { DPO_Service } from 'src/app/API/DPO/dpo.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog'; // Add this import
import { MatPaginator } from '@angular/material/paginator';
import { ETADetailDto } from '@proxy/etadetails';





@Component({
  selector: 'app-eta-etd',
  templateUrl: './eta-etd.component.html',
  styleUrls: ['./eta-etd.component.scss']
})
export class EtaEtdComponent implements OnInit {

  columnsToDisplay: string[] = ['statusCode', 'invoiceNo', 'golfaCode', 'qty', 'etd', 'eta'];
  dataSource3 = new MatTableDataSource<ETADetailDto>();
  get_Data: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any  ) {}

  ngOnInit(): void {

    if (Array.isArray(this.data?.etdData)){
      this.dataSource3.data = this.data.etdData;
    } 
  }
  

  ngAfterViewInit() {
    this.dataSource3.paginator = this.paginator;
  }
}
