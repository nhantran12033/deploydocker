import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EtaEtdComponent } from './eta-etd.component';

const routes: Routes = [{ path: '', component: EtaEtdComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EtaEtdRoutingModule { }
