import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EtaEtdRoutingModule } from './eta-etd-routing.module';
import { EtaEtdComponent } from './eta-etd.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTableModule } from '@angular/material/table';
import { SharedModule } from 'src/app/shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MatPaginatorModule } from '@angular/material/paginator';


@NgModule({
  declarations: [
    EtaEtdComponent
  ],
  imports: [
    CommonModule,
    EtaEtdRoutingModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatTableModule,
    SharedModule,
    MatPaginatorModule
    
  ]
})
export class EtaEtdModule { }
