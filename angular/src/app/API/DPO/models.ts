export interface ETD_ETA {
    status: string;
    invoice_No: string;
    golfaCode: string;
    quantity: string;
    ETD: string;
    ETA: string;
  }
  
  export interface DO_Details {
    status: string;
    DO_Date: string;
    invoice_No: string;
    invoice_Date: string;
    quantity: string;
  }
  
  export interface DPO_Details {
    status: string;
    golfaCode: string;
    model: string;
    requested_ETA: string;
    quantity: string;
    requested_Price: string;
    amount: string;
    customer_Name: string;
    delivered: string;
    in_progress: string;
    ETD_ETA: ETD_ETA[];
    DO_Details: DO_Details[];
  }
  
  export interface DPO {
    status: string;
    DPO_No: string;
    type: string;
    date: string;
    total: string;
    note: string;
    DPO_Details: DPO_Details[];
    id: number;
   expanded: boolean;

    
  }
  
  export interface DPOList {
    dpo: DPO[];
  }
  