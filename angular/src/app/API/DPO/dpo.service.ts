import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { DPO } from './models';

@Injectable({
    providedIn: 'root',
})

export class DPO_Service {
    private baseUrl = 'http://localhost:3000/dpo';

    constructor(private http : HttpClient){}

    getDPO(): Observable<any> {
        return this.http.get(this.baseUrl);
    }
    
}
